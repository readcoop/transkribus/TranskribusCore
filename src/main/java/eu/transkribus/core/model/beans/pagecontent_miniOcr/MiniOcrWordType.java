package eu.transkribus.core.model.beans.pagecontent_miniOcr;

import java.awt.Rectangle;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlValue;

@XmlRootElement(name = "w")
public class MiniOcrWordType {

    @XmlAttribute(name= "x")
    String coords;

    @XmlValue
    String text;

    public void setCoords(int x, int y, int width, int height) {
        this.coords = x + " " + y + " " + width + " " + height;
    }

    public void setText(String text) {
        this.text = text;
    }
    
    public String getTextContent() {
    	return text;
    }
    
    public Rectangle getRect() {
    	return new Rectangle(Integer.parseInt(coords.split(" ")[0]), Integer.parseInt(coords.split(" ")[1]), 
    			Integer.parseInt(coords.split(" ")[2]), Integer.parseInt(coords.split(" ")[3]));
    }
    
    public int overlap(MiniOcrWordType other) {
    	Rectangle ri = getRect().intersection(other.getRect());
    	return ri.isEmpty() ? 0 : ri.width*ri.height;
    }
    
}
