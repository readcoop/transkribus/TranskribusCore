package eu.transkribus.core.model.beans;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class TrpStructureStats {

    private List<RegionTypeStats> regionTypes;

    public TrpStructureStats() {
        regionTypes = new ArrayList<>();
    }

    public List<RegionTypeStats> getRegionTypes() {
        return regionTypes;
    }

    public void setRegionTypes(List<RegionTypeStats> regionTypes) {
        this.regionTypes = regionTypes;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TrpStructureStats)) return false;
        TrpStructureStats that = (TrpStructureStats) o;
        return Objects.equals(getRegionTypes(), that.getRegionTypes());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getRegionTypes());
    }

    @Override
    public String toString() {
        return "TrpStructureStats{" +
                "regionTypes=" + regionTypes +
                '}';
    }

    public static class RegionTypeStats {
        private RegionType regionType;
        private Integer overallCount;
        private List<StructureType> structureTypes;

        public RegionTypeStats() {
            structureTypes = new ArrayList<>();
        }

        /** Copy constructor */
        public RegionTypeStats(RegionTypeStats other) {
            this();
            this.regionType = other.regionType;
            this.overallCount = other.overallCount;
            other.structureTypes.forEach(t -> this.structureTypes.add(new StructureType(t)));
        }

        public RegionType getRegionType() {
            return regionType;
        }

        public void setRegionType(RegionType regionType) {
            this.regionType = regionType;
        }

        public Integer getOverallCount() {
            return overallCount;
        }

        public void setOverallCount(Integer overallCount) {
            this.overallCount = overallCount;
        }

        public List<StructureType> getStructureTypes() {
            return structureTypes;
        }

        public void setStructureTypes(List<StructureType> structureTypes) {
            this.structureTypes = structureTypes;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof RegionTypeStats)) return false;
            RegionTypeStats that = (RegionTypeStats) o;
            return getRegionType() == that.getRegionType() && Objects.equals(getOverallCount(), that.getOverallCount()) && Objects.equals(getStructureTypes(), that.getStructureTypes());
        }

        @Override
        public int hashCode() {
            return Objects.hash(getRegionType(), getOverallCount(), getStructureTypes());
        }

        @Override
        public String toString() {
            return "RegionTypeStats{" +
                    "regionType=" + regionType +
                    ", overallCount=" + overallCount +
                    ", structureTypes=" + structureTypes +
                    '}';
        }
    }

    public static class StructureType {
        private String name;
        private Integer count;

        public StructureType() {}

        public StructureType(StructureType other) {
            this.name = other.name;
            this.count = other.count;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Integer getCount() {
            return count;
        }

        public void setCount(Integer count) {
            this.count = count;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof StructureType)) return false;
            StructureType type = (StructureType) o;
            return Objects.equals(getName(), type.getName()) && Objects.equals(getCount(), type.getCount());
        }

        @Override
        public int hashCode() {
            return Objects.hash(getName(), getCount());
        }

        @Override
        public String toString() {
            return "StructureType{" +
                    "name='" + name + '\'' +
                    ", count=" + count +
                    '}';
        }
    }

    public enum RegionType {
        TABLE_REGION, TEXT_REGION, TEXT_LINE, SEPARATOR, GRAPHIC_REGION, IMAGE_REGION, BASE_LINE, WORD;
    }
}
