package eu.transkribus.core.model.beans.job.enums;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public enum JobImpl {	
	// --- utility jobs ---
	DeleteDocumentJob(JobTask.DeleteDocument, JobTask.DeleteDocument.getLabel(), "DeleteDocJob"),
	MetsImportJob(JobTask.CreateDocument, JobTask.CreateDocument.getLabel(), "MetsImportJob"),
	DocImportJob(JobTask.CreateDocument, JobTask.CreateDocument.getLabel(), "DocImportJob"),
	PdfImportJob(JobTask.CreateDocument, JobTask.CreateDocument.getLabel(), "PdfImportJob"),
	UploadImportJob(JobTask.CreateDocument, JobTask.CreateDocument.getLabel(), "UploadImportJob"),
	@Deprecated
	ZipDocImportJob(JobTask.CreateDocument, JobTask.CreateDocument.getLabel(), "ZipDocImportJob"),
	GoobiMetsImportJob(JobTask.CreateDocument, JobTask.CreateDocument.getLabel(), "GoobiMetsImportJob"),
	IiifImportJob(JobTask.CreateDocument, JobTask.CreateDocument.getLabel(), "IiifImportJob"),
	IobImportJob(JobTask.CreateDocument, JobTask.CreateDocument.getLabel(), "IobImportJob"),
	//plain old DuplicateDocJob that copies all pages of a document to a new one.
	DuplicateDocumentJob(JobTask.DuplicateDocument, JobTask.DuplicateDocument.getLabel(), "DuplicateDocJob"),
	//CopyJob that accepts DocumentSelectionDescriptors or GroundTruthSelectionDesctiptors for more control over the new document's content than DuplicateDocumentJob allows
	CopyJob(JobTask.DuplicateDocument, JobTask.DuplicateDocument.getLabel(), "CopyJob"),
	ComputeSampleJob(JobTask.Htr,"Sample Computation","ComputeSampleJob"),
	CreateSampleDocJob(JobTask.CreateSample, JobTask.CreateSample.getLabel(), "CreateSampleDocJob"),
	DeleteDocJob(JobTask.DeleteDocument, JobTask.DeleteDocument.getLabel(), "DeleteDocJob"),
	RebuildSolrIndexJob(JobTask.RebuildSolrIndex, JobTask.RebuildSolrIndex.getLabel(), "RebuildSolrIndexJob"),	
	IndexDocumentJob(JobTask.IndexDocument, JobTask.IndexDocument.getLabel(), "IndexDocumentJob"),
	DocExportJob(JobTask.Export, JobTask.Export.getLabel(), "ExportDocumentJob"),
	ReplaceJob(JobTask.Replace, JobTask.Replace.getLabel(), "ReplaceJob"),
	UndoJob(JobTask.Undo, JobTask.Undo.getLabel(), "UndoJob"),

	S3ImportJob(JobTask.CreateDocument, "S3 Import Job", "S3ImportJob"),
	S3AddPagesJob(JobTask.CreateDocument, "S3 Add Pages Job", "S3AddPagesJob"),
	S3PdfImportJob(JobTask.CreateDocument, "S3 Import Job", "S3PdfImportJob"),

	// --- LA jobs ---

	TranskribusLaJobNextGen(JobTask.DetectLines, "Baseline Detection NextGen", "LaJob"),
	TranskribusLaJobNextGenMultiThread(JobTask.DetectLines, "Baseline Detection NextGen", "MultiThreadLaJob"),
	TranskribusLaJob(JobTask.DetectLines, "Baseline Detection", "LaJob"),
	TranskribusLaJobMultiThread(JobTask.DetectLines, "Baseline Detection", "MultiThreadLaJob"),

	@Deprecated
	NcsrDetectLinesJob(JobTask.DetectLines, JobTask.DetectLines.getLabel(), "NcsrDetectLinesJob"),
	@Deprecated
	NcsrDetectBlocksJob(JobTask.DetectBlocks, JobTask.DetectBlocks.getLabel(), "NcsrDetectBlocksJob"),
	@Deprecated
	NcsrBatchLaJob(JobTask.DetectBlocks, "Block/Line Segmentation", "NcsrBatchLaJob"),
	@Deprecated
	NcsrOldLaJob(JobTask.DetectLines, JobTask.DetectLines.getLabel(), "LaJob"),
	@Deprecated
	NcsrLaJob(JobTask.DetectLines, JobTask.DetectLines.getLabel(), "LaJob", "libNCSR_TextLineSegmentation.so"),
	@Deprecated
	CITlabLaJob(JobTask.DetectLines, JobTask.DetectLines.getLabel(), "LaJob"),
	@Deprecated
	CITlabAdvancedLaJob(JobTask.DetectLines, JobTask.DetectLines.getLabel(), "LaJob"),
	@Deprecated
	CvlLaJob(JobTask.DetectLines, JobTask.DetectLines.getLabel(), "LaJob"),
	@Deprecated
	CvlTableJob(JobTask.DetectLines, JobTask.DetectLines.getLabel(), "LaJob"),
	@Deprecated
	CvlLaJobMultiThread(JobTask.DetectLines, JobTask.DetectLines.getLabel(), "MultiThreadLaJob"),
	@Deprecated
	CvlTableJobMultiThread(JobTask.DetectLines, JobTask.DetectLines.getLabel(), "MultiThreadLaJob"),
	@Deprecated
	UpvlcLaJob(JobTask.DetectLines, JobTask.DetectLines.getLabel(), "LaJob"),
	@Deprecated
	UpvlcLaJobMultiThread(JobTask.DetectLines, JobTask.DetectLines.getLabel(), "MultiThreadLaJob"),
	@Deprecated
	P2PaLAJob(JobTask.DetectLines, "Layout and structure analysis", "P2PaLAJob", null, true),
	@Deprecated
	P2PaLAJobMultiThread(JobTask.DetectLines, "Layout and structure analysis", "P2PaLAJobMultiThread"),
	@Deprecated
	P2PaLATrainJob(JobTask.LaTraining, "P2PaLATraining", "P2PaLATrainJob"),
	@Deprecated
	KrakenLaJob(JobTask.DetectLines, "Baseline and TextRegion Detection using Kraken", "LaJob"),
	@Deprecated
	KrakenLaJobMultiThread(JobTask.DetectLines, "Baseline and TextRegion Detection using Kraken", "MultiThreadLaJob"),
	@Deprecated
	T2IJob(JobTask.DetectLines, "Text2Image", "T2IJob"),
	@Deprecated
	T2IJobMultiThread(JobTask.DetectLines, "Text2Image", "T2IJobMultiThread"),
	@Deprecated
	NcsrOldLaJobMultiThread(JobTask.DetectLines, JobTask.DetectLines.getLabel(), "MultiThreadLaJob"),
	@Deprecated
	NcsrLaJobMultiThread(JobTask.DetectLines, JobTask.DetectLines.getLabel(), "MultiThreadLaJob", "libNCSR_TextLineSegmentation.so"),
	@Deprecated
	CITlabLaJobMultiThread(JobTask.DetectLines, JobTask.DetectLines.getLabel(), "MultiThreadLaJob"),
	@Deprecated
	CITlabAdvancedLaJobMultiThread(JobTask.DetectLines, JobTask.DetectLines.getLabel(), "MultiThreadLaJob"),
	
	// --- Text recognition & training jobs ---
	BaselineTrainingJob(JobTask.HtrTraining, "Baseline Training", "CITlabBaselineTrainingJob"),
	PyLaiaTrainingJob(JobTask.HtrTraining, "PyLaia Training", "PyLaiaTrainJob"),
	PyLaiaTrainingJobLarge(JobTask.HtrTraining, "Large PyLaia Training", "PyLaiaTrainJobLarge"),
	PyLaiaDecodingJob(JobTask.Htr, "PyLaia Decoding", "PyLaiaDecodingJob"),
	PyLaiaKwsDecodingJob(JobTask.Htr, "PyLaia SmartSearch Decoding", "PyLaiaDecodingJob"),
	PyLaiaLocalDecodingJob(JobTask.Htr, "PyLaia Local Decoding", "PyLaiaLocalDecodingJob"),
	PyLaiaProcApiJob(JobTask.Htr, "PyLaia Proc Api Decoding", "PyLaiaLocalDecodingJob"),
	PyLaiaProjectLocalDecodingJob(JobTask.Htr, "PyLaia Project Local Decoding", "PyLaiaLocalDecodingJob"),
	PyLaiaProjectLocalDecodingJob1(JobTask.Htr, "PyLaia Project Local Decoding", "PyLaiaLocalDecodingJob"),

	TrHtrJob(JobTask.Htr, "Transformer Text Recognition", "TrHtrJob"), // transformer based text recognition job
	TrHtrProcApiJob(JobTask.Htr, "Transformer Proc Api Text Recognition", "TrHtrJob"),
	Text2ImageJob(JobTask.Htr, "Text to Image Matching", "Text2ImageJob"),

	LayoutAnalysis2Job(JobTask.LayoutAnalysis2, "Field Recognition", "LayoutAnalysis2Job", null, true),
	LayoutAnalysis2TrainJob(JobTask.LayoutAnalysis2Train, "Field Model Training", "LayoutAnalysis2TrainJob", null, true),
	TableRecognitionJob(JobTask.TableRecognition, "Table Recognition", "TableRecognitionJob", null, true),
	TableRecognitionTrainJob(JobTask.TableRecognitionTrain, "Table Recognition Training", "TableRecognitionTrainJob", null, true),

	ErrorRateJob(JobTask.Htr, "Error Rate Computation", "ErrorRateJob"),

	@Deprecated
	CITlabHtrTrainingJob(JobTask.HtrTraining, "CITlab " + JobTask.HtrTraining.getLabel(), "CITlabHtrTrainingJob"),
	@Deprecated
	CITlabHtrPlusTrainingJob(JobTask.HtrTraining, "CITlab HTR+ Training", "CITlabHtrPlusTrainingJob"),
	@Deprecated
	CITlabHtrPlusTrainingJobLarge(JobTask.HtrTraining, "Large CITlab HTR+ Training", "CITlabHtrPlusTrainingJobLarge"),
	@Deprecated
	PyLaiaOcrJob(JobTask.Htr, "Transkribus OCR", "PyLaiaOcrJob"),
	@Deprecated
	CITlabSemiSupervisedHtrTrainingJob(JobTask.HtrTraining, "CITlab Text2Image", "CITlabSemiSupervisedHtrTrainingJob"),
	@Deprecated
	CITlabHtrJob(JobTask.Htr, "CITlab " + JobTask.Htr.getLabel(), "CITlabHtrJob"),
	@Deprecated
	CITlabOcrJob(JobTask.Htr, "Transkribus OCR", "CITlabOcrJob"),
	@Deprecated
	CITlabLocalHtrJob(JobTask.Htr, "Local CITlab " + JobTask.Htr.getLabel(), "CITlabLocalHtrJob"),
	@Deprecated
	CITlabProcApiJob(JobTask.Htr, "CITlab Processing API " + JobTask.Htr.getLabel(), "CITlabHtrJob"),
	@Deprecated
	FinereaderOcrJob(JobTask.Ocr, JobTask.Ocr.getLabel(), "OcrJob"),
	@Deprecated
	FinereaderLaJob(JobTask.DetectBlocks, "Printed Block Detection", "FinereaderLaJob"),
	@Deprecated
	FinereaderSepJob(JobTask.DetectBlocks, "Separator Detection", "FinereaderSepJob"),
	@Deprecated
	CITlabKwsJob(JobTask.Kws, "CITlab " + JobTask.Kws.getLabel(), "CITlabKwsJob"),
	@Deprecated
	CITlabClusterRecognitionJob(JobTask.Htr, "CITlab Cluster Recognition", "CITlabClusterRecognition"),
	@Deprecated
	DocUnderstandingRecognitionJob(JobTask.Htr, "DU Decoding", "DocUnderstandingRecognitionJob"),
	
	//for testing
	DummyJob(JobTask.CreateDocument, "Dummy Job", "DummyJob"),
	DummyMailJob(JobTask.CreateDocument, "Dummy Mail Job", "DummyMailJob"),
	DummyConfigurableJob(JobTask.CreateDocument, "Dummy Configurable Job", "DummyConfigurableJob"),
	DummyStressJob(JobTask.CreateDocument, "Dummy Stress Job", "DummyStressJob");

	
	private final static Logger logger = LoggerFactory.getLogger(JobImpl.class);
	public final static String MULTI_THREAD_LA_JOB_SUFFIX = "MultiThread";
	private JobTask task;
	private String label;
	private String className;
	private String libName;
	private boolean isMultiThreaded=false; // whether this job is intrinsically multi-threaded 
	
	JobImpl(JobTask task, String label, String className){
		this(task, label, className, null, false);
	}	
	
	JobImpl(JobTask task, String label, String className, String libName){
		this(task, label, className, libName, false);
	}
	
	JobImpl(JobTask task, String label, String className, String libName, boolean isMultiThreaded){
		this.task = task;
		this.label = label;
		this.className = className;
		this.libName = libName;
		this.isMultiThreaded = isMultiThreaded;
	}	
	
	public JobTask getTask(){
		return task;
	}
	public String getClassName(){
		return className;
	}
	public String getLabel() {
		return label;
	}
	public String getLibName() {
		return libName;
	}
	public boolean isMultiThreaded() {
		return isMultiThreaded;
	}
	
	/**
	 * Same as {@link #valueOf} but will return null if invalid value is passed!
	 * 
	 * @param str
	 * @return
	 */
	public static JobImpl fromStr(String str) {
		try {
			return JobImpl.valueOf(str); 
		}
		catch (Exception e) {
			logger.warn("Could not parse JobImpl from string: "+str+" - returning null value!");
			return null;
		}
	}
	
	public static List<String> valuesAsString() {
		return Arrays.stream(JobImpl.values()).map(ji -> ji.toString()).collect(Collectors.toList());
	}
	
	public static void main(String[] args) {
		System.out.println(JobImpl.valueOf(null)); 
	}

	public static JobImpl getBaseLaJob(JobImpl impl) {
		if(impl == null || !JobType.layoutAnalysis.equals(impl.getTask().getJobType())) {
			throw new IllegalArgumentException("No Layout Analysis job type: " + impl);
		}
		//Normalize to base job name (MultiThread suffix should only occur on server)
		final String baseImplStr = impl.toString().endsWith("MultiThread") ? impl.toString().replace("MultiThread", "") : impl.toString();
		return JobImpl.valueOf(baseImplStr);
	}
}
