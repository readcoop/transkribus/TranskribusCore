package eu.transkribus.core.model.beans;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.transkribus.core.util.GsonUtil;

public class TrpPreprocPars {
	private static final Logger logger = LoggerFactory.getLogger(TrpPreprocPars.class);
	
    String dewarp_method = "rotate"; // currently one of: dewarp, rotate, none
    boolean delete_background=true;
    float scaling_factor=1.0f;
    boolean do_sauvola=true; // binarization -> no-binarization could lead to crappy results on heterogeneous data (mixed binarized/non-binarized pages)
    int padding=10;
    int line_height=128;
    
    public TrpPreprocPars() {
    }

	public String getDewarp_method() {
		return dewarp_method;
	}

	public void setDewarp_method(String dewarp_method) {
		this.dewarp_method = dewarp_method;
	}

	public boolean isDelete_background() {
		return delete_background;
	}

	public void setDelete_background(boolean delete_background) {
		this.delete_background = delete_background;
	}

	public float getScaling_factor() {
		return scaling_factor;
	}

	public void setScaling_factor(float scaling_factor) {
		this.scaling_factor = scaling_factor;
	}

	public boolean isDo_sauvola() {
		return do_sauvola;
	}

	public void setDo_sauvola(boolean do_sauvola) {
		this.do_sauvola = do_sauvola;
	}

	public int getPadding() {
		return padding;
	}

	public void setPadding(int padding) {
		this.padding = padding;
	}

	public int getLine_height() {
		return line_height;
	}

	public void setLine_height(int line_height) {
		this.line_height = line_height;
	}
	
	public String toJson() {
		return GsonUtil.toJson(this);
	}
	
	public static TrpPreprocPars fromJson2(String json) {
		return GsonUtil.fromJson2(json, TrpPreprocPars.class);
	}	

	@Override
	public String toString() {
		return "TrpPreprocPars [dewarp_method=" + dewarp_method + ", delete_background=" + delete_background
				+ ", scaling_factor=" + scaling_factor + ", do_sauvola=" + do_sauvola + ", padding=" + padding
				+ ", line_height=" + line_height + "]";
	}
	
//	public static void main(String[] args) {
//		TrpPreprocPars p = new TrpPreprocPars();
//		String json = p.toJson();
//		System.out.println(json);
//		TrpPreprocPars p1 = TrpPreprocPars.fromJson2(json);
//		System.out.println(p1);
//	}
    
    
}
