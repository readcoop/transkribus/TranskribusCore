package eu.transkribus.core.model.beans;


import java.util.Objects;
import java.util.Properties;

import javax.xml.bind.annotation.XmlTransient;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonIgnore;

import eu.transkribus.core.io.util.TrpProperties;
import eu.transkribus.core.model.beans.job.enums.JobImpl;
import eu.transkribus.core.model.beans.job.enums.JobType;
import eu.transkribus.core.rest.JobConst;
import io.swagger.v3.oas.annotations.Hidden;

public class LaConfig {	
	
	private static final Logger logger = LoggerFactory.getLogger(LaConfig.class);
	
	protected boolean overwriteExistingLayout = true;
	
	protected boolean doBlockSeg=false;
	protected boolean doLineSeg=true;
	protected boolean doWordSeg=false;
	
	protected boolean doPolygonToBaseline=false;
	protected boolean doBaselineToPolygon=true;
	protected boolean doSimplifyPolygons=false;
	
	protected String b2pBackend = "Legacy";
	
	protected JobImpl jobImpl;

	protected Properties props;
	
	/* empty default constructor needed for deserialization */
	public LaConfig() {
		//init with empty props
		props = new Properties();
	}
	
	public LaConfig(final JobImpl jobImpl) {
		this(jobImpl, (Properties) null);
	}
	
	public LaConfig(final JobImpl jobImpl, TrpProperties props) {
		this(jobImpl, props.getProperties());
	}
	
	public LaConfig(final JobImpl jobImpl, Properties props) {
		this();
		if(jobImpl == null || !jobImpl.getTask().getJobType().equals(JobType.layoutAnalysis)) {
			throw new IllegalArgumentException("Not a layout analysis JobImpl!");
		}
		if(props == null) {
			props = new Properties();
		}
		this.jobImpl = jobImpl;
		this.props = props;
		this.initFieldsFromJobProps(props);
		logger.debug(toString());
	}
	
	protected void initFieldsFromJobProps(Properties props) {
		TrpProperties trpProps = TrpProperties.fromProperties(props);
		if (props.getProperty(JobConst.PROP_DO_BLOCK_SEG) != null) {
			setDoBlockSeg(trpProps.getBoolProperty(JobConst.PROP_DO_BLOCK_SEG));
		}
		if (props.getProperty(JobConst.PROP_DO_LINE_SEG) != null) {
			setDoLineSeg(trpProps.getBoolProperty(JobConst.PROP_DO_LINE_SEG));
		}
		if (props.getProperty(JobConst.PROP_DO_WORD_SEG) != null) {
			setDoWordSeg(trpProps.getBoolProperty(JobConst.PROP_DO_WORD_SEG));
		}
		if (props.getProperty(JobConst.PROP_DO_POLYGON_TO_BASELINE) != null) {
			setDoPolygonToBaseline(trpProps.getBoolProperty(JobConst.PROP_DO_POLYGON_TO_BASELINE));
		}
		if (props.getProperty(JobConst.PROP_DO_BASELINE_TO_POLYGON) != null) {
			setDoBaselineToPolygon(trpProps.getBoolProperty(JobConst.PROP_DO_BASELINE_TO_POLYGON));
		}
		if (props.getProperty(JobConst.PROP_DO_LINE_POLYGON_SIMPLIFICATION) != null) {
			setDoSimplifyPolygons(trpProps.getBoolProperty(JobConst.PROP_DO_LINE_POLYGON_SIMPLIFICATION));
		}
		if (props.getProperty(JobConst.PROP_B2P_BACKEND) != null) {
			setB2pBackend(trpProps.getProperty(JobConst.PROP_B2P_BACKEND));
		}
	}
	
	@Hidden
	@XmlTransient
	@JsonIgnore
	public Integer getModelIdAsInteger() {
		try {
			return Integer.parseInt(getModelId());	
		} catch (Exception e) {
			return null;
		}
	}
	
	@Hidden
	@XmlTransient
	@JsonIgnore	
	public String getModelId() {
		return props.getProperty(JobConst.PROP_MODEL_ID);
	}
	
	@Hidden
	@XmlTransient
	@JsonIgnore	
	public String getModelName () {
		return props.getProperty(JobConst.PROP_MODELNAME);
	}
	
	@Hidden
	@XmlTransient
	@JsonIgnore
	public String getModelLabel() {
		if (doLineSeg && (
				jobImpl.equals(JobImpl.CITlabAdvancedLaJob) || jobImpl.equals(JobImpl.CITlabAdvancedLaJobMultiThread) ||
				jobImpl.equals(JobImpl.TranskribusLaJob) || jobImpl.equals(JobImpl.TranskribusLaJobMultiThread) ||
				jobImpl.equals(JobImpl.TranskribusLaJobNextGen) || jobImpl.equals(JobImpl.TranskribusLaJobNextGenMultiThread)
				)) {
			String lbl = "Model: ";
			String modelId = getModelId();
			String modelName = getModelName();
			if (modelId != null) {
				lbl += modelId+", ";
			}
			if (modelName != null) {
				lbl += modelName;
			}
			else {
				lbl += "Preset";
			}
			return lbl;
		}
		else {
			return "";
		}
	}

	public boolean isDoBlockSeg() {
		return doBlockSeg;
	}

	public void setDoBlockSeg(boolean doBlockSeg) {
		this.doBlockSeg = doBlockSeg;
	}

	public boolean isDoLineSeg() {
		return doLineSeg;
	}

	public void setDoLineSeg(boolean doLineSeg) {
		this.doLineSeg = doLineSeg;
	}

	public boolean isDoWordSeg() {
		return doWordSeg;
	}

	public void setDoWordSeg(boolean doWordSeg) {
		this.doWordSeg = doWordSeg;
	}

	public boolean isDoPolygonToBaseline() {
		return doPolygonToBaseline;
	}

	public void setDoPolygonToBaseline(boolean doPolygonToBaseline) {
		this.doPolygonToBaseline = doPolygonToBaseline;
	}

	public boolean isDoBaselineToPolygon() {
		return doBaselineToPolygon;
	}

	public void setDoBaselineToPolygon(boolean doBaselineToPolygon) {
		this.doBaselineToPolygon = doBaselineToPolygon;
	}
	
	public boolean isDoSimplifyPolygons() {
		return doSimplifyPolygons;
	}

	public void setDoSimplifyPolygons(boolean doSimplifyPolygons) {
		this.doSimplifyPolygons = doSimplifyPolygons;
	}

	public boolean isOverwriteExistingLayout() {
		return overwriteExistingLayout;
	}

	public void setOverwriteExistingLayout(boolean overwriteExistingLayout) {
		this.overwriteExistingLayout = overwriteExistingLayout;
	}

	public String getB2pBackend() {
		return b2pBackend;
	}

	public void setB2pBackend(String b2pBackend) {
		this.b2pBackend = b2pBackend;
	}

	public JobImpl getJobImpl() {
		return jobImpl;
	}

	public Properties getProps() {
		return props;
	}

	public void setProps(Properties props) {
		this.props = props;
		initFieldsFromJobProps(this.props);
	}

	@Hidden
	@XmlTransient
	@JsonIgnore
	public boolean isB2POnly() {
		return doBaselineToPolygon && !doLineSeg;
	}
	
	@Hidden
	@XmlTransient
	@JsonIgnore
	public TrpProperties getTrpProperties() {
		return TrpProperties.fromProperties(getProps());
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		LaConfig laConfig = (LaConfig) o;
		return overwriteExistingLayout == laConfig.overwriteExistingLayout && doBlockSeg == laConfig.doBlockSeg && doLineSeg == laConfig.doLineSeg && doWordSeg == laConfig.doWordSeg && doPolygonToBaseline == laConfig.doPolygonToBaseline && doBaselineToPolygon == laConfig.doBaselineToPolygon && doSimplifyPolygons == laConfig.doSimplifyPolygons && Objects.equals(b2pBackend, laConfig.b2pBackend) && jobImpl == laConfig.jobImpl && Objects.equals(props, laConfig.props);
	}

	@Override
	public int hashCode() {
		return Objects.hash(overwriteExistingLayout, doBlockSeg, doLineSeg, doWordSeg, doPolygonToBaseline, doBaselineToPolygon, doSimplifyPolygons, b2pBackend, jobImpl, props);
	}

	@Override
	public String toString() {
		return "LaConfig [overwriteExistingLayout=" + overwriteExistingLayout + ", doBlockSeg=" + doBlockSeg
				+ ", doLineSeg=" + doLineSeg + ", doWordSeg=" + doWordSeg + ", doPolygonToBaseline="
				+ doPolygonToBaseline + ", doBaselineToPolygon=" + doBaselineToPolygon + ", doSimplifyPolygons="
				+ doSimplifyPolygons + ", b2pBackend=" + b2pBackend + ", jobImpl=" + jobImpl + ", props=" + props + "]";
	}
}