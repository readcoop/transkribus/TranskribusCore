package eu.transkribus.core.model.beans;

import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;

import org.apache.commons.collections.CollectionUtils;
import org.dea.fimgstoreclient.beans.ImgType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.transkribus.core.io.util.TrpProperties;
import eu.transkribus.core.model.beans.DocumentSelectionDescriptor.PageDescriptor;
import eu.transkribus.core.model.beans.rest.ParameterMap;
import eu.transkribus.core.rest.JobConst;
import eu.transkribus.core.util.CoreUtils;
import eu.transkribus.core.util.JobDataUtils;
import eu.transkribus.core.util.ModelUtil;
import io.swagger.v3.oas.annotations.Hidden;
import io.swagger.v3.oas.annotations.media.Schema;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@XmlSeeAlso(DocumentSelectionDescriptor.class)
//@XmlSeeAlso({DocumentSelectionDescriptor.class, TrpModelMetadata.class})
public abstract class HtrTrainConfig implements Serializable {
	private static final Logger logger = LoggerFactory.getLogger(HtrTrainConfig.class);
	
	@Hidden
	public final static String NUM_EPOCHS_KEY = "Nr. of Epochs";
	@Hidden
	public final static String LEARNING_RATE_KEY ="Learning Rate";
	@Hidden
	public final static String NOISE_KEY = "Noise";
	@Hidden
	public final static String TRAIN_SIZE_KEY = "Train Size per Epoch";
	@Hidden
	public final static String BASE_MODEL_ID_KEY = "HTR Base Model ID";
	@Hidden
	public final static String BASE_MODEL_NAME_KEY = "HTR Base Model Name";
	@Hidden
	public final static String EARLY_STOPPING_KEY = "Early Stopping";	
	@Hidden
	public final static String OMITTED_TAGS_KEY = "Omitted Tags";	

	private static final long serialVersionUID = 1434111712220564100L;
	
	@Schema(description = "The metadata to be stored with the new HTR model", required=true)
	private TrpModelMetadata modelMetadata;
	
	/**
	 * @deprecated used by legacy clients
	 */
	@Schema(description = "the name of the new HTR model", required=true)
	private String modelName;
	/**
	 * @deprecated used by legacy clients
	 */
	@Schema(description = "textual description", required=true)
	private String description;
	/**
	 * @deprecated used by legacy clients
	 */
	@Schema(description = "specifies the language of the training data's text content", required=true)
	private String language;
	/**
	 * @deprecated used by legacy clients
	 */
	@Schema(description = "the HTR techology provider", allowableValues =  {ModelUtil.PROVIDER_CITLAB, ModelUtil.PROVIDER_CITLAB_PLUS})
	private String provider;
	/**
	 * @deprecated used by legacy clients
	 */
	@Schema(description = "Optional. Can be used to specify an existing HTR to be used as starting point for the training. Provider string must match the one given.", required=false)
	private Integer baseModelId;
	
	private int colId;
	@Schema(description = "map with custom parameters", implementation=ParameterMap.class)
	private ParameterMap customParams;
	
	public HtrTrainConfig() {
		provider = null;
		customParams = new ParameterMap();
		modelMetadata = new TrpModelMetadata();
	}
	
	@XmlElementWrapper(name="trainList")
	@XmlElement
	private List<DocumentSelectionDescriptor> train = new LinkedList<>();

	@XmlElementWrapper(name="testList")
	@XmlElement
	private List<DocumentSelectionDescriptor> test = new LinkedList<>();
	
	@XmlElementWrapper(name="trainGtList")
	@XmlElement
	private List<GroundTruthSelectionDescriptor> trainGt;
	
	@XmlElementWrapper(name="testGtList")
	@XmlElement
	private List<GroundTruthSelectionDescriptor> testGt;
	
	public TrpModelMetadata getModelMetadata() {
		//on POST from legacy clients, the member fields will be set
		if(modelMetadata.getName() == null && modelMetadata.getDescriptions() == null && modelMetadata.getLanguage() == null) {
			logger.debug("getModelMetadata() called. Filling in data from legacy train config fields.");
			modelMetadata.setName(modelName);
			modelMetadata.setLanguage(language);
			modelMetadata.setDescriptions(new HashMap<>());
			modelMetadata.getDescriptions().put(Locale.ENGLISH.getLanguage(), description);
			modelMetadata.setBaseModelId(baseModelId);
			modelMetadata.setProvider(provider);
		}
		return modelMetadata;
	}
	
	public void setModelMetadata(TrpModelMetadata modelMetadata) {
		this.modelMetadata = modelMetadata;
	}
	
	public int getColId() {
		return colId;
	}
	
	public void setColId(int colId) {
		this.colId = colId;
	}

	public ParameterMap getCustomParams() {
		if (customParams == null) {
			customParams = new ParameterMap();
		}
		
		return customParams;
	}

	public void setCustomParams(ParameterMap customParams) {
		this.customParams = customParams;
	}

	public void setCustomParam(String key, Object value) {
		getCustomParams().addParameter(key, value);
	}
	
	public void removeCustomParam(String key) {
		getCustomParams().remove(key);
	}
	
	public void setImgType(ImgType imgType) {
		setCustomParam(JobConst.PROP_IMG_TYPE, imgType);
	}
	
	public ImgType getImgType() {
		String imgTypeStr = getCustomParams().getParameterValue(JobConst.PROP_IMG_TYPE);
		if (imgTypeStr == null) {
			return ImgType.orig;
		}
		try {	
			return ImgType.valueOf(imgTypeStr);
		} catch (Exception e) {
			logger.warn("Could not parse image type: "+imgTypeStr+" - using imgType "+ImgType.orig);
			return ImgType.orig;
		}
	}
	
	public boolean isUseExistingLinePolygons() {
		return getCustomParams() != null && getCustomParams().getBoolParam(JobConst.PROP_USE_EXISTING_LINE_POLYGONS, false);
	}
	
	public void setUseExistingLinePolygons(boolean useExistingLinePolygons) {
		setCustomParam(JobConst.PROP_USE_EXISTING_LINE_POLYGONS, useExistingLinePolygons);
	}
	
	public boolean isDoNotDeleteWorkDir() {
		return getCustomParams() != null && getCustomParams().getBoolParam(JobConst.PROP_DO_NOT_DELETE_WORKDIR, false);
	}
	
	public void setDoNotDeleteWorkDir(boolean doNotDeleteWorkDir) {
		setCustomParam(JobConst.PROP_DO_NOT_DELETE_WORKDIR, doNotDeleteWorkDir);
	}
	
	public String getB2PBackend() {
		return getCustomParams() != null ? getCustomParams().getParameterValue(JobConst.PROP_B2P_BACKEND, "Legacy") : "Legacy";
	}
	
	public void setB2PBackend(String b2pBackend) {
		setCustomParam(JobConst.PROP_B2P_BACKEND, b2pBackend);
	}
	
	public List<DocumentSelectionDescriptor> getTrain() {
		return train;
	}

	public void setTrain(List<DocumentSelectionDescriptor> train) {
		if (train == null) {
			train = new LinkedList<>();
		} else {
			this.train = train;	
		}
	}
	
	public List<DocumentSelectionDescriptor> getTest() {
		return test;
	}

	public void setTest(List<DocumentSelectionDescriptor> test) {
		if (test == null) {
			test = new LinkedList<>();
		} else {
			this.test = test;	
		}
	}
	
	public List<GroundTruthSelectionDescriptor> getTrainGt() {
		return trainGt;
	}

	public void setTrainGt(List<GroundTruthSelectionDescriptor> trainGt) {
		this.trainGt = trainGt;	
	}
	
	public List<GroundTruthSelectionDescriptor> getTestGt() {
		return testGt;
	}

	public void setTestGt(List<GroundTruthSelectionDescriptor> testGt) {
		this.testGt = testGt;
	}
	
	public ReverseTextParams getReverseTextParams() {
		boolean reverseText = getCustomParams().getBoolParam(JobConst.PROP_REVERSE_TEXT, false);
		boolean excludeDigits = getCustomParams().getBoolParam(JobConst.PROP_REVERSE_TEXT_EXCLUDE_DIGITS, false);
		List<String> tagExceptions = getCustomParams().getStringListParameterValue(JobConst.PROP_REVERSE_TEXT_TAG_EXCEPTIONS);
		return new ReverseTextParams(reverseText, excludeDigits, tagExceptions);
	}
	
	public void setReverseTextParams(ReverseTextParams params) {
		if (params == null) {
			return;
		}
		getCustomParams().addBoolParam(JobConst.PROP_REVERSE_TEXT, params.reverseText);
		getCustomParams().addBoolParam(JobConst.PROP_REVERSE_TEXT_EXCLUDE_DIGITS, params.excludeDigits);
		getCustomParams().addStringListParameter(JobConst.PROP_REVERSE_TEXT_TAG_EXCEPTIONS, params.tagExceptions);
	}
	
	public CustomTagTrainingParams getCustomTagParams() {
		boolean trainAbbrevs = getCustomParams().getBoolParam(JobConst.PROP_CUSTOM_ABBREVS_TRAINING, false);
		boolean trainTags = getCustomParams().getBoolParam(JobConst.PROP_CUSTOM_TAG_TRAINING, false);
		boolean shortForm = getCustomParams().getBoolParam(JobConst.PROP_SHORT_FORM, false);
		boolean trainProps = getCustomParams().getBoolParam(JobConst.PROP_TRAIN_PROPERTIES, false);
		List<String> tagsToTrain = getCustomParams().getStringListParameterValue(JobConst.PROP_CUSTOM_TAG_TRAINING_TAG_SELECTION);
		Map<String, Object> tagMap = getCustomParams().getParamStringMap(JobConst.PROP_CUSTOM_TAG_MAP);
		return new CustomTagTrainingParams(trainTags, tagsToTrain, tagMap, trainProps, shortForm, trainAbbrevs);
	}
	
	public void setCustomTagParams(CustomTagTrainingParams params) {
		if (params == null) {
			return;
		}
		getCustomParams().addBoolParam(JobConst.PROP_CUSTOM_ABBREVS_TRAINING, params.trainAbbrevs);
		getCustomParams().addBoolParam(JobConst.PROP_CUSTOM_TAG_TRAINING, params.trainCustomTags);
		getCustomParams().addBoolParam(JobConst.PROP_SHORT_FORM, params.useShortForm);
		getCustomParams().addBoolParam(JobConst.PROP_TRAIN_PROPERTIES, params.useProperties);
		getCustomParams().addStringListParameter(JobConst.PROP_CUSTOM_TAG_TRAINING_TAG_SELECTION, params.tagsToTrain);
		getCustomParams().addStringMapParameter(JobConst.PROP_CUSTOM_TAG_MAP, params.tagMap);
	}
	
	public List<String> getTagsToOmit() {
		return JobDataUtils.getStringList(getCustomParams().toProperties(), JobConst.PROP_HTR_OMIT_LINES_BY_TAG);
	}
	
	public void setTagsToOmit(List<String> tagsToOmit) {
		getCustomParams().addStringListParameter(JobConst.PROP_HTR_OMIT_LINES_BY_TAG, tagsToOmit);
	}	
	
	public boolean hasTagsToOmit() {
		return !CoreUtils.isEmpty(getTagsToOmit());
	}
	
	@Hidden
	public boolean isTestAndTrainOverlapping() {
		for(DocumentSelectionDescriptor trainDsd : train) {
			for(DocumentSelectionDescriptor testDsd : test) {
				if(trainDsd.getDocId() == testDsd.getDocId()) {
					//found overlap in doc selection; compare each of the pages
					for(PageDescriptor trainP : trainDsd.getPages()) {
						for(PageDescriptor testP : testDsd.getPages()) {
							if(trainP.getPageId() == testP.getPageId()) {
								//same page was selected for test and train
								return true;
							}
						}
					}
				}
			}
		}
		return false;
	}
	
	/**
	 * Sets parameters for training config to a TrpProperties object for DB storage. 
	 */
	public TrpProperties getParamProps() {
		TrpProperties p = new TrpProperties();
		
		if (getReverseTextParams().reverseText) {
			p.setProperty(JobConst.PROP_REVERSE_TEXT, getReverseTextParams().reverseText);
			p.setProperty(JobConst.PROP_REVERSE_TEXT_EXCLUDE_DIGITS, getReverseTextParams().excludeDigits);
			if (!CollectionUtils.isEmpty(getReverseTextParams().tagExceptions)) {
				p.setProperty(JobConst.PROP_REVERSE_TEXT_TAG_EXCEPTIONS, CoreUtils.join(getReverseTextParams().tagExceptions));	
			}
		}
		
		if (getCustomTagParams().trainCustomTags) {
			p.setProperty(JobConst.PROP_CUSTOM_TAG_TRAINING, getCustomTagParams().trainCustomTags);
			p.setProperty(JobConst.PROP_SHORT_FORM, getCustomTagParams().useShortForm);
			p.setProperty(JobConst.PROP_TRAIN_PROPERTIES, getCustomTagParams().useProperties);
			if (!CollectionUtils.isEmpty(getCustomTagParams().tagsToTrain)) {
				p.setProperty(JobConst.PROP_CUSTOM_TAG_TRAINING_TAG_SELECTION, CoreUtils.join(getCustomTagParams().tagsToTrain));	
			}
			if (!getCustomTagParams().tagMap.isEmpty()) {
				p.setProperty(JobConst.PROP_CUSTOM_TAG_MAP, CoreUtils.mapToString(getCustomTagParams().tagMap));
			}
		}
		
		if (hasTagsToOmit()) {
			p.setProperty(OMITTED_TAGS_KEY, CoreUtils.join(getTagsToOmit()));
		}
		
		return p;
	}

	@Override
	public String toString() {
		return "HtrTrainConfig [modelMetadata=" + modelMetadata + ", modelName=" + modelName + ", description="
				+ description + ", language=" + language + ", colId=" + colId + ", provider=" + provider
				+ ", customParams=" + customParams + ", train=" + train + ", test=" + test + ", trainGt=" + trainGt
				+ ", testGt=" + testGt + "]";
	}
	
	//TODO remove
	/**
	 * @deprecated use accessors in {@link #getModelMetadata()}
	 */
	public String getModelName() {
		if(modelMetadata.getName() == null && modelName != null) {
			//JaxB will not use the setter. Remove this fallback when removing the fields.
			setModelName(this.modelName);
		}
		return modelMetadata.getName();
	}
	/**
	 * @deprecated use accessors in {@link #getModelMetadata()}
	 */
	public void setModelName(String modelName) {
		this.modelName = modelName;
		this.modelMetadata.setName(modelName);
	}
	/**
	 * @deprecated use accessors in {@link #getModelMetadata()}
	 */
	public String getDescription() {
		if(modelMetadata.getDescriptions() == null && description != null) {
			//JaxB will not use the setter. Remove this fallback when removing the fields.
			setDescription(description);
		}
		if(modelMetadata.getDescriptions() == null) {
			return null;
		}
		return modelMetadata.getDescriptions().get(Locale.ENGLISH.getLanguage());
	}
	/**
	 * @deprecated use accessors in {@link #getModelMetadata()}
	 */
	public void setDescription(String description) {
		this.description = description;
		if(modelMetadata.getDescriptions() == null) {
			modelMetadata.setDescriptions(new HashMap<>());
		}
		this.modelMetadata.getDescriptions().put(Locale.ENGLISH.getLanguage(), description);
	}
	/**
	 * @deprecated use accessors in {@link #getModelMetadata()}
	 */
	public String getLanguage() {
		if(modelMetadata.getLanguage() == null && language != null) {
			//JaxB will not use the setter. Remove this fallback when removing the fields.
			setLanguage(language);
		}
		return modelMetadata.getLanguage();
	}
	/**
	 * @deprecated use accessors in {@link #getModelMetadata()}
	 */
	public void setLanguage(String language) {
		this.language = language;
		this.modelMetadata.setLanguage(language);
	}
	/**
	 * @deprecated use accessors in {@link #getModelMetadata()}
	 */
	public String getProvider() {
		if(modelMetadata.getProvider() == null && provider != null) {
			//JaxB will not use the setter. Remove this fallback when removing the fields.
			setProvider(provider);
		}
		return modelMetadata.getProvider();
	}
	/**
	 * @deprecated use accessors in {@link #getModelMetadata()}
	 */
	public void setProvider(String provider) {
		this.provider = provider;
		this.modelMetadata.setProvider(provider);
	}
	/**
	 * @deprecated use accessors in {@link #getModelMetadata()}
	 */
	public Integer getBaseModelId() {
		if(modelMetadata.getBaseModelId() == null && baseModelId != null) {
			//JaxB will not use the setter. Remove this fallback when removing the fields.
			setBaseModelId(baseModelId);
		}
		return modelMetadata.getBaseModelId();
	}
	/**
	 * @deprecated use accessors in {@link #getModelMetadata()}
	 */
	public void setBaseModelId(Integer baseModelId) {
		this.baseModelId = baseModelId;
		this.modelMetadata.setBaseModelId(baseModelId);
	}
	
	public abstract String getType();
}
