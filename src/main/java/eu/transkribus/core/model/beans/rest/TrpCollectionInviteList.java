package eu.transkribus.core.model.beans.rest;

import eu.transkribus.core.model.beans.TrpCollectionInvite;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import java.util.List;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@XmlSeeAlso({TrpCollectionInvite.class})
public class TrpCollectionInviteList extends JaxbPaginatedList<TrpCollectionInvite> {

	public TrpCollectionInviteList() {
		super();
	}

	public TrpCollectionInviteList(List<TrpCollectionInvite> list, int total, int index, int nValues, String sortColumnField,
								   String sortDirection) {
		super(list, total, index, nValues, sortColumnField, sortDirection);
	}
}
