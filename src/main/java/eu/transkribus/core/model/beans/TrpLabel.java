package eu.transkribus.core.model.beans;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Objects;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAccessType;


@Entity
@Table(name="LABELS")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class TrpLabel  {

    public TrpLabel() {}

    @Id
    @Column(name="LABEL_ID", updatable = false)
    private Integer labelId;

    @Column
    private String name;

    @Column
    private String color;

    @Column(name = "CHANGED", insertable = false, updatable = false)
    private Date changed;

    @Column
    private String type;

    @Column(name="USER_ID", updatable = false)
    private Integer userId;


    public Integer getLabelId() {
        return labelId;
    }

    public void setLabelId(Integer labelId) {
        this.labelId = labelId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Date getChanged() {
        return changed;
    }

    public void setChanged(Date changed) {
        this.changed = changed;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TrpLabel)) return false;
        TrpLabel trpLabel = (TrpLabel) o;
        return userId == trpLabel.userId && Objects.equals(labelId, trpLabel.labelId) && Objects.equals(name, trpLabel.name) && Objects.equals(color, trpLabel.color) && Objects.equals(changed, trpLabel.changed) && Objects.equals(type, trpLabel.type);
    }

    @Override
    public int hashCode() {
        return Objects.hash(labelId, name, color, changed, type, userId);
    }

    @Override
    public String toString() {
        return "TrpLabel{" +
                "labelId=" + labelId +
                ", name='" + name + '\'' +
                ", color='" + color + '\'' +
                ", changed=" + changed +
                ", type='" + type + '\'' +
                ", userId=" + userId +
                '}';
    }
}
