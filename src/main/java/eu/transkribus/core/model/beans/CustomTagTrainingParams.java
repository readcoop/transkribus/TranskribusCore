package eu.transkribus.core.model.beans;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import eu.transkribus.core.io.util.TrpProperties;
import eu.transkribus.core.model.beans.customtags.AbbrevTag;
import eu.transkribus.core.rest.JobConst;
import eu.transkribus.core.util.UnicodeList;

public class CustomTagTrainingParams {
	
	UnicodeList unicodes4CustomTags = new UnicodeList("miscellaneousSymbols", "U+2600-U+26FF");
	
	/*
	 * not sure if it is useful to train the 'short form' (=properties, e.g. the expansion is replaced by a two string symbol combination)
	 * 	-> did not really work very well - seems to be random combination of these two symbols during recognition
	 * properties could be trained as well (not sure how this behaves with several property values
	 * so default values are taken at the beginning
	 */
	public boolean trainCustomTags = false;
	public boolean useShortForm = false;
	public boolean useProperties = false;
	public boolean trainAbbrevs = false;

	public List<String> tagsToTrain = new ArrayList<>();	
	
	//map for storing the 'used special character' for each 'tag to train'
	public Map<String, Object> tagMap = new HashMap<String, Object>();
	
	public CustomTagTrainingParams() {
	}
	
	public CustomTagTrainingParams(boolean trainTags, List<String> tags4Training, boolean useProperties, boolean useShortForm) {
		super();
		this.trainCustomTags = trainTags;
		this.tagsToTrain = tags4Training;
		this.useProperties = useProperties;
		this.useShortForm = useShortForm;
		initTagMap();

	}
	
	public CustomTagTrainingParams(boolean trainTags, List<String> tags4Training, boolean useProperties, boolean useShortForm, boolean trainAbbrevs) {
		super();
		this.trainCustomTags = trainTags;
		this.tagsToTrain = tags4Training;
		this.useProperties = useProperties;
		this.useShortForm = useShortForm;
		this.trainAbbrevs = trainAbbrevs;
		initTagMap();

	}
	
	public CustomTagTrainingParams(boolean trainTags, List<String> tags4Training, Map<String, Object> tagMap, boolean useProperties, boolean useShortForm) {
		super();
		this.trainCustomTags = trainTags;
		this.tagsToTrain = tags4Training;
		this.useProperties = useProperties;
		this.useShortForm = useShortForm;
		this.tagMap = tagMap;

	}
	
	public CustomTagTrainingParams(boolean trainTags, List<String> tags4Training, Map<String, Object> tagMap, boolean useProperties, boolean useShortForm, boolean trainAbbrevs) {
		super();
		this.trainAbbrevs = trainAbbrevs;
		this.trainCustomTags = trainTags;
		this.tagsToTrain = tags4Training;
		this.useProperties = useProperties;
		this.useShortForm = useShortForm;
		this.tagMap = tagMap;

	}
	
	public CustomTagTrainingParams(boolean trainTags, boolean useShortForm, boolean useProperties, List<String> tags4Training, Map<String, Object> tagMap2) {
		super();
		this.trainCustomTags = trainTags;
		this.useShortForm = useShortForm;
		this.useProperties = useProperties;
		this.tagsToTrain = tags4Training;
		this.tagMap = tagMap2;
	}
	
	@Override
	public String toString() {
		return "CustomTagTrainingParams [trainTags=" + trainCustomTags + ", useShortForm=" + useShortForm
				+ ", useProperties=" + useProperties + ", tagsToTrain=" + tagsToTrain + ", tagMap=" + tagMap + "]";
	}
	
	private void initTagMap() {
		int i = 0;
		
		/*
		 * abbrev training is handled as a special training if...
		 * ... expansion is used; otherwise handled like a normal tag training
		 */
		if(isTrainAbbrevs()) {
			tagMap.put(AbbrevTag.TAG_NAME, "$");
		}
		for (String tag : tagsToTrain) {
			tagMap.put(tag, unicodes4CustomTags.getUnicodesAsStrings().get(i));
			i++;
			
//			if (tag.contentEquals(AbbrevTag.TAG_NAME)) {
//				tagMap.put(tag, "$");
//			}
//			else {
//
//			}

		}
	}
	
	public static CustomTagTrainingParams fromProps(Properties props) {
		return fromProps(new TrpProperties(props));
	}
	
	public static CustomTagTrainingParams fromProps(TrpProperties props) {
		boolean trainTags = props.getOrDefault(JobConst.PROP_CUSTOM_TAG_TRAINING, false);
		boolean useShortForm = props.getOrDefault(JobConst.PROP_SHORT_FORM, true);
		boolean useProperties = props.getOrDefault(JobConst.PROP_TRAIN_PROPERTIES, false);
		List<String> tagsToTrain = props.getStrListOrDefault(JobConst.PROP_CUSTOM_TAG_TRAINING_TAG_SELECTION, new ArrayList<>());
		Map<String, Object> tagMap = props.getMapOrDefault(JobConst.PROP_CUSTOM_TAG_MAP, new HashMap<String, Object>());
		return new CustomTagTrainingParams(trainTags, useShortForm, useProperties, tagsToTrain, tagMap);		
	}
//
	public boolean isUseShortForm() {
		return useShortForm;
	}

	public void setUseShortForm(boolean useShortForm) {
		this.useShortForm = useShortForm;
	}
	
	public boolean isTrainCustomTags() {
		return trainCustomTags;
	}

	public void setTrainCustomTags(boolean trainCustomTags) {
		this.trainCustomTags = trainCustomTags;
	}
	
	public boolean isUseProperties() {
		return useProperties;
	}

	public void setUseProperties(boolean useProperties) {
		this.useProperties = useProperties;
	}

	public boolean isTrainAbbrevs() {
		return trainAbbrevs;
	}

	public void setTrainAbbrevs(boolean trainAbbrevs) {
		this.trainAbbrevs = trainAbbrevs;
	}


}