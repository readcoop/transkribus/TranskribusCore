package eu.transkribus.core.model.beans.enums;

public enum CreditSelectionStrategy {
	/**
	 * Server picks strategy: COLLECTION_ONLY or USER_ONLY if there are no collection packages
	 */
	AUTO,
	/**
	 * Consider packages owned by user only
	 */
	USER_ONLY,
	/**
	 * Consider packages linked to collection only
	 */
	COLLECTION_ONLY,
	/**
	 * Consider user-owned packages with higher priority than collection-linked packages
	 * 
	 * @deprecated does not fit the credit account logic. Treated like AUTO.
	 */
	USER_THEN_COLLECTION,
	/**
	 * Consider collection-linked packages with higher priority than user-owned packages
	 * 
	  * @deprecated does not fit the credit account logic. Treated like USER_ONLY.
	 */
	COLLECTION_THEN_USER;
	
	public static CreditSelectionStrategy fromString(final String value) {
		if(value == null) {
			return null;
		}
		try {
			return valueOf(value);
		} catch (IllegalArgumentException e) {
			return null;
		}
	}
}
