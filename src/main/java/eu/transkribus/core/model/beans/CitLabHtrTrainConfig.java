package eu.transkribus.core.model.beans;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import eu.transkribus.core.io.util.TrpProperties;
import eu.transkribus.core.rest.JobConst;
import io.swagger.v3.oas.annotations.Hidden;
import io.swagger.v3.oas.annotations.media.Schema;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class CitLabHtrTrainConfig extends HtrTrainConfig implements Serializable {
	private static final long serialVersionUID = 6826017343706433307L;

	@Schema(description = "the number of epochs. A positive natural number.", required=true)
	protected Integer numEpochs = 20;
	@Schema(description = "the learning rate. Floating point in scientific notation, e.g. 2E-3", required=false)
	protected String learningRate;
	@Schema(description = "noise setting", allowableValues= {"no", "preproc", "net", "both"}, required=false)
	protected String noise;
	@Schema(description = "train size per epoch. Positive natural number", required=false)
	protected Integer trainSizePerEpoch;
	@Schema(description = "Optional. If CER does not improve after this number of epochs, stop the training early.", required=false)
	protected Integer earlyStopping;
	
	public CitLabHtrTrainConfig() {
		super();
	}
	
//	private int threads=4;
//	private int subSampling = 4; // how many subsets are the pages divided into to reduce overfitting 
//								// max is the number of input pages, elsewise a RuntimeException is thrown during training!
	
	public Integer getNumEpochs() {
		return numEpochs;
	}
	public void setNumEpochs(Integer numEpochs) {
		this.numEpochs = numEpochs;
	}
	public String getLearningRate() {
		return learningRate;
	}
	public void setLearningRate(String learningRate) {
		this.learningRate = learningRate;
	}
	public String getNoise() {
		return noise;
	}
	public void setNoise(String noise) {
		this.noise = noise;
	}
	public Integer getTrainSizePerEpoch() {
		return trainSizePerEpoch;
	}
	public void setTrainSizePerEpoch(Integer trainSizePerEpoch) {
		this.trainSizePerEpoch = trainSizePerEpoch;
	}
	public Integer getEarlyStopping() {
		return earlyStopping;
	}
	public void setEarlyStopping(Integer earlyStopping) {
		this.earlyStopping = earlyStopping;
	}

	@Override
	public String toString() {
		return "CitLabHtrTrainConfig [numEpochs=" + numEpochs + ", learningRate=" + learningRate + ", noise=" + noise
				+ ", trainSizePerEpoch=" + trainSizePerEpoch + ", earlyStopping=" + earlyStopping
				+ ", getModelMetadata()=" + getModelMetadata() + ", getColId()=" + getColId() + ", getCustomParams()="
				+ getCustomParams() + ", getImgType()=" + getImgType() + ", getTrain()=" + getTrain() + ", getTest()="
				+ getTest() + ", getTrainGt()=" + getTrainGt() + ", getTestGt()=" + getTestGt()
				+ ", getReverseTextParams()=" + getReverseTextParams() + ", getCustomTagParams()="
				+ getCustomTagParams() + ", getTagsToOmit()=" + getTagsToOmit() + ", hasTagsToOmit()=" + hasTagsToOmit()
				+ ", isTestAndTrainOverlapping()=" + isTestAndTrainOverlapping() + "]";
	}

	@Hidden
	public TrpProperties getParamProps() {
		TrpProperties p = super.getParamProps();
		p.setProperty(NUM_EPOCHS_KEY, numEpochs);
		p.setProperty(LEARNING_RATE_KEY, learningRate);
		p.setProperty(NOISE_KEY, noise);
		p.setProperty(TRAIN_SIZE_KEY, trainSizePerEpoch);
		if(getModelMetadata().getBaseModelId() != null) {
			p.setProperty(BASE_MODEL_ID_KEY, getModelMetadata().getBaseModelId());
		}
		if(getCustomParams() != null) {
			String earlyStopping = getCustomParams().getParameterValue(JobConst.PROP_EARLY_STOPPING);
			if(earlyStopping != null) {
				p.setProperty("Early Stopping", earlyStopping);
			}
		}
		if (earlyStopping!=null) {
			p.setProperty(EARLY_STOPPING_KEY, earlyStopping);
		}
		return p;
	}

	@Override
	public String getType() {
		return TrpHtr.TYPE_TEXT;
	}
}
