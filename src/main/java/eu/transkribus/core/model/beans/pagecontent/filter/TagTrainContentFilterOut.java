package eu.transkribus.core.model.beans.pagecontent.filter;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.transkribus.core.model.beans.customtags.CustomTag;
import eu.transkribus.core.model.beans.pagecontent.PcGtsType;
import eu.transkribus.core.model.beans.pagecontent.TextRegionType;
import eu.transkribus.core.model.beans.pagecontent_trp.TrpTextLineType;
import eu.transkribus.core.model.beans.pagecontent_trp.TrpTextRegionType;
import eu.transkribus.core.model.beans.pagecontent_trp.TrpWordType;
import eu.transkribus.core.util.PageXmlUtils;

/**
 * Filter that removes line-regions that contain specific text tags.
 * <br>
 * Use case: Users want to have the HTR training ignore lines with unclear or gap tags. 
 */
public class TagTrainContentFilterOut implements IPageContentFilter {
	Logger logger = LoggerFactory.getLogger(TagTrainContentFilterOut.class);
	private int removedLinesCount;
	
	/**
	 * tagsToTrain: list that contains the tags which should be filtered
	 * tagMap: mapping of tag name and used symbol for this tag
	 * propMap: contains the stored properties (as cssString) and the replacement symbols (Pair of two symbols, contained in 'propertyLabels')
	 * propertyLabels: the special character map for mapping tag names and special characters
	 */
	public boolean replaceProps = false;
	public List<String> tagsToTrain = new ArrayList<>();
	private Map<String, Object> tagMap = new HashMap<String, Object>();	
	private static Map<String, Object> propMap = new HashMap<String, Object>();
	
	private static int index = 0;
	private static int countTags = 0;
	
	/*
	 * 
	 * 
	 * LocalRecognitionJobManager: line 82: store props into DB and HTR model
	 * ACITlabHtrTrainingJob: trpTrainer.storeTrainAndValInput(); da werden die Filter gesetzt
	 * htr = loadDataFromFileSystem(htr); und hier werden die params zum Modell gespeichert für die Erkennung
	 */
	
	
	
	public TagTrainContentFilterOut(boolean useProps, Map<String, Object> tagMap2) {
		super();
		this.replaceProps = useProps;
		this.tagMap = tagMap2;
		this.propMap = propMap;

	}

	@Override
	public void doFilter(PcGtsType pc) {

		List<TextRegionType> regions = PageXmlUtils.getTextRegions(pc);
		for(TextRegionType r : regions) {
			Iterator<TrpTextLineType> lineIt = ((TrpTextRegionType) r).getTrpTextLine().iterator();
			while(lineIt.hasNext()) {
				TrpTextLineType l = lineIt.next();
				//What's the difference between getTagList() and getCustomTagList()?
				
				/*
				 * for Pylaia we need to go to the words as well
				 */
				Iterator<TrpWordType> wordIt = l.getTrpWord().iterator(); 
				while(wordIt.hasNext()) {
					TrpWordType w = wordIt.next();
					PageXmlUtils.convertSpecialStringIntoCustomTag(w, tagMap, propMap);
				}
				
				PageXmlUtils.convertSpecialStringIntoCustomTag(l, tagMap, propMap);
				/*
				 * CssSyntaxTag: parseCssAttributes zum die Attribute wieder zurück zu wandeln
				 */
//				for (String tag : tagMap.keySet()){
//					String symbol = (String) tagMap.get(tag);
//					
//					logger.debug("used symbol: " + symbol);
//					//for testing
//					/*
//					 * search line for this symbol and re-create custom tag
//					 */
//					if (l.getUnicodeText().contains(symbol)) {
//						
//					}
//					
//				}
			}
		}
		
		for (String i : propMap.keySet()) {
			Object j = propMap.get(i);
			logger.debug("propMap size is: " + propMap.size());
			logger.debug("number of tags is: " + countTags);
			logger.debug("key is: " + i + " and its value is: " + j);
			
		}
 
	}
	
	public String getCharacter(String tagName) {
		// TODO Auto-generated method stub
		return (String) tagMap.get(tagName);
	}

	
	class SortByOffset implements Comparator<CustomTag> {
	    // Used for sorting in desscending order of
	    // offset number
	    public int compare(CustomTag a, CustomTag b)
	    {
	        return b.getOffset() - a.getOffset();
	    }
	}
	
}
