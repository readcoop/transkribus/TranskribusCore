package eu.transkribus.core.model.beans;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class TrpModelMetadata {
	
	private int modelId;
	private String type;
	private String name;
//	private String description;
	private String provider;
	private Date created;
	private String language;
	private Integer baseModelId;
	private String baseModelName;
	private String trainJobId;
	private Double finalCer;
	private Double finalValidationCer;
	/** list of symbols known by model - HTR specific */
	private List<String> symbols;
	/** Structure type counts - LA2 aka field/table specific */
	private StructureTrainValidationStats structures;
	private Boolean bestNetStored;
	private Boolean languageModelExists;
	private Integer nrOfLines;
	private Integer nrOfWords;
	private Map<String, String> parameters;
	private String userName;
	private Integer userId;
	private Integer nrOfTrainGtPages;
	private Integer nrOfValidationGtPages;
	private String releaseLevel;
	private String publisher;
	private Boolean gtAccessible;
	private Date delTime;
	private Integer internalRating;
	private List<Double> cerLog;
	private List<Double> cerValidationLog;
	private String contactEmail;
	private String creator;
	private String exampleImageUrl;
	private Set<Integer> centuries;
	private Set<String> isoLanguages;
	private Set<String> scriptTypes;
	private String docType;
	private Map<String, String> descriptions;
	private Long nrOfTranscriptsProduced;
	private Boolean featured;
	private List<String> apps;
	
	public TrpModelMetadata() {}
	
	public TrpModelMetadata(TrpModelMetadata otherModel) {
		this();
		this.modelId = otherModel.modelId;
		this.type = otherModel.type;
		this.name = otherModel.name;
		this.provider = otherModel.provider;
		this.created = otherModel.created;
		this.language = otherModel.language;
		this.baseModelId = otherModel.baseModelId;
		this.trainJobId = otherModel.trainJobId;
		this.finalCer = otherModel.finalCer;
		this.finalValidationCer = otherModel.finalValidationCer;
		this.symbols = otherModel.symbols;
		this.structures = otherModel.structures;
		this.bestNetStored = otherModel.bestNetStored;
		this.languageModelExists = otherModel.languageModelExists;
		this.nrOfLines = otherModel.nrOfLines;
		this.nrOfWords = otherModel.nrOfWords;
		this.parameters = otherModel.parameters;
		this.userName = otherModel.userName;
		this.userId = otherModel.userId;
		this.nrOfTrainGtPages = otherModel.nrOfTrainGtPages;
		this.nrOfValidationGtPages = otherModel.nrOfValidationGtPages;
		this.releaseLevel = otherModel.releaseLevel;
		this.publisher = otherModel.publisher;
		this.gtAccessible = otherModel.gtAccessible;
		this.delTime = otherModel.delTime;
		this.internalRating = otherModel.internalRating;
		this.cerLog = otherModel.cerLog;
		this.cerValidationLog = otherModel.cerValidationLog;
		this.contactEmail = otherModel.contactEmail;
		this.creator = otherModel.creator;
		this.exampleImageUrl = otherModel.exampleImageUrl;
		this.centuries = otherModel.centuries;
		this.isoLanguages = otherModel.isoLanguages;
		this.scriptTypes = otherModel.scriptTypes;
		this.docType = otherModel.docType;
		this.descriptions = otherModel.descriptions;
		this.nrOfTranscriptsProduced = otherModel.nrOfTranscriptsProduced;
		this.featured = otherModel.featured;
		this.apps = otherModel.apps;
	}

	public int getModelId() {
		return modelId;
	}

	public void setModelId(int modelId) {
		this.modelId = modelId;
	}
	
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getProvider() {
		return provider;
	}

	public void setProvider(String provider) {
		this.provider = provider;
	}
	
	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public Integer getBaseModelId() {
		return baseModelId;
	}

	public void setBaseModelId(Integer baseHtrId) {
		this.baseModelId = baseHtrId;
	}

	public String getBaseModelName() {
		return baseModelName;
	}

	public void setBaseModelName(String baseModelName) {
		this.baseModelName = baseModelName;
	}
	
	public String getTrainJobId() {
		return trainJobId;
	}

	public void setTrainJobId(String trainJobId) {
		this.trainJobId = trainJobId;
	}
	
	public Double getFinalCer() {
		return finalCer;
	}

	public void setFinalCer(Double finalCer) {
		this.finalCer = finalCer;
	}

	public Double getFinalValidationCer() {
		return finalValidationCer;
	}

	public void setFinalValidationCer(Double finalValidationCer) {
		this.finalValidationCer = finalValidationCer;
	}

	public List<String> getSymbols() {
		return symbols;
	}

	public void setSymbols(List<String> symbols) {
		this.symbols = symbols;
	}

	public StructureTrainValidationStats getStructures() {
		return structures;
	}

	public void setStructures(StructureTrainValidationStats structures) {
		this.structures = structures;
	}

	public Boolean isBestNetStored() {
		return bestNetStored;
	}

	public void setBestNetStored(Boolean bestNetStored) {
		this.bestNetStored = bestNetStored;
	}

	public Boolean isLanguageModelExists() {
		return languageModelExists;
	}

	public void setLanguageModelExists(Boolean dictionaryExists) {
		this.languageModelExists = dictionaryExists;
	}

	public Map<String, String> getParameters() {
		return parameters;
	}
	
	public void setParameters(Map<String, String> parameters) {
		this.parameters = parameters;
	}

	public Integer getNrOfLines() {
		return nrOfLines;
	}

	public void setNrOfLines(Integer nrOfLines) {
		this.nrOfLines = nrOfLines;
	}

	public Integer getNrOfWords() {
		return nrOfWords;
	}

	public void setNrOfWords(Integer nrOfWords) {
		this.nrOfWords = nrOfWords;
	}
	
	public List<Double> getCerLog() {
		return cerLog;
	}
	
	public void setCerLog(List<Double> cerLog) {
		this.cerLog = cerLog;
	}
	
	public List<Double> getCerValidationLog() {
		return cerValidationLog;
	}
	
	public void setCerValidationLog(List<Double> cerValidationLog) {
		this.cerValidationLog = cerValidationLog;
	}
	
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	
	public String getReleaseLevel() {
		return releaseLevel;
	}

	public void setReleaseLevel(String releaseLevel) {
		this.releaseLevel = releaseLevel;
	}

	public String getPublisher() {
		return publisher;
	}
	
	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}
	
	public Boolean isGtAccessible() {
		return gtAccessible;
	}

	public void setGtAccessible(Boolean gtAccessible) {
		this.gtAccessible = gtAccessible;
	}
	
	public Boolean isFeatured() {
		return featured;
	}

	public void setFeatured(Boolean featured) {
		this.featured = featured;
	}

	public Integer getNrOfTrainGtPages() {
		return nrOfTrainGtPages;
	}
	
	public void setNrOfTrainGtPages(Integer nrOfTrainGtPages) {
		this.nrOfTrainGtPages = nrOfTrainGtPages;
	}
	
	public Integer getNrOfValidationGtPages() {
		return nrOfValidationGtPages;
	}
	
	public void setNrOfValidationGtPages(Integer nrOfValidationGtPages) {
		this.nrOfValidationGtPages = nrOfValidationGtPages;
	}

	public Date getDelTime() {
		return delTime;
	}
	
	public void setDelTime(Date delTime) {
		this.delTime = delTime;
	}
	
	public String getDocType() {
		return docType;
	}
	
	public void setDocType(String docType) {
		this.docType = docType;
	}
		
	public Set<String> getScriptTypes() {
		return scriptTypes;
	}

	public void setScriptTypes(Set<String> scriptTypes) {
		this.scriptTypes = scriptTypes;
	}

	public Integer getInternalRating() {
		return internalRating;
	}

	public void setInternalRating(Integer internalRating) {
		this.internalRating = internalRating;
	}
	
	public String getContactEmail() {
		return contactEmail;
	}

	public void setContactEmail(String contactEmail) {
		this.contactEmail = contactEmail;
	}

	public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

	public String getExampleImageUrl() {
		return exampleImageUrl;
	}

	public void setExampleImageUrl(String exampleImageUrl) {
		this.exampleImageUrl = exampleImageUrl;
	}

	public Set<Integer> getCenturies() {
		return centuries;
	}

	public void setCenturies(Set<Integer> centuries) {
		this.centuries = centuries;
	}

	public Set<String> getIsoLanguages() {
		return isoLanguages;
	}

	public void setIsoLanguages(Set<String> isoLanguages) {
		this.isoLanguages = isoLanguages;
	}
	
	public Map<String, String> getDescriptions() {
		return descriptions;
	}
	
	public void setDescriptions(Map<String, String> descriptions) {
		this.descriptions = descriptions;
	}

	public Long getNrOfTranscriptsProduced() {
		return nrOfTranscriptsProduced;
	}

	public void setNrOfTranscriptsProduced(Long nrOfTranscriptsProduced) {
		this.nrOfTranscriptsProduced = nrOfTranscriptsProduced;
	}
	
	public List<String> getApps() {
		return apps;
	}
	
	public void setApps(List<String> apps) {
		this.apps = apps;
	}

	@Override
	public String toString() {
		return "TrpModelMetadata [modelId=" + modelId + ", type=" + type + ", name=" + name + ", provider=" + provider
				+ ", created=" + created + ", language=" + language + ", baseModelId=" + baseModelId + ", trainJobId="
				+ trainJobId + ", finalCer=" + finalCer + ", finalValidationCer=" + finalValidationCer + ", symbols="
				+ symbols + ", structures=" + structures + ", bestNetStored=" + bestNetStored + ", languageModelExists=" + languageModelExists
				+ ", nrOfLines=" + nrOfLines + ", nrOfWords=" + nrOfWords + ", parameters=" + parameters + ", userName="
				+ userName + ", userId=" + userId + ", nrOfTrainGtPages=" + nrOfTrainGtPages
				+ ", nrOfValidationGtPages=" + nrOfValidationGtPages + ", releaseLevel=" + releaseLevel + ", publisher="
				+ publisher + ", gtAccessible=" + gtAccessible + ", delTime=" + delTime + ", internalRating="
				+ internalRating + ", cerLog=" + cerLog + ", cerValidationLog=" + cerValidationLog + ", contactEmail="
				+ contactEmail + ", creator=" + creator + ", exampleImageUrl=" + exampleImageUrl + ", centuries="
				+ centuries + ", isoLanguages=" + isoLanguages + ", scriptTypes=" + scriptTypes + ", docType=" + docType
				+ ", descriptions=" + descriptions + ", nrOfTranscriptsProduced=" + nrOfTranscriptsProduced
				+ ", featured=" + featured + ", apps=" + apps + "]";
	}
	
	
}
