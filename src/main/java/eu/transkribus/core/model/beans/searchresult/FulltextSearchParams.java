package eu.transkribus.core.model.beans.searchresult;

import java.util.Map;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class FulltextSearchParams {


    private int start = 0;
    private int rows = 10;
    private boolean highlights = true;
    private boolean uppercase = false;
    private Map<String,String> filters;

    public void setFilters(Map<String,String> f){
        this.filters = f;
    }

    public Map<String,String> getFilters(){
        return filters;
    }

    public void setUppercase(boolean b){
        this.uppercase = b;
    }

    public boolean getUppercase(){
        return uppercase;
    }

    public void setHighlights(boolean b){
        this.highlights = b;
    }

    public boolean getHighlights(){
        return highlights;
    }

    public void setStart(int s){
        this.start = s;
    }

    public int getStart(){
        return start;
    }

    public void setRows(int r){
        this.rows = r;
    }

    public int getRows(){
        return rows;
    }

    
}
