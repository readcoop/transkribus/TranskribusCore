package eu.transkribus.core.model.beans;

import io.swagger.v3.oas.annotations.media.Schema;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class LA2InputTypeList {
    @Schema(description = "input_def_json", required = true)
    private List<LA2InputType> input_def_json;

    public List<LA2InputType> getInputDefJson() {
        return input_def_json;
    }

    public void setInputDefJson(List<LA2InputType> input_def_json) {
        this.input_def_json = input_def_json;
    }
}
