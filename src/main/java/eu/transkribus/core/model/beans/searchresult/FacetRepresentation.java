package eu.transkribus.core.model.beans.searchresult;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class FacetRepresentation {
	private String facetField;
	private List<FacetValueRepresentation> values;	
	
	public FacetRepresentation(){}

	public List<FacetValueRepresentation> getValues() {
		return values;
	}
	public void setValues(List<FacetValueRepresentation> values) {
		this.values = values;
	}
	public String getName() {
		return facetField;
	}
	public void setName(String name) {
		this.facetField = name;
	}
	
	public static class FacetValueRepresentation {
		private String value;
		private Long count;
		public FacetValueRepresentation() {}
		public FacetValueRepresentation(String term, Long count) {
			this.value = term;
			this.count = count;
		}
		public String getValue() {
			return value;
		}
		public void setValue(String value) {
			this.value = value;
		}
		public Long getCount() {
			return count;
		}
		public void setCount(Long count) {
			this.count = count;
		}
	}
}