package eu.transkribus.core.model.beans;

import java.awt.Dimension;
import java.io.Serializable;
import java.net.URL;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.JAXBException;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.transkribus.core.exceptions.NullValueException;
import eu.transkribus.core.model.beans.adapters.SqlTimestampAdapter;
import eu.transkribus.core.model.beans.enums.EditStatus;
import eu.transkribus.core.model.beans.pagecontent.PcGtsType;
import eu.transkribus.core.util.CoreUtils;

@Entity
@Table(name = "pages")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class TrpPage extends ATranscriptStatistics implements ITrpFile, Serializable, Comparable<TrpPage> {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LoggerFactory.getLogger(TrpPage.class);
	//objid for parentDoc is ID in DB
	@Id
	@Column
	private int pageId = -1;
	@Column
	private int docId;
	@Column
	private int pageNr;
	@Column(name = "imagekey")
	@Transient
	private String key = null; //The fimagestore key for getting the image file
	//imageUrl represents a link to the local filesystem or the link to the fimagestore
	@Column(name="IMAGE_ID")
	private int imageId;
	@Column
	@Transient
	private URL url;
	@Column
	@Transient
	private URL thumbUrl;

	@Column(name = "CHECKSUM")
	@Transient
	private String md5Sum;

	@Column
	@Transient
	private Long fileSize;
	
	@Column(name="IMGFILENAME")
	private String imgFileName = "";
	
	/**
	 * This field is used to store information on problems with the given image for this page
	 * LocalDocReader: message on corrupt image (dimension can not be read)
	 * GoobiMetsImporter: message upon brokenUrl, no or corrupt image
	 */
	@Column(name="IMG_PROBLEM")
	private String imgFileProblem = null;
	
	@Column(name="INDEX_PROBLEM")
	private String indexProblem = "";

	@Column(name="HIDE_ON_SITES")
	private boolean hideOnSites = false;


	//TODO SortedList from DB. comparator?
	@XmlElementWrapper(name="tsList")
	@XmlElement
	private List<TrpTranscriptMetadata> transcripts = new LinkedList<>();
	
	@Column // add to result set
	@Transient // from a join or computation
	private int width;
	@Column
	@Transient
	private int height;
	
	@XmlJavaTypeAdapter(SqlTimestampAdapter.class)
	@Column
	@Transient
	private Timestamp created;
	
	@Column(name="IS_INDEXED")
	private boolean indexed = false;
	
	@XmlJavaTypeAdapter(SqlTimestampAdapter.class)
	@Column(name="TAGS_STORED")
	private Timestamp tagsStored;
	
	@XmlJavaTypeAdapter(SqlTimestampAdapter.class)
	@Column(name="LAST_MODIFIED")
	private Timestamp lastModified;
	
	@XmlJavaTypeAdapter(SqlTimestampAdapter.class)
	@Column(name="LAST_INDEXED")
	private Timestamp lastIndexed;



	// new transient fields from current transcript and model used to generate it:
	@Column(name="CT_CLIENT_ID")
	@Transient
	private Integer ctClientId;

	@Column(name="CT_MODEL_ID")
	@Transient
	private Integer ctModelId;

	@Column(name="CT_JOBID")
	@Transient
	private Integer ctJobId;

	@Column(name="RECOGNITION_TYPE")
	@Transient
	private String ctRecognitionType;

	@Column(name="CLIENT_NAME")
	@Transient
	private String ctClientName;

	@Column(name="MODEL_NAME")
	@Transient
	private String ctModelName;

	@Column(name="MODEL_TYPE")
	@Transient
	private String ctModelType;

	@Column(name="CT_STATUS")
	@Transient
	private String ctStatus;

	@Column(name="CT_TOOLNAME")
	@Transient
	private String ctToolname;


	@Column(name="CT_TIMESTAMP")
	@Transient
	private Long ctTimestamp;


	@Column(name="CT_USER_ID")
	@Transient
	private Integer ctUserId;

	@Column(name="CT_USER_NAME")
	@Transient
	private String ctUserName;





	/**
	 * TrpTRanscriptStatistics
	 */
	@Column(name="NR_OF_REGIONS")
	@Transient
	protected Integer nrOfRegions;

	@Column(name="NR_OF_TRANSCRIBED_REGIONS")
	@Transient
	protected Integer nrOfTranscribedRegions;

	@Column(name="NR_OF_WORDS_IN_REGIONS")
	@Transient
	protected Integer nrOfWordsInRegions;

	@Column(name="NR_OF_LINES")
	@Transient
	protected Integer nrOfLines;

	@Column(name="NR_OF_TRANSCRIBED_LINES")
	@Transient
	protected Integer nrOfTranscribedLines;

	@Column(name="NR_OF_WORDS_IN_LINES")
	@Transient
	protected Integer nrOfWordsInLines;

	@Column(name="NR_OF_WORDS")
	@Transient
	protected Integer nrOfWords;

	@Column(name="NR_OF_TRANSCRIBED_WORDS")
	@Transient
	protected Integer nrOfTranscribedWords;

	@Column(name="NR_OF_CHARS_IN_LINES")
	@Transient
	protected Integer nrOfCharsInLines;

	@Column(name="NR_OF_TABLES")
	@Transient
	protected Integer nrOfTables;

	private List<TrpLabel> labels;




	public TrpPage() {}
	
	public TrpPage(TrpPage p) {
		this();
		pageId = p.getPageId();
		docId = p.getDocId();
		pageNr = p.getPageNr();
		key = p.getKey();
		imageId = p.getImageId();
		url = p.getUrl();
		thumbUrl = p.getThumbUrl();
		md5Sum = p.getMd5Sum();
		fileSize = p.getFileSize();
		imgFileName = p.getImgFileName();
		imgFileProblem = p.getImgFileProblem();
		width = p.getWidth();
		height = p.getHeight();
		created = p.getCreated();
		indexed = p.isIndexed();
		tagsStored = p.getTagsStored();
		lastModified = p.getLastModified();
		lastIndexed = p.getLastIndexed();
		indexProblem = p.getIndexProblem();
		hideOnSites = p.isHideOnSites();

		// new ct fields
		ctStatus = p.getCtStatus();
		nrOfTables = p.getNrOfTables();
		nrOfWords = p.getNrOfWords();
		nrOfRegions = p.getNrOfRegions();
		nrOfCharsInLines = p.getNrOfCharsInLines();
		nrOfLines = p.getNrOfLines();
		nrOfTranscribedLines = p.getNrOfTranscribedLines();
		nrOfTranscribedRegions = p.getNrOfTranscribedRegions();
		nrOfTranscribedWords = p.getNrOfTranscribedWords();
		nrOfWordsInLines = p.getNrOfWordsInLines();
		ctToolname = p.getCtToolname();
		ctTimestamp = p.getCtTimestamp();
		ctClientId = p.getCtClientId();
		ctModelId = p.getCtModelId();
		ctJobId = p.getCtJobId();
		ctRecognitionType = p.getCtRecognitionType();
		ctClientName = p.getCtClientName();
		ctModelName = p.getCtModelName();
		ctModelType = p.getModelType();




		for(TrpTranscriptMetadata m : p.getTranscripts()) {
			transcripts.add(new TrpTranscriptMetadata(m, this));
		}
	}



	public int getPageId() {
		return this.pageId;
	}

	public void setPageId(int pageId) {
		this.pageId = pageId;
	}
	
	public int getDocId() {
		return this.docId;
	}

	public void setDocId(int docId) {
		this.docId = docId;
	}

	public int getPageNr() {
		return pageNr;
	}

	public void setPageNr(int pageNr) {
		this.pageNr = pageNr;
	}

	public int getImageId() {
		return imageId;
	}

	public void setImageId(int imageId) {
		this.imageId = imageId;
	}

	public String getKey() {
		return key;
	}

	/**
	 * The key for retrieving the image file from the file store.
	 * This is a leftover because now the key is stored in {@link TrpImage#getKey()}.
	 * The property will not be persisted to the database.
	 *
	 * @param imageKey key in fimagestore
	 * @deprecated use {@link TrpImage#setKey(String)} ()}
	 */
	public void setKey(String imageKey) {
		this.key = imageKey;
	}
	
	public int getNTranscripts() {
		return transcripts!=null ? transcripts.size() : 0;
	}

	public List<TrpTranscriptMetadata> getTranscripts() {
		return transcripts;
	}
	
	public void setTranscripts(List<TrpTranscriptMetadata> transcripts) {
		this.transcripts = transcripts;
	}
	
	// helper method to add a transcript to the current list of transcripts
	public void addTranscript(TrpTranscriptMetadata transcript) {
		this.transcripts.add(transcript);
	}

	public URL getUrl() {
		return this.url;
	}

	public void setUrl(URL url) {
		this.url = url;
	}

	public URL getThumbUrl() {
		return thumbUrl;
	}

	public void setThumbUrl(URL thumbUrl) {
		this.thumbUrl = thumbUrl;
	}

	/**
	 * Checksum is transient.
	 * @return checksum
	 */
	public String getMd5Sum() {
		return this.md5Sum;
	}

	public void setMd5Sum(String md5Sum) {
		this.md5Sum = md5Sum;
	}
	
	public String getImgFileName() {
		return imgFileName;
	}

	public void setImgFileName(String imgFileName) {
		this.imgFileName = imgFileName;
	}

	public String getImgFileProblem() {
		return imgFileProblem;
	}

	public void setImgFileProblem(String imgFileProblem) {
		this.imgFileProblem = imgFileProblem;
	}
	
	public String getIndexProblem() {
		return indexProblem;
	}

	public void setIndexProblem(String indexProblem) {
		this.indexProblem = indexProblem;
	}

	public boolean isHideOnSites() {
		return hideOnSites;
	}

	public void setHideOnSites(boolean hideOnSites) {
		this.hideOnSites = hideOnSites;
	}


	public Dimension getImgDimension() {
		return new Dimension(width, height);
	}

	/**
	 * Due to corrupt or missing input image during import
	 * @return true if dummy image is linked to this page
	 */
	public boolean isImgMissing() {
		return !StringUtils.isEmpty(imgFileProblem);
	}
	
	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public Timestamp getCreated() {
		return created;
	}

	public void setCreated(Timestamp created) {
		this.created = created;
	}

	public Timestamp getTagsStored() {
		return tagsStored;
	}

	public void setTagsStored(Timestamp tagsStored) {
		this.tagsStored = tagsStored;
	}

	public Timestamp getLastModified() {
		return lastModified;
	}

	public void setLastModified(Timestamp lastModified) {
		this.lastModified = lastModified;
	}
	
	public Timestamp setLastModifiedNow() {
		this.lastModified = Timestamp.from(Instant.now());
		return this.lastModified;
	}	

	public Timestamp getLastIndexed() {
		return lastIndexed;
	}

	public void setLastIndexed(Timestamp lastIndexed) {
		this.lastIndexed = lastIndexed;
	}
	
	public Timestamp setLastIndexedNow() {
		this.lastIndexed = Timestamp.from(Instant.now());
		return this.lastIndexed;
	}

	public Integer getCtUserId() {
		return ctUserId;
	}

	public void setCtUserId(Integer ctUserId) {
		this.ctUserId = ctUserId;
	}

	public String getCtUserName() {
		return ctUserName;
	}

	public void setCtUserName(String ctUserName) {
		this.ctUserName = ctUserName;
	}

	public TrpTranscriptMetadata getCurrentTranscript() {
		List<TrpTranscriptMetadata> tList = getTranscripts();
		if(tList.isEmpty()) {
			//might happen if image file is broken and no PAGE XML can be generated
			return new TrpTranscriptMetadata();
		}
		Collections.sort(tList, Collections.reverseOrder());
		return tList.get(0);
	}
	
	/*
	 * position means: transcripts are sorted with respect to creation time, i=0 means the current, i=1 means the second youngest, and so on
	 */
	public TrpTranscriptMetadata getTranscriptInCreationHistoryAtPosition(int i) {
		List<TrpTranscriptMetadata> tList = getTranscripts();
		if(tList.isEmpty()) {
			//might happen if image file is broken and no PAGE XML can be generated
			return new TrpTranscriptMetadata();
		}
		Collections.sort(tList, Collections.reverseOrder());
		return tList.get(i);
	}
	
	/**
	 * Return the most recent transcript with status. If status is not found return current.
	 * 
	 * @param status
	 * @return
	 */
	public TrpTranscriptMetadata getTranscriptWithStatus(EditStatus status) {
		return getTranscriptWithStatus(status == null ? null : status.getStr());
	}
	
	public TrpTranscriptMetadata getMostRecentTranscriptWithAToolname() {
		List<TrpTranscriptMetadata> tList = getTranscripts();
		
		Collections.sort(tList, Collections.reverseOrder());
		for (TrpTranscriptMetadata md : tList){
			if (md.getToolName() != null && !md.getToolName().equals("")){
				logger.debug("Transcript has a toolname set");
				return md;
			}
				
		}
		
		//if no transcript with this status was found return null
		return null;
	}
	
	/**
	 * Return the most recent transcript with status. Status is also used to get transcripts with this as 'toolname'. If status is not found return null.
	 */
	public TrpTranscriptMetadata getTranscriptWichContainsStringInToolnameOrNull(String part_of_toolname) {
		List<TrpTranscriptMetadata> tList = getTranscripts();
		if(part_of_toolname == null) {
			return null;
		}
		
		Collections.sort(tList, Collections.reverseOrder());
		for (TrpTranscriptMetadata md : tList){
			logger.trace("Checking tsId {} status '{}': tmd has status '{}', toolname '{}'", md.getTsId(), part_of_toolname, md.getStatus().getStr(), md.getToolName());
			if (md.getToolName() != null && md.getToolName().contains(part_of_toolname)){
				logger.trace("Transcripts fits status. return it.");
				return md;
			}
				
		}
		
		//if no transcript with this status was found return null
		return null;
	}
	
	/**
	 * Return the most recent transcript with status. If status is not found return current.
	 * 
	 * @param status
	 * @return
	 */
	public TrpTranscriptMetadata getTranscriptWithStatus(String status) {
		TrpTranscriptMetadata tmd = getTranscriptWithStatusOrNull(status);

		//if no transcript with this status was found return the latest one
		return tmd == null ? getTranscripts().get(0) : tmd;
	}
	/**
	 * Return the most recent transcript with status. If status is not found return null.
	 * 
	 * @param status
	 * @return
	 */
	public TrpTranscriptMetadata getTranscriptWithStatusOrNull(EditStatus status) {
		return getTranscriptWithStatusOrNull(status == null ? null : status.getStr());
	}
	
	/**
	 * Return the most recent transcript with status. Status is also used to get transcripts with this as 'toolname'. If status is not found return null.
	 * 
	 * @param status
	 * @return
	 */
	public TrpTranscriptMetadata getTranscriptWithStatusOrNull(String status) {
		List<TrpTranscriptMetadata> tList = getTranscripts();
		if(status == null) {
			return null;
		}
		
		Collections.sort(tList, Collections.reverseOrder());
		for (TrpTranscriptMetadata md : tList){
			logger.trace("Checking tsId {} status '{}': tmd has status '{}', toolname '{}'", md.getTsId(), status, md.getStatus().getStr(), md.getToolName());
			if (md.getStatus().getStr().equals(status) || (md.getToolName() != null && md.getToolName().equals(status))){
				logger.trace("Transcripts fits status. return it.");
				return md;
			}
				
		}
		
		//if no transcript with this status was found return null
		return null;
	}
	
	public PcGtsType unmarshallCurrentTranscript() throws NullValueException, JAXBException {
		if (getCurrentTranscript()==null)
			throw new NullValueException("Current transcript is null!");
		
		return getCurrentTranscript().unmarshallTranscript();	
	}

	public String getTranscriptsStr() {
		String str="";
		for (TrpTranscriptMetadata md : this.getTranscripts()) {
			 str += md.toString()+"\n";
		}
		
		return str;
	}

	public TrpImage getImage() {
		TrpImage i = new TrpImage();
		i.setImageId(imageId);
		i.setImgFileName(imgFileName);
		i.setMd5Sum(md5Sum);
		i.setFileSize(fileSize);
		i.setKey(key);
		i.setCreated(created);
		i.setWidth(width);
		i.setHeight(height);
		i.setUrl(url);
		i.setThumbUrl(thumbUrl);
		return i;
	}
	
	public void setImage(TrpImage i) {
		this.imageId = i.getImageId();
		this.imgFileName = i.getImgFileName();
		this.md5Sum = i.getMd5Sum();
		this.fileSize = i.getFileSize();
		this.key = i.getKey();
		this.created = i.getCreated();
		this.width = i.getWidth();
		this.height = i.getHeight();
		this.url = i.getUrl();
		this.thumbUrl = i.getThumbUrl();
	}

	public boolean isIndexed() {
		return indexed;
	}

	public void setIndexed(boolean isIndexed) {
		this.indexed = isIndexed;
		if (this.indexed) { // if is_indexed flag is true --> page was indexed --> set last-indexed date to now
			setLastIndexedNow();
		}
		else { // if is_indexed flag is false --> page was changed and has to be indexed --> set last-modified date to now
			setLastModifiedNow();	
		}
	}

	/**
	 * Uses the page number for comparison
	 * 
	 * @see Comparable#compareTo(Object)
	 */
	@Override
	public int compareTo(TrpPage p) {
		if (this.getPageNr() > p.getPageNr()) {
			return 1;
		}
		if (this.getPageNr() < p.getPageNr()) {
			return -1;
		}
		return 0;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TrpPage other = (TrpPage) obj;
		if (created == null) {
			if (other.created != null)
				return false;
		} else if (!created.equals(other.created))
			return false;
		if (docId != other.docId)
			return false;
		if (height != other.height)
			return false;
		if (imageId != other.imageId)
			return false;
		if (imgFileName == null) {
			if (other.imgFileName != null)
				return false;
		} else if (!imgFileName.equals(other.imgFileName))
			return false;
		if (indexed != other.indexed)
			return false;
		if (hideOnSites != other.hideOnSites)
			return false;
		if (key == null) {
			if (other.key != null)
				return false;
		} else if (!key.equals(other.key))
			return false;
		if (md5Sum == null) {
			if (other.md5Sum != null)
				return false;
		} else if (!md5Sum.equals(other.md5Sum))
			return false;
		if (pageId != other.pageId)
			return false;
		if (pageNr != other.pageNr)
			return false;
		if (tagsStored == null) {
			if (other.tagsStored != null)
				return false;
		} else if (!tagsStored.equals(other.tagsStored))
			return false;
		if (!CoreUtils.equalsObjects(lastModified, other.lastModified)) {
			return false;
		}		
		if (!CoreUtils.equalsObjects(lastIndexed, other.lastIndexed)) {
			return false;
		}
		if (thumbUrl == null) {
			if (other.thumbUrl != null)
				return false;
		} else if (!thumbUrl.equals(other.thumbUrl))
			return false;
		if (transcripts == null) {
			if (other.transcripts != null)
				return false;
		} else if (!transcripts.equals(other.transcripts))
			return false;
		if (url == null) {
			if (other.url != null)
				return false;
		} else if (!url.equals(other.url))
			return false;
		if (width != other.width)
			return false;
		if (!CoreUtils.equalsObjects(indexProblem, other.indexProblem)) {
			return false;
		}
		if (!CoreUtils.equalsObjects(ctStatus, other.ctStatus)) {
			return false;
		}
		if (!CoreUtils.equalsObjects(nrOfTables, other.nrOfTables)) {
			return false;
		}
		if (!CoreUtils.equalsObjects(nrOfWords, other.nrOfWords)) {
			return false;
		}
		if (!CoreUtils.equalsObjects(nrOfRegions, other.nrOfRegions)) {
			return false;
		}
		if (!CoreUtils.equalsObjects(nrOfCharsInLines, other.nrOfCharsInLines)) {
			return false;
		}
		if (!CoreUtils.equalsObjects(nrOfLines, other.nrOfLines)) {
			return false;
		}
		if (!CoreUtils.equalsObjects(nrOfTranscribedLines, other.nrOfTranscribedLines)) {
			return false;
		}
		if (!CoreUtils.equalsObjects(nrOfTranscribedRegions, other.nrOfTranscribedRegions)) {
			return false;
		}
		if (!CoreUtils.equalsObjects(nrOfTranscribedWords, other.nrOfTranscribedWords)) {
			return false;
		}
		if (!CoreUtils.equalsObjects(nrOfWordsInLines, other.nrOfWordsInLines)) {
			return false;
		}
		if (!CoreUtils.equalsObjects(ctToolname, other.ctToolname)) {
			return false;
		}
		if (!CoreUtils.equalsObjects(ctTimestamp, other.ctTimestamp)) {
			return false;
		}
		if (!CoreUtils.equalsObjects(ctClientId, other.ctClientId)) {
			return false;
		}
		if (!CoreUtils.equalsObjects(ctModelId, other.ctModelId)) {
			return false;
		}
		if (!CoreUtils.equalsObjects(ctJobId, other.ctJobId)) {
			return false;
		}
		if (!CoreUtils.equalsObjects(ctRecognitionType, other.ctRecognitionType)) {
			return false;
		}
		if (!CoreUtils.equalsObjects(ctClientName, other.ctClientName)) {
			return false;
		}
		if (!CoreUtils.equalsObjects(ctModelName, other.ctModelName)) {
			return false;
		}
		if (!CoreUtils.equalsObjects(ctModelType, other.ctModelType)) {
			return false;
		}



		return true;
	}
	

	/**
	 * This method is just for testing equivalence of documents selected via different DocManager methods
	 * Same as normal equals, but iterates transcripts and checks equivalence
	 * @param obj
	 * @return
	 */
	public boolean testEquals(TrpPage obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TrpPage other = (TrpPage) obj;
		if (created == null) {
			if (other.created != null)
				return false;
		} else if (!created.equals(other.created))
			return false;
		if (docId != other.docId)
			return false;
		if (height != other.height)
			return false;
		if (imageId != other.imageId)
			return false;
		if (imgFileName == null) {
			if (other.imgFileName != null)
				return false;
		} else if (!imgFileName.equals(other.imgFileName))
			return false;
		if (indexed != other.indexed)
			return false;
		if (hideOnSites != other.hideOnSites)
			return false;
		if (key == null) {
			if (other.key != null)
				return false;
		} else if (!key.equals(other.key))
			return false;
		if (md5Sum == null) {
			if (other.md5Sum != null)
				return false;
		} else if (!md5Sum.equals(other.md5Sum))
			return false;
		if (pageId != other.pageId)
			return false;
		if (pageNr != other.pageNr)
			return false;
		if (tagsStored == null) {
			if (other.tagsStored != null)
				return false;
		} else if (!tagsStored.equals(other.tagsStored))
			return false;
		if (!CoreUtils.equalsObjects(lastModified, other.lastModified)) {
			return false;
		}		
		if (!CoreUtils.equalsObjects(lastIndexed, other.lastIndexed)) {
			return false;
		}		
		if (thumbUrl == null) {
			if (other.thumbUrl != null)
				return false;
		} else if (!thumbUrl.equals(other.thumbUrl))
			return false;
		if (transcripts == null) {
			if (other.transcripts != null)
				return false;
		}
		if (url == null) {
			if (other.url != null)
				return false;
		} else if (!url.equals(other.url))
			return false;
		if (width != other.width)
			return false;
		if(transcripts != null) {
			if(other.transcripts == null) {
				return false;
			}
			if(transcripts.size() != other.transcripts.size()) {
				logger.info("Transcript list size is unequal on page nr. " + this.pageNr + ": " 
						+ transcripts.size() + " != " + other.transcripts.size());
				return false;
			}
			for(int i = 0; i < transcripts.size(); i++) {
				if(!transcripts.get(i).testEquals(other.transcripts.get(i))) {
					logger.info("Unequal transcript on page nr. " + this.pageNr
							+ "\n" + transcripts.get(i).toString() 
							+ "\n" + other.transcripts.get(i).toString());
					return false;
				}
			}
		}
		if (!CoreUtils.equalsObjects(indexProblem, other.indexProblem)) {
			return false;
		}
		if (!CoreUtils.equalsObjects(ctStatus, other.ctStatus)) {
			return false;
		}
		if (!CoreUtils.equalsObjects(nrOfTables, other.nrOfTables)) {
			return false;
		}
		if (!CoreUtils.equalsObjects(nrOfWords, other.nrOfWords)) {
			return false;
		}
		if (!CoreUtils.equalsObjects(nrOfRegions, other.nrOfRegions)) {
			return false;
		}
		if (!CoreUtils.equalsObjects(nrOfCharsInLines, other.nrOfCharsInLines)) {
			return false;
		}
		if (!CoreUtils.equalsObjects(nrOfLines, other.nrOfLines)) {
			return false;
		}
		if (!CoreUtils.equalsObjects(nrOfTranscribedLines, other.nrOfTranscribedLines)) {
			return false;
		}
		if (!CoreUtils.equalsObjects(nrOfTranscribedRegions, other.nrOfTranscribedRegions)) {
			return false;
		}
		if (!CoreUtils.equalsObjects(nrOfTranscribedWords, other.nrOfTranscribedWords)) {
			return false;
		}
		if (!CoreUtils.equalsObjects(nrOfWordsInLines, other.nrOfWordsInLines)) {
			return false;
		}
		if (!CoreUtils.equalsObjects(ctToolname, other.ctToolname)) {
			return false;
		}
		if (!CoreUtils.equalsObjects(ctTimestamp, other.ctTimestamp)) {
			return false;
		}
		if (!CoreUtils.equalsObjects(ctClientId, other.ctClientId)) {
			return false;
		}
		if (!CoreUtils.equalsObjects(ctModelId, other.ctModelId)) {
			return false;
		}
		if (!CoreUtils.equalsObjects(ctJobId, other.ctJobId)) {
			return false;
		}
		if (!CoreUtils.equalsObjects(ctRecognitionType, other.ctRecognitionType)) {
			return false;
		}
		if (!CoreUtils.equalsObjects(ctClientName, other.ctClientName)) {
			return false;
		}
		if (!CoreUtils.equalsObjects(ctModelName, other.ctModelName)) {
			return false;
		}
		if (!CoreUtils.equalsObjects(ctModelType, other.ctModelType)) {
			return false;
		}

		return true;
	}

	public String toString(boolean withTranscripts) {
		String labelsStr = labels!=null?getLabels().stream().map(TrpLabel::toString).reduce("", (a, b) -> a + b):"[]";

		String str = "TrpPage [pageId=" + pageId + ", docId=" + docId + ", pageNr=" + pageNr + ", key=" + key + ", imageId="
				+ imageId + ", url=" + url + ", thumbUrl=" + thumbUrl + ", md5Sum=" + md5Sum + ", imgFileName="
				+ imgFileName /*+ ", transcripts=" + transcripts*/ + ", width=" + width + ", height=" + height
				+ ", created=" + created + ", indexed=" + indexed + ", tagsStored=" + tagsStored
				+ ", lastModified=" + lastModified + ", lastIndexed=" + lastIndexed + ", hideOnSites=" + hideOnSites
				+ ", imgFileProblem=" + imgFileProblem + ", indexProblem=" + indexProblem + ", ctStatus=" + ctStatus
				+ ", nrOfTables=" + nrOfTables + ", nrOfWords=" + nrOfWords + ", nrOfRegions=" + nrOfRegions
				+ ", nrOfCharsInLines=" + nrOfCharsInLines + ", nrOfLines=" + nrOfLines + ", nrOfTranscribedLines="
				+ nrOfTranscribedLines + ", nrOfTranscribedRegions=" + nrOfTranscribedRegions + ", nrOfTranscribedWords="
				+ nrOfTranscribedWords + ", nrOfWordsInLines=" + nrOfWordsInLines + ", ctToolname=" + ctToolname
				+ ", ctTimestamp=" + ctTimestamp + ", ctClientId=" + ctClientId + ", ctModelId=" + ctModelId + ", ctJobId="
				+ ctJobId + ", ctRecognitionType=" + ctRecognitionType + ", ctClientName=" + ctClientName + ", ctModelName=" + ctModelName
				+ ", ctModelType=" + ctModelType + ", labels=" + labelsStr + "]";


		if (withTranscripts) {
			for (TrpTranscriptMetadata t : this.transcripts) {
				str += "\n\t" + t.toString();
			}
		}

		return str;
	}	

	@Override
	public String toString() {
		return toString(true);
	}

	public boolean hasImgError() {
		return !StringUtils.isEmpty(imgFileProblem);
	}

	/**
	 * returns the transcript with the given ID from the transcript list or null if not found
	 * 
	 * @param tsId
	 * @return
	 */
	public TrpTranscriptMetadata getTranscriptById(int tsId) {
		TrpTranscriptMetadata tmd = null;
		if(!getTranscripts().isEmpty()) {
			for(TrpTranscriptMetadata t : getTranscripts()) {
				if(t.getTsId() == tsId) {
					tmd = t;
					break;
				}
			}
		}
		return tmd;
	}
	
	/**
	 * returns the transcript with the given jobID from the transcript list or null if not found
	 */
	public TrpTranscriptMetadata getTranscriptByJobId(int jobId) {
		TrpTranscriptMetadata tmd = null;
		if(!getTranscripts().isEmpty()) {
			for(TrpTranscriptMetadata t : getTranscripts()) {
				if(t.getJobId() != null && t.getJobId() == jobId) {
					tmd = t;
					break;
				}
			}
		}
		return tmd;
	}

	/**
	 * The file size is transient.
	 * @return size in bytes
	 */
	public Long getFileSize() {
		return fileSize;
	}

	public void setFileSize(Long fileSize) {
		this.fileSize = fileSize;
	}





	// new ct fields Getters and Setters
	public String getCtStatus() {
		return ctStatus;
	}

	public void setCtStatus(String ctStatus) {
		this.ctStatus = ctStatus;
	}


	public String getCtToolname() {
		return ctToolname;
	}

	public void setCtToolname(String ctToolname) {
		this.ctToolname = ctToolname;
	}

	public Long getCtTimestamp() {
		return ctTimestamp;
	}

	public void setCtTimestamp(Long ctTimestamp) {
		this.ctTimestamp = ctTimestamp;
	}

	public Integer getCtClientId() {
		return ctClientId;
	}

	public void setCtClientId(Integer ctClientId) {
		this.ctClientId = ctClientId;
	}

	public Integer getCtModelId() {
		return ctModelId;
	}

	public void setCtModelId(Integer ctModelId) {
		this.ctModelId = ctModelId;
	}

	public Integer getCtJobId() {
		return ctJobId;
	}

	public void setCtJobId(Integer ctJobId) {
		this.ctJobId = ctJobId;
	}

	public String getCtRecognitionType() {
		return ctRecognitionType;
	}

	public void setCtRecognitionType(String ctRecognitionType) {
		this.ctRecognitionType = ctRecognitionType;
	}

	public String getCtClientName() {
		return ctClientName;
	}

	public void setCtClientName(String ctClientName) {
		this.ctClientName = ctClientName;
	}

	public String getCtModelName() {
		return ctModelName;
	}

	public void setModelName(String ctModelName) {
		this.ctModelName = ctModelName;
	}

	public String getModelType() {
		return ctModelType;
	}

	public void setModelType(String modelType) {
		this.ctModelType = modelType;
	}

	@Override
	public Integer getNrOfRegions() {
		return nrOfRegions;
	}

	@Override
	public void setNrOfRegions(Integer nrOfRegions) {
		this.nrOfRegions = nrOfRegions;
	}

	@Override
	public Integer getNrOfTranscribedRegions() {
		return nrOfTranscribedRegions;
	}

	@Override
	public void setNrOfTranscribedRegions(Integer nrOfTranscribedRegions) {
		this.nrOfTranscribedRegions = nrOfTranscribedRegions;
	}

	@Override
	public Integer getNrOfWordsInRegions() {
		return nrOfWordsInRegions;
	}

	@Override
	public void setNrOfWordsInRegions(Integer nrOfWordsInRegions) {
		this.nrOfWordsInRegions = nrOfWordsInRegions;
	}

	@Override
	public Integer getNrOfLines() {
		return nrOfLines;
	}

	@Override
	public void setNrOfLines(Integer nrOfLines) {
		this.nrOfLines = nrOfLines;
	}

	@Override
	public Integer getNrOfTranscribedLines() {
		return nrOfTranscribedLines;
	}

	@Override
	public void setNrOfTranscribedLines(Integer nrOfTranscribedLines) {
		this.nrOfTranscribedLines = nrOfTranscribedLines;
	}

	@Override
	public Integer getNrOfWordsInLines() {
		return nrOfWordsInLines;
	}

	@Override
	public void setNrOfWordsInLines(Integer nrOfWordsInLines) {
		this.nrOfWordsInLines = nrOfWordsInLines;
	}

	@Override
	public Integer getNrOfWords() {
		return nrOfWords;
	}

	@Override
	public void setNrOfWords(Integer nrOfWords) {
		this.nrOfWords = nrOfWords;
	}

	@Override
	public Integer getNrOfTranscribedWords() {
		return nrOfTranscribedWords;
	}

	@Override
	public void setNrOfTranscribedWords(Integer nrOfTranscribedWords) {
		this.nrOfTranscribedWords = nrOfTranscribedWords;
	}

	@Override
	public Integer getNrOfCharsInLines() {
		return nrOfCharsInLines;
	}

	@Override
	public void setNrOfCharsInLines(Integer nrOfCharsInLines) {
		this.nrOfCharsInLines = nrOfCharsInLines;
	}

	@Override
	public Integer getNrOfTables() {
		return nrOfTables;
	}

	@Override
	public void setNrOfTables(Integer nrOfTables) {
		this.nrOfTables = nrOfTables;
	}


	public List<TrpLabel> getLabels() {return labels; }

	public void setLabels(List<TrpLabel> labels) { this.labels = labels;}



}
