package eu.transkribus.core.model.beans.customtags;

import java.io.IOException;
import java.io.StringReader;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Observer;
import java.util.Set;
import java.util.concurrent.ConcurrentSkipListMap;
import java.util.regex.Matcher;
import java.util.stream.Collectors;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonReader;

import org.apache.commons.beanutils.BeanUtilsBean;
import org.apache.commons.beanutils.ConvertUtilsBean;
//import org.apache.commons.collections.map.CaseInsensitiveMap;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.transkribus.core.util.CoreUtils;
import eu.transkribus.core.util.MyObservable;
import eu.transkribus.core.util.RegexPattern;

/**
 * Factory class to create CustomTag objects.<br>
 * Contains a registry that stores a list of derived CustomTag types (e.g.
 * {@link TextStyleTag})
 */
public class CustomTagFactory {

	public static class EnumConvertUtilsBean extends ConvertUtilsBean {
		@Override
		public Object convert(String value, Class clazz) {
			if (clazz.isEnum()) {
				return Enum.valueOf(clazz, value);
			} else {
				return super.convert(value, clazz);
			}
		}
	};

	public static final String IS_EMPTY_TAG_PROPERTY_NAME = "_isEmptyTag";

	/**
	 * Contains the data for each CustomTag registry entry: a CustomTag prototype
	 * object, i.e. the CustomTag 'prototype' with all non-set properties, its class
	 * object, color, isEmptyTag, ...
	 */
	private static class CustomTagWrapper {
		public CustomTag customTag = null;
		public Class<? extends CustomTag> clazz = null;

		public String color = null;
		public boolean isEmptyTag = false;

		/*
		 * webUI specific properties not changed in expert client but remembered for
		 * writing back
		 */
		public String extra = null;
		public String label = null;
		public Integer icon = null;

		public CustomTagWrapper(CustomTag customTagToRegister, String color, boolean isEmptyTag) {
			if (customTagToRegister == null) {
				throw new IllegalArgumentException("customTag cannot be null!");
			}
			this.customTag = customTagToRegister.copy();
			this.customTag.setAttributes(customTagToRegister, true, false); // clear all attribute values
			this.clazz = customTagToRegister.getClass();
			this.color = color;
			this.isEmptyTag = isEmptyTag;
		}
		
		public CustomTagWrapper(CustomTag customTagToRegister, String color, boolean isEmptyTag, String label, String extra, Integer icon) {
			if (customTagToRegister == null) {
				throw new IllegalArgumentException("customTag cannot be null!");
			}
			this.customTag = customTagToRegister.copy();
			this.customTag.setAttributes(customTagToRegister, true, false); // clear all attribute values
			this.clazz = customTagToRegister.getClass();
			this.color = color;
			this.isEmptyTag = isEmptyTag;
			//these are from webUI and will be written back as they are
			this.label = label;
			this.extra = extra;
			this.icon = icon;
		}
	}

	private final static BeanUtilsBean beanUtilsBean = new BeanUtilsBean(new EnumConvertUtilsBean());

	private final static Logger logger = LoggerFactory.getLogger(CustomTagFactory.class);

//	private static final Map<CustomTag, Constructor<? extends CustomTag> > registry = new HashMap<>();
//	private static final Map<String, Constructor<? extends CustomTag> > registry = new HashMap<>();

	// case insensitve maps:
//	private static final Map<String, Class<? extends CustomTag> > registry = new CaseInsensitiveMap<>();
//	private static final ConcurrentMap<String, CustomTag > objectRegistry = new CaseInsensitiveMap<>();
//	private static final Map<String, String > colorRegistry = new CaseInsensitiveMap<>();

//	private static final Map<String, Class<? extends CustomTag> > registry = new ConcurrentSkipListMap<>(String.CASE_INSENSITIVE_ORDER);
	private static final Map<String, CustomTagWrapper> collTagsRegistry = new ConcurrentSkipListMap<>(
			String.CASE_INSENSITIVE_ORDER);
	private static final Map<String, CustomTagWrapper> objectRegistry = new ConcurrentSkipListMap<>(
			String.CASE_INSENSITIVE_ORDER);
//	private static final Map<String, String > colorRegistry = new ConcurrentSkipListMap<>(String.CASE_INSENSITIVE_ORDER);

	// case sensitive maps:
//	private static final Map<String, Class<? extends CustomTag> > registry = new HashMap<>();
//	private static final Map<String, CustomTag > objectRegistry = new HashMap<>();
//	private static final Map<String, String > colorRegistry = new HashMap<>();

	private static final MyObservable registryObserver = new MyObservable();

	public static class TagRegistryChangeEvent {
		public static final String ADDED_TAG = "ADDED_TAG";
		public static final String REMOVED_TAG = "REMOVED_TAG";
		public static final String ADDED_TAG_ATTRIBUTES = "ADDED_TAG_ATTRIBUTES";
		public static final String CHANGED_TAG_COLOR = "CHANGED_TAG_COLOR";

		public TagRegistryChangeEvent(String type, CustomTag tag) {
			super();
			this.type = type;
			this.tag = tag;
		}

		public String type;
		public CustomTag tag;

		public String toString() {
			final String TAB = ", ";
			String retValue = "TagRegistryChangeEvent ( " + super.toString();
			retValue += TAB + "type = " + this.type;
			retValue += TAB + "tag = " + this.tag;
			retValue += " )";
			return retValue;
		}
	}

//	private static final Map<String, Pair<Class, CustomTag> > registry = new HashMap<>();

	static {
		try {
			// add some custom tags to the registry:

			// non-indexed:
			CustomTagFactory.addToRegistry(new StructureTag(), null, true, true);
			CustomTagFactory.addToRegistry(new ReadingOrderTag(), null, true, true);
			CustomTagFactory.addToRegistry(new RegionTypeTag(), null, true, true);

			// indexed:
			CustomTagFactory.addToRegistry(new TextStyleTag(), null, false, true);
			CustomTagFactory.addToRegistry(new AbbrevTag(), null, false, true);
			CustomTagFactory.addToRegistry(new PersonTag(), null, false, true);
			CustomTagFactory.addToRegistry(new OrganizationTag(), null, false, true);
			CustomTagFactory.addToRegistry(new PlaceTag(), null, false, true);
			CustomTagFactory.addToRegistry(new SpeechTag(), null, false, true);
			CustomTagFactory.addToRegistry(new DateTag(), null, false, true);
			CustomTagFactory.addToRegistry(new WorkTag(), null, false, true);
			CustomTagFactory.addToRegistry(new SicTag(), null, false, true);
			CustomTagFactory.addToRegistry(new GapTag(), null, true, true);
			CustomTagFactory.addToRegistry(new DivTag(), null, false, true);
			CustomTagFactory.addToRegistry(new UnclearTag(), null, false, true);
			CustomTagFactory.addToRegistry(new BlackeningTag(), null, false, true);
			CustomTagFactory.addToRegistry(new SuppliedTag(), null, false, true);
			CustomTagFactory.addToRegistry(new AdditionTag(), null, false, true);

			CustomTagFactory.addToRegistry(new CommentTag(), null, false, true); // no color needed since extra
																					// rendering is done!
		} catch (NoSuchMethodException | SecurityException | IllegalAccessException | InvocationTargetException e) {
			logger.error(e.getMessage(), e);
		}
	}

//	public static void addPredifinedTagsToRegistry(String tagNamesProp) {
////		String tagNamesProp = TrpConfig.getTrpSettings().getTagNames();
//		logger.debug("tagNames = "+tagNamesProp);
//
//		Matcher wordMatcher = Pattern.compile("(\\w|_|-)+").matcher(tagNamesProp);
//		while (wordMatcher.find()) {
//			String tn = tagNamesProp.substring(wordMatcher.start(), wordMatcher.end());
//			logger.debug("adding tag: '"+tn+"'");
//			try {
//				addToRegistry(CustomTagFactory.create(tn));
//			} catch (Exception e1) {
//				logger.warn(e1.getMessage());
//			}
//		}
//	}

	public static void addObserver(Observer o) {
		registryObserver.addObserver(o);
	}

	public static void deleteObserver(Observer o) {
		registryObserver.deleteObserver(o);
	}

	/**
	 * Constructs a String for the config.properties file that stores non-predefined
	 * tags and additionals properties from predefined tags also stores if predfined
	 * color has changed
	 * 
	 * New: We store these values to the DB as user specific tag definitions
	 */
	public static String createTagDefPropertyForConfigFile() {
		String p = "";

		for (CustomTag t : getRegisteredCustomTags()) {
			if (!t.showInTagWidget())
				continue;

			Class<? extends CustomTag> clazz = t.getClass();

			boolean addToProp = true;
//			if (!clazz.equals(CustomTag.class)) { // predifined tag
			if (t.isPredefined()) {
//				logger.info("ss = "+t.getClass().getSimpleName()+" - "+t.getClass().isAssignableFrom(CustomTag.class));
				CustomTag tProto;
				try {
					tProto = clazz.newInstance();

					boolean hasSameColor = StringUtils.equalsIgnoreCase(tProto.getDefaultColor(),
							getTagColor(t.getTagName()));

					if (tProto.hasSameAttributes(t) && hasSameColor) {
						logger.debug(clazz.getSimpleName()
								+ " has not changed properties - not adding to properties string!");
						addToProp = false;
					} else {
						logger.info(clazz.getSimpleName() + " has changed properties - adding to properties string!");
					}
				} catch (InstantiationException | IllegalAccessException e) {
					addToProp = false;
					logger.error(e.getMessage(), e);
				}
			}

			if (addToProp) {
				p += t.getTagName() + "{";
				String color = getTagColor(t.getTagName());
				if (color != null)
					p += color + ",";
				boolean isEmptyTag = isEmptyTag(t.getTagName());
				if (isEmptyTag && !t.isPredefined()) {
					p += IS_EMPTY_TAG_PROPERTY_NAME + ",";
				}

				for (String pn : t.getAttributeNames()) {
					if (!CustomTag.isOffsetOrLengthOrContinuedProperty(pn) && !t.isPredefinedAttribute(pn))
						p += pn + ",";
				}

				p = StringUtils.removeEnd(p, ",");
				p += "} ";
			}
		}
		p = p.trim();

		return p;
	}

	public static void addCollectionDBTagsToRegistry(String tagNamesProp) {
		logger.info("adding local tags to registry, tagNamesProp = " + tagNamesProp);
		
		//clear registry
		collTagsRegistry.clear();

		JsonReader jsonReader = Json.createReader(new StringReader(tagNamesProp));
		JsonObject obj = jsonReader.readObject();
		jsonReader.close();
		
	   final javax.json.JsonArray tags = obj.getJsonObject("definitions").getJsonArray("annotations");
	   
	   logger.debug("collection DB tags " + tags);
	   final int n = tags.size();
	   for (int i = 0; i < n; ++i) {
	       final JsonObject tag = tags.getJsonObject(i);
    	   String tagName = null;
	       String tagLabel = null;
	       String color = null;
	       String extra = null;
	       Integer icon = null;
	       javax.json.JsonArray atts = null;
	       Map<String, Object> attributes = new HashMap<>();
       
	       if (tag.containsKey("name")) {
	    	   tagName = tag.getString("name");
	       }
	       if (tag.containsKey("label")) {
	    	   tagLabel = tag.getString("label");
	       }
	       if (tag.containsKey("color")) {
	    	   color = tag.getString("color");
	       }
	       if (tag.containsKey("extra")) {
	    	   try {
	    		   extra = tag.getJsonArray("extra").toString();
			   } catch (ClassCastException e) {
					// TODO Auto-generated catch block
				   extra = tag.getString("extra");
			   }
	       }
	       if (tag.containsKey("icon")) {
	    	   try {
	    		   icon = tag.getInt("icon");
	    	   } catch (ClassCastException e) {
					//set to null if this happens
	    		   icon = null;
				}
	    	   
	       }
	       if (tag.containsKey("attributes")) {
	    	   atts = tag.getJsonArray("attributes");
	       }
	       
			boolean isEmptyTag = false;
			if (atts != null) {
				final int m = atts.size();
				for (int j = 0; j < m; ++j) {
					final String att = atts.getString(j);
					logger.debug("attribute = " + att.toString());
					if (att.equals(IS_EMPTY_TAG_PROPERTY_NAME)){
						isEmptyTag = true;
					}
					else {
						attributes.put(att, null);
					}
					
				}
			}
			
			CustomTagWrapper ct;
			try {
				ct = new CustomTagWrapper(CustomTagFactory.create(tagName, attributes, true), color, isEmptyTag, tagLabel, extra, icon);
				addToCollectionRegistry(ct, false, false);
			} catch (InstantiationException | IllegalAccessException | IllegalArgumentException
					| InvocationTargetException | ClassNotFoundException | NoSuchMethodException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

	    }
	   
	   /*
	    * to avoid many ChangeEvents we call it after all DB tags are added into the registry
	    */
		TagRegistryChangeEvent e = new TagRegistryChangeEvent(TagRegistryChangeEvent.ADDED_TAG, null);
		registryObserver.setChangedAndNotifyObservers(e);

	}
	
	public static String createCollectionDBTagsString(String oldCollectionTagsString, JsonArray tagDefString) {
		logger.debug("adding local tags to registry, tagNamesProp = " + oldCollectionTagsString);

		JsonReader jsonReader = Json.createReader(new StringReader(oldCollectionTagsString));
		JsonObject obj = jsonReader.readObject();
		jsonReader.close(); 
		
	   JsonObject defObject = insertValue(obj.getJsonObject("definitions"), "annotations", tagDefString);
	   JsonObject completeObject = insertValue(obj, "definitions", defObject);
	   
	   return completeObject.toString();

	}
	
	private static JsonObject insertValue(JsonObject source, String key, JsonArray tagDefString) {
	    JsonObjectBuilder builder = Json.createObjectBuilder();
	    
	    source.entrySet().
	            forEach(e -> {logger.debug("e.getKey() : " +e.getKey());logger.debug("e.getValue() : " +e.getValue());
	            	if (e.getKey().contentEquals(key)) {
	            		builder.add(key, tagDefString); 
	            	}else {
	            		builder.add(e.getKey(), e.getValue());
	            }});
	            	
	    return builder.build();
	}
	
	private static JsonObject insertValue(JsonObject source, String key, JsonObject tagDefString) {
	    JsonObjectBuilder builder = Json.createObjectBuilder();
	    
	    source.entrySet().
	            forEach(e -> {logger.debug("e.getKey() : " +e.getKey());logger.debug("e.getValue() : " +e.getValue());
	            	if (e.getKey().contentEquals(key)) {
	            		builder.add(key, tagDefString); 
	            	}else {
	            		builder.add(e.getKey(), e.getValue());
	            }});
	            	
	    return builder.build();
	}
	
  public static void addDBTagsToRegistry(String tagNamesProp, boolean isCollectionTags) {
		logger.info("adding local tags to registry, tagNamesProp = " + tagNamesProp);

		Matcher m = RegexPattern.TAG_DEFINITIONS_PATTERN.matcher(tagNamesProp);
		while (m.find()) {
			String tag = tagNamesProp.substring(m.start(), m.end());
			logger.debug("found tag: '" + tag + "'");

			String tagName = m.group(1);
			logger.debug("tagname = " + tagName);

			Map<String, Object> attributes = new HashMap<>();
			String atts = m.group(3);

			String color = null;
			boolean isEmptyTag = false;
			if (atts != null)
				for (String a : m.group(3).split(",")) {
					a = a.trim();
					logger.debug("attribute = " + a);
					if (a.startsWith("#")) { // color attribute!
						logger.debug("setting color for tag " + tagName + " to: " + color);
						color = a;
					} else if (a.equals(IS_EMPTY_TAG_PROPERTY_NAME)) {
						isEmptyTag = true;
					} else {
						attributes.put(a, null);
					}
				}

			try {

				addToRegistry(CustomTagFactory.create(tagName, attributes), color, isEmptyTag, true, isCollectionTags,
						false);
			} catch (Exception e1) {
				logger.warn(e1.getMessage());
			}
		}
		TagRegistryChangeEvent e = new TagRegistryChangeEvent(TagRegistryChangeEvent.ADDED_TAG, null);
		registryObserver.setChangedAndNotifyObservers(e);
	}

//	/**
//	 * @deprecated does this even work? never used anyway...
//	 */
//	public static List<CustomTag> getCustomTagListFromProperties(String tagNamesProp) {		
//		List<CustomTag> cts = new ArrayList<CustomTag>();
//		
//		Matcher m = RegexPattern.TAG_DEFINITIONS_PATTERN.matcher(tagNamesProp);
//		while (m.find()) {
//			String tag = tagNamesProp.substring(m.start(), m.end());
//			logger.debug("found tag: '"+tag+"'");
//			
//			String tagName = m.group(1);
//			logger.debug("tagname = "+tagName);
//			
//			CustomTag ct = objectRegistry.get(tagName);
//		    cts.add(ct);
//		}
//		return cts;
//	}

	public static boolean removeFromRegistry(String tagName, boolean isCollectionTag) throws IOException {
		CustomTag t = getCustomTag(tagName, isCollectionTag);
		if (t != null) {
			if (t.isDeleteable()) {
				logger.debug("deleting tag '" + tagName + "'");
				if (isCollectionTag) {
					collTagsRegistry.remove(tagName);
				} else {
					objectRegistry.remove(tagName);
				}

				TagRegistryChangeEvent e = new TagRegistryChangeEvent(TagRegistryChangeEvent.REMOVED_TAG, t);
				registryObserver.setChangedAndNotifyObservers(e);
				return true;
			} else {
				throw new IOException("Cannot delete tag " + tagName + "'");
			}
		}
		return false;
	}

	public static boolean removeFromRegistry(String tagName) throws IOException {

		return removeFromRegistry(tagName, false);

	}

	/**
	 * Register the given tag in the tag registry. If it is already present,
	 * attributes are merged.
	 * 
	 * @param ct                                 The tag to register
	 * @param color                              The color for the tag. Set to
	 *                                           <code>null</code> to use default
	 *                                           color
	 *                                           (CustomTag::getDefaultColor).<br>
	 * @param mergeAttributesIfAlreadyRegistered If the tag is already registered,
	 *                                           attributes from ct will be merged
	 *                                           into the existing registered tag.
	 *                                           If no default color is set a new
	 *                                           one is generated automatically.
	 */
	public static boolean addToRegistry(CustomTag ct, String color, boolean isEmptyTag,
			boolean mergeAttributesIfAlreadyRegistered, boolean useCollectionRegistry, boolean notify)
			throws NoSuchMethodException, SecurityException, IllegalAccessException, InvocationTargetException {
		CustomTag t = getCustomTag(ct.getTagName(), useCollectionRegistry);
		logger.trace("tag to add " +t);
		if (t == null) { // not registered yet
			logger.debug("registering new tag: " + ct);
			if (color == null) { // color not given -> get default color
				color = ct.getDefaultColor();
			}
			if (!CoreUtils.isValidColorCode(color)) { // still no valid color --> create a new one
				color = getNewTagColor();
			}

			CustomTagWrapper cw = new CustomTagWrapper(ct, color, isEmptyTag);

			if (useCollectionRegistry) {
				collTagsRegistry.put(ct.getTagName(), cw);
			} else {
				objectRegistry.put(ct.getTagName(), cw);
			}

			if (notify) {

				TagRegistryChangeEvent e = new TagRegistryChangeEvent(TagRegistryChangeEvent.ADDED_TAG, cw.customTag);
				registryObserver.setChangedAndNotifyObservers(e);
			}

			return true;
		} else if (mergeAttributesIfAlreadyRegistered) {
			//t.deleteCustomAttributes();
			boolean addedAttributes = t.setAttributes(ct, false, false); // add attributes (without values!!!)
			if (addedAttributes) {
				TagRegistryChangeEvent e = new TagRegistryChangeEvent(TagRegistryChangeEvent.ADDED_TAG_ATTRIBUTES, t);
				registryObserver.setChangedAndNotifyObservers(e);
			}
			// set new color (if it's a new one)
			if (CoreUtils.isValidColorCode(color) && !color.equals(getTagColor(ct.getTagName()))) {
				setTagColor(ct.getTagName(), color);
			}
//			logger.warn("A tag with this name is already defined: "+ct.getTagName()+" - not adding to registry!");
			return false;
		}
		return false;
	}
	
	/**
	 * Register the given tag in the tag registry. If it is already present,
	 * attributes are merged.
	 * 
	 * @param ct                                 The tag to register
	 * @param color                              The color for the tag. Set to
	 *                                           <code>null</code> to use default
	 *                                           color
	 *                                           (CustomTag::getDefaultColor).<br>
	 * @param mergeAttributesIfAlreadyRegistered If the tag is already registered,
	 *                                           attributes from ct will be merged
	 *                                           into the existing registered tag.
	 *                                           If no default color is set a new
	 *                                           one is generated automatically.
	 */
	public static boolean addToCollectionRegistry(CustomTagWrapper ct, boolean mergeAttributesIfAlreadyRegistered, boolean notify)
			throws NoSuchMethodException, SecurityException, IllegalAccessException, InvocationTargetException {
		CustomTag t = getCustomTag(ct.customTag.getTagName(), true);
		if (t == null) { // not registered yet
			logger.trace("registering new tag: " + ct);
			if (ct.color == null) { // color not given -> get default color
				ct.color = ct.customTag.getDefaultColor();
			}
			if (!CoreUtils.isValidColorCode(ct.color)) { // still no valid color --> create a new one
				ct.color = getNewTagColor();
			}


			collTagsRegistry.put(ct.customTag.getTagName(), ct);


			if (notify) {

				TagRegistryChangeEvent e = new TagRegistryChangeEvent(TagRegistryChangeEvent.ADDED_TAG, ct.customTag);
				registryObserver.setChangedAndNotifyObservers(e);
			}

			return true;
		} else if (mergeAttributesIfAlreadyRegistered) {
			boolean addedAttributes = t.setAttributes(ct.customTag, false, false); // add attributes (without values!!!)
			if (addedAttributes) {
				TagRegistryChangeEvent e = new TagRegistryChangeEvent(TagRegistryChangeEvent.ADDED_TAG_ATTRIBUTES, t);
				registryObserver.setChangedAndNotifyObservers(e);
			}
			// set new color (if it's a new one)
			if (CoreUtils.isValidColorCode(ct.color) && !ct.color.equals(getTagColor(ct.customTag.getTagName()))) {
				setTagColor(ct.customTag.getTagName(), ct.color);
			}
//			logger.warn("A tag with this name is already defined: "+ct.getTagName()+" - not adding to registry!");
			return false;
		}
		return false;
	}

	/**
	 * Register the given tag in the tag registry. If it is already present,
	 * attributes are merged.
	 * 
	 * @param ct                                 The tag to register
	 * @param color                              The color for the tag. Set to
	 *                                           <code>null</code> to use default
	 *                                           color
	 *                                           (CustomTag::getDefaultColor).<br>
	 * @param mergeAttributesIfAlreadyRegistered If the tag is already registered,
	 *                                           attributes from ct will be merged
	 *                                           into the existing registered tag.
	 *                                           If no default color is set a new
	 *                                           one is generated automatically.
	 */
	public static boolean addToRegistry(CustomTag ct, String color, boolean isEmptyTag,
			boolean mergeAttributesIfAlreadyRegistered)
			throws NoSuchMethodException, SecurityException, IllegalAccessException, InvocationTargetException {

		return addToRegistry(ct, color, isEmptyTag, mergeAttributesIfAlreadyRegistered, false, true);

	}

	public static boolean addToRegistry(CustomTag ct, String color, boolean isEmptyTag,
			boolean mergeAttributesIfAlreadyRegistered, boolean notify)
			throws NoSuchMethodException, SecurityException, IllegalAccessException, InvocationTargetException {

		return addToRegistry(ct, color, isEmptyTag, mergeAttributesIfAlreadyRegistered, false, notify);

	}

//	public static void addToRegistry(CustomTag ct, String color, boolean mergeAttributesIfAlreadyRegistered) throws NoSuchMethodException, SecurityException, IllegalAccessException, InvocationTargetException {
//		addToRegistry(ct, color, false, mergeAttributesIfAlreadyRegistered);
//	}

	public static String getNewTagColor() {
		return ColorTable.getNewColor(getRegisteredColors());
	}

	/*
	 * first get the color form the collection -> if not available get the 
	 */
	public static String getTagColor(String tagName) {
		if (collTagsRegistry.get(tagName)==null && objectRegistry.get(tagName)==null) {
			return null;
		}
		return collTagsRegistry.get(tagName) == null ? objectRegistry.get(tagName).color : getCollectionTagColor(tagName);
	}

	public static String getCollectionTagColor(String tagName) {
		return collTagsRegistry.get(tagName) == null ? null : collTagsRegistry.get(tagName).color;
	}
	
	public static String getCollectionTagLabel(String tagName) {
		return collTagsRegistry.get(tagName) == null ? null : collTagsRegistry.get(tagName).label;
	}
	
	public static String getCollectionTagExtra(String tagName) {
		return collTagsRegistry.get(tagName) == null ? null : collTagsRegistry.get(tagName).extra;
	}
	
	public static Integer getCollectionTagIcon(String tagName) {
		return collTagsRegistry.get(tagName) == null ? null : collTagsRegistry.get(tagName).icon;
	}

	public static boolean setTagColor(String tagName, String color) {

		return setTagColor(tagName, color, false);

	}

	public static boolean setTagColor(String tagName, String color, boolean isCollectionTag) {
		CustomTagWrapper cw = isCollectionTag ? collTagsRegistry.get(tagName) : objectRegistry.get(tagName);

		if (CoreUtils.isValidColorCode(color) && cw != null) {
//			colorRegistry.put(tagName, color.toUpperCase());
			cw.color = color.toUpperCase();

			TagRegistryChangeEvent e = new TagRegistryChangeEvent(TagRegistryChangeEvent.CHANGED_TAG_COLOR,
					cw.customTag);
			registryObserver.setChangedAndNotifyObservers(e);
			return true;
		} else {
//			logger.warn("no valid color specified: "+color+" tag: "+tagName);
			return false;
		}
	}

	public static boolean isEmptyTag(String tagName) {
		if (StringUtils.isEmpty(tagName)) {
			return false;
		}

		CustomTagWrapper cw = objectRegistry.get(tagName);
		return cw != null ? cw.isEmptyTag : false;
	}

	public static void setIsEmptyTag(String tagName, boolean isEmptyTag) {
		if (StringUtils.isEmpty(tagName)) {
			return;
		}

		CustomTagWrapper cw = objectRegistry.get(tagName);
		if (cw != null) {
			cw.isEmptyTag = isEmptyTag;
		}
	}

	public static List<CustomTag> getRegisteredCollectionTags() {
		return collTagsRegistry.values().stream().map(tw -> tw.customTag).collect(Collectors.toList());
	}

	public static List<CustomTag> getRegisteredCustomTags() {
		return objectRegistry.values().stream().map(tw -> tw.customTag).collect(Collectors.toList());
	}

	public static List<String> getRegisteredColors() {
		return objectRegistry.values().stream().map(tw -> tw.color).collect(Collectors.toList());
	}

	public static List<String> getRegisteredCollectionColors() {
		return collTagsRegistry.values().stream().map(tw -> tw.color).collect(Collectors.toList());
	}

	public static Class<? extends CustomTag> getTagClassFromRegistry(String tagName) {
		CustomTagWrapper cw = objectRegistry.get(tagName);
		if (tagName == null || cw == null) {
			return null;
		}
		return cw.clazz;
	}

	public static CustomTag getCustomTag(String tagName) {

		return getCustomTag(tagName, false);
	}

	public static CustomTag getCustomTag(String tagName, boolean collectionTags) {
		CustomTagWrapper cw = collectionTags ? collTagsRegistry.get(tagName) : objectRegistry.get(tagName);
		if (cw != null) {
			return cw.customTag;
		} else {
			return null;
		}
	}

	public static CustomTag getTagObjectFromRegistry(String tagName, boolean isCollectionTags) {
		return getCustomTag(tagName, isCollectionTags);
	}

	public static Set<String> getRegisteredTagNames() {
		return objectRegistry.keySet();
	}

	public static Set<String> getRegisteredCollectionTagNames() {
		return collTagsRegistry.keySet();
	}

	public static List<String> getRegisteredTagNamesSorted() {
		List<String> tags = new ArrayList<>();
		tags.addAll(getRegisteredTagNames());
		Collections.sort(tags);
		return tags;
	}

	public static List<String> getRegisteredCollectionTagNamesSorted() {
		List<String> tags = new ArrayList<>();
		tags.addAll(getRegisteredCollectionTagNames());
		Collections.sort(tags);
		return tags;
	}

//	public static Collection<CustomTag> getRegisteredTagObjects() { return objectRegistry.values(); }
//	public static Map<String, CustomTag> getRegisteredObjects() { return objectRegistry; } 

	public static List<CustomTag> getRegisteredTagObjectsSortedByName(boolean caseInsensitve,
			boolean isCollectionTags) {
		List<CustomTag> tmpTags = isCollectionTags ? CustomTagFactory.getRegisteredCollectionTags()
				: CustomTagFactory.getRegisteredCustomTags();
		List<CustomTag> registeredTagsSorted = new ArrayList<>(tmpTags);
		Collections.sort(registeredTagsSorted, new Comparator<CustomTag>() {
			@Override
			public int compare(CustomTag t1, CustomTag t2) {
				if (caseInsensitve) {
					return t1.getTagName().toLowerCase().compareTo(t2.getTagName().toLowerCase());
				} else {
					return t1.getTagName().compareTo(t2.getTagName());
				}
			}
		});

		return registeredTagsSorted;
	}

	public static Set<CustomTagAttribute> getTagAttributes(String tagName) {
		CustomTag t = getTagObjectFromRegistry(tagName, false);
		if (t == null)
			return null;

		return t.getAttributes();
	}

	public static Set<CustomTagAttribute> getTagAttributes(String tagName, boolean isCollectionTags) {
		CustomTag t = getTagObjectFromRegistry(tagName, isCollectionTags);
		if (t == null)
			return null;

		return t.getAttributes();
	}

	public static CustomTagAttribute getAttribute(String tagName, String attributeName) {
		Set<CustomTagAttribute> atts = getTagAttributes(tagName);
		if (atts == null)
			return null;

		for (CustomTagAttribute a : atts) {
			if (a.getName().equals(attributeName)) {
				return a;
			}
		}

		return null;
	}

	public static CustomTag create(String tagName) throws Exception {
		return create(tagName, new HashMap<String, Object>());
	}

//	public static CustomTag create(String tagName, Property ...entries) throws Exception {
//		Map<String, Object> attributes = new HashMap<>();
//		for (Map.Entry<String, Object> e : entries) {
//			attributes.put(e.getKey(), e.getValue());
//		}
//		return create(tagName, attributes);
//	}

//	public static CustomTag create(String tagName, int offset, int length) throws Exception {
//		return create(tagName, Property.create(CustomTag.OFFSET_PROPERTY_NAME, offset), Property.create(CustomTag.LENGTH_PROPERTY_NAME, length));
//	}

	public static CustomTag create(String tagName, int offset, int length) throws Exception {
		return create(tagName, offset, length, null);
	}

	public static CustomTag create(String tagName, int offset, int length, Map<String, Object> attributes)
			throws Exception {
		CustomTag t = create(tagName, attributes);

		// add those attributes later to make sure they are set correclty according to
		// the given parameters:
		t.setOffset(offset);
		t.setLength(length);

		return t;
	}

	/**
	 * Creates a CustomTag with the given name and attributes. If the tagName is
	 * found in the tag registry the constructor of this derived tag is called,
	 * elsewise the default CustomTag constructor will be used.
	 * 
	 * @param tagName
	 * @param attributes
	 * @return The created CustomTag object
	 */
	public static CustomTag create(String tagName, Map<String, Object> attributes)
			throws InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException,
			ClassNotFoundException, NoSuchMethodException, IOException {
		Class<? extends CustomTag> tagClazz = getTagClassFromRegistry(tagName);
//		if (t==null)
//			throw new ClassNotFoundException("Class for tagName = "+tagName+" not found in registry - use addToRegistry method to register a CustomTag!");

		CustomTag ct = null;
		if (tagClazz != null && !tagClazz.equals(CustomTag.class)) { // found in registry for custom tags (i.e. not of
																		// class CustomTag!)
			ct = tagClazz.newInstance();
		} else if (objectRegistry.containsKey(tagName)) { // found in object registry for all custom tags
//			ct = new CustomTag(tagName);
			ct = new CustomTag(getCustomTag(tagName));
		} else // not found in registry --> use constructor with tagName in CustomTag class
			ct = new CustomTag(tagName);

		// set attributes:
		final boolean FORCE_ADDING_OF_ATTRIBUTES = true;
		if (attributes != null) {
			for (String an : attributes.keySet()) {
				logger.trace("setting attribute: " + an);
				ct.setAttribute(an, attributes.get(an), FORCE_ADDING_OF_ATTRIBUTES);
			}

			// FIXME: is this call necessary ?????
			beanUtilsBean.populate(ct, attributes);
		}

//		for (String attribute : attributes.keySet()) {
//			PropertyUtils.setProperty(ct, attribute, attributes.get(attribute));			
//		}

		return ct;
	}
	
	/**
	 * Creates a CustomTag with the given name and attributes. If the tagName is
	 * found in the tag registry the constructor of this derived tag is called,
	 * elsewise the default CustomTag constructor will be used.
	 * 
	 * @param tagName
	 * @param attributes
	 * @return The created CustomTag object
	 */
	public static CustomTag create(String tagName, Map<String, Object> attributes, boolean collectionTag)
			throws InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException,
			ClassNotFoundException, NoSuchMethodException, IOException {
		Class<? extends CustomTag> tagClazz = getTagClassFromRegistry(tagName);
//		if (t==null)
//			throw new ClassNotFoundException("Class for tagName = "+tagName+" not found in registry - use addToRegistry method to register a CustomTag!");

		CustomTag ct = null;
		if (tagClazz != null && !tagClazz.equals(CustomTag.class)) { // found in registry for custom tags (i.e. not of
																		// class CustomTag!)
			ct = tagClazz.newInstance();
		} else if (objectRegistry.containsKey(tagName) && !collectionTag) { // found in object registry for all custom tags
//			ct = new CustomTag(tagName);
			ct = new CustomTag(getCustomTag(tagName, false));
		} else if (collTagsRegistry.containsKey(tagName) && collectionTag) { // found in object registry for all custom tags
//			ct = new CustomTag(tagName);
			ct = new CustomTag(getCustomTag(tagName, true));
		} else // not found in registry --> use constructor with tagName in CustomTag class
			ct = new CustomTag(tagName);

		// set attributes:
		final boolean FORCE_ADDING_OF_ATTRIBUTES = true;
		if (attributes != null) {
			for (String an : attributes.keySet()) {
				logger.trace("setting attribute: " + an);
				ct.setAttribute(an, attributes.get(an), FORCE_ADDING_OF_ATTRIBUTES);
			}

			// FIXME: is this call necessary ?????
			beanUtilsBean.populate(ct, attributes);
		}

//		for (String attribute : attributes.keySet()) {
//			PropertyUtils.setProperty(ct, attribute, attributes.get(attribute));			
//		}

		return ct;
	}

	public static void main(String[] args) throws Exception {
//    	TextStyleTag ts = (TextStyleTag) create(TextStyleTag.TAG_NAME, 1, 3, null);	
//		logger.info(ts.toString());

		//addDBTagsToRegistry("[Person{prop1, prop2, prop3,prop4  } Location Speech Address]", false);
		
		addCollectionDBTagsToRegistry("{\"definitions\":{\"annotations\":[{\"name\":\"abbrev\",\"label\":\"Abbrev\",\"attributes\":[\"expansion\"],\"extra\":[{\"name\":\"expansion\",\"label\":\"Expansion\",\"type\":\"string\"}],\"color\":\"#ff0000\",\"icon\":8228}]}}");
	}

}
