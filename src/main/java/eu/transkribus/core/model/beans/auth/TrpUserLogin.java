package eu.transkribus.core.model.beans.auth;

import java.io.Serializable;
import java.security.Principal;
import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class TrpUserLogin extends TrpUser implements Principal, Serializable {
	private static final long serialVersionUID = 2370532427197634733L;
	private Date loginTime = new Date();
	private String sessionId;
	private String userAgent;
	private String ip;
	private String guiVersion;
	private String clientId;
	
	/**
	 * DO NOT USE! Just here because JaxB needs it :-/
	 */
	public TrpUserLogin(){
	}
	
	public TrpUserLogin(TrpUser user) {
		super(user);
	}
		
	public TrpUserLogin(final int userId, final String userName, final String sessionId){
		super(userId, userName);
		this.sessionId = sessionId;
		loginTime = new Date();
	}
	public String getSessionId() {
		return sessionId;
	}
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	public Date getLoginTime() {
		return loginTime;
	}
	public void setLoginTime(Date loginTime) {
		this.loginTime = loginTime;
	}
	public String getUserAgent() {
		return userAgent;
	}

	public void setUserAgent(String userAgent) {
		this.userAgent = userAgent;
	}
	
	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}
	
	public void setGuiVersion(String guiVersion) {
		this.guiVersion = guiVersion;
	}
	
	public String getGuiVersion() {
		return guiVersion;
	}
	
	public void setClientId(String clientId) {
		this.clientId = clientId;
	}
	
	public String getClientId() {
		return clientId;
	}

	@Override
	public String getName() {
		return this.getUserName();
	}

	@Override
	public String toString() {
		return "TrpUserLogin{" +
				"loginTime=" + loginTime +
				", sessionId='" + sessionId + '\'' +
				", userAgent='" + userAgent + '\'' +
				", ip='" + ip + '\'' +
				", guiVersion='" + guiVersion + '\'' +
				", clientId='" + clientId + '\'' +
				"} " + super.toString();
	}
}
