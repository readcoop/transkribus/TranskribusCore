package eu.transkribus.core.model.beans;

import java.io.File;
import java.io.Serializable;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import eu.transkribus.core.io.RemoteDocConst;
import eu.transkribus.core.model.beans.adapters.DocTypeAdapter;
import eu.transkribus.core.model.beans.adapters.ScriptTypeAdapter;
import eu.transkribus.core.model.beans.enums.DocType;
import eu.transkribus.core.model.beans.enums.ScriptType;
import io.swagger.v3.oas.annotations.Hidden;

@Entity
@Table(name="DOC_MD")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class TrpDocMetadata extends ATotalTranscriptStatistics implements Serializable, Comparable<TrpDocMetadata> {
	private static final long serialVersionUID = 1L;

	@Id
	@Column
	private int docId=-1;
	@Column
	private String title;
	@Column
	private String author;
	@Column(name="ultimestamp")
	private long uploadTimestamp;
	@Column
	private String genre;
	@Column
	private String writer;

	@Column
	@XmlJavaTypeAdapter(ScriptTypeAdapter.class)
	private ScriptType scriptType;
	
	//Owner is defined via docPermissions
	//TODO rename column to creator
	@Column
	private String uploader;
	
	@Column
	private int uploaderId;
	
	//actually nrOfPages is no real column. Transient annotated fields are ignored on inserts
	@Column
	@Transient
	private int nrOfPages;
	
	@Column(name="PAGE_ID")
	private Integer pageId;
	
	@Column(name="DEL_TIME")
	private Date deletedOnDate;

	@Column
	@Transient
	private URL url;
	@Column
	@Transient
	private URL thumbUrl;
	
	@Column(name="EXTID")
	private String externalId;
	
	@Column
	private String authority;
	
	@Column
	private String hierarchy;
	
	@Column
	private String backlink;
	
	@Column(name="DESCRIPTION")
	private String desc;
	
	@Column(name="DOCTYPE")
	@XmlJavaTypeAdapter(DocTypeAdapter.class)
	private DocType type;

	@Column(name="LANGUAGE")
	private String language;
	
	@Column(name="STATUS")
	private Integer status;
	
	@Column
	private String fimgStoreColl = null; // == null if local document
	private File localFolder = null; // != null when local document

	@Column(name="CREATEDFROM")
	private Long createdFromTimestamp;
	
	@Column(name="CREATEDTO")
	private Long createdToTimestamp;
	
	@Column
	private Integer origDocId = null;
	
	@XmlElementWrapper(name="collectionList")
	@XmlElement
	protected List<TrpCollection> colList = new ArrayList<>();

	private List<TrpEntityAttribute> attributes = new ArrayList<>();

	private List<TrpLabel> labels = new ArrayList<>();


    /** ID of the main collection of this document */
	private Integer mainColId;

	/** Indicates if it's the main collection or not, if the document was loaded in a collection context. */
	private Boolean isInMain;

	public TrpDocMetadata() {}

	public TrpDocMetadata(TrpDocMetadata other) {
		this.docId = other.docId;
		this.title = other.title;
		this.author = other.author;
		this.uploadTimestamp = other.uploadTimestamp;
		this.genre = other.genre;
		this.writer = other.writer;
		this.scriptType = other.scriptType;
		this.uploader = other.uploader;
		this.uploaderId = other.uploaderId;
		this.nrOfPages = other.nrOfPages;
		this.pageId = other.pageId;
		this.deletedOnDate = other.deletedOnDate;
		this.url = other.url;
		this.thumbUrl = other.thumbUrl;
		this.externalId = other.externalId;
		this.authority = other.authority;
		this.hierarchy = other.hierarchy;
		this.backlink = other.backlink;
		this.desc = other.desc;
		this.type = other.type;
		this.language = other.language;
		this.status = other.status;
		this.fimgStoreColl = other.fimgStoreColl;
		this.localFolder = other.localFolder;
		this.createdFromTimestamp = other.createdFromTimestamp;
		this.createdToTimestamp = other.createdToTimestamp;
		this.origDocId = other.origDocId;
		this.mainColId = other.mainColId;
		this.isInMain = other.isInMain;
		for(TrpCollection c : other.getColList()) {
			this.colList.add(new TrpCollection(c));
		}
		if (other.getAttributes() != null && attributes != null) {
			for (TrpEntityAttribute att : other.getAttributes()) {
				this.attributes.add(new TrpEntityAttribute(att));
			}
		}
	}

	public int getDocId() {
		return docId;
	}

	public void setDocId(int docId) {
		this.docId = docId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public long getUploadTimestamp() {
		return uploadTimestamp;
	}
	
	/**
	 * A helper that converts the timestamp to a Date
	 * 
	 * @return
	 */
	public Date getUploadTime() {
		return new Date(this.uploadTimestamp);
	}
	
	/**
	 * Helper to set the timestamp via a Date object
	 * @param uploadTime
	 */
	public void setUploadTime(Date uploadTime) {
		this.uploadTimestamp = uploadTime.getTime();
	}

	public void setUploadTimestamp(long uploadTimestamp) {
		this.uploadTimestamp = uploadTimestamp;
	}
	
	public String getGenre() {// set null
		return genre;
	}

	public void setGenre(String genre) {
		this.genre = genre;
	}

	public String getWriter() {
		return writer;
	}

	public void setWriter(String writer) {
		this.writer = writer;
	}
	
	public int getNrOfPages() {
		return this.nrOfPages;
	}
	
	public void setNrOfPages(int nrOfPages) {
		this.nrOfPages = nrOfPages;
	}

	public ScriptType getScriptType() {
		return scriptType;
	}

	public void setScriptType(ScriptType scriptType) {
		this.scriptType = scriptType;
	}

	public String getUploader() {
		return uploader;
	}

	public boolean isDeleted() {
		return deletedOnDate != null;
	}
	
	public void setDeleted(boolean deleted) {
		if(deleted && getDeletedOnDate() != null) {
			return;
		}
		setDeletedOnDate(deleted ? new Date() : null);
	}
	
	public void setDeletedOnDate(Date deletedOnDate) {
		this.deletedOnDate = deletedOnDate;
	}
	
	public Date getDeletedOnDate() {
		return deletedOnDate;
	}
	
	public void setUploader(String uploader) {
		this.uploader = uploader;
	}
	
	public int getUploaderId() {
		return uploaderId;
	}

	public void setUploaderId(int uploaderId) {
		this.uploaderId = uploaderId;
	}

	public String getExternalId() {
		return externalId;
	}

	public void setExternalId(String externalId) {
		this.externalId = externalId;
	}

	public String getAuthority() {
		return authority;
	}

	public void setAuthority(String authority) {
		this.authority = authority;
	}

	public String getHierarchy() {
		return hierarchy;
	}

	public void setHierarchy(String hierarchy) {
		this.hierarchy = hierarchy;
	}

	public String getBacklink() {
		return backlink;
	}

	public void setBacklink(String backlink) {
		this.backlink = backlink;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public DocType getType() {
		return type;
	}

	public void setType(DocType type) {
		this.type = type;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getFimgStoreColl() {
		return fimgStoreColl;
	}

	public void setFimgStoreColl(String fimgStoreColl) {
		this.fimgStoreColl = fimgStoreColl;
	}

	public File getLocalFolder() {
		return localFolder;
	}
	
	public void setLocalFolder(File localFolder) {
		this.localFolder = localFolder;
	}

	public Long getCreatedFromTimestamp() {
		return createdFromTimestamp;
	}
	
	public Date getCreatedFromDate(){
		return (createdFromTimestamp == null) ? null : new Date(createdFromTimestamp);
	}

	public void setCreatedFromTimestamp(Long createdFromTimestamp) {
		this.createdFromTimestamp = createdFromTimestamp;
	}
	
	public void setCreatedFromDate(Date createdFromDate) {
		this.createdFromTimestamp = createdFromDate == null ? null : createdFromDate.getTime();
	}

	public Long getCreatedToTimestamp() {
		return createdToTimestamp;
	}
	
	public Date getCreatedToDate(){
		return (createdToTimestamp == null) ? null : new Date(createdToTimestamp);
	}

	public void setCreatedToTimestamp(Long createdToTimestamp) {
		this.createdToTimestamp = createdToTimestamp;
	}
	
	public void setCreatedToDate(Date createdToDate) {
		this.createdToTimestamp = createdToDate == null ? null : createdToDate.getTime();
	}
	
	public TrpCollection getCollection(int collId) {
		for (TrpCollection c : getColList()) {
			if (c.getColId() == collId)
				return c;
		}
		return null;
	}
	
	public int getFirstCollectionId() {
		if (colList!=null && !colList.isEmpty()) {
			return colList.get(0).getColId();
		} else {
			return -1;
		}
	}

	public List<TrpCollection> getColList() {
		return colList;
	}

	public void setColList(List<TrpCollection> colList) {
		this.colList = colList;
	}
	
	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}
	
	@Hidden
	public boolean isSampleDoc() {
		return status!=null && status == RemoteDocConst.STATUS_SAMPLE_DOC;
	}
	
	@Hidden 
	public boolean isGtDoc() {
		return status!=null && status == RemoteDocConst.STATUS_GROUND_TRUTH_DOC;
	}
	
	public Integer getOrigDocId(){
		return origDocId;
	}
	
	public void setOrigDocId(Integer origDocId){
		this.origDocId = origDocId;
	}

	public Integer getPageId() {
		return pageId;
	}

	public void setPageId(Integer pageId) {
		this.pageId = pageId;
	}

	public URL getUrl() {
		return url;
	}

	public void setUrl(URL url) {
		this.url = url;
	}

	public URL getThumbUrl() {
		return thumbUrl;
	}

	public void setThumbUrl(URL thumbUrl) {
		this.thumbUrl = thumbUrl;
	}

	public List<TrpEntityAttribute> getAttributes() {
		return attributes;
	}

	public void setAttributes(List<TrpEntityAttribute> attributes) {
		this.attributes = attributes;
	}


	public List<TrpLabel> getLabels() {return labels; }

	public void setLabels(List<TrpLabel> labels) { this.labels = labels;}


	public Boolean isInMain() {
		return isInMain;
	}

	public void setInMain(Boolean isInMain) {
		this.isInMain = isInMain;
	}

	public Integer getMainColId() {
		return mainColId;
	}

	public void setMainColId(Integer mainColId) {
		this.mainColId = mainColId;
	}

	public String getColString() {
		String colsStr = "";
		if (getColList() != null) {
			for (TrpCollection c : getColList())
				colsStr += "("+c.getColName()+","+c.getColId()+") ";
		}
		colsStr = colsStr.trim();
		return colsStr;
	}

	/**
	 * Uses the docid for comparison by default.
	 * 
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(TrpDocMetadata md) {
		if (this.getDocId() > md.getDocId()) {
			return 1;
		}
		if (this.getDocId() < md.getDocId()) {
			return -1;
		}
		return 0;
	}

	@Override
	public String toString() {
		return "TrpDocMetadata{" +
				"docId=" + docId +
				", title='" + title + '\'' +
				", author='" + author + '\'' +
				", uploadTimestamp=" + uploadTimestamp +
				", genre='" + genre + '\'' +
				", writer='" + writer + '\'' +
				", scriptType=" + scriptType +
				", uploader='" + uploader + '\'' +
				", uploaderId=" + uploaderId +
				", nrOfPages=" + nrOfPages +
				", pageId=" + pageId +
				", deletedOnDate=" + deletedOnDate +
				", url=" + url +
				", thumbUrl=" + thumbUrl +
				", externalId='" + externalId + '\'' +
				", authority='" + authority + '\'' +
				", hierarchy='" + hierarchy + '\'' +
				", backlink='" + backlink + '\'' +
				", desc='" + desc + '\'' +
				", type=" + type +
				", language='" + language + '\'' +
				", status=" + status +
				", fimgStoreColl='" + fimgStoreColl + '\'' +
				", localFolder=" + localFolder +
				", createdFromTimestamp=" + createdFromTimestamp +
				", createdToTimestamp=" + createdToTimestamp +
				", origDocId=" + origDocId +
				", colList=" + colList +
				", attributes=" + attributes +
				", mainColId=" + mainColId +
				", isInMain=" + isInMain +
				", labels=" + getLabels().stream().map(TrpLabel::toString).reduce("", (a, b) -> a + b) +
				'}';
	}

	@Override
	public TrpDocMetadata clone(){
		try {
			return (TrpDocMetadata) super.clone();
		} catch (CloneNotSupportedException e) {
			throw new AssertionError(); //can't happen
		}
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof TrpDocMetadata)) return false;
		TrpDocMetadata that = (TrpDocMetadata) o;
		return docId == that.docId && uploadTimestamp == that.uploadTimestamp && uploaderId == that.uploaderId && nrOfPages == that.nrOfPages && Objects.equals(title, that.title) && Objects.equals(author, that.author) && Objects.equals(genre, that.genre) && Objects.equals(writer, that.writer) && scriptType == that.scriptType && Objects.equals(uploader, that.uploader) && Objects.equals(pageId, that.pageId) && Objects.equals(deletedOnDate, that.deletedOnDate) && Objects.equals(url, that.url) && Objects.equals(thumbUrl, that.thumbUrl) && Objects.equals(externalId, that.externalId) && Objects.equals(authority, that.authority) && Objects.equals(hierarchy, that.hierarchy) && Objects.equals(backlink, that.backlink) && Objects.equals(desc, that.desc) && type == that.type && Objects.equals(language, that.language) && Objects.equals(status, that.status) && Objects.equals(fimgStoreColl, that.fimgStoreColl) && Objects.equals(localFolder, that.localFolder) && Objects.equals(createdFromTimestamp, that.createdFromTimestamp) && Objects.equals(createdToTimestamp, that.createdToTimestamp) && Objects.equals(origDocId, that.origDocId) && Objects.equals(colList, that.colList) && Objects.equals(attributes, that.attributes) && Objects.equals(mainColId, that.mainColId) && Objects.equals(isInMain, that.isInMain);
	}

	@Override
	public int hashCode() {
		return Objects.hash(docId, title, author, uploadTimestamp, genre, writer, scriptType, uploader, uploaderId, nrOfPages, pageId, deletedOnDate, url, thumbUrl, externalId, authority, hierarchy, backlink, desc, type, language, status, fimgStoreColl, localFolder, createdFromTimestamp, createdToTimestamp, origDocId, colList, attributes, mainColId, isInMain);
	}
}
