package eu.transkribus.core.model.beans;

import java.io.Serializable;
import java.net.URL;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import eu.transkribus.core.model.beans.adapters.TrpRoleAdapter;
import eu.transkribus.core.model.beans.auth.TrpRole;

@Entity
@Table(name="COLLECTION")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class TrpCollection extends ATotalTranscriptStatistics implements Serializable {
	private static final long serialVersionUID = -6247876122034400418L;
	
	private static final String IS_CROWDSOURCING_COLUMN_NAME = "IS_CROWDSOURCING";
	private static final String IS_ELEARNING_COLUMN_NAME = "IS_ELEARNING";
	
	@Id
	@Column(name="COLLECTION_ID")
	private int colId;
	@Column(name="NAME")
	private String colName;
	@Column
	private String description;
	@Column
	private Date created;
	
	@Column(name="DEFAULT_FOR_APP")
//	@Transient
	private String defaultForApp = null;
	
	@Column(name=IS_CROWDSOURCING_COLUMN_NAME)
	private boolean crowdsourcing = false;
	
	@Column(name=IS_ELEARNING_COLUMN_NAME)
	private boolean elearning = false;
	
	//id of thee symbolic image
	@Column(name="PAGE_ID")
	private Integer pageId;
	
	@Column
	@Transient
	private URL url;
	@Column
	@Transient
	private URL thumbUrl;
	
	@Column
	@Transient
	private String label;
	
	//actually nrOfDocuments is no real column. Transient annotated fields are ignored on inserts
	@Column
	@Transient
	private int nrOfDocuments = 0;
	
	@Column
	@Transient
	@XmlJavaTypeAdapter(TrpRoleAdapter.class)
	private TrpRole role = null;

	@Column
	@Transient
	private Date favoriteTimestamp = null;

	private Boolean isFavorite = false;


	@XmlElement
	@Transient
	private TrpCrowdProject crowdProject = null;
	
	@Column(name="ACCOUNTING_STATUS")
	private Integer accountingStatus;

	/**
	 * May transport a model-collection link's release level for a model.
	 * Same as {@link #role} the field is only contextually set and null otherwise.
	 */
	@Column
	@Transient
	private String modelCollectionReleaseLevel;

	/**
	 * May transport a document-collection link's isMain property,
	 * i.e. if this is the original collection a document belongs to.
	 */
	@Column
	@Transient
	private Boolean isMainDocumentCollection;

	@Transient
	private List<TrpRole> assignableRoles;
	
	public TrpCollection(){}
	
	public TrpCollection(final int colId, final String name, final String description){
		this.colId = colId;
		this.colName = name;
		this.description = description;
	}

	public TrpCollection(TrpCollection other) {
		this.colId = other.colId;
		this.colName = other.colName;
		this.description = other.description;
		this.defaultForApp = other.defaultForApp;
		this.crowdsourcing = other.crowdsourcing;
		this.elearning = other.elearning;
		this.pageId = other.pageId;
		this.url = other.url;
		this.thumbUrl = other.thumbUrl;
		this.label = other.label;
		this.nrOfDocuments = other.nrOfDocuments;
		this.role = other.role;
		this.crowdProject = other.crowdProject;
		this.accountingStatus = other.accountingStatus;
		this.modelCollectionReleaseLevel = other.modelCollectionReleaseLevel;
		this.isMainDocumentCollection = other.isMainDocumentCollection;
		this.assignableRoles = other.assignableRoles;
	}

	public int getColId() {
		return colId;
	}
	public void setColId(int colId) {
		this.colId = colId;
	}
	public String getColName() {
		return colName;
	}
	public void setColName(String colName) {
		this.colName = colName;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Date getCreated() {
		return created;
	}
	public void setCreated(Date created) {
		this.created = created;
	}
	public TrpRole getRole() {
		return role;
	}
	public void setRole(TrpRole role) {
		this.role = role;
	}
	
	public String getDefaultForApp() {
		return defaultForApp;
	}
	public void setDefaultForApp(String defaultForApp) {
		this.defaultForApp = defaultForApp;
	}
	
	public boolean isCrowdsourcing() {
		return crowdsourcing;
	}

	public void setCrowdsourcing(boolean isCrowdsourcing) {
		this.crowdsourcing = isCrowdsourcing;
	}
	
	public boolean isElearning() {
		return elearning;
	}

	public void setElearning(boolean isElearning) {
		this.elearning = isElearning;
	}
	
	public Integer getPageId() {
		return pageId;
	}
	public void setPageId(Integer pageId) {
		this.pageId = pageId;
	}
	public URL getUrl() {
		return url;
	}
	public void setUrl(URL url) {
		this.url = url;
	}
	public URL getThumbUrl() {
		return thumbUrl;
	}
	public void setThumbUrl(URL thumbUrl) {
		this.thumbUrl = thumbUrl;
	}
	public String getSummary() {
		return getColName() +" ("+getColId()+", "+ (getRole() == null ? "Admin" : getRole())+")";
	}
	
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	
	public int getNrOfDocuments() {
		return nrOfDocuments;
	}
	public void setNrOfDocuments(int nrOfDocuments) {
		this.nrOfDocuments = nrOfDocuments;
	}
	
	public TrpCrowdProject getCrowdProject() {
		return crowdProject;
	}
	public void setCrowdProject(TrpCrowdProject crowdProject) {
		this.crowdProject = crowdProject;
	}
	public Integer getAccountingStatus() {
		return accountingStatus;
	}
	public void setAccountingStatus(Integer accountingStatus) {
		this.accountingStatus = accountingStatus;
	}
	public String getModelCollectionReleaseLevel() {
		return modelCollectionReleaseLevel;
	}
	public void setModelCollectionReleaseLevel(String modelCollectionReleaseLevel) {
		this.modelCollectionReleaseLevel = modelCollectionReleaseLevel;
	}
	public Boolean getIsMainDocumentCollection() {
		return isMainDocumentCollection;
	}
	public void setIsMainDocumentCollection(Boolean isMainDocumentCollection) {
		this.isMainDocumentCollection = isMainDocumentCollection;
	}
	public List<TrpRole> getAssignableRoles() {
		return assignableRoles;
	}
	public void setAssignableRoles(List<TrpRole> assignableRoles) {
		this.assignableRoles = assignableRoles;
	}

	public void setFavoriteTimestamp(Date timestamp) {
		this.favoriteTimestamp = timestamp;
		this.isFavorite = timestamp != null;
	}

	public Date getFavoriteTimestamp() {
		return favoriteTimestamp;
	}

	public Boolean isFavorite() {
		return isFavorite;
	}


	public String toShortString() {
		return 	this.getColId() 
				+ " - " 
				+ this.getColName() 
				+ " - " 
				+ this.getDescription() 
				+ " - "
				+ this.getCreated()
				+ " - "
				+ this.getRole()
				+ " - "
				+ this.getIsMainDocumentCollection();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof TrpCollection)) return false;
		if (!super.equals(o)) return false;
		TrpCollection that = (TrpCollection) o;
		return colId == that.colId && crowdsourcing == that.crowdsourcing && elearning == that.elearning && nrOfDocuments == that.nrOfDocuments && Objects.equals(colName, that.colName) && Objects.equals(description, that.description) && Objects.equals(created, that.created) && Objects.equals(defaultForApp, that.defaultForApp) && Objects.equals(pageId, that.pageId) && Objects.equals(url, that.url) && Objects.equals(thumbUrl, that.thumbUrl) && Objects.equals(label, that.label) && role == that.role && Objects.equals(crowdProject, that.crowdProject) && Objects.equals(accountingStatus, that.accountingStatus) && Objects.equals(modelCollectionReleaseLevel, that.modelCollectionReleaseLevel) && Objects.equals(isMainDocumentCollection, that.isMainDocumentCollection) && Objects.equals(assignableRoles, that.assignableRoles);
	}

	@Override
	public int hashCode() {
		return Objects.hash(super.hashCode(), colId, colName, description, created, defaultForApp, crowdsourcing, elearning, pageId, url, thumbUrl, label, nrOfDocuments, role, crowdProject, accountingStatus, modelCollectionReleaseLevel, isMainDocumentCollection, assignableRoles);
	}

	@Override
	public String toString() {
		return "TrpCollection{" +
				"colId=" + colId +
				", colName='" + colName + '\'' +
				", description='" + description + '\'' +
				", created=" + created +
				", defaultForApp='" + defaultForApp + '\'' +
				", crowdsourcing=" + crowdsourcing +
				", elearning=" + elearning +
				", pageId=" + pageId +
				", url=" + url +
				", thumbUrl=" + thumbUrl +
				", label='" + label + '\'' +
				", nrOfDocuments=" + nrOfDocuments +
				", role=" + role +
				", crowdProject=" + crowdProject +
				", accountingStatus=" + accountingStatus +
				", modelCollectionReleaseLevel='" + modelCollectionReleaseLevel + '\'' +
				", isMainDocumentCollection=" + isMainDocumentCollection +
				", assignableRoles = " + assignableRoles +
				", favorite = " + isFavorite +
				", favoriteTimestamp = " + favoriteTimestamp +
				"}";
	}


	public enum TrpCollectionFlag {
		crowdsourcing(IS_CROWDSOURCING_COLUMN_NAME),
		eLearning(IS_ELEARNING_COLUMN_NAME);
		
		private final String colName;
		TrpCollectionFlag(String colName) {
			this.colName = colName;
		}
		public final String getColumnName() {
			return this.colName;
		}
	}
}
