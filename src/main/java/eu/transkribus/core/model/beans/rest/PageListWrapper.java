package eu.transkribus.core.model.beans.rest;

import eu.transkribus.core.model.beans.TrpLabel;
import eu.transkribus.core.model.beans.TrpPage;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import java.util.List;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@XmlSeeAlso({TrpPage.class})
public class PageListWrapper extends JaxbPaginatedList<TrpPage> {


    private List<TrpLabel> containedLabels;

    public PageListWrapper() {
        super();
    }

    public PageListWrapper(List<TrpPage> pageList, int pageCount, List<TrpLabel> containedLabels,  int index, int nValues, String sortColumn, String sortDirection) {
        super(pageList, pageCount, index, nValues, sortColumn, sortDirection);
        this.containedLabels = containedLabels;
    }

    public List<TrpLabel> getContainedLabels() {
        return containedLabels;
    }
    public void setContainedLabels(List<TrpLabel> containedLabels) {
        this.containedLabels = containedLabels;
    }

}
