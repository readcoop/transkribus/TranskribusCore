package eu.transkribus.core.model.beans;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "clients")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class TrpClient {


    @Id
    @Column(name="CLIENT_ID")
    private int clientId;

    @Column(name="CLIENT_NAME")
    private int clientName;


    public int getClientId() {
        return clientId;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    public int getClientName() {
        return clientName;
    }

    public void setClientName(int clientName) {
        this.clientName = clientName;
    }




}
