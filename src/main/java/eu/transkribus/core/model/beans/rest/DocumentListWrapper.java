package eu.transkribus.core.model.beans.rest;

import eu.transkribus.core.model.beans.TrpCreditTransaction;
import eu.transkribus.core.model.beans.TrpDocMetadata;
import eu.transkribus.core.model.beans.TrpLabel;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import java.util.List;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@XmlSeeAlso({TrpDocMetadata.class})
public class DocumentListWrapper extends JaxbPaginatedList<TrpDocMetadata> {

    private List<TrpLabel> containedLabels;

    public DocumentListWrapper() {
        super();
    }

    public DocumentListWrapper(List<TrpDocMetadata> list, int total, int index, int nValues, String sortColumnField,
                                    String sortDirection) {
        super(list, total, index, nValues, sortColumnField, sortDirection);
    }

    public DocumentListWrapper(List<TrpDocMetadata> list, List<TrpLabel> containedLabels, int total, int index, int nValues, String sortColumnField, String sortDirection) {
        this(list, total, index, nValues, sortColumnField, sortDirection);
        this.containedLabels = containedLabels;
    }

    public List<TrpLabel> getContainedLabels() {
        return containedLabels;
    }
    public void setContainedLabels(List<TrpLabel> containedLabels) {
        this.containedLabels = containedLabels;
    }
}
