package eu.transkribus.core.model.beans.rest;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.xml.transform.stream.StreamSource;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.eclipse.persistence.jaxb.MarshallerProperties;
import org.eclipse.persistence.jaxb.UnmarshallerProperties;
import org.eclipse.persistence.jaxb.rs.MOXyJsonProvider;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fasterxml.jackson.annotation.JsonTypeName;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import eu.transkribus.core.model.beans.searchresult.PageHit;
import eu.transkribus.core.util.GsonUtil;
import eu.transkribus.core.util.JaxbUtils;

@XmlRootElement(name="List")
@JsonRootName("Item")
public class PageHits{
	
    protected List<PageHit> list;
    String name;

    public PageHits(){
    	this.list = new ArrayList<PageHit>();
    }

    public PageHits(List<PageHit> list){
    	this.list=list;
    }
    
    public PageHits(String list){
    	this.name=list;
    }

    @XmlElement(name="Item")
    @JsonProperty("Item")
    public List<PageHit> getList(){
    	return list;
    }
    
    public void setList(List<PageHit> list){
    	this.list=list;
    }
    
    public void add(PageHit e) {
    	this.list.add(e);
    }
    
    public void remove(PageHit o) {
    	this.list.remove(o);
    }
    
    public static void main(String[] args) throws Exception {
    	try {
			List<Integer> l = new ArrayList<>();
			l.add(1);
			l.add(3);
			l.add(6);
			
			PageHits phList = new PageHits();
			
//			String jsonParsStr = "{\r\n" + 
//					"   \"Item\" : [ \"PageHit [docId=39931, pageNr=1, pageUrl=null, highlights=[<em>schorsch</em> Auentinus ſtudioſis. Grammatice. Salutem. et ingenuos laboꝛes. ¶ Quãuis multa variaqꝫ a], wordCoords=[schorsch:r_1_1/tl_1/tl_1_1:457,290 738,290 738,203 457,203]]\" ]\r\n" + 
//					"}";
			
			String jsonParsStr = "{\"Item\":[{\"docId\":39931,\"pageNr\":1,\"docTitle\":\"P2PaLA_test\",\"pageUrl\":null,\"collectionIds\":[235],\"highlights\":[\"<em>schorsch</em> Auentinus ſtudioſis. Grammatice. Salutem. et ingenuos laboꝛes. ¶ Quãuis multa variaqꝫ a\"],\"wordCoords\":[\"schorsch:r_1_1/tl_1/tl_1_1:457,290 738,290 738,203 457,203\"]}]}";
			
			JAXBContext jaxbContext = JAXBContext.newInstance(PageHits.class);
			Marshaller marshaller = jaxbContext.createMarshaller();
			//marshaller.setProperty("eclipselink.media-type", "application/json");
			
			ObjectMapper mapper = new ObjectMapper();
			

			mapper.configure(DeserializationFeature.UNWRAP_ROOT_VALUE, false);
			mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			//mapper.enable(SerializationFeature.WRAP_ROOT_VALUE);
			phList = mapper.readValue(jsonParsStr, PageHits.class);

			
			for (PageHit ph : phList.getList()) {
				System.out.println("ph: " + (PageHit) ph);
				System.out.println("the hit: " + ph.getDocId());
				System.out.println("the hit: " + ph.getPageNr());
				System.out.println("the hit: " + ph.getDocTitle());
				System.out.println("the hit: " + ph.getWordCoords());
			}
			
			
			
			PageHits hits = JaxbUtils.unmarshalJson(jsonParsStr, PageHits.class);
			System.out.println("size of list: " + hits.getList().size());
			
			for (PageHit hit : hits.getList()) {
				System.out.println("the hit: " + hit.getDocId());
				System.out.println("the hit: " + hit.getPageNr());
				System.out.println("the hit: " + hit.getDocTitle());
				System.out.println("the hit: " + hit.getWordCoords());
			}
			
			System.out.println(JaxbUtils.unmarshalJson(jsonParsStr, PageHits.class));
			
			System.out.println(JaxbUtils.marshalToJsonString(phList, false));
			System.out.println(GsonUtil.toJson(phList));
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}