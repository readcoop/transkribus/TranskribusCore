package eu.transkribus.core.model.beans.pagecontent.filter;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.transkribus.core.model.beans.pagecontent.PcGtsType;
import eu.transkribus.core.util.PageXmlUtils;

/**
 * Reverses the text of all *text-lines* excluding digits and text inside some defined tags on demand.
 * May be used for training documents that are written RTL but transcribed LTR e.g.
 */
public class ReversePageTextFilter implements IPageContentFilter {
	private static final Logger logger = LoggerFactory.getLogger(ReversePageTextFilter.class);
	
	private boolean excludeDigits;
	private String[] tagExceptions;
	
	public ReversePageTextFilter(boolean excludeDigits, List<String> tagExceptions) {
		this(excludeDigits, tagExceptions!=null ? tagExceptions.toArray(new String[0]) : new String[0]);
	}
	
	public ReversePageTextFilter(boolean excludeDigits, String... tagExceptions) {
		this.excludeDigits = excludeDigits;
		this.tagExceptions = tagExceptions!=null ? tagExceptions : new String[0]; 	
	}

	@Override
	public void doFilter(PcGtsType pc) {
		PageXmlUtils.reverseTextForAllLines(pc, excludeDigits, tagExceptions);

	}

}
