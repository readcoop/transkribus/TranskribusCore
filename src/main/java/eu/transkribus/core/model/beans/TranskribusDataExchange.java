package eu.transkribus.core.model.beans;

import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import eu.transkribus.core.util.JaxbUtils;

/**
 * transkribus_metadata.xml file type -> used for KWS projects (NAF, Greifswald ...) 
 */
@XmlRootElement
public class TranskribusDataExchange {
	public TranskribusDataExchange() {
	}
	
	public void setHierarchy(String[] hierarchyElements) {
		hierarchy = new JSONHierarchy();
		hierarchy.fonds = hierarchyElements[0];
		hierarchy.series = hierarchyElements[1];
		hierarchy.archival_unit = hierarchyElements[2];
	}
	
	@XmlRootElement
	public static class JSONHierarchy {
	    @XmlElement(name="fonds")
	    public String fonds;
	    @XmlElement(name="series")
	    public String series;
	    @XmlElement(name="archival_unit")
	    public String archival_unit;
	    
	    @Override
	    public String toString() {
	    	return fonds+"/"+series+"/"+archival_unit;
	    }
	}	
	
    @XmlElement(name="authority")
    public String authority;
    @XmlElement(name="hierarchy")
    public JSONHierarchy hierarchy;
    @XmlElement(name="backlink")
    public String backlink;
    @XmlElement(name="title")
    public String title;
    @XmlElement(name="dateRange")
    public String dateRange;
    @XmlElement(name="signature")
    public String signature;
    @XmlElement(name="identifier")
    public String identifier;
    @XmlElement(name="author")
    public String author;
    
    @XmlElement(name="additionalFields")
    public Map<String, String> additionalFields;
    
	@Override
	public String toString() {
		return "TranskribusDataExchange [authority=" + authority + ", hierarchy=" + hierarchy + ", backlink=" + backlink
				+ ", title=" + title + ", dateRange=" + dateRange + ", signature=" + signature + ", identifier="
				+ identifier + ", additionalFields=" + additionalFields+ ", author=" + author
				+ "]";
	}
	
	public static void main(String[] args) throws Exception {
		TranskribusDataExchange tmd = new TranskribusDataExchange();
		tmd.identifier = "asdf";
		tmd.additionalFields = new HashMap<>();
		tmd.additionalFields.put("arc_sig", "asdfasdfasdf");
		tmd.additionalFields.put("series_sig", "asdfasdfasdf");
		
		String str = JaxbUtils.marshalToString(tmd);
		System.out.println(str);
		tmd = JaxbUtils.unmarshal(str, TranskribusDataExchange.class);
		System.out.println(tmd);
	}
}


