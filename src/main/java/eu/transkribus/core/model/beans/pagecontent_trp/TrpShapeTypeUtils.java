package eu.transkribus.core.model.beans.pagecontent_trp;

import java.awt.Rectangle;
import java.awt.geom.Rectangle2D;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.StringTokenizer;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.transkribus.core.model.beans.customtags.AbbrevTag;
import eu.transkribus.core.model.beans.customtags.CustomTag;
import eu.transkribus.core.model.beans.customtags.CustomTagFactory;
import eu.transkribus.core.model.beans.customtags.CustomTagList;
import eu.transkribus.core.model.beans.customtags.CustomTagUtil;
import eu.transkribus.core.model.beans.customtags.TextStyleTag;
import eu.transkribus.core.model.beans.pagecontent.RegionType;
import eu.transkribus.core.model.beans.pagecontent.TextEquivType;
import eu.transkribus.core.model.beans.pagecontent.TextRegionType;
import eu.transkribus.core.model.beans.pagecontent_trp.observable.TrpObserveEvent.TrpTextChangedEvent;
import eu.transkribus.core.util.CoreUtils;
import eu.transkribus.core.util.IntRange;
import eu.transkribus.core.util.OverlapType;
import eu.transkribus.core.util.PointStrUtils;
import eu.transkribus.core.util.SebisStopWatch;

public class TrpShapeTypeUtils {
	private final static Logger logger = LoggerFactory.getLogger(TrpShapeTypeUtils.class);

	public static <T> void sortShapesByReadingOrderOrCoordinates(List<T> shapes) {
		try {
			Collections.sort(shapes, new TrpElementReadingOrderComparator<T>(true));
		} catch (Exception e) {
			logger.warn("could not sort regions by reading order, exception = " + e.getMessage()
					+ " - now sorting by yx coordinates(1)!");
			sortShapesByCoordinates(shapes, true);
		}
	}

	public static <T> void sortShapesByCoordinates(List<T> shapes, boolean forceCompareByYX) {
		try {
			Collections.sort(shapes, new TrpElementCoordinatesComparator<T>(forceCompareByYX));
			
			//for testing only
			//Collections.sort(shapes, new TrpElementCoordinatesComparator_Columns<T>(false));
		} catch (Exception e) {
			logger.error(
					"could not coordinates, exception = " + e.getMessage() + " - now sorting by yx coordinates(2)!");
			try {
				Collections.sort(shapes, new TrpElementCoordinatesComparator<T>(false));
			} catch (Exception e1) {
				logger.error("Still could not sort shapes -> should not happen here: " + e1.getMessage()
						+ " - skipping sorting", e1);
			}
		}
	}
	
	/*
	 * general method to sort shapes in columns (if they appear on the same height) and with YX else
	 * developed for Chorbücher - but should work for other similar use cases as well because it takes 
	 * coordinates only
	 */
	public static <T> void sortShapesByCoordinates_smartseder(List<T> shapes, boolean ltr) {
		
		List<ITrpShapeType> myNewList= new ArrayList<ITrpShapeType>();
		myNewList.addAll((Collection<? extends ITrpShapeType>) shapes);
				
		/*
		 * shows IDs of current sorting
		 */
//		for (T shape : shapes) {
//			if (shape instanceof TrpTextRegionType) {
//				TrpTextRegionType reg = (TrpTextRegionType) shape;
//				logger.debug(" curr shape to add : " + ((ITrpShapeType) shape).getId());
//			}
//		}
		
		try {
			int idxToAdd = 0;
			while (!myNewList.isEmpty()) {
				
				logger.debug("myNewList amount of REMAINING shapes to sort: " + myNewList.size());
				
//				for (int j=0; j<myNewList.size();j++) {
//					ITrpShapeType curr = (ITrpShapeType) myNewList.get(j);
//					if (curr instanceof TrpTextLineType) {
//						TrpTextLineType ttlt = (TrpTextLineType) curr;
//						logger.debug(" next in the remaining list: " + ttlt.getId());
//					}
//				}
				
				/*
				 * here we search for all shapes appearing on the same height (all in one row, the shape with the biggest height is found during the method
				 * and used as new reference. The center coordinates are used for compare
				 */
				ITrpShapeType currShape = (ITrpShapeType) myNewList.get(0);
				List<T> shapesOfSameHeight = (List<T>) addShapesOfSameY((List<? extends ITrpShapeType>) myNewList, currShape);
				
				logger.debug(" size of shapes on equal heigth: " + shapesOfSameHeight.size());
				
				/*
				 * more then one shape in a row
				 */
				if (shapesOfSameHeight.size()>1) {
					sortShapesByXY((List<ITrpShapeType>) shapesOfSameHeight, ltr);
					//for those we need a columnwise sorting
					sortShapesByCoordinates_columnwise(shapesOfSameHeight, ltr, true);
					for (int i=0; i<shapesOfSameHeight.size();i++) {

						int currIdx = shapes.indexOf(shapesOfSameHeight.get(i));
						logger.debug(" currIdx is " + currIdx);
						ITrpShapeType k = (ITrpShapeType) shapes.get(currIdx);
												
						//logger.debug(" ID of curr shape: " + k.getId());
						
						shapes.remove(currIdx);
						logger.debug("new reading order: " + (idxToAdd));
						if (idxToAdd<=shapes.size()) {
							shapes.add(idxToAdd, (T) k);
						}
						else {
							shapes.add((T) k);
						}
						idxToAdd++;

					}
					//remove all handled shapes to reach determination
					for (int j=0; j<shapesOfSameHeight.size();j++) {
						
						ITrpShapeType k = (ITrpShapeType) shapesOfSameHeight.get(j);
						logger.debug(" next to remove: " + k.getId());

						if (!myNewList.remove(k)){
							logger.debug("this shape could not be removed : " + k.getId());
						}

					}

				}
				else {
					
					//debug
//					if (currShape instanceof TrpTextRegionType) {
//						TrpTextRegionType reg = (TrpTextRegionType) currShape;
//						logger.debug(" curr shape to add : " + currShape.getId());
//					} 
					
					//to remove the correct shape
					int idxToRemove = shapes.indexOf(currShape);
					
					if (idxToRemove == -1) {
						logger.debug(" currShape not found: " + idxToRemove);
						return;
					}
					
					logger.debug(" next to remove: " + currShape.getId());
					logger.debug(" new reading order of single shape is: " + idxToAdd);
					
					shapes.remove(idxToRemove);
					if (idxToAdd<=shapes.size()) {
						shapes.add(idxToAdd, (T) currShape);
					}
					else {
						shapes.add((T) currShape);
					}
					
					if (myNewList.remove(currShape)) {
						idxToAdd++;
					}
					else {
						logger.debug("cannot remove : " + currShape.getId());
					}
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			logger.warn(
					"could not coordinates, exception = " + e.getMessage() + " - now sorting by yx coordinates(2)!");
			try {
				//sortShapesByCoordinates_columnwise(shapes, true);
			} catch (Exception e1) {
				logger.error("Still could not sort shapes -> should not happen here: " + e1.getMessage()
						+ " - skipping sorting", e1);
			}
		}
	}
	
	
	
	public static <T> void sortShapesByCoordinates_XIfYOverlaps_OtherwiseY(List<T> shapes, boolean ltr) {
		try {
			Collections.sort(shapes, new TrpElementCoordinatesComparator_XY_WhenYOverlaps<T>(ltr));
		} catch (Exception e) {
			logger.warn(
					"could not coordinates, exception = " + e.getMessage() + " - now sorting by yx coordinates(2)!");
			try {
				Collections.sort(shapes, new TrpElementCoordinatesComparator<T>(true, ltr));
			} catch (Exception e1) {
				logger.error("Still could not sort shapes -> should not happen here: " + e1.getMessage()
						+ " - skipping sorting", e1);
			}
		}
	}
	
	public static <T> void sortShapesByCoordinates_columnwise(List<T> shapes, boolean ltr, boolean doColumns) {
		try {
			Collections.sort(shapes, new TrpElementCoordinatesComparator<T>(doColumns, false, ltr));
		} catch (Exception e) {
			logger.error(
					"could not coordinates, exception = " + e.getMessage() + " - now sorting by yx coordinates(2)!");
			try {
				//System.in.read();
				Collections.sort(shapes, new TrpElementCoordinatesComparator<T>(true, ltr));
			} catch (Exception e1) {
				logger.error("Still could not sort shapes -> should not happen here: " + e1.getMessage()
						+ " - skipping sorting", e1);
			}
		}
	}

	public static void sortShapesByXY(List<ITrpShapeType> shapes, boolean ltr) {
		try {
			Collections.sort(shapes, new TrpShapeTypeXYComparator(ltr));
		} catch (Exception e) {
			logger.error("Could not sort shape by XY coordinates - skipping!", e);
		}
	}

	public static Pair<String, String> invertCoordsCommonRegionOrientation(String coords1, String coords2, Object o1,
			Object o2) {
		Double orientationInRadiants = null;
		if (o1 instanceof ITrpShapeType && o2 instanceof ITrpShapeType && !(o1 instanceof RegionType)
				&& !(o2 instanceof RegionType)) {
			TrpTextRegionType tr1 = TrpShapeTypeUtils.getTextRegion((ITrpShapeType) o1);
			TrpTextRegionType tr2 = TrpShapeTypeUtils.getTextRegion((ITrpShapeType) o2);

			if (tr1 != null && tr2 != null && StringUtils.equals(tr1.getId(), tr2.getId())
					&& tr1.getOrientation() != null) {
				orientationInRadiants = Math.toRadians(tr1.getOrientation());
			}
		}

		if (orientationInRadiants != null) {
			coords1 = PointStrUtils.rotatePoints(coords1, orientationInRadiants);
			coords2 = PointStrUtils.rotatePoints(coords2, orientationInRadiants);
			logger.trace("orientation set: " + orientationInRadiants + " rotated points: " + coords1 + ", " + coords2);
		}

		return Pair.of(coords1, coords2);
	}

	public static boolean removeShape(ITrpShapeType s) {
		if (s == null) {
			return false;
		}

		s.removeFromParent();

		// FIXME: what about the links on undo??
		// remove all links related to this shape:
		if (s.getPage() != null) {
			s.getPage().removeLinks(s);
			s.getPage().removeDeadLinks();
		}

		// sort children: means create new reading order without the deleted shape
		if (s.getParentShape() != null) {
			s.getParentShape().sortChildren(true);
		}

		return true;
	}

	/**
	 * Currently only implemented for TrpWordType, TrpTextLineType and
	 * TrpTextRegionType
	 * 
	 * @param shape    The shape for which its neighbor shape shall be found.
	 * @param previous True to get the previous sibling, false to get the next one.
	 * @param wrap     True to wrap the search, i.e. to return the first shape if
	 *                 the end was reached and vice versa.
	 * @return
	 */
	public static ITrpShapeType getNeighborShape(ITrpShapeType shape, boolean previous, boolean wrap) {
		List<? extends ITrpShapeType> shapes = null;
		if (shape instanceof TrpWordType) {
			shapes = shape.getPage().getWords();
		} else if (shape instanceof TrpTextLineType) {
			shapes = shape.getPage().getLines();
		} else if (shape instanceof TrpTextRegionType) {
			shapes = shape.getPage().getTextRegions(false);
		}

		return CoreUtils.getNeighborElement(shapes, shape, previous, wrap);
	}

	/**
	 * Tries to get the (parent) text region of the specified shape; returns null if
	 * not found
	 */
	public static TrpTextRegionType getTextRegion(ITrpShapeType st) {
		if (RegionTypeUtil.isBaseline(st)) {
			TrpBaselineType bl = (TrpBaselineType) st;
			if (bl.getLine() != null) {
				return bl.getLine().getRegion();
			}
		} else if (RegionTypeUtil.isLine(st)) {
			return ((TrpTextLineType) st).getRegion();
		} else if (RegionTypeUtil.isWord(st)) {
			TrpWordType w = (TrpWordType) st;
			if (w.getLine() != null) {
				return w.getLine().getRegion();
			}
		}

		return null;
	}

	public static TrpTextLineType getLine(ITrpShapeType st) {
		if (RegionTypeUtil.isBaseline(st))
			return ((TrpBaselineType) st).getLine();
		else if (RegionTypeUtil.isLine(st)) {
			return (TrpTextLineType) st;
		} else if (RegionTypeUtil.isWord(st)) {
			return ((TrpWordType) st).getLine();
		}

		return null;
	}

	public static ITrpShapeType getParentShape(ITrpShapeType st) {
		if (st != null && st.getParent() instanceof ITrpShapeType) {
			return (ITrpShapeType) st.getParent();
		} else {
			return null;
		}
	}

	public static TrpBaselineType getBaseline(ITrpShapeType st) {
		if (RegionTypeUtil.isBaseline(st))
			return (TrpBaselineType) st;
		else if (RegionTypeUtil.isLine(st)) {
			return (TrpBaselineType) ((TrpTextLineType) st).getBaseline();
		} else if (RegionTypeUtil.isWord(st)) {
			return (TrpBaselineType) ((TrpWordType) st).getLine().getBaseline();
		}

		return null;
	}

	public static void setUnicodeText(ITrpShapeType shape, String unicode, Object who) {
		logger.trace("setting unicode text in " + shape.getName() + ", id: " + shape.getId() + ", text: " + unicode);
		int lBefore = shape.getUnicodeText().length();
		TextEquivType te = new TextEquivType();
		te.setUnicode(unicode);
		shape.setTextEquiv(te);

		shape.getCustomTagList().onTextEdited(0, lBefore, unicode);
		shape.getObservable().setChangedAndNotifyObservers(new TrpTextChangedEvent(who, 0, lBefore, unicode));
	}

	public static void editUnicodeText(ITrpShapeType shape, int start, int end, String replacement, Object who) {
		logger.trace("editing unicode text in " + shape.getName() + ", id: " + shape.getId() + ", start/end: " + start
				+ "/" + end + ", text: " + replacement);
		StringBuilder sb = new StringBuilder(shape.getUnicodeText());
		sb.replace(start, end, replacement);
		String unicode = sb.toString();
		if (shape instanceof TrpWordType && unicode.equals(TrpWordType.EMPTY_WORD_FILL)) {
			unicode = "";
			replacement = "";
		}

		TextEquivType te = new TextEquivType();
		te.setUnicode(unicode);
		shape.setTextEquiv(te);

		SebisStopWatch.SW.start();
		shape.getCustomTagList().onTextEdited(start, end, replacement);
		SebisStopWatch.SW.stop(true, "onTextEdited: ", logger);

		shape.getObservable().setChangedAndNotifyObservers(new TrpTextChangedEvent(who, start, end, replacement));
	}

	/**
	 * Reverses the text of given shape -> used for RTL <-> LTR purposes (e.g.
	 * Arabic script but Latin transcription)<br/>
	 * The method also reverses existing Tag-Indexes.
	 * 
	 * @param who           Object who is calling this method -> used for
	 *                      notifications -> can be null
	 * @param excludeDigits Exclude digits from reversion?
	 * @param tagExceptions Exclude all of the given tags from reversion
	 * 
	 */
	public static void reverseUnicodeText(ITrpShapeType shape, Object who, boolean excludeDigits,
			String... tagExceptions) {
		String str = shape.getUnicodeText();
		if (StringUtils.isEmpty(str)) {
			return;
		}

		logger.debug("reversing unicode text in " + shape.getName() + ", id: " + shape.getId() + ", excludeDigits: "
				+ excludeDigits + ", tagExceptions" + tagExceptions);
		StringTokenizer st = new StringTokenizer(str, " \t\n\r\f", true);
		int i = 0;
		String strOut = "";

		List<IntRange> notReversedRanges = new ArrayList<>();
		while (st.hasMoreTokens()) {
			String token = st.nextToken();
			if (StringUtils.isEmpty(token)) {
				continue;
			}
			boolean doReverse = true;
			if (excludeDigits && NumberUtils.isNumber(token)) {
				doReverse = false;
			} else if (tagExceptions != null && tagExceptions.length > 0) { // find out if this token is tagged as an
																			// exception
				for (String tagName : tagExceptions) {
					try {
						// this checks if there is an overlap of *any* tag in given token
//						List<CustomTag> tags = shape.getCustomTagList().getOverlappingTags(tagName, i, token.length());
//						if (tags!=null && tags.size()>0) {
//							logger.debug("overlapping tags: "+tags);
//							doReverse=false;
//							break;
//						}

						// checks if there is an overlapping tag at the exact position
//						CustomTag tag = shape.getCustomTagList().getOverlappingTag(tagName, i);
//						if (tag != null) {
//							doReverse = false;
//							break;
//						}
						List<CustomTag> tags = shape.getCustomTagList().getOverlappingTags(tagName, i);
						if (!CoreUtils.isEmpty(tags)) {
							doReverse = false;
							break;
						}
					} catch (Exception e) {
						logger.error("Error parsing overlapping tag at position " + i + " while reversing: "
								+ e.getMessage(), e);
					}
				}
			}
			if (doReverse) {
				token = StringUtils.reverse(token);
			} else {
				notReversedRanges.add(new IntRange(i, token.length()));
			}
			strOut = token + strOut;
			i += token.length();
		}
		if (str.length() != strOut.length()) {
			String msg = "Reversed string size unqeual to input string size (" + str.length() + "/" + strOut.length()
					+ ")";
			logger.error(msg);
			throw new IllegalStateException(msg);
		}

		// reverse tag indices - also respects token, where no reversion has been done
		// NOTE: this still does *not* work when tags are overlapping a reversed *and* a
		// not-reversed token!
		for (CustomTag tag : shape.getCustomTagList().getIndexedTags()) {
			IntRange irNotRev = null;
			for (IntRange ir : notReversedRanges) {
				if (ir.getOverlapType(tag.getOffset(), 0) == OverlapType.INSIDE) {
					irNotRev = ir;
					break;
				}
			}
			if (irNotRev != null) { // FIXME is this correct now...??
				tag.setOffset(strOut.length() - irNotRev.offset - tag.getLength());
			} else {
				tag.setOffset(strOut.length() - tag.getOffset() - tag.getLength());
			}
		}

		TextEquivType te = shape.getTextEquiv();
		if (te == null || te.getUnicode() == null) {
			throw new IllegalStateException("Could not retrieve TextEquivType!");
		}
		te.setUnicode(strOut);
		shape.getObservable().setChangedAndNotifyObservers(new TrpTextChangedEvent(who, 0, strOut.length(), strOut));
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static void applyReadingOrderFromCoordinates(List<? extends ITrpShapeType> shapes, boolean fireEvents,
			boolean deleteReadingOrder, boolean recursive) {
		// sort with coordinates
//		Collections.sort(shapes, new TrpElementCoordinatesComparator());
		TrpShapeTypeUtils.sortShapesByCoordinates(shapes, true);

		int i = 0;
		for (ITrpShapeType st : shapes) {

			List<? extends ITrpShapeType> children = st.getChildren(false);
			if (st instanceof TrpTextLineType) {
				TrpTextLineType tl = (TrpTextLineType) st;
				children = tl.getChildrenWithoutBaseline();
			}

			if (!CoreUtils.isEmpty(children)) {
				applyReadingOrderFromCoordinates(children, fireEvents, deleteReadingOrder, recursive);
			}

			if (!deleteReadingOrder)
				logger.trace("setting reading order " + i + " to: " + st.getId());
			else
				logger.trace("deleting reading order from: " + st.getId());

			boolean wasObserverActiveBefore = st.getObservable().isActive();
			if (!fireEvents)
				st.getObservable().setActive(false);

			if (deleteReadingOrder)
				st.setReadingOrder(null, st);
			else
				st.setReadingOrder(i++, st);

			if (!fireEvents)
				st.getObservable().setActive(wasObserverActiveBefore);
		}
	}
	
	public static List<? extends ITrpShapeType> filterShapesByEmptiness(List<? extends ITrpShapeType> shapes) {
		return shapes.stream().filter(s -> {
			try {
				return (!s.getChildren(true).isEmpty() || !(s instanceof TextRegionType));
			} catch (Exception e) {
				logger.error("Error getting children of shape: " + e.getMessage(), e);
				return false;
			}
		}).collect(Collectors.toList());
	}

	public static List<? extends ITrpShapeType> filterShapesByAreaThreshold(List<? extends ITrpShapeType> shapes,
			double thresholdArea) {
		return shapes.stream().filter(s -> {
			try {
				double area = PointStrUtils.getArea(s.getCoordinates());
				logger.debug("id = " + s.getId() + ", polygon area = " + area + ", thresholdArea = " + thresholdArea);
				return area >= thresholdArea;
			} catch (Exception e) {
				logger.error("Error calculating area of shape: " + e.getMessage(), e);
				return false;
			}
		}).collect(Collectors.toList());
	}

	public static List<? extends ITrpShapeType> filterShapesByRegionThreshold(List<? extends ITrpShapeType> shapes,
			double thresholdRegion, ITrpShapeType region) {
		return shapes.stream().filter(s -> {
			try {
				double lineWidth = PointStrUtils.getBoundingBox(s.getCoordinates()).getWidth();
				double regionWidth = PointStrUtils.getBoundingBox(region.getCoordinates()).getWidth();
				double treshhold = regionWidth * thresholdRegion;
				logger.trace("id = " + s.getId() + ", shape width = " + lineWidth + ", threshold = " + treshhold);
				return lineWidth >= treshhold;
			} catch (Exception e) {
				logger.error("Error calculating  width of shape: " + e.getMessage(), e);
				return false;
			}
		}).collect(Collectors.toList());
	}
	
	public static List<? extends ITrpShapeType> filterShapesByOverlap(List<? extends ITrpShapeType> shapes, double thresholdRegion) {
		return shapes.stream().filter(s -> {
			try {
				//ICanvasShape currCanvas = (ICanvasShape) s.getData();
				
				for (ITrpShapeType trpShape : shapes) {
					if (s.getId() == trpShape.getId() || s.getStructure() != trpShape.getStructure())
						continue;
//					ICanvasShape otherCanvas = (ICanvasShape) trpShape.getData();
//					double intersection = currCanvas.intersectionArea(otherCanvas);
					Rectangle2D intersectRect = s.getBoundingBox().createIntersection(trpShape.getBoundingBox());
					if (intersectRect.isEmpty()) {
						continue;
					}
					double sizeIntersect = intersectRect.getHeight()*intersectRect.getWidth();
					double theArea = s.getBoundingBox().getHeight()*s.getBoundingBox().getWidth();
					//logger.debug("intersection area: " + sizeIntersect);
					//logger.debug("area of shape: " + theArea);
					logger.debug("overlap percentage: " + (sizeIntersect/theArea));
					//a shape has more then 95% of intersection with another region
					if ( (sizeIntersect/theArea) > thresholdRegion) {
						return false;
					}	
				}
				return true;
			} catch (Exception e) {
				logger.error("Error calculating intersection of shape: " + e.getMessage(), e);
				return false;
			}
		}).collect(Collectors.toList());
	}
	
	public static List<List<ITrpShapeType>> mergeShapesInRow(List<? extends ITrpShapeType> shapes, double thresholdRow,
			ITrpShapeType region) {

		List<List<ITrpShapeType>> result = new ArrayList();

		boolean alreadyProcessed = false;

		for (ITrpShapeType shape : shapes) {
			alreadyProcessed = false;

			// check if the next shape was already added to another list
			for (List<?> formerList : result) {
				// logger.debug("former list found: size is: " + formerList.size());
				if (formerList.contains(shape)) {
					alreadyProcessed = true;
					break;
				}
			}

			if (!alreadyProcessed) {
				logger.debug("this shape is not already processed: " + shape.getId());
				double firstY = PointStrUtils.getBoundingBox(shape.getCoordinates()).getY();
				List<ITrpShapeType> currList = new ArrayList<ITrpShapeType>();
				currList = addShapesOfSameHeigth(shapes, firstY, thresholdRow);
				result.add(currList);
			}
//			else {
//				logger.debug("already processed: " + shape.getId() );
//			}

		}

		return result;

	}

	private static List<ITrpShapeType> addShapesOfSameHeigth(List<? extends ITrpShapeType> shapes, double firstY,
			double thresholdRow) {

		List<ITrpShapeType> newList = shapes.stream().filter(x -> {
			return Math.abs(PointStrUtils.getBoundingBox(x.getCoordinates()).getY() - firstY) < thresholdRow;
		}).collect(Collectors.toList());

		return newList;

	}
	
	private static List<? extends ITrpShapeType> addShapesOfSameY(List<? extends ITrpShapeType> shapes, ITrpShapeType startShape) {
		
		List<ITrpShapeType> newList = new ArrayList<ITrpShapeType>();
		for (int i = 0; i<shapes.size(); i++) {
			Rectangle currRect = PointStrUtils.getBoundingBox(shapes.get(i).getCoordinates());
			Rectangle startRect = PointStrUtils.getBoundingBox(startShape.getCoordinates());
			double startRectCenterX = startRect.getCenterX();
			double startRectCenterY = startRect.getCenterY();
			double currRectCenterY = currRect.getCenterY();
			double currRectCenterX = currRect.getCenterX();
//			logger.debug("startRectCenterX " + startRectCenterX);
//			logger.debug("currRectCenterX " + currRectCenterX);
//			logger.debug("currRectCenterY " + currRectCenterY);

			if(startRect.contains(startRectCenterX+10, currRectCenterY) || currRect.contains(currRectCenterX, startRectCenterY)) {
				newList.add(shapes.get(i));
				if(startRect.getHeight()<currRect.getHeight()) {
					//startShapeChanged -> redo everything
					startShape = shapes.get(i);
					return (List<ITrpShapeType>) addShapesOfSameY(shapes, startShape);
				}
			}
		}
		return newList;
		
	}

	public static void convertDifferences(ITrpShapeType shape1, ITrpShapeType shape2) {

		String str1 = shape1.getUnicodeText();
		String str2 = shape2.getUnicodeText();
		if (StringUtils.isEmpty(str1) || StringUtils.isEmpty(str2)) {
			return;
		}
		
		//because in Noscemus the word gets followed directly by : instead of having empty char in between
		if (str1.contains(" : ")) {
			str1 = str1.replace(" : ", ": ");
			logger.debug(" :  replaced " + str1);
			shape1.setUnicodeText(str1, null);
		}

		String[] strings1 = str1.split("\\s");
		String[] strings2 = str2.split("\\s");

		if (strings1.length != strings2.length) {
			logger.debug("lines contain not same number of words!!");
			return;
		}
		int tmpOffset = -1;

		for (int i = 0; i < strings1.length; i++) {
			if (strings1[i].contentEquals(strings2[i])) {
				continue;
			} else {
				tmpOffset = str1.indexOf(strings1[i], tmpOffset + 1); // avoid duplicates
				//logger.debug("diff found " + strings1[i] + " at offset " + tmpOffset);
				
				//int distance = calculate(strings1[i], strings1[i]);

				CustomTag ct;
				try {
					ct = CustomTagFactory.create("abbrev", tmpOffset, strings1[i].length());
					ct.setAttribute("expansion", strings2[i], true);
					shape1.getCustomTagList().addOrMergeTag(ct, null);
					CustomTagUtil.writeCustomTagListToCustomTag(shape1);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		}
	}
	
	static int calculate(String x, String y) {
	    int[][] dp = new int[x.length() + 1][y.length() + 1];

	    for (int i = 0; i <= x.length(); i++) {
	        for (int j = 0; j <= y.length(); j++) {
	            if (i == 0) {
	                dp[i][j] = j;
	            }
	            else if (j == 0) {
	                dp[i][j] = i;
	            }
	            else {
	                dp[i][j] = min(dp[i - 1][j - 1] 
	                 + costOfSubstitution(x.charAt(i - 1), y.charAt(j - 1)), 
	                  dp[i - 1][j] + 1, 
	                  dp[i][j - 1] + 1);
	            }
	        }
	    }

	    return dp[x.length()][y.length()];
	}
	
    public static int costOfSubstitution(char a, char b) {
        return a == b ? 0 : 1;
    }
    
    public static int min(int... numbers) {
        return Arrays.stream(numbers)
          .min().orElse(Integer.MAX_VALUE);
    }

	public static void replaceWordEndings(ITrpShapeType shape, File mappings) {

		String str = shape.getUnicodeText();
		if (StringUtils.isEmpty(str)) {
			return;
		}

		// logger.debug("replace word endings "+shape.getName()+", id: "+shape.getId());
		StringTokenizer st = new StringTokenizer(str, " \t\n\r\f", true);
		int i = 0;
		String strOut = "";

		while (st.hasMoreTokens()) {
			String token = st.nextToken();
			if (StringUtils.isEmpty(token)) {
				continue;
			}
			String newToken = "";
			Scanner scanner;
			try {
				scanner = new Scanner(mappings);
				while (scanner.hasNextLine()) {
					String expansion = "";
					String specialChar = "";
					String place = "";

					int counter = 0;
					String line = scanner.nextLine();
					// System.out.println("line " + line+"\n");
					Scanner scanLine = new Scanner(line);
					scanLine.useDelimiter(",");
					while (scanLine.hasNext()) {
						String tmp = scanLine.next();

						if (counter == 0) {
							expansion = tmp;
						} else if (counter == 1) {
							specialChar = tmp;
						} else if (counter == 2) {
							place = tmp;
						}
						counter++;

					}

					// logger.debug("place " + place+"\n");

					if (place.contentEquals("full") && token.contentEquals(expansion)) {
						newToken = token.replace(expansion, "$" + specialChar + "$" + expansion + "$");
						logger.debug("replacement 'full': " + newToken);
					}
					if (place.contentEquals("postfix")
							&& (token.endsWith(expansion) && !token.contentEquals(expansion))) {
						newToken = token.replace(expansion, "$" + specialChar + "$" + expansion + "$");
						logger.debug("replacement 'postfix': " + newToken);
					}
					if (place.contentEquals("prefix")
							&& (token.startsWith(expansion) && !token.contentEquals(expansion))) {
						newToken = token.replace(expansion, "$" + specialChar + "$" + expansion + "$");
						logger.debug("replacement 'prefix': " + newToken);
					}
					if (place.contentEquals("middle") && (!token.startsWith(expansion) && token.contains(expansion))) {
						newToken = token.replace(expansion, "$" + specialChar + "$" + expansion + "$");
						logger.debug("replacement 'middle': " + newToken);
					}
				}

				if (newToken != "") {
					strOut += newToken;
				} else {
					strOut += token;
				}

				scanner.close();

			} catch (FileNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			TextEquivType te = shape.getTextEquiv();
			if (te == null || te.getUnicode() == null) {
				throw new IllegalStateException("Could not retrieve TextEquivType!");
			}
			te.setUnicode(strOut);
			shape.getObservable()
					.setChangedAndNotifyObservers(new TrpTextChangedEvent(null, 0, strOut.length(), strOut));
		}
//        logger.debug("the old line string: " + str);
//        logger.debug("the new line string: " + strOut);
//        try {
//			System.in.read();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
	}
	
	/*
	 * if shape is line -> use tokenizer to get words
	 * searchString should be equal to that word
	 */
	public static ITrpShapeType searchReplace(ITrpShapeType shape, String searchString, String replaceString) {

		String str = shape.getUnicodeText();
		if (StringUtils.isEmpty(str)) {
			return null;
		}
		logger.debug("initial shape text: " + str);
		//logger.debug("replace chars "+str);
		StringTokenizer st = new StringTokenizer(str, " \t\n\r\f", false);
		int offset = 0;
		String strOut = "";

		while (st.hasMoreTokens()) {
			String token = st.nextToken();
			if (StringUtils.isBlank(token)) {
				continue;
			}
			String newToken = "";
			if (token.equals(searchString)) {
				//logger.debug("token replace " + searchString + " with " + replaceString);
				newToken = token.replace(searchString, replaceString);
				//logger.debug("newToken  " + newToken);
			}
			
			if (StringUtils.isNotBlank(newToken)) {
				strOut += newToken;
			} else {
				strOut += token;
			}
			strOut += " ";

		}
		
		/*
		 * resulting string is smaller initial string
		 * tags may go beyond that length 
		 * delete them or correct the length
		 * ToDo: try to correct the tags position so that the tags are correct after replacements
		 * -> correct offsets for tags after the replacement
		 */
		if (strOut.length() < str.length()) {
			CustomTagList tags = shape.getCustomTagList();
			for (CustomTag tag : tags.getIndexedTags()) {
				if (tag.getOffset()+tag.getLength()>strOut.length()) {
					//if offset is beyond length of string -> delete
					if (tag.getOffset() > strOut.length()) {
						tags.deleteTagAndContinuations(tag);
					}
					//otherwise correct the length to the end
					else {
						int sub = (tag.getOffset()+tag.getLength()) - strOut.length();
						tag.setLength(tag.getLength()-sub);
					}
				}
				
			}
		}
		
		strOut = strOut.trim();
		logger.debug("replaced shape text: " + strOut);
		TextEquivType te = shape.getTextEquiv();
		if (te == null || te.getUnicode() == null) {
			throw new IllegalStateException("Could not retrieve TextEquivType!");
		}
		te.setUnicode(strOut);
		return shape;
	}
	
	/*
	 * 
	 * hand over custom tag (inclusive properties) as cssString
	 */
	public static ITrpShapeType searchAnnotate(ITrpShapeType shape, String searchString, String cssString) {

		String str = shape.getUnicodeText();
		if (StringUtils.isEmpty(str)) {
			return null;
		}
		logger.debug("initial shape text: " + str);

		int offset = 0;
		String strOut = "";
		
		while (offset <= str.length()) {
			offset = str.indexOf(searchString, offset);
			if (offset == -1) {
				return shape;
			}
			
			if (str.substring(offset).startsWith(searchString)) {
				int endIdx = offset + searchString.length();
				int length = endIdx - offset;
				CustomTag neuerTag;
				try {
					neuerTag = CustomTagUtil.parseSingleCustomTag(cssString);
					neuerTag.setOffset(offset);
					neuerTag.setLength(length);
					offset = endIdx;
					//shape.getCustomTagList().getIndexedTags().add(neuerTag);
					logger.debug("tag created: " + neuerTag.toString());
					shape.getCustomTagList().addOrMergeTag(neuerTag, null);
					//CustomTagUtil.writeCustomTagListToCustomTag(shape);
				} catch (InstantiationException | IllegalAccessException | IllegalArgumentException
						| InvocationTargetException | ClassNotFoundException | NoSuchMethodException | ParseException
						| IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return shape;
	}
	
	public static void replaceChars_linebased(ITrpShapeType shape, File mappings) {

		String str = shape.getUnicodeText();
		if (StringUtils.isEmpty(str)) {
			return;
		}
		String strOut = str;

		String newToken = "";
		Scanner scanner;
		try {
			scanner = new Scanner(mappings);
			while (scanner.hasNextLine()) {
				String searchString = "";
				String replaceString = "";

				int counter = 0;
				String line = scanner.nextLine();
				
				if (StringUtils.isBlank(line)) {
					//logger.debug("line is blank -> continue; replacements must contain ,");
					continue;
				}
				Scanner scanLine = new Scanner(line);
				scanLine.useDelimiter(",");
				while (scanLine.hasNext()) {
					String tmp = scanLine.next();
					if (counter == 0) {
						searchString = tmp;
					} else if (counter == 1) {
						replaceString = tmp;
					}
					counter++;

				}
				
				if (str.contains(searchString)) {
					//logger.debug("token replace " + searchString + " with " + replaceString);
					strOut = strOut.replace(searchString, replaceString);
					//logger.debug("newToken  " + newToken);
				}

			}

			scanner.close();

		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		TextEquivType te = shape.getTextEquiv();
		if (te == null || te.getUnicode() == null) {
			throw new IllegalStateException("Could not retrieve TextEquivType!");
		}
		te.setUnicode(strOut);
		shape.getObservable().setChangedAndNotifyObservers(new TrpTextChangedEvent(null, 0, strOut.length(), strOut));
		
	}
	
	public static void replaceChars(ITrpShapeType shape, File mappings) {

		String str = shape.getUnicodeText();
		if (StringUtils.isEmpty(str)) {
			return;
		}

		//logger.debug("replace chars "+str);
		StringTokenizer st = new StringTokenizer(str, " \t\n\r\f", true);
		int i = 0;
		String strOut = "";

		while (st.hasMoreTokens()) {
			String token = st.nextToken();
			if (StringUtils.isEmpty(token)) {
				continue;
			}
			String newToken = "";
			Scanner scanner;
			try {
				scanner = new Scanner(mappings);
				while (scanner.hasNextLine()) {
					String searchString = "";
					String replaceString = "";

					int counter = 0;
					String line = scanner.nextLine();
					
					if (StringUtils.isBlank(line)) {
						//logger.debug("line is blank -> continue; replacements must contain ,");
						continue;
					}
		
//					Scanner scanLine = new Scanner(line);
//					scanLine.useDelimiter(",");
//					while (scanLine.hasNext()) {
//						String tmp = scanLine.next();
//						if (counter == 0) {
//							searchString = tmp;
//						} else if (counter == 1) {
//							replaceString = tmp;
//						}
//						counter++;
//
//					}
					
					/*
					 * with this also ',' can be replaced - not with solution above
					 */
					searchString=line.substring(0, line.lastIndexOf(","));
					replaceString=line.substring(line.lastIndexOf(",")+1);
					
					if (!searchString.isEmpty() && !replaceString.isEmpty() && token.contains(searchString)) {
						//logger.debug("token replace " + searchString + " with " + replaceString);
						newToken = token.replace(searchString, replaceString);
						//logger.debug("newToken  " + newToken);
					}

				}

				if (newToken != "") {
					strOut += newToken;
				} else {
					strOut += token;
				}

				scanner.close();

			} catch (FileNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			TextEquivType te = shape.getTextEquiv();
			if (te == null || te.getUnicode() == null) {
				throw new IllegalStateException("Could not retrieve TextEquivType!");
			}
			te.setUnicode(strOut);
			shape.getObservable()
					.setChangedAndNotifyObservers(new TrpTextChangedEvent(null, 0, strOut.length(), strOut));
		}
	}

	/*
	 * convert custom tags into 'trainable' strings first start with abbrevs
	 * add all kinds of custom tags - use different special characters to distinct
	 * between different custom tags
	 * 
	 * we train for abbrevs in long form only the expansion -> works best and is the the only 'predefined' property
	 *
	 */
	public static void convertFromCustomTag(ITrpShapeType shape, CustomTag ct, String label4Tag, String propLabel, boolean trainProperties, boolean useShortForm, boolean trainAbbrevs) {

		String str = shape.getUnicodeText();
		if (StringUtils.isEmpty(str)) {
			return;
		}

		/*
		 * if it is an abbrev and the expansion is set we train it in the form tagvalue$expansion$ -> 
		 * otherwise we train it like the other tags with the 'getAttributeCssStr' (propertyName + ":" + propertyValue + ";")
		 * advantage is a smaller string to train and therefore it works better
		 */
		if (ct instanceof AbbrevTag && trainAbbrevs) {
			/*
			 * for abbrevs we set the expansion in long form -> works much better then with label
			 * reason: for all differnet expansions we have extra label -> during recognition many labels have no mapping to a expansion or a mapping to a wrong expansion
			 */
			if (propLabel != null && useShortForm) {
				shape.editUnicodeText((ct.getOffset() + ct.getLength()), (ct.getOffset() + ct.getLength()),
						label4Tag + propLabel, null);
			} else {
				shape.editUnicodeText((ct.getOffset() + ct.getLength()), (ct.getOffset() + ct.getLength()),
						label4Tag + ct.getAttributeValue("expansion") + label4Tag, null);

			}

		}
		/*
		 * we try to train custom tag types (e.g. persons, places, user-defined,...)
		 * each tag type gets a single 'special' character for identification in
		 * training 
		 */
		else {		
			// get the special charater ajd add before the string starts
			if (propLabel != null && useShortForm) {
				shape.editUnicodeText((ct.getOffset() + ct.getLength()), (ct.getOffset() + ct.getLength()),
						label4Tag + propLabel, null);
			}
			else {
				if (!useShortForm && trainProperties) {
					String ctTrainString = label4Tag + ct.getAttributeCssStr(true) + label4Tag;
					ctTrainString = ctTrainString.replaceAll("\\s", "");
					logger.debug("train the custom tag properties: " + ctTrainString);
					shape.editUnicodeText(ct.getOffset()+ct.getLength(), ct.getOffset()+ct.getLength(), ctTrainString, null);
				}
				else {
					shape.editUnicodeText(ct.getOffset()+ct.getLength(), ct.getOffset()+ct.getLength(), label4Tag, null);
				}
				
			}
			
		}
		shape.editUnicodeText(ct.getOffset(), ct.getOffset(), label4Tag, null);

	}
	
	/*
	 * convert a 'special' string into a custom tag first start with abbrevs ToDo:
	 * add all kinds of custom tags - use different special characters to distinct
	 * between different custom tags further ToDo: add all kinds of properties ->
	 * low priority since not be certain that this will work
	 * 
	 * CssSyntaxTag: parseCssAttributes zum die Attribute wieder zurück zu wandeln
	 * 
	 * first get the words to avoid wrong abbrev findings
	 *
	 */
	public static void convertIntoCustomTag(ITrpShapeType shape, Map<String, Object> tagMap, Map<String, Object> propMap, boolean useShortForm) {
		try {
			String str = shape.getUnicodeText();
			if (StringUtils.isEmpty(str)) {
				logger.debug("line string is empty: " + shape.getId());
				return;
			}
		
			/*
			 * we use symbol to mark the tag type, e.g. abbrev: $abbrev$expansion$
			 * each tagname has its own symbol
			 * 
			 */
			
			int offset = 0;
			
			List<CustomTag> createdTags = new ArrayList<CustomTag>();
			String finalString = "";

			/*
			 * tagMap contains the symbol for each different tagname
			 */
			for (String tagname : tagMap.keySet()){
				
				int reducedSize = 0;
				offset = 0;
				
				String symbol = (String) tagMap.get(tagname);
				//logger.debug("symbol for tagname: " + tagname + " is " + symbol);
				
				String replacedString = "";
				str = shape.getUnicodeText();
				
				if (!str.contains(symbol)) {
					continue;
				}

		    	/*
		    	 * abbrev must not be a complete word but part of a word -> either beginning, in the middle or at the end
		    	 * preAbbrev: remember substring before the abbrev starts
		    	 * postAbbrev: remember substring after the abbrev expansion ends
		    	 */
				if (symbol.contentEquals("$")){
					
					//case abbrev: use word split - otherwise the abbrev could contain several words
					String[] splitWords = str.split("\\s");
					
					//go through all words in this shape and handle the abbrevs
					for (int j = 0;j<splitWords.length;j++) {
						
						String buffer = splitWords[j];
						String abbrevString = splitWords[j];
						String preAbbrev = "";
						String postAbbrev = "";
						
						long count = buffer.chars().filter(ch -> ch == '$').count();
						logger.debug("count $: " + count);
						
						/*
						 * abbrev starts in the middle of the word
						 * correct abbrev contains exactly 3 '$' symbols in the recogniced word
						 * $abbrevString$expansionString$
						 * --> this is done with check 'count==3'
						 * but: we want to rescue abbrevs containing only 2 '$' as well
						 * examples:
						 * abbrevString$expansionString$ or
						 * $abbrevString$expansionString
						 * then we have to start from the beginning
						 * 
						 */
						if (buffer.contains(symbol) && !buffer.startsWith("$") && count==3) {
							abbrevString = buffer.substring(buffer.indexOf("$"));
						}
						
						/*
						 * with using limit=4 as argument 'empty' expansions are also considered -> otherwise empty split is not in result
						 */
						String[] splitStringArray = abbrevString.split("\\"+symbol,4);
						
						for (int a = 0;a<splitStringArray.length;a++) {
							logger.debug("current split to find abbrev: " + splitStringArray[a]);
						}
						
						
						String expansion = "";
	
						if (splitStringArray.length > 2) {
							
							/*
							 * abbrev starts in the middle of the word
							 * we need to remember substrings before or after the abbrev
							 */
							if (buffer.contains(symbol) && !buffer.startsWith("$") && count==3) {
								preAbbrev = buffer.substring(0, buffer.indexOf("$"));
								offset += preAbbrev.length();
							}
							
							if (buffer.length() > buffer.lastIndexOf("$") && count==3) {
								postAbbrev = buffer.substring(buffer.lastIndexOf("$")+1);
							}
							
							logger.debug("current word to find abbrev: " + buffer);
							for (int i = 0; i < splitStringArray.length; i++) {
								
								String tmp = splitStringArray[i];
	
								if (tmp.trim().isEmpty()) {
									++i;
									if (i>=splitStringArray.length) {
										replacedString += tmp;
										break;
									}
								}
								
								String abbrev = splitStringArray[i];
								++i;
								if (i>=splitStringArray.length) {
									break;
								}
								
								CustomTag ct = CustomTagFactory.create("abbrev", offset, abbrev.length());
								
								if (propMap != null && !propMap.isEmpty() && useShortForm) {
									
									String propLabel = splitStringArray[i];
									searchAndAddAttributes(propMap, propLabel, ct, buffer, reducedSize);
									
								}
								else {
									expansion = splitStringArray[i];
									ct.setAttribute("expansion", expansion, true);
									logger.debug("abbrev found: " + abbrev);
									logger.debug("expansion found: " + expansion);
								}
								
								++i;
								
								/*
								 * adopt the offset/length of previously created tags if necessary, e.g. they overlap
								 * important for different tag types
								 */
								reducedSize = buffer.length()-(abbrev.length());
								adaptCreatedTags(createdTags, reducedSize, offset, abbrev.length());

								createdTags.add(ct);
								
								logger.debug("offset for curr tag: " + offset);
								
								offset += abbrev.length()+postAbbrev.length()+1;
								
								logger.debug("offset for next tag: " + offset);
	
								replacedString += preAbbrev + abbrev + postAbbrev + " ";
								//System.in.read();
								finalString =replacedString;

							}
						}
						else {
							/*
							 * not matching condition that at least two symbols mark a tag
							 * remove the symbol from the text
							 */
							int intialSize = buffer.length();
							buffer = buffer.replaceAll("\\"+symbol, "");
							reducedSize = intialSize-buffer.length();
							
//							logger.debug("intialSize " + intialSize);
//							logger.debug("reducedSize " + reducedSize);
//							logger.debug("offset " + offset);
//							logger.debug("buffer.length() " + buffer.length());
							
							adaptCreatedTags(createdTags, reducedSize, offset, 0);
							
							replacedString += buffer + " ";
							offset += buffer.length()+1;
							
							finalString =replacedString;
						}
	
					}

					logger.debug("recogniced text with encrypted tags: " + str);
					logger.debug("edited text as in image " + replacedString.trim());
					
					shape.setUnicodeText(replacedString, null);
	
				}
				/*
				 * all other tags are handled here
				 */
				else {
					/*
					 * 
					 * ToDO: propMap is used to get the property of the used encoding
					 * first approach: only train and create the pure tag name
					 * short form was not really useful
					 * long form needs to be tested but NER will be a service of its own
					 */
					
					String buffer = str;
					
					logger.debug("buffer: " + buffer);
					logger.debug("symbol: " + symbol);
					
					while (buffer.contains(symbol) && symbol != ""){
						int start = buffer.indexOf(symbol);
						int middle = (start == -1) ? -1 : buffer.indexOf(symbol, start+1);
						int end = (middle == -1) ? -1 : buffer.indexOf(symbol, middle+1);
	
						if (start != -1 && middle != -1 && end != -1) {
							String value = buffer.substring(start+1, middle);
							logger.debug("new custom tag found: " + tagname + " with value: " + value + " and length " + value.length());
							offset = start;

							reducedSize = 2;
							
							CustomTag ct = CustomTagFactory.create(tagname, offset, value.length());
							
							if (propMap != null && !propMap.isEmpty() && useShortForm) {
								int startProp = end+1;
								int endProp = Math.min(end+3, buffer.length());
								String propLabel = buffer.substring(startProp, endProp);
								logger.debug("propLabel: " + propLabel);
	//							for (String prop : propMap.keySet()) {
	//								logger.debug("next key in propMap: " + prop);
	//								logger.debug("next value in propMap: " + propMap.get(prop));
	//							}
								searchAndAddAttributes(propMap, propLabel, ct, buffer, reducedSize);
							}
							/*
							 * the long form of trained properties
							 */
							else {
								
								String ccsProps = buffer.substring(middle+1, end);
								logger.debug("ccsProps in long form " + ccsProps);
								
								//here we use a short form for some predefined text-style attributes
								if (ct instanceof TextStyleTag) {
									TextStyleTag tst = (TextStyleTag) ct;
									tst.setAttributesFromCssStrSpecial(ccsProps);
								}
								else {
									ct.setAttributesFromCssStr(ccsProps);
								}
								
								reducedSize += ccsProps.length()+1;
								
								buffer = buffer.replaceFirst("\\"+symbol+ccsProps+"\\"+symbol, "");
								
								//buffer = buffer.replaceFirst("\\"+symbol, "");
							}
							
							//buffer = buffer.replaceFirst("\\"+symbol, "");
							buffer = buffer.replaceFirst("\\"+symbol, "");

							adaptCreatedTags(createdTags, reducedSize, offset, value.length());
							
							createdTags.add(ct);
	
							//offset += value.length();
						}
						//not enough symbols
						else {
							
							int countMissing = 0;
							if (start == -1) {
								countMissing++;
							}
							if (middle == -1) {
								countMissing++;
							}
							if (end == -1) {
								countMissing++;
							}
							
							int intialSize = buffer.length();
							int tmpOffset = buffer.indexOf(symbol);
							while (countMissing > 0) {
								buffer = buffer.replaceFirst("\\"+symbol, "");
								countMissing--;
							}
							
							reducedSize = intialSize-buffer.length();
							
//							logger.debug("intialSize " + intialSize);
//							logger.debug("reducedSize " + reducedSize);
//							logger.debug("tmpOffset " + tmpOffset);
//							logger.debug("buffer.length() " + buffer.length());
							
							adaptCreatedTags(createdTags, reducedSize, tmpOffset, 1);
						}
	
					}
	
					/*
					 * at the end remove all 'unused' symbols -> happens if the number of symbols does not match 'custom tag' string
					 */
					replacedString = buffer;
	
					logger.debug("edited text " + replacedString);
					shape.setUnicodeText(replacedString, null);
					
					/*
					 * if we add the custom tags immediately after the creation of them the setUncidodeText method from the line before deletes them!!
					 */
	//				for (CustomTag ct : createdTags) {
	//					shape.getCustomTagList().addOrMergeTag(ct, null);
	//					CustomTagUtil.writeCustomTagListToCustomTag(shape);
	//				}
					finalString =replacedString;
				}
			}
			
			for (CustomTag ct : createdTags) {
//				logger.debug("finalString cut " + finalString);
//				logger.debug("ct " + ct.getAttributeCssStr());
//				logger.debug("ct OFFSET " + ct.getOffset());
//				logger.debug("ct length: " + ct.getLength());
//				logger.debug("finalString length: " + finalString.length());
//				if (ct != null && ct.getOffset()>0){
//					logger.debug("replacedString cut " + finalString.substring(ct.getOffset(), ct.getOffset()+ct.getLength()));
//				}
				shape.getCustomTagList().addOrMergeTag(ct, null);
				CustomTagUtil.writeCustomTagListToCustomTag(shape);
			}

			//System.in.read();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	

	}

	private static void searchAndAddAttributes(Map<String, Object> propMap, String propLabel, CustomTag ct, String buffer, int reducedSize) throws ParseException, IOException {
		if (propMap.containsKey(propLabel)) {
			logger.debug("properties found: " + propMap.get(propLabel));
			String cssStringAttributes = (String) propMap.get(propLabel);
			ct.setAttributesFromCssStr(cssStringAttributes);
			buffer = buffer.replaceFirst(propLabel, "");
			
			reducedSize += propLabel.length();
			
		}
	}

	/*
	 * ToDO: not all possible cases are considered
	 */
	private static void adaptCreatedTags(List<CustomTag> createdTags, int reducedSize, int offset, int length) {
		for (CustomTag created : createdTags) {
//			logger.debug("modified offseet start " + created.getOffset());
//			logger.debug("modified length start" + created.getLength());
//			logger.debug("offset " + offset);
//			logger.debug("abbrev.length() " + length);
			
			/*
			 * current tag is smaller then already created tag and we have to reduce its offset
			 */
			if (offset<created.getOffset() && offset+length<=created.getOffset()) {
				logger.debug("reduce offset of already created tag from " + created.getOffset() + " into "+ (created.getOffset()-reducedSize));
				//avoid values less then 0
				int newOffset = Math.max(0, created.getOffset()-reducedSize);
				created.setOffset(newOffset);
			}
			/*
			 * already created tag completely 'contains' the current tag: length of tag has to be corrected
			 */
			else if(offset>=created.getOffset() && (offset+length)<(created.getOffset()+created.getLength())) {
				logger.debug("reduce length of already created tag: from " + created.getLength() + " into "+ (created.getLength()-reducedSize));
				//avoid values less then 0
				int newLength = Math.max(0, created.getLength()-reducedSize);
				created.setLength(newLength);
			}
			/*
			 * current tag starts before 'created tag' and ends after or in-between it: offset is reduced 
			 */
			else if (offset<created.getOffset() && offset+length>created.getOffset()) {
				logger.debug("reduce offset of already created tag by 1 into " + (created.getOffset()-1));
				int newOffset = Math.max(0, created.getOffset()-1);
				created.setOffset(newOffset);
			}
			
//			logger.debug("modified offseet " + created.getOffset());
//			logger.debug("modified length " + created.getLength());
		}
		
	}

	public static void copyCustomTags(TrpTextLineType l1, TrpTextLineType l2) {
		
		if (l1.getId().contentEquals(l2.getId())) {
			l1.setCustom(l2.getCustom());
		}
		
	}

	public static List<ITrpShapeType> filterLinesByOverlap(List<? extends ITrpShapeType> lines,
			List<? extends ITrpShapeType> allLines, double threshold) {
		return lines.stream().filter(s -> {
			try {
				//ICanvasShape currCanvas = (ICanvasShape) s.getData();
				
				for (ITrpShapeType trpShape : allLines) {
					if (s.getId() == trpShape.getId())
						continue;
//					ICanvasShape otherCanvas = (ICanvasShape) trpShape.getData();
//					double intersection = currCanvas.intersectionArea(otherCanvas);
					Rectangle2D intersectRect = s.getBoundingBox().createIntersection(trpShape.getBoundingBox());
					if (intersectRect.isEmpty()) {
						continue;
					}
					double sizeIntersect = intersectRect.getHeight()*intersectRect.getWidth();
					double theArea = s.getBoundingBox().getHeight()*s.getBoundingBox().getWidth();
					//logger.debug("intersection area: " + sizeIntersect);
					//logger.debug("area of shape: " + theArea);
					logger.debug("overlap percentage: " + (sizeIntersect/theArea));
					//a shape has more then 95% of intersection with another region
					if ( (sizeIntersect/theArea) > threshold) {
						return false;
					}	
				}
				return true;
			} catch (Exception e) {
				logger.error("Error calculating intersection of shape: " + e.getMessage(), e);
				return false;
			}
		}).collect(Collectors.toList());
	}

}
