package eu.transkribus.core.model.beans;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import eu.transkribus.core.io.util.TrpProperties;
import eu.transkribus.core.rest.JobConst;

public class ReverseTextParams {
	public boolean reverseText = false;
	public boolean excludeDigits = true;
	public List<String> tagExceptions = new ArrayList<>();	
	
	public ReverseTextParams() {
	}
	
	public ReverseTextParams(boolean reverseText, boolean omitDigits, List<String> tagExceptions) {
		super();
		this.reverseText = reverseText;
		this.excludeDigits = omitDigits;
		this.tagExceptions = tagExceptions;
	}

	@Override
	public String toString() {
		return "ReverseTextParams [reverseText=" + reverseText + ", excludeDigits=" + excludeDigits
				+ ", tagExceptions=" + tagExceptions + "]";
	}
	
	public static ReverseTextParams fromProps(Properties props) {
		return fromProps(new TrpProperties(props));
	}
	
	public static ReverseTextParams fromProps(TrpProperties props) {
		boolean reverseText = props.getOrDefault(JobConst.PROP_REVERSE_TEXT, false);
		boolean excludeDigits = props.getOrDefault(JobConst.PROP_REVERSE_TEXT_EXCLUDE_DIGITS, false);
		List<String> tagExceptions = props.getStrListOrDefault(JobConst.PROP_REVERSE_TEXT_TAG_EXCEPTIONS, new ArrayList<>());
		return new ReverseTextParams(reverseText, excludeDigits, tagExceptions);		
	}

}