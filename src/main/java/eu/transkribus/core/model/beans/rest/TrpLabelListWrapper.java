package eu.transkribus.core.model.beans.rest;

import eu.transkribus.core.model.beans.TrpCreditTransaction;
import eu.transkribus.core.model.beans.TrpDocMetadata;
import eu.transkribus.core.model.beans.TrpLabel;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import java.util.List;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@XmlSeeAlso({TrpLabel.class})
public class TrpLabelListWrapper extends JaxbPaginatedList<TrpLabel> {

    public TrpLabelListWrapper() {
        super();
    }

    public TrpLabelListWrapper(List<TrpLabel> list, int total, int index, int nValues, String sortColumnField,
                               String sortDirection) {
        super(list, total, index, nValues, sortColumnField, sortDirection);
    }

}
