package eu.transkribus.core.model.beans;

import java.io.File;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.transkribus.core.model.beans.rest.ParameterMap;
import eu.transkribus.core.util.HtrPyLaiaUtils;

@XmlRootElement(name="createModelPars")
public class PyLaiaCreateModelPars extends ParameterMap {
	private static final Logger logger = LoggerFactory.getLogger(PyLaiaCreateModelPars.class);
	
	// Possible parameter
//    [--logging_also_to_stderr LOGGING_ALSO_TO_STDERR]
//    [--logging_config LOGGING_CONFIG]
//    [--logging_file LOGGING_FILE]
//    [--logging_level LOGGING_LEVEL]
//    [--logging_overwrite [LOGGING_OVERWRITE]]
//    [--print_args [PRINT_ARGS]]
//    [--train_path TRAIN_PATH] [--seed SEED]
//    [--fixed_input_height FIXED_INPUT_HEIGHT]
//    [--adaptive_pooling ADAPTIVE_POOLING]
//    [--model_filename MODEL_FILENAME]
//    [--cnn_num_features CNN_NUM_FEATURES [CNN_NUM_FEATURES ...]]
//    [--cnn_kernel_size CNN_KERNEL_SIZE [CNN_KERNEL_SIZE ...]]
//    [--cnn_stride CNN_STRIDE [CNN_STRIDE ...]]
//    [--cnn_dilation CNN_DILATION [CNN_DILATION ...]]
//    [--cnn_activations {ReLU,Tanh,LeakyReLU} [{ReLU,Tanh,LeakyReLU} ...]]
//    [--cnn_poolsize CNN_POOLSIZE [CNN_POOLSIZE ...]]
//    [--cnn_dropout CNN_DROPOUT [CNN_DROPOUT ...]]
//    [--cnn_batchnorm CNN_BATCHNORM [CNN_BATCHNORM ...]]
//    [--rnn_units RNN_UNITS]
//    [--rnn_layers RNN_LAYERS]
//    [--rnn_dropout RNN_DROPOUT]
//    [--lin_dropout LIN_DROPOUT]
//    [--rnn_type {LSTM,GRU}]
//    [--vertical_text [VERTICAL_TEXT]]
//    [--use_masked_conv [USE_MASKED_CONV]]
	
	public PyLaiaCreateModelPars() {
	}
	
	public PyLaiaCreateModelPars(PyLaiaCreateModelPars copy) {
		super(copy);
	}

	public static PyLaiaCreateModelPars getDefault() {
		PyLaiaCreateModelPars pars = new PyLaiaCreateModelPars();
		pars.addParameter("--print_args", "True");
		pars.addParameter("--train_path", "./model");
		pars.addParameter("--model_filename", "model");
		pars.addParameter("--logging_level", "info");
//		pars.addParameter("--fixed_input_height", "128");
		pars.addParameter("--cnn_kernel_size", "3 3 3 3");
		pars.addParameter("--cnn_dilation", "1 1 1 1");
		pars.addParameter("--cnn_num_features", "12 24 48 48");
		pars.addParameter("--cnn_batchnorm", "True True True True");
		pars.addParameter("--cnn_activations", "LeakyReLU LeakyReLU LeakyReLU LeakyReLU");
		pars.addParameter("--cnn_poolsize", "2 2 0 2");
		pars.addParameter("--use_masked_conv", "False");
		pars.addParameter("--rnn_type", "LSTM");
		pars.addParameter("--rnn_layers", "3");
		pars.addParameter("--rnn_units", "256");
		pars.addParameter("--rnn_dropout", "0.5");
		pars.addParameter("--lin_dropout", "0.5");
		
		return pars;
	}

	public String getNextGenConf(String baseConf) {
		List<String> keysToIgnore = Arrays.asList("print_args", "logging_level", "logging_also_to_stderr", "logging_file");
		Map<String, String> keyReplaceMap = new HashMap<>();
		keyReplaceMap.put("use_masked_conv", "use_masks");
		keyReplaceMap.put("cnn_activations", "cnn_activation");		
		
		return HtrPyLaiaUtils.getPyLaiaNextGenConf(baseConf, this, keysToIgnore, keyReplaceMap);
	}	
	
//	public void setFixedInputHeight(int fixedInputHeight) {
//		if (fixedInputHeight > 0) {
//			addParameter("--fixed_input_height", fixedInputHeight);	
//		}
//		else {
//			remove("--fixed_input_height");
//		}
//	}
	
	public Integer getFixedInputHeight() {
		return getIntParam("--fixed_input_height");
	}
	
	@Override
	public String toString() {
		return toSingleLineString();
	}
	
	public static PyLaiaCreateModelPars fromSingleLineString(String str) {
		ParameterMap m = ParameterMap.fromSingleLineString(str, "--", " ");
		PyLaiaCreateModelPars p = new PyLaiaCreateModelPars();
		p.setParamMap(m.getParamMap());
		return p;
	}
	
	public static PyLaiaCreateModelPars fromSingleLineString2(String str) {
		try {
			return fromSingleLineString(str);
		} catch (Exception e) {
			return null;
		}
	}	
}
