package eu.transkribus.core.model.beans;

import java.util.List;

import eu.transkribus.core.model.beans.rest.ParameterMap;

public class Text2ImageConf {
    private TrpModelMetadata model=null;
	private ParameterMap params = new ParameterMap();
	
	private boolean currentTranscript=true;
	private String pagesStr=null;
	
	private boolean isDocsSelection=false;
	private List<DocSelection> docsSelected = null;
	
	public static final String PERFORM_LA_PARAM = "performLa";
	public static final String KEEP_UNMATCHED_LINES_PARAM = "keepUnmatchedLines";
	public static final String PRESERVE_LINE_ORDER_PARAM = "preserveLineOrder";
	public static final String WRITE_SIMILARITY_TAG_PARAM = "writeSimilarityTag";
	public static final String BLOCK_THRESH_PARAM = "blockThresh";
	public static final String LINE_THRESH_PARAM = "lineThresh";
	public static final String ALLOW_DOUBLE_MATCHING = "allowDoubleMatching";
	public static final String REDUCTION_METHOD_PARAM = "reductionMethod";
	public static final String KEEP_LINE_BREAKS_PARAM = "keepLineBreaks";
	public static final String SOURCE_TEXT_PATH = "sourceTextPath";

	public static final String USE_CURRENT_TRANSCRIPT = "useCurrentTranskript";
	public static final String USE_SOURCE_LINE_FEEDS = "useSourceLineFeeds";
	public static final String ADD_NOT_MATCHED_TEXT_IN_LAST_LINE = "addNotMatchedTextInLastLine";

	
	public Text2ImageConf() {
		params.addBoolParam(PERFORM_LA_PARAM, true);
		params.addBoolParam(KEEP_UNMATCHED_LINES_PARAM, false);
		params.addBoolParam(PRESERVE_LINE_ORDER_PARAM, false);
		params.addBoolParam(WRITE_SIMILARITY_TAG_PARAM, true);
		params.addDoubleParam(BLOCK_THRESH_PARAM, 0.0);
		params.addDoubleParam(LINE_THRESH_PARAM, 0.45);
		params.addBoolParam(KEEP_LINE_BREAKS_PARAM, true);
		params.addBoolParam(ALLOW_DOUBLE_MATCHING, false);
		params.addBoolParam(USE_CURRENT_TRANSCRIPT, false);
		params.addParameter(REDUCTION_METHOD_PARAM, "REMOVE_INSERT");
		params.addParameter(SOURCE_TEXT_PATH, null);
	}
	
	public Text2ImageConf(TrpModelMetadata model) {
		super();
		this.model = model;
	}
	
	public TrpModelMetadata getModel() {
		return model;
	}
	public void setModel(TrpModelMetadata model) {
		this.model = model;
	}
	public ParameterMap getParams() {
		return params;
	}
	public void setParams(ParameterMap params) {
		this.params = params;
	}
	public boolean isCurrentTranscript() {
		return currentTranscript;
	}
	public void setCurrentTranscript(boolean currentTranscript) {
		this.currentTranscript = currentTranscript;
	}
	public String getPagesStr() {
		return pagesStr;
	}
	public void setPagesStr(String pagesStr) {
		this.pagesStr = pagesStr;
	}
	public boolean isDocsSelection() {
		return isDocsSelection;
	}
	public void setDocsSelection(boolean isDocsSelection) {
		this.isDocsSelection = isDocsSelection;
	}
	public List<DocSelection> getDocsSelected() {
		return docsSelected;
	}
	public void setDocsSelected(List<DocSelection> docsSelected) {
		this.docsSelected = docsSelected;
	}
	
}