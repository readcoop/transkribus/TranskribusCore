//package eu.transkribus.core.model.beans.adapters;
//
//import java.io.IOException;
//import java.io.InputStream;
//import java.io.OutputStream;
//import java.lang.annotation.Annotation;
//import java.lang.reflect.ParameterizedType;
//import java.lang.reflect.Type;
//import java.util.Collection;
//
//import javax.ws.rs.Consumes;
//import javax.ws.rs.WebApplicationException;
//import javax.ws.rs.core.GenericEntity;
//import javax.ws.rs.core.MediaType;
//import javax.ws.rs.core.MultivaluedMap;
//import javax.ws.rs.ext.MessageBodyReader;
//import javax.ws.rs.ext.MessageBodyWriter;
//import javax.ws.rs.ext.Provider;
//
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//
//import com.fasterxml.jackson.databind.ObjectMapper;
//
//import eu.transkribus.core.model.beans.TrpModelMetadata;
//import eu.transkribus.core.util.JacksonUtil;
//
//@Provider
//@Consumes(MediaType.APPLICATION_JSON)
//public class TrpModelMetadataMessageBodyReaderWriter implements MessageBodyReader<TrpModelMetadata>, MessageBodyWriter<TrpModelMetadata> {
//	private final static Logger logger = LoggerFactory.getLogger(TrpModelMetadataMessageBodyReaderWriter.class);
//
//	ObjectMapper mapper;
//	
//	public TrpModelMetadataMessageBodyReaderWriter() {
//		logger.debug("Instantiating new ObjectMapper");
//		mapper = JacksonUtil.createDefaultMapper(true);
//	}
//
//	@Override
//	public boolean isReadable(Class<?> type, Type genericType, Annotation[] annotations, MediaType mediaType) {
//		final boolean res = TrpModelMetadata.class.isAssignableFrom(type);
//		logger.debug("isReadable() in TrpModelMetadataMessageBodyReader: " + res);
//		logger.debug("type = " + type + " | genericType = " + genericType + " | mediaType = " + mediaType);
//		return res;
//	}
//
//	@Override
//	public TrpModelMetadata readFrom(Class<TrpModelMetadata> type, Type genericType, Annotation[] annotations, MediaType mediaType,
//			MultivaluedMap<String, String> httpHeaders, InputStream entityStream)
//			throws IOException, WebApplicationException {
//		try {
//			logger.debug("unmarshalling TrpModelMetadata from input stream, type = " + type + " genericType = " + genericType
//					+ " mediaType = " + mediaType);
//			return mapper.readValue(entityStream, TrpModelMetadata.class);
//		} catch (Exception e) {
//			logger.error(e.getMessage(), e);
//			throw new WebApplicationException(e);
//		}
//	}
//	
//	@Override
//	public boolean isWriteable(Class<?> type, Type genericType, Annotation[] annotations,
//			MediaType mediaType) {
//		if (TrpModelMetadata.class.isAssignableFrom(type)) {
//			return true;
//		}
//		if (Collection.class.isAssignableFrom(type) && genericType instanceof ParameterizedType) {
//            ParameterizedType parameterizedType = (ParameterizedType) genericType;
//            Type[] actualTypeArgs = parameterizedType.getActualTypeArguments();
//            logger.info("Checking type argument of {}: {}", genericType.getTypeName(), actualTypeArgs);
//            return (actualTypeArgs.length == 1 && actualTypeArgs[0].equals(TrpModelMetadata.class));
//        }
//		return false;
//	}
//	
//	@Override
//	public long getSize(TrpModelMetadata t, Class<?> type, Type genericType, Annotation[] annotations, MediaType mediaType) {
//		// return -1 with respect to Javadoc of this method
//		return -1;
//	}
//
//	@Override
//	public void writeTo(TrpModelMetadata modelMd, Class<?> type, Type genericType,
//			Annotation[] annotations, MediaType mediaType,
//			MultivaluedMap<String, Object> httpHeaders, OutputStream entityStream)
//			throws IOException, WebApplicationException {
//		logger.debug("Marshalling {} to String", type);
//		mapper.writeValue(entityStream, modelMd);
//	}
//
//}
