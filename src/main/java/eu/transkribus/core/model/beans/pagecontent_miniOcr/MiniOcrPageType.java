package eu.transkribus.core.model.beans.pagecontent_miniOcr;

import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "p")
public class MiniOcrPageType {

    @XmlElement(name = "b")
    List<MiniOcrBlockType> blocks;

    @XmlElement(name = "l")
    List<MiniOcrLineType> lines;

    @XmlAttribute(name= "xml:id")
    int pageId;

    @XmlAttribute(name="wh")
    String dimensions;

    public void setId(int id) {
        this.pageId = id;
    }

    public void setBlocks(List<MiniOcrBlockType> blocks){
        this.blocks = blocks;
    }

    public void setLines(List<MiniOcrLineType> lines){
        this.lines = lines;
    }

    public void setDimensions(int width, int height) {
        this.dimensions = width + " " + height;
    }



    
}
