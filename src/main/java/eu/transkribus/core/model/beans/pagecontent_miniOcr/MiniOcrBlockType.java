package eu.transkribus.core.model.beans.pagecontent_miniOcr;

import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "b")
public class MiniOcrBlockType {

    @XmlAttribute(name= "id")
    String regionId;

    @XmlElement(name = "l")
    List<MiniOcrLineType> lines;

    public void setLines(List<MiniOcrLineType> lines){
        this.lines = lines;
    }

    public void setRegionId(String regionId){
        this.regionId = regionId;
    }

    
}
