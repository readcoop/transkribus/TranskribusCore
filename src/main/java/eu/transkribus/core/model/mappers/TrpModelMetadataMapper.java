package eu.transkribus.core.model.mappers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

import eu.transkribus.core.model.beans.StructureTrainValidationStats;
import eu.transkribus.core.util.HtrPyLaiaUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.transkribus.core.model.beans.HtrTrainConfig;
import eu.transkribus.core.model.beans.ReleaseLevel;
import eu.transkribus.core.model.beans.TrpEntityAttribute;
import eu.transkribus.core.model.beans.TrpHtr;
import eu.transkribus.core.model.beans.TrpModelMetadata;
import eu.transkribus.core.model.beans.enums.DocType;
import eu.transkribus.core.model.beans.job.TrpJobStatus;
import eu.transkribus.core.util.AttributeUtils;
import eu.transkribus.core.util.ModelUtil;

public class TrpModelMetadataMapper {
	private static final Logger logger = LoggerFactory.getLogger(TrpModelMetadataMapper.class);
	
	public static TrpModelMetadata toBriefRepresentation(TrpHtr htr) {
		TrpModelMetadata m = new TrpModelMetadata();
		m.setModelId(htr.getHtrId());
		m.setType(htr.getType());
		m.setName(htr.getName());
		m.setProvider(htr.getProvider());
		m.setCreated(htr.getCreated());
		m.setLanguage(htr.getLanguage());
		m.setFinalCer(htr.getFinalCer());
		m.setFinalValidationCer(htr.getFinalCerTest());
		m.setNrOfLines(htr.getNrOfLines());
		m.setNrOfWords(htr.getNrOfWords());
		m.setUserName(htr.getUserName());
		m.setUserId(htr.getUserId());
		m.setNrOfTrainGtPages(htr.getNrOfTrainGtPages());
		m.setNrOfValidationGtPages(htr.getNrOfValidationGtPages());
		m.setReleaseLevel("" + htr.getReleaseLevel());
		//TODO publisher
		if(htr.getReleaseLevelValue() != 0 && htr.getPublisher() == null) {
			m.setPublisher("READ COOP");
		}
		m.setGtAccessible(htr.isGtAccessible());
		m.setFeatured(htr.isFeatured());
		m.setDelTime(htr.getDelTime());
		m.setDocType(DocType.fromValue(htr.getDocType()).toString().toLowerCase());
		m.setInternalRating(htr.getInternalRating());
		m.setCenturies(toSet(htr.getCenturies()));
		m.setIsoLanguages(toSet(htr.getIsoLanguages()));
		m.setScriptTypes(toSet(htr.getScriptTypes()));
		m.setCreator(htr.getCreator());
		m.setContactEmail(htr.getContactEmail());
		m.setExampleImageUrl(htr.getExampleImageUrl());
		m.setNrOfTranscriptsProduced(htr.getNrOfTranscriptsProduced());
		return m;
	}
	
	public static TrpModelMetadata toDetailedRepresentation(TrpHtr htr) {
		TrpModelMetadata m = toBriefRepresentation(htr);
		m.setBaseModelId(htr.getBaseHtrId());
		if(htr.getParamsProps().getProperty(HtrPyLaiaUtils.BASE_MODEL_NAME_KEY) != null) {
			//base model name may have changed since the training. TrpHtr should transport the current model name.
			m.setBaseModelName(htr.getParamsProps().getProperty(HtrPyLaiaUtils.BASE_MODEL_NAME_KEY));
		}
		m.setTrainJobId(htr.getTrainJobId());
		m.setSymbols(getCharacterList(htr));
		m.setBestNetStored(htr.isBestNetStored());
		m.setLanguageModelExists(htr.isLanguageModelExists());
		m.setCerLog(Arrays.stream(htr.getCerLog()).boxed().collect(Collectors.toList()));
		m.setCerValidationLog(Arrays.stream(htr.getCerTestLog()).boxed().collect(Collectors.toList()));
		
		//TODO map PyLaia stuff correctly
		m.setParameters(htr.getParamsProps().entrySet().stream()
				.collect(Collectors.toMap(k -> k.getKey().toString(), v -> v.getValue().toString())));
		m = mapAttributes(htr, m);
		
		return m;
	}

	public static TrpHtr fromRepresentation(TrpModelMetadata m) {
		TrpHtr h = new TrpHtr();
		h.setHtrId(m.getModelId());
		h.setType(m.getType());
		h.setName(m.getName());
		h.setProvider(m.getProvider());
		if(m.getCreated() != null) {
			h.setCreated(new java.sql.Timestamp(m.getCreated().getTime()));
		}
		h.setLanguage(m.getLanguage());
		h.setFinalCer(m.getFinalCer());
		h.setFinalCerTest(m.getFinalValidationCer());
		if(m.getNrOfLines() != null) {
			h.setNrOfLines(m.getNrOfLines());
		}
		if(m.getNrOfWords() != null) {
			h.setNrOfWords(m.getNrOfWords());
		}
		h.setUserName(m.getUserName());
		if(m.getUserId() != null) {
			h.setUserId(m.getUserId());
		}
		h.setNrOfTrainGtPages(m.getNrOfTrainGtPages());
		h.setNrOfValidationGtPages(m.getNrOfValidationGtPages());
		h.setReleaseLevel(ReleaseLevel.fromString(m.getReleaseLevel()));
		h.setPublisher(m.getPublisher());
		h.setGtAccessible(m.isGtAccessible());
		h.setFeatured(m.isFeatured());
		if(m.getDelTime() != null) {
			h.setDelTime(new java.sql.Timestamp(m.getDelTime().getTime()));
		}
		DocType t = DocType.fromString(m.getDocType());
		if(!DocType.UNDEFINED.equals(t)) {
			h.setDocType(t.getValue());
		}
		h.setInternalRating(m.getInternalRating());
		h.setCenturies(fromSet(m.getCenturies()));
		h.setIsoLanguages(fromSet(m.getIsoLanguages()));
		h.setScriptTypes(fromSet(m.getScriptTypes()));
		h.setCreator(m.getCreator());
		h.setContactEmail(m.getContactEmail());
		h.setExampleImageUrl(m.getExampleImageUrl());
		h.setNrOfTranscriptsProduced(m.getNrOfTranscriptsProduced());
		
		h = mapAttributes(m, h);
		
		h.setBaseHtrId(m.getBaseModelId());
		//TODO baseModelName
		h.setTrainJobId(m.getTrainJobId());
		if(m.isBestNetStored() != null) {
			h.setBestNetStored(m.isBestNetStored());
		}
		if(m.isLanguageModelExists() != null) {
			h.setLanguageModelExists(m.isLanguageModelExists());
		}
		//do not map character list, the logs and parameters for now. we won't update them on the server coming from clients anyway.
//		m.setCharacterList(getCharacterList(htr));
//		m.setCerLog(Arrays.stream(htr.getCerLog()).boxed().collect(Collectors.toList()));
//		m.setCerValidationLog(Arrays.stream(htr.getCerTestLog()).boxed().collect(Collectors.toList()));
		//TODO map PyLaia stuff correctly
//		m.setParameters(htr.getParamsProps().entrySet().stream()
//				.collect(Collectors.toMap(k -> k.getKey().toString(), v -> v.getValue().toString())));
		return h;
	}
	
	public static TrpHtr fromTrainConfig(HtrTrainConfig config, TrpJobStatus trainJob) {
		TrpModelMetadata md = copyMutableFields(config.getModelMetadata());
		TrpHtr htr = TrpModelMetadataMapper.fromRepresentation(md);
		// init fields with common HTR defaults
		htr.setReleaseLevel(ReleaseLevel.None);
		htr.setType(config.getType());
		htr.setInternalRating(null);
		htr.setCreated(new java.sql.Timestamp(System.currentTimeMillis()));
		htr.setDocType(DocType.HANDWRITTEN.getValue());
		if(trainJob != null) {
			htr.setUserId(trainJob.getUserId());
			htr.setUserName(trainJob.getUserName());
			htr.setTrainJobId(trainJob.getJobId());
		} else {
			logger.debug("trainJob argument is null => train job ID and owner will not be set.");
		}
		return htr;
	}
	
	/**
	 * Copy all metadata fields that can be set when starting a training to a new TrpModelMetadata instance
	 * @param other the model metadata to copy
	 * @return a copy with only user-mutable fields set
	 */
	private static TrpModelMetadata copyMutableFields(TrpModelMetadata other) {
		TrpModelMetadata m = new TrpModelMetadata();
		m.setBaseModelId(other.getBaseModelId());
		m.setCenturies(other.getCenturies());
		m.setContactEmail(other.getContactEmail());
		m.setCreator(other.getCreator());
		m.setDescriptions(other.getDescriptions());
		m.setExampleImageUrl(other.getExampleImageUrl());
		m.setIsoLanguages(other.getIsoLanguages());
		m.setLanguage(other.getLanguage());
		m.setName(other.getName());
		m.setProvider(other.getProvider());
		m.setPublisher(other.getPublisher());
		m.setScriptTypes(other.getScriptTypes());
		return m;
	}

	private static TrpModelMetadata mapAttributes(TrpHtr htr, TrpModelMetadata m) {
		Map<String, String> descriptions = new HashMap<>();
		descriptions.put(Locale.ENGLISH.toString(), htr.getDescription());
		
		if(htr.getAttributes() != null) {
			List<String> apps = new ArrayList<>(0);
			for(TrpEntityAttribute a : htr.getAttributes()) {
				switch(a.getType()) {
				case TrpHtr.ATTRIBUTE_APP:
					apps.add(a.getName());
					break;
				case TrpHtr.ATTRIBUTE_DESCRIPTION:
					descriptions.put(a.getName(), a.getValue());
					break;
				case TrpHtr.ATTRIBUTE_PROPERTY_OBJ:
					Object o = AttributeUtils.deserializeObject(a);
					if(o instanceof StructureTrainValidationStats) {
						logger.debug("Mapped structure stats attribute to model metadata: {}", o);
						m.setStructures((StructureTrainValidationStats) o);
					} else {
						logger.warn("Not mapping unknown property object type attribute with ID = {} of type {}: {}", a.getAttributeId(), a.getName(), o);
					}
					break;
				default: break;
				}
			}
			m.setApps(apps);
		}
		m.setDescriptions(descriptions);
		return m;
	}
	
	private static TrpHtr mapAttributes(TrpModelMetadata m, TrpHtr htr) {
		List<TrpEntityAttribute> attList = new ArrayList<>();
		if(m.getDescriptions() != null) {
			htr.setDescription(m.getDescriptions().get(Locale.ENGLISH.getLanguage()));
			for(Entry<String, String> d : m.getDescriptions().entrySet()) {
				Locale lang = new Locale(d.getKey());
				if(lang == null) {
					//TODO what if an illegal code is passed?
					logger.warn("Illegal language code: {}", d.getKey());
					continue;
				}
				logger.debug("Mapped description to attribute: {} - {}", lang.getLanguage(), d.getValue());
				attList.add(AttributeUtils.buildAttribute(TrpHtr.ATTRIBUTE_DESCRIPTION, htr.getHtrId(), lang.getLanguage(), d.getValue()));
			}
		}
		if(m.getApps() != null) {
			for(String app : m.getApps()) {
				attList.add(AttributeUtils.buildAttribute(TrpHtr.ATTRIBUTE_APP, htr.getHtrId(), app, null));
				logger.debug("Mapped application to attribute: {} - {}", app, null);
			}
		}
		if(m.getStructures() != null) {
			attList.add(AttributeUtils.serializeObject(htr.getHtrId(), m.getStructures()));
		}
		htr.setAttributes(attList);
		return htr;
	}

	private static List<String> getCharacterList(TrpHtr htr) {
		if(StringUtils.isEmpty(htr.getCharSetString())) {
			return new ArrayList<>();
		}
		if(StringUtils.isEmpty(htr.getProvider())) {
			return new ArrayList<>();
		}
		switch(htr.getProvider()) {
		case ModelUtil.PROVIDER_PYLAIA:
			return Arrays.asList(htr.getCharSetString().split("\\s+"));
		case ModelUtil.PROVIDER_CITLAB_PLUS:
		default:
			return Arrays.asList(htr.getCharSetString().split("\n"));
		}
	}
	
	public static String mapSortColumn(String representationSortField) {
		if(StringUtils.isEmpty(representationSortField)) {
			return null;
		}
		switch(representationSortField) {
		//handle sortable fields with name mismatch
		case "modelId": 
			return "htrId";
		case "finalValidationCer": 
			return "finalCerTest";
		case "releaseLevel" : 
			return "releaseLevelValue";
		//handle fields where we don't allow sorting
		case "userId":
		case "centuries":
		case "isoLanguages":
		case "scriptTypes":
		case "exampleImageUrl":
		case "parameters":
		case "trainJobId":
		case "characterList":
		case "cerLog":
		case "cerValidationLog":
			//return null => DAO will use default sorting
			return null;
		//allow sorting for the rest. Name of field in DB model must match the representation field
		default:
			return representationSortField;
		}
	}
	
	public static <T> List<T> fromSet(Set<T> set) {
		if(set == null) {
			return null;
		}
		return new ArrayList<>(set);
	}
	
	public static <T> Set<T> toSet(List<T> list) {
		if(list == null) {
			return null;
		}
		return new TreeSet<>(list);
	}
}
