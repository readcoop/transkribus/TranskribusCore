package eu.transkribus.core.model.builder.mets;

public class LogicalDivEntry {

	private String type;
	private String shapeId;
    private String pageId;
    private String altoId;
    
    public LogicalDivEntry(String shapeId, String pageId, String altoId) {
		super();
		this.shapeId = shapeId;
		this.pageId = pageId;
		this.altoId = altoId;
	}
    
	public LogicalDivEntry(String type, String shapeId, String pageId, String altoId) {
		super();
		this.type = type;
		this.shapeId = shapeId;
		this.pageId = pageId;
		this.altoId = altoId;
	}
	
	public String getShapeId() {
		return shapeId;
	}
	public void setShapeId(String shapeId) {
		this.shapeId = shapeId;
	}
	public String getPageId() {
		return pageId;
	}
	public void setPageId(String pageId) {
		this.pageId = pageId;
	}
	public String getAltoId() {
		return altoId;
	}
	public void setAltoId(String altoId) {
		this.altoId = altoId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}


}
