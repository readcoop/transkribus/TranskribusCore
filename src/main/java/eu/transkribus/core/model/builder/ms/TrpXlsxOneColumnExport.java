package eu.transkribus.core.model.builder.ms;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.imageio.ImageIO;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.common.usermodel.HyperlinkType;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.ClientAnchor;
import org.apache.poi.ss.usermodel.ClientAnchor.AnchorType;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Picture;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.WorkbookUtil;
import org.apache.poi.util.Units;
import org.apache.poi.xssf.usermodel.XSSFDrawing;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.dea.fimgstoreclient.FimgStoreGetClient;
import org.eclipse.core.runtime.IProgressMonitor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.transkribus.core.TrpFimgStoreConf;
import eu.transkribus.core.io.LocalDocReader;
import eu.transkribus.core.model.beans.JAXBPageTranscript;
import eu.transkribus.core.model.beans.TrpDoc;
import eu.transkribus.core.model.beans.TrpFImagestore;
import eu.transkribus.core.model.beans.TrpPage;
import eu.transkribus.core.model.beans.TrpTranscriptMetadata;
import eu.transkribus.core.model.beans.pagecontent_trp.ITrpShapeType;
import eu.transkribus.core.model.beans.pagecontent_trp.TrpPageType;
import eu.transkribus.core.model.beans.pagecontent_trp.TrpRegionType;
import eu.transkribus.core.model.beans.pagecontent_trp.TrpTableCellType;
import eu.transkribus.core.model.beans.pagecontent_trp.TrpTableRegionType;
import eu.transkribus.core.model.builder.ExportCache;
import eu.transkribus.core.model.builder.NoTablesException;



public class TrpXlsxOneColumnExport {
	private final static Logger logger = LoggerFactory.getLogger(TrpXlsxOneColumnExport.class);

	XSSFWorkbook wb;
	String singleSheetName = "datasheet";
	
	FimgStoreGetClient imgStoreClient;
	int currHeight;
	int currWidth;
	int rowCount = 0;
	
	CellStyle style=null;
	CellStyle rowStyle = null;
	CellStyle hlinkstyle = null;
	
	CreationHelper createHelper = null;
	
	public TrpXlsxOneColumnExport() {
		
	}
	
	/*
	 * this was to export a single column of a table (e.g. the last names) with its image snippet. Aim: correct the content easily and fast (sort and do it for several equal names)
	 * one document - one Excel file
	 */
	public void writeXlsxForColumnX_single(TrpDoc doc, File exportFile, Set<Integer> pageIndices, IProgressMonitor monitor, ExportCache cache, int colNr) throws NoTablesException, IOException, InterruptedException {
		//TrpTableRegionType is contained in the regions too
		TrpFImagestore storeConfig = TrpFimgStoreConf.getFImagestore();
		imgStoreClient = new FimgStoreGetClient(storeConfig);

		List<TrpPage> pages = doc.getPages();
		String exportPath = exportFile.getPath();
		
		int totalPages = pageIndices==null ? pages.size() : pageIndices.size();
		if (monitor!=null) {
			monitor.beginTask("Exporting tables to Excel", totalPages);
		}
		
		/*
		 * respect column index starts with 0, but user input starts with 1
		 */
		if(colNr>0) {
			colNr=colNr-1;
		}
		int initialColNr = colNr;
		
		//wbMaster = new XSSFWorkbook(); 

		//at beginning create the new xlsx
		//wb = new SXSSFWorkbook(1000);
		int c=0;
		int tableId = 0;
		for (int i=0; i<pages.size(); ++i) {
			
			if (pageIndices!=null && !pageIndices.contains(i))
				continue;
			
			exportPath = exportFile.getPath();
			/*
			 * we store the xlsx file for each page
			 */
			if (exportFile.exists()) {
				FileInputStream fip = null;
				try {
					logger.debug("open the wb once more");
					// for getting the information of the file
					fip = new FileInputStream(exportFile);

					wb= new XSSFWorkbook(fip);
					// Getting the workbook instance for XLSX file
					//wb = new XSSFWorkbook(exportPath);
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				finally {
					if (fip != null) {
						try {
							fip.close();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
			}
			else {
				logger.debug("export file does not exist: " + exportFile.getPath());
			}
			
			if (monitor!=null) {
				if (monitor.isCanceled()) {
					throw new InterruptedException("Export was canceled by user");
//					logger.debug("Xlsx export cancelled!");
//					return;
				}
				monitor.subTask("Processing page "+(c+1));
			}
			
			TrpPage page = pages.get(i);
			//try to get previously loaded JAXB transcript
			JAXBPageTranscript tr = null;
			if(cache != null) {
				tr = cache.getPageTranscriptAtIndex(i);
			}
			if (tr == null){
				TrpTranscriptMetadata md = page.getCurrentTranscript();
				tr = new JAXBPageTranscript(md);
				tr.build();
			}			
			
			TrpPageType trpPage = tr.getPage();
			String imgUrl = null;
			if (page != null && page.getImage() != null && page.getImage().getUrl() != null) {
				imgUrl = page.getImage().getUrl().toString();
			}
			if (imgUrl == null && trpPage.getPcGtsType().getMetadata().getTranskribusMetadata() != null) {
				imgUrl = trpPage.getPcGtsType().getMetadata().getTranskribusMetadata().getImgUrl();
			}
			
			logger.debug("imgUrl " + imgUrl);
			logger.debug("writing xlsx for page "+(i+1)+"/"+doc.getNPages());
			
			String fname = page.getImgFileName();
			
			List<TrpRegionType> regions = trpPage.getRegions();
						
			for (int j=0; j<regions.size(); ++j) {
				TrpRegionType r = regions.get(j);
				
				if (r instanceof TrpTableRegionType){
					tableId++;
					
					TrpTableRegionType table = (TrpTableRegionType) r;
					
					int cols = table.getNCols();
					int rows = table.getNRows();
					
					
					if (colNr<0) {
						colNr=0;
					}
					else if (colNr>=cols) {
						colNr=(cols-1);
					}

					logger.debug("is table - one column export of column idx " + colNr);
					List<TrpTableCellType> allColCells = new ArrayList<TrpTableCellType>();
					allColCells.addAll(table.getColCells(colNr));
					
					colNr = initialColNr;
		          
		            createTable(rows, cols, allColCells, page, fname, imgUrl, doc.getCollection().getColId(), true);

				}

				if (monitor!=null) {
					monitor.worked(c);
				}
			}
			++c;
			
			/*
			 * auto size the columns
			 */
//			for (int a = 0; a < wb.getNumberOfSheets(); a++){
//	            int numberOfCells = 0;
//	            Iterator<Row> rowIterator = wb.getSheetAt(a).rowIterator();
//	            /**
//	             * Escape the header row *
//	             */
//	            if (rowIterator.hasNext())
//	            {
//	                Row headerRow = (Row) rowIterator.next();
//	                               
//	                //get the number of cells in the header row
//	                numberOfCells = headerRow.getPhysicalNumberOfCells();
//	                for (int j = 0; j<numberOfCells; j++){
//	                	wb.getSheetAt(a).autoSizeColumn(j, true);
//	                }
//	                              
//	            }
//	                        
//			}
			
//			FileOutputStream fOut;
//			fOut = new FileOutputStream(exportPath);
//			wb.write(fOut);
//			fOut.close();
//			wb.close();
			
		}

		if (exportFile.exists()) {
			FileInputStream fip = null;
			try {
				logger.debug("open the wb once more");
				// for getting the information of the file
				fip = new FileInputStream(exportFile);

				wb = new XSSFWorkbook(fip);
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			finally {
				if (fip != null) {
					try {
						fip.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}

		/*
		 * auto size the columns
		 */
//		for (int i = 0; i < wb.getNumberOfSheets(); i++){
//            int numberOfCells = 0;
//            Iterator<Row> rowIterator = wb.getSheetAt(i).rowIterator();
//            /**
//             * Escape the header row *
//             */
//            if (rowIterator.hasNext())
//            {
//                Row headerRow = (Row) rowIterator.next();
//                               
//                //get the number of cells in the header row
//                numberOfCells = headerRow.getPhysicalNumberOfCells();
//                for (int j = 0; j<numberOfCells; j++){
//                	wb.getSheetAt(i).autoSizeColumn(j, true);
//                }
//                              
//            }
//                        
//		}

		FileOutputStream fOut;
		try {
			//means no tables at all
			if (wb.getNumberOfSheets() == 0){
				//throw new NoTablesException("Sorry - No tables available for export");
				logger.info("Sorry - No tables available for export");
				Sheet noTables = wb.createSheet(WorkbookUtil.createSafeSheetName("No tables found"));
				CreationHelper crHelper = wb.getCreationHelper();
				Row firstRow = noTables.createRow(0);
				firstRow.createCell(0).setCellValue(crHelper.createRichTextString("Sorry - there were no tables available for your export. Please check the transcripts!"));
			}
			fOut = new FileOutputStream(exportPath);
			wb.write(fOut);
			fOut.close();
			wb.close();
		} catch (IOException e) {
			if (!(e instanceof NoTablesException)) {
				logger.error(e.getMessage(), e);
			}
			throw e;
		}
		logger.info("wrote xlsx to: "+ exportPath);
	}
	
	
	/*
	 * this was to export a single column of a table (e.g. the last names) with its image snippet. Aim: correct the content easily and fast (sort and do it for several equal names)
	 * one document - extend already existent file
	 * works only for local exports
	 * ToDo: check how this can be solved for server export
	 */
	public void writeXlsxForColumnX(TrpDoc doc, File exportFile, Set<Integer> pageIndices, IProgressMonitor monitor, ExportCache cache, int colNr) throws NoTablesException, IOException, InterruptedException {
		//TrpTableRegionType is contained in the regions too
		TrpFImagestore storeConfig = TrpFimgStoreConf.getFImagestore();
		imgStoreClient = new FimgStoreGetClient(storeConfig);

		List<TrpPage> pages = doc.getPages();
		
		String basename = FilenameUtils.getBaseName(exportFile.getName());
		String extension = FilenameUtils.getExtension(exportFile.getName());
		String parentPath = exportFile.getParentFile().getPath();
		
		String exportPath = null;
		File currExportFile = null;

		exportPath = parentPath + "/" + basename + "." + extension;
		
		/*
		 * ToDo: extend a xlsx if it already exists!!!!!!
		 */
		currExportFile = new File(exportPath);
		if (currExportFile.exists()) {
			try {
				logger.debug("extend the xlsx because it exists");
				// for getting the information of the file
				FileInputStream fip = new FileInputStream(currExportFile);

				// Getting the workbook instance for XLSX file
				wb = new XSSFWorkbook(fip);
				//wb = new XSSFWorkbook(exportPath);
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		else {
			wb = new XSSFWorkbook();
		}

		//createHelper = wb.getCreationHelper();

		List<TrpTableCellType> allColCells = new ArrayList<TrpTableCellType>();
		
		int totalPages = pageIndices==null ? pages.size() : pageIndices.size();
		if (monitor!=null) {
			monitor.beginTask("Exporting tables to Excel", totalPages);
		}
		
		/*
		 * respect column index starts with 0, but user input starts with 1
		 */
		if(colNr>0) {
			colNr=colNr-1;
		}
		int initialColNr = colNr;
			

		int c=0;
		boolean isStored = false;
		

		for (int i=0; i<pages.size(); ++i) {
			
			if (pageIndices!=null && !pageIndices.contains(i))
				continue;

			/*
			 * we store the xlsx file after every 19th page, after this create the wb new to 'refresh' and speed up the export
			 */
			if (isStored) {
				isStored = false;
				FileInputStream fip = null;
				try {
					logger.debug("open the wb once more");
					// for getting the information of the file
					fip = new FileInputStream(currExportFile);

					// Getting the workbook instance for XLSX file
					wb = new XSSFWorkbook();
					wb = new XSSFWorkbook(fip);
					//wb = new XSSFWorkbook(exportPath);
					style=wb.createCellStyle();
					style.setBorderBottom(BorderStyle.THIN);
					style.setBorderTop(BorderStyle.THIN);
					style.setBorderRight(BorderStyle.THIN);
					style.setBorderLeft(BorderStyle.THIN);
					style.setWrapText(true);
					
					rowStyle = (CellStyle) wb.createCellStyle();
					rowStyle.setWrapText(true);
					
					hlinkstyle = wb.createCellStyle();
					Font hlinkfont = wb.createFont();
					hlinkfont.setUnderline(Font.U_SINGLE);
					hlinkfont.setColor(IndexedColors.BLUE.index);
					hlinkstyle.setFont(hlinkfont);
					
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				finally {
					if (fip != null)
						try {
							fip.close();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
				}
			}
			else {
				logger.debug("go on with this wb: " + currExportFile.getPath());
			}

			if (monitor!=null) {
				if (monitor.isCanceled()) {
					throw new InterruptedException("Export was canceled by user");
				}
				monitor.subTask("Processing page "+(c+1));
			}
			
			TrpPage page = pages.get(i);
			//try to get previously loaded JAXB transcript
			JAXBPageTranscript tr = null;
			if(cache != null) {
				tr = cache.getPageTranscriptAtIndex(i);
			}
			if (tr == null){
				TrpTranscriptMetadata md = page.getCurrentTranscript();
				tr = new JAXBPageTranscript(md);
				tr.build();
			}			
			
			TrpPageType trpPage = tr.getPage();
			String imgUrl = null;
			if (page != null && page.getImage() != null && page.getImage().getUrl() != null) {
				imgUrl = page.getImage().getUrl().toString();
			}
			if (imgUrl == null && trpPage.getPcGtsType().getMetadata().getTranskribusMetadata() != null) {
				imgUrl = trpPage.getPcGtsType().getMetadata().getTranskribusMetadata().getImgUrl();
			}
			
			logger.debug("imgUrl " + imgUrl);
			logger.debug("writing xlsx for page "+(i+1)+"/"+doc.getNPages());
			
			String fname = page.getImgFileName();
			
			List<TrpRegionType> regions = trpPage.getRegions();
						
			for (int j=0; j<regions.size(); ++j) {
				TrpRegionType r = regions.get(j);
				
				if (r instanceof TrpTableRegionType){
					
					TrpTableRegionType table = (TrpTableRegionType) r;
					
					int cols = table.getNCols();
					int rows = table.getNRows();

					if (colNr<0) {
						colNr=0;
					}
					else if (colNr>=cols) {
						colNr=(cols-1);
					}

					logger.debug("is table - one column export of column idx " + colNr);
					allColCells.clear();
					allColCells.addAll(table.getColCells(colNr));
					
					colNr = initialColNr;
		          
					Integer collId = doc.getCollection() != null ? doc.getCollection().getColId() : null;
		            createTable(rows, cols, allColCells, page, fname, imgUrl, collId, (pages.size()<=10));

				}

				if (monitor!=null) {
					monitor.worked(c);
				}
			}
			++c;
			
			if (i>0 && i%19 == 0) {
				
				FileOutputStream fOut;
				fOut = new FileOutputStream(exportPath);
				wb.write(fOut);
				isStored = true;
				fOut.close();
				wb.close();
			}
			
		}

		if (currExportFile.exists()) {
			try {
				logger.debug("open the wb once more");
				// for getting the information of the file
				FileInputStream fip = new FileInputStream(currExportFile);

				// Getting the workbook instance for XLSX file
				wb = new XSSFWorkbook(fip);
				//wb = new XSSFWorkbook(exportPath);
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		
		XlsxUtils.autosizeTable(wb);

		FileOutputStream fOut;
		try {
			//means no tables at all
			if (wb.getNumberOfSheets() == 0){
				//throw new NoTablesException("Sorry - No tables available for export");
				logger.info("Sorry - No tables available for export");
				Sheet noTables = wb.createSheet(WorkbookUtil.createSafeSheetName("No tables found"));
				CreationHelper crHelper = wb.getCreationHelper();
				Row firstRow = noTables.createRow(0);
				firstRow.createCell(0).setCellValue(crHelper.createRichTextString("Sorry - there were no tables available for your export. Please check the transcripts!"));
			}
			fOut = new FileOutputStream(exportPath);
			wb.write(fOut);
			fOut.close();
			wb.close();
		} catch (IOException e) {
			if (!(e instanceof NoTablesException)) {
				logger.error(e.getMessage(), e);
			}
			throw e;
		}
		logger.info("wrote xlsx to: "+ exportPath);
	}
	

	private void createTable(int rows, int cols, List<TrpTableCellType> allColCells, TrpPage page, String filename, String imgUrl, Integer collId, boolean singleSheet){
		
		XSSFSheet currSheet = null;
		
    	String url = page.getUrl().toString();
    	if (imgUrl != null) {
    		url = imgUrl ;
    	}
    	String imgKey = StringUtils.substringBetween(url, "Get?id=", "&fileType=view");	
    	
    	byte[] imgByteArray;
    	BufferedImage src = null;
		try {
			imgByteArray = imgStoreClient.getImg(imgKey).getData();
	        // convert byte[] back to a BufferedImage
	        InputStream is = new ByteArrayInputStream(imgByteArray);
	        src = ImageIO.read(is);
	        is.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.debug("io of image store ");
			e.printStackTrace();
		}   	

    	/*
    	 * https://lite.transkribus.eu/collection/169901/doc/1274482/detail/100?key=KKTEZEBNGXETSNOSNNRCMLYO&view=layout
    	 * 
    	 * add linking to Transkribus Lite
    	 * works in principle but the links are not recogniced as links in Excel - they do not open. 
    	 * investigate on this before adding it to production
    	 */
    	String transcriptKey = page.getCurrentTranscript().getKey();
		String link2Lite = null;
		if (collId != null) {
			link2Lite = "https://app.transkribus.eu/collection/"+collId+"/doc/"+page.getDocId()+"/detail/"+ page.getPageNr() +"?key="+transcriptKey+"&view=both";
			logger.debug("link 2 lite " + link2Lite);
		}
	    
	    /*
	     * ToDO: create headings for first page if wanted
	     */

	    for (TrpTableCellType entry : allColCells) {
	    	
	    	for (ITrpShapeType currLine : entry.getChildren(false)) {
	    		
		    	//String text = entry.getUnicodeTextFromLines();
	    		String text = currLine.getUnicodeText();
		    	int colIdtmp = 0;
		    	String startChar = text.length()>1 ? text.substring(0, 1) : "";
		    	String alphabetSheetName = "sheet_"+startChar;
		    	
		    	//for each starting character -> extra sheet in xlsx
		    	if (!singleSheet) {
		    		if (wb.getSheet(alphabetSheetName) != null){
		    			currSheet = wb.getSheet(alphabetSheetName);
		    		}
		    		else{
		    			currSheet = wb.createSheet(WorkbookUtil.createSafeSheetName(alphabetSheetName));
		    		}
		    	}
		    	else {
		    		if (wb.getSheet(singleSheetName) != null){
		    			currSheet = wb.getSheet(singleSheetName);
		    		}
		    		else{
		    			currSheet = wb.createSheet(WorkbookUtil.createSafeSheetName(singleSheetName));
		    		}
		    	}
		    	
	    		currSheet.setColumnWidth(0, 10000);
	    		rowCount = currSheet.getLastRowNum();
	        
		    	Row nextRow = currSheet.createRow(++rowCount);
		    	//rowCount++;
		    	
		    	nextRow.setRowStyle(rowStyle);

		    	Cell currCell = nextRow.createCell(colIdtmp++);
	        	currCell.setCellValue(text);   
	        	currCell.setCellStyle(style);
	        	
			 	String coords = currLine.getCoordinates();
												
				int[] cropValues = XlsxUtils.getCropValues(coords, src);
				currHeight = cropValues[3];
				currWidth = cropValues[2];
	        	
	        	BufferedImage dest = src.getSubimage(cropValues[0], cropValues[1], cropValues[2], cropValues[3]);
	            // convert BufferedImage to byte[]
	            ByteArrayOutputStream baos = new ByteArrayOutputStream();
	            src.flush();
	            
	            byte[] bytes = null;
				try {
					bytes = XlsxUtils.compressImageToByteArray(dest,0.20f);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				
				}

		    	int inputImage = wb.addPicture(bytes, Workbook.PICTURE_TYPE_PNG);
		    	bytes = null;
		    	
		    	XSSFDrawing drawing = (XSSFDrawing) currSheet.createDrawingPatriarch();
		    	ClientAnchor imgAnchor = createHelper.createClientAnchor();

		    	/*
		    	 * for sorting it is necessary that the setRow1 and setRow2 is the same row index and that
		    	 * the setDx1, setDx2,... are used!! beside using AnchorType.MOVE_AND_RESIZE
		    	 */
		    	imgAnchor.setCol1(0);
		    	imgAnchor.setCol2(0);
		    	imgAnchor.setRow1(nextRow.getRowNum());
		    	imgAnchor.setRow2(nextRow.getRowNum());
		    	imgAnchor.setAnchorType(AnchorType.MOVE_AND_RESIZE);
		    	
		        float rowHeight = (float)Units.pixelToPoints(currHeight); // picture's height must fit into row height
		        nextRow.setHeightInPoints(rowHeight);

		    	imgAnchor.setDx1(Units.toEMU(1));
		    	imgAnchor.setDy1(Units.toEMU(1));
		    	imgAnchor.setDx2(Units.toEMU(currWidth-1));
		    	imgAnchor.setDy2(Units.toEMU(currHeight-1));

		    	//do not resize pic -> sorting will not work
		    	Picture pic = drawing.createPicture(imgAnchor, inputImage);
		    	
	        	currCell = nextRow.createCell(colIdtmp++);
	        	currCell.setCellValue(text);   
	        	currCell.setCellStyle(style);
	        	
	        	currCell = nextRow.createCell(colIdtmp++);
	        	currCell.setCellValue(entry.getId() + "/" + currLine.getId());   
	        	currCell.setCellStyle(style);
	        	
	        	currCell = nextRow.createCell(colIdtmp++);
	        	currCell.setCellValue(page.getPageId());   
	        	currCell.setCellStyle(style);
	        		        	
	    	    if (link2Lite != null) {
	    			org.apache.poi.ss.usermodel.Hyperlink link = createHelper.createHyperlink(HyperlinkType.URL);
	    		    link.setAddress(link2Lite);
	    		    link.setLabel(link2Lite);
	    		    
	    			currCell = nextRow.createCell(colIdtmp++);
	    			currCell.setCellValue(link2Lite);
	    			currCell.setHyperlink(link);
	    			currCell.setCellStyle(hlinkstyle);
	    	    }
	    	}
	    }
	}
	
	
//	private byte[] addImage(TrpTableCellType entry, String imgKey) {
//		 	String coords = entry.getCoordinates();
//			
//			//Extract key from URL
//		 	//logger.debug("image url: " + url);
//			
//			//logger.debug("image key in addImage: " + imgKey);
//
//			try {						
//				Image img;							
//				int[] cropValues = getCropValues(coords);
//				
//				currHeight = cropValues[3];
//				currWidth = cropValues[2];
//
//				URL url2 = imgStoreClient.getUriBuilder().getImgCroppedUri(imgKey, cropValues[0], cropValues[1], cropValues[2], cropValues[3]).toURL();
//				
//				byte[] bytes = IOUtils.toByteArray(url2);
//
//				return bytes;
//
//				
//			} catch (Exception ex) {								
//				logger.error("Could not load preview image!", ex);
//			}
//			return null;
//	}
	

	public static void main(String[] args) throws Exception {
		
//		convertXslxIntoCsv("Y:/DIG_auftraege_archiv/tmp/StazH/match_xslx");
		
		
		TrpDoc docWithoutTables = LocalDocReader.load("C:\\Users\\Schorsch\\Desktop\\Transkribus_tests\\TRAINING_VALIDATION_SET_Filae_firstnames\\TRAINING_VALIDATION_SET_Filae_firstnames\\");
		//TrpDoc docWithTables = LocalDocReader.load("C:/Neuer Ordner/TRAINING_TESTSET_NAF_Poll_Tax_M_5/TRAINING_TESTSET_NAF_Poll_Tax_M_5/");
		
		Set<Integer> pageIndices = null; // null means every page
		
		//pageIndices must be set here instead of being null because it gets used in ExportUtils
		if (pageIndices == null){
			pageIndices = new HashSet<Integer>();
			for (int i = 0; i<docWithoutTables.getNPages(); i++){
				pageIndices.add(i);
			}
		}
//		DocExporter docExporter =  new DocExporter();
//		docExporter.getCache().storePageTranscripts4Export(docWithoutTables, pageIndices, null, "Latest", -1, null);
		
		TrpXlsxOneColumnExport txslx = new TrpXlsxOneColumnExport();
		txslx.writeXlsxForColumnX(docWithoutTables, new File("C:\\Users\\Schorsch\\Desktop\\Transkribus_tests\\TRAINING_VALIDATION_SET_Filae_firstnames\\TRAINING_VALIDATION_SET_Filae_firstnames\\testExport.xlsx"), pageIndices, null, null,2);
		System.out.println("finished");
	}
	
	BufferedImage resizeImage(BufferedImage originalImage, int targetWidth, int targetHeight) throws IOException {
	    BufferedImage resizedImage = new BufferedImage(targetWidth, targetHeight, BufferedImage.TYPE_INT_RGB);
	    Graphics2D graphics2D = resizedImage.createGraphics();
	    graphics2D.drawImage(originalImage, 0, 0, targetWidth, targetHeight, null);
	    graphics2D.dispose();
	    return resizedImage;
	}

}
