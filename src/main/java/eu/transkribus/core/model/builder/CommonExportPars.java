package eu.transkribus.core.model.builder;

import java.io.IOException;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang3.StringUtils;
import org.dea.fimgstoreclient.beans.ImgType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.transkribus.core.io.ExportFilePatternUtils;
import eu.transkribus.core.io.LocalDocConst;
import eu.transkribus.core.model.builder.tei.TeiExportPars;
import eu.transkribus.core.util.CoreUtils;

/**
 * A general set of export parameters. Can and shall be subclassed for special exports as e.g. in {@link TeiExportPars}
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class CommonExportPars {
	private static final Logger logger = LoggerFactory.getLogger(CommonExportPars.class);
	
	public static final String PARAMETER_KEY = "commonPars";
	
	String pages = null;
//	Set<Integer> pageIndices = null;
	
	boolean doExportDocMetadata=true;
	boolean doWriteMets=true;
	boolean doWriteImages=true;
	boolean doExportPageXml=true; 
	boolean doExportAltoXml=true;
	boolean doExportSingleTxtFiles=false;
	boolean doWritePdf=false;
	boolean doWriteTei=false;
	boolean doWriteDocx=false;
	boolean doWriteOneTxt=false;
	boolean doSplitTxt=false;
	boolean doWriteTagsXlsx = false;
	boolean doWriteTagsIob = false;
	boolean doWriteTablesXlsx = false;
	boolean doWriteTableSingleColumn = false;
	boolean doIncludeImagesInXlsx = false;
	boolean doWriteOneTableForDoc = false;
	boolean doWriteStructuresInXlsx = false;
	boolean doWriteWordsIntoTable = false;
	boolean doWritePageMdXlsx = false;
	boolean doWriteStructureInMets=false;
	boolean doWriteStructuresRowBased=false;

	boolean useCache=true;
	int tableColumn = -1;
	
	boolean doCreateTitle=false;
	String useVersionStatus="Latest";
	
	String start_tagname_for_split = null;
	String end_tagname_for_split = null;
	String attributes_for_split_name = null;
	String tags_to_ignore_for_split = null;
	
	boolean writeTextOnWordLevel = false;
	boolean doBlackening = false;
	Set<String> selectedTags = null;
	List<String> structureTagNames = null;
	
	// from ExportOptions:
	String dir=null;
	String font = "FreeSerif";
	
	boolean splitIntoWordsInAltoXml=false;
	
	String pageDirName = LocalDocConst.PAGE_FILE_SUB_FOLDER;
	String fileNamePattern = ExportFilePatternUtils.PAGENR_FILENAME_PATTERN;
	boolean useHttps=true;
	
	/**
	 * defines which prepared image type should be exported.
	 * <br>
	 * Set to private as the setter will enforce that this is never null
	 */
	private ImgType remoteImgQuality = ImgType.orig;
	boolean doOverwrite=true;
	boolean useOcrMasterDir=true;
	boolean exportTranscriptMetadata = true;
	//if next value is true all elements are deleted from the PAGE XML which are against the schema definition
	boolean storeValidPageXml = false;
	boolean store2019PageXml = false;
	boolean updatePageXmlImageDimensions = true;
	/** if true, existing files will not be exported again - currently only supported for remote docs and image and PAGE-XMLs (no ALTO, TEI etc.) */
	boolean doResume = false;

	/** Allows to pass a custom email address to be notified with download link */
	String customEmail = null;
	private boolean filterHiddenPages = false;

	public CommonExportPars() {
	}
	
//	//TODO: for backward compatibility - old export document job, delete when replaced
//	public CommonExportPars(String pages, boolean doWriteMets, boolean doWriteImages, boolean doExportPageXml,
//			boolean doExportAltoXml, boolean doWritePdf, boolean doWriteTei, boolean doWriteDocx, 
//			boolean doWriteTxt, boolean doWriteTagsXlsx, boolean doWriteTagsIob, boolean doWriteTablesXlsx,
//			boolean doCreateTitle, String useVersionStatus, boolean writeTextOnWordLevel, 
//			boolean doBlackening, Set<String> selectedTags) {
//		
//		this(pages, doWriteMets, doWriteImages, doExportPageXml, doExportAltoXml, false, doWritePdf, doWriteTei, doWriteDocx, 
//				doWriteTxt, doWriteTagsXlsx, doWriteTablesXlsx, doWriteTagsIob, doCreateTitle, useVersionStatus, writeTextOnWordLevel, doBlackening, null, null);
//	}
	
	/*
	 * backward compatibility: write structure into Mets is called with parameter=false
	 */
	public CommonExportPars(String pages, boolean doWriteMets, boolean doWriteImages, boolean doExportPageXml,
			boolean doExportAltoXml, boolean doExportSingleTxtFiles, boolean doWritePdf, boolean doWriteTei, boolean doWriteDocx, 
			boolean doWriteTxt, boolean doWriteTagsXlsx,boolean doWriteTagsIob, boolean doWriteTablesXlsx,
			boolean doCreateTitle, String useVersionStatus, boolean writeTextOnWordLevel, 
			boolean doBlackening, Set<String> selectedTags, String font) {
		this(pages, doWriteMets, doWriteImages, doExportPageXml, doExportAltoXml, doExportSingleTxtFiles, false, doWritePdf, doWriteTei, doWriteDocx, 
				doWriteTxt, doWriteTagsXlsx, doWriteTagsIob, doWriteTablesXlsx, doCreateTitle, useVersionStatus, writeTextOnWordLevel, doBlackening, selectedTags, font);
	}
	
	/*
	 * backward compatibility: write page md into Excel is false
	 */
	public CommonExportPars(String pages, boolean doWriteMets, boolean doWriteImages, boolean doExportPageXml,
			boolean doExportAltoXml, boolean doExportSingleTxtFiles, boolean doWriteStructureInMets, boolean doWritePdf, boolean doWriteTei, boolean doWriteDocx, 
			boolean doWriteTxt, boolean doWriteTagsXlsx,boolean doWriteTagsIob, boolean doWriteTablesXlsx, 
			boolean doCreateTitle, String useVersionStatus, boolean writeTextOnWordLevel, 
			boolean doBlackening, Set<String> selectedTags, String font) {
		this(pages, doWriteMets, doWriteImages, doExportPageXml, doExportAltoXml, doExportSingleTxtFiles, false, doWritePdf, doWriteTei, doWriteDocx, 
				doWriteTxt, doWriteTagsXlsx, doWriteTagsIob, doWriteTablesXlsx, false, doCreateTitle, useVersionStatus, writeTextOnWordLevel, doBlackening, selectedTags, font);
	
	}
	
	/*
	 * backward compatibility: no splits for text file done
	 */
	public CommonExportPars(String pages, boolean doWriteMets, boolean doWriteImages, boolean doExportPageXml,
			boolean doExportAltoXml, boolean doExportSingleTxtFiles, boolean doWriteStructureInMets, boolean doWritePdf, boolean doWriteTei, boolean doWriteDocx, 
			boolean doWriteTxt, boolean doWriteTagsXlsx,boolean doWriteTagsIob, boolean doWriteTablesXlsx, boolean doWritePageMdXlsx,
			boolean doCreateTitle, String useVersionStatus, boolean writeTextOnWordLevel, 
			boolean doBlackening, Set<String> selectedTags, String font) {
		this(pages, doWriteMets, doWriteImages, doExportPageXml, doExportAltoXml, doExportSingleTxtFiles, false, doWritePdf, doWriteTei, doWriteDocx, 
				doWriteTxt, doWriteTagsXlsx, doWriteTagsIob, doWriteTablesXlsx, doWritePageMdXlsx, doCreateTitle, useVersionStatus, writeTextOnWordLevel, doBlackening, false, null, null, null, null, selectedTags, font);
		
	}

	/*
	 * added pars for exporting text files - use case: split document into several text files for each 'tag start' - 'tag end' pair, attribute_names are used for getting unique filenames
	 * ToDo: this is implemented for text files but could be expanded for all kind of export format (logical structmap in METS, several docx exports and so on)
	 */
	public CommonExportPars(String pages, boolean doWriteMets, boolean doWriteImages, boolean doExportPageXml,
			boolean doExportAltoXml, boolean doExportSingleTxtFiles, boolean doWriteStructureInMets, boolean doWritePdf, boolean doWriteTei, boolean doWriteDocx, 
			boolean doWriteTxt, boolean doWriteTagsXlsx,boolean doWriteTagsIob, boolean doWriteTablesXlsx, boolean doWritePageMdXlsx,
			boolean doCreateTitle, String useVersionStatus, boolean writeTextOnWordLevel, boolean doBlackening, 
			boolean splitTextfiles, String start_tag, String end_tag, String attribute_names, String ignoreString, Set<String> selectedTags, String font) {
		this();
		this.pages = pages;
		this.doWriteMets = doWriteMets;
		this.doWriteImages = doWriteImages;
		this.doExportPageXml = doExportPageXml;
		this.doExportAltoXml = doExportAltoXml;
		this.doWriteStructureInMets = doWriteStructureInMets;
		this.doExportSingleTxtFiles = doExportSingleTxtFiles;
		this.doWritePdf = doWritePdf;
		this.doWriteTei = doWriteTei;
		this.doWriteDocx = doWriteDocx;
		this.doWriteOneTxt = doWriteTxt;
		this.doWriteTagsXlsx = doWriteTagsXlsx;
		this.doWriteTagsIob = doWriteTagsIob;
		this.doWriteTablesXlsx = doWriteTablesXlsx;
		this.doWritePageMdXlsx = doWritePageMdXlsx;
		this.doCreateTitle = doCreateTitle;
		this.useVersionStatus = useVersionStatus;
		this.writeTextOnWordLevel = writeTextOnWordLevel;
		this.doBlackening = doBlackening;
		this.selectedTags = selectedTags;
		this.font = font;
		this.doSplitTxt = splitTextfiles;
		this.start_tagname_for_split = start_tag;
		this.end_tagname_for_split = end_tag;
		this.attributes_for_split_name = attribute_names;
		this.tags_to_ignore_for_split = ignoreString;
	}
	
	

	public boolean isDoWriteTagsXlsx() {
		return doWriteTagsXlsx;
	}

	public void setDoWriteTagsXlsx(boolean doWriteTagsXlsx) {
		this.doWriteTagsXlsx = doWriteTagsXlsx;
	}
	
	public boolean isDoWriteTagsIob() {
		return doWriteTagsIob;
	}

	public void setDoWriteTagsIob(boolean doWriteTagsIob) {
		this.doWriteTagsIob = doWriteTagsIob;
	}

	public boolean isDoWriteTablesXlsx() {
		return doWriteTablesXlsx;
	}

	public void setDoWriteTablesXlsx(boolean doWriteTablesXlsx) {
		this.doWriteTablesXlsx = doWriteTablesXlsx;
	}
	
	public boolean isDoWriteTableSingleColumn() {
		return doWriteTableSingleColumn;
	}

	public void setDoWriteTableSingleColumn(boolean doWriteTableSingleColumn) {
		this.doWriteTableSingleColumn = doWriteTableSingleColumn;
	}

	public int getTableColumn() {
		return tableColumn;
	}

	public void setTableColumn(int tableColumn) {
		this.tableColumn = tableColumn;
	}

	public boolean isDoWriteOneTableForDoc() {
		return doWriteOneTableForDoc;
	}

	public void setDoWriteOneTableForDoc(boolean doWriteOneTableForDoc) {
		this.doWriteOneTableForDoc = doWriteOneTableForDoc;
	}

	public boolean isDoWritePageMdXlsx() {
		return doWritePageMdXlsx;
	}

	public void setDoWritePageMdXlsx(boolean doWritePageMdXlsx) {
		this.doWritePageMdXlsx = doWritePageMdXlsx;
	}

	public boolean isWriteTextOnWordLevel() {
		return writeTextOnWordLevel;
	}

	public void setWriteTextOnWordLevel(boolean writeTextOnWordLevel) {
		this.writeTextOnWordLevel = writeTextOnWordLevel;
	}

	public boolean isDoBlackening() {
		return doBlackening;
	}

	public void setDoBlackening(boolean doBlackening) {
		this.doBlackening = doBlackening;
	}

	/**
	 * Helper method that parses the pages string (this.pages) with a given number of pages (nPages) into a set of page indices (starting from 0!)
	 */
	public Set<Integer> getPageIndices(int nPages) {
		if (StringUtils.isEmpty(this.pages))
			return null;
		
		try {
			return CoreUtils.parseRangeListStr(this.pages, nPages); 
		} catch (IOException e) {
			logger.warn("Could not pares pages string '"+pages+"' - "+e.getMessage());
			return null;
		}
	}

//	public void setPageIndices(Set<Integer> pageIndices) {
//		this.pageIndices = pageIndices;
//		
//		if (this.pageIndices != null && this.pageIndices.isEmpty()) // if no pages -> set to null -> means all pages in tei export
//			this.pageIndices = null;
//	}
	
	

	public String getFont() {
		return font;
	}

	public void setFont(String font) {
		this.font = font;
	}

	public Set<String> getSelectedTags() {
		return selectedTags;
	}

	public String getPages() {
		return pages;
	}

	public void setPages(String pages) {
		this.pages = pages;
	}

	public void setSelectedTags(Set<String> selectedTags) {
		this.selectedTags = selectedTags;
	}

	public boolean isDoExportDocMetadata() {
		return doExportDocMetadata;
	}
	
	public void setDoExportDocMetadata(boolean doExportDocMetadata) {
		this.doExportDocMetadata = doExportDocMetadata;
	}
	
	public boolean isDoWriteMets() {
		return doWriteMets;
	}

	public void setDoWriteMets(boolean doWriteMets) {
		this.doWriteMets = doWriteMets;
	}

	public boolean isDoWriteImages() {
		return doWriteImages;
	}

	public void setDoWriteImages(boolean doWriteImages) {
		this.doWriteImages = doWriteImages;
	}

	public boolean isDoExportPageXml() {
		return doExportPageXml;
	}

	public void setDoExportPageXml(boolean doExportPageXml) {
		this.doExportPageXml = doExportPageXml;
	}

	public boolean isDoWriteStructureInMets() {
		return doWriteStructureInMets;
	}

	public void setDoWriteStructureInMets(boolean doWriteStructureInMets) {
		this.doWriteStructureInMets = doWriteStructureInMets;
	}

	public boolean isDoExportAltoXml() {
		return doExportAltoXml;
	}

	public void setDoExportAltoXml(boolean doExportAltoXml) {
		this.doExportAltoXml = doExportAltoXml;
	}

	public boolean isDoExportSingleTxtFiles() {
		return doExportSingleTxtFiles;
	}

	public void setDoExportSingleTxtFiles(boolean doExportSingleTxtFiles) {
		this.doExportSingleTxtFiles = doExportSingleTxtFiles;
	}

	public boolean isDoWritePdf() {
		return doWritePdf;
	}

	public void setDoWritePdf(boolean doWritePdf) {
		this.doWritePdf = doWritePdf;
	}

	public boolean isDoWriteTei() {
		return doWriteTei;
	}

	public void setDoWriteTei(boolean doWriteTei) {
		this.doWriteTei = doWriteTei;
	}

	public boolean isDoWriteDocx() {
		return doWriteDocx;
	}

	public void setDoWriteDocx(boolean doWriteDocx) {
		this.doWriteDocx = doWriteDocx;
	}

	public boolean isDoWriteTxt() {
		return doWriteOneTxt;
	}

	public boolean isDoSplitTxt() {
		return doSplitTxt;
	}

	public void setDoSplitTxt(boolean doSplitTxt) {
		this.doSplitTxt = doSplitTxt;
	}

	public String getStart_tagname_for_split() {
		return start_tagname_for_split;
	}

	public void setStart_tagname_for_split(String start_tagname_for_split) {
		this.start_tagname_for_split = start_tagname_for_split;
	}

	public String getEnd_tagname_for_split() {
		return end_tagname_for_split;
	}

	public void setEnd_tagname_for_split(String end_tagname_for_split) {
		this.end_tagname_for_split = end_tagname_for_split;
	}

	public String getAttributes_for_split_name() {
		return attributes_for_split_name;
	}

	public void setAttributes_for_split_name(String attributes_for_split_name) {
		this.attributes_for_split_name = attributes_for_split_name;
	}

	public String getTags_to_ignore_for_split() {
		return tags_to_ignore_for_split;
	}

	public void setTags_to_ignore_for_split(String tags_to_ignore_for_split) {
		this.tags_to_ignore_for_split = tags_to_ignore_for_split;
	}

	public void setDoWriteTxt(boolean doWriteTxt) {
		this.doWriteOneTxt = doWriteTxt;
	}

	public boolean isDoCreateTitle() {
		return doCreateTitle;
	}

	public void setDoCreateTitle(boolean doCreateTitle) {
		this.doCreateTitle = doCreateTitle;
	}

	public String getUseVersionStatus() {
		return useVersionStatus;
	}

	public void setUseVersionStatus(String useVersionStatus) {
		this.useVersionStatus = useVersionStatus;
	}
	
	public String getDir() {
		return dir;
	}

	public void setDir(String dir) {
		this.dir = dir;
	}

	public boolean isSplitIntoWordsInAltoXml() {
		return splitIntoWordsInAltoXml;
	}

	public void setSplitIntoWordsInAltoXml(boolean splitIntoWordsInAltoXml) {
		this.splitIntoWordsInAltoXml = splitIntoWordsInAltoXml;
	}

	public String getPageDirName() {
		return pageDirName;
	}

	public void setPageDirName(String pageDirName) {
		this.pageDirName = pageDirName;
	}

	public String getFileNamePattern() {
		return fileNamePattern;
	}

	public void setFileNamePattern(String fileNamePattern) {
		this.fileNamePattern = fileNamePattern;
	}

	public boolean isUseHttps() {
		return useHttps;
	}

	public void setUseHttps(boolean useHttps) {
		this.useHttps = useHttps;
	}

	public ImgType getRemoteImgQuality() {
		return remoteImgQuality;
	}

	public void setRemoteImgQuality(ImgType remoteImgQuality) {
		if(remoteImgQuality == null) {
			//remoteImgQuality shall never be null
			this.remoteImgQuality = ImgType.orig;
		} else {
			this.remoteImgQuality = remoteImgQuality;
		}
	}

	public boolean isDoOverwrite() {
		return doOverwrite;
	}

	public void setDoOverwrite(boolean doOverwrite) {
		this.doOverwrite = doOverwrite;
	}

	public boolean isUseOcrMasterDir() {
		return useOcrMasterDir;
	}

	public void setUseOcrMasterDir(boolean useOcrMasterDir) {
		this.useOcrMasterDir = useOcrMasterDir;
	}

	public boolean isExportTranscriptMetadata() {
		return exportTranscriptMetadata;
	}

	public void setExportTranscriptMetadata(boolean exportTranscriptMetadata) {
		this.exportTranscriptMetadata = exportTranscriptMetadata;
	}
	
	public boolean isUpdatePageXmlImageDimensions() {
		return updatePageXmlImageDimensions;
	}

	public void setUpdatePageXmlImageDimensions(boolean updatePageXmlImageDimensions) {
		this.updatePageXmlImageDimensions = updatePageXmlImageDimensions;
	}

	// --- utility methods ---

	public boolean isTagSelected(String tagName) {
		return selectedTags == null || selectedTags.contains(tagName);
	}
	
	public boolean isTaggableExport() {
		return (isDoWritePdf() || isDoWriteDocx() || isDoWriteTagsXlsx() || isDoWriteTei() || isDoWriteTagsIob());			
//		return (isDoWritePdf() || isDoWriteDocx() || isDoWriteTagsXlsx() || isDoWriteTei())
//				&& (isHighlightTags() || isDocxTagExport() || isTagXlsxExport());
	}
	
	/*
	 * export only images - that means we do not load the transcripts during export and save time
	 */
	public boolean exportImagesOnly(){
		return (isDoWriteMets() && doWriteImages && !doExportAltoXml && !doExportPageXml && !doExportSingleTxtFiles &&
				!(isDoWritePdf() || isDoWriteDocx() || isDoWriteTxt() || isDoWriteTagsXlsx() || isDoWriteTei() || isDoWriteTablesXlsx() || isDoWritePageMdXlsx()));
		
	}

	public static CommonExportPars getDefaultParSetForHtrTraining() {
		CommonExportPars opts = new CommonExportPars();
		//don't write a metadata XML
		opts.setDoExportDocMetadata(false);
		opts.setDoWriteImages(true);
		opts.setDoExportAltoXml(false);
		opts.setDoExportPageXml(true);
		//export pageXml to same dir as images
		opts.setPageDirName("");
		opts.setUseOcrMasterDir(false);
		opts.setDoWriteMets(false);
		//use page ID as filename to not run into problems with overlaps. Consider e.g. "0000001.jpg"
		opts.setFileNamePattern(ExportFilePatternUtils.PAGEID_PATTERN);
		return opts;
	}

	public static CommonExportPars getDefaultParSetForOcr() {
		CommonExportPars opts = new CommonExportPars();
		opts.setDoOverwrite(true);
		opts.setDoWriteMets(false);
		opts.setUseOcrMasterDir(true);
		opts.setDoWriteImages(true);
		opts.setDoExportPageXml(false);
		opts.setDoExportAltoXml(false);
		//store files with pageID as name. Important for matching result files to original document!
		opts.setFileNamePattern(ExportFilePatternUtils.PAGEID_PATTERN);
		return opts;
	}

	public boolean isDoWriteWordsIntoTable() {
		return doWriteWordsIntoTable;
	}

	public void setDoWriteWordsIntoTable(boolean doWriteWordsIntoTable) {
		this.doWriteWordsIntoTable = doWriteWordsIntoTable;
	}

	public List<String> getStructureTagNames() {
		return structureTagNames;
	}

	public void setStructureTagNames(List<String> structureTagNames) {
		this.structureTagNames = structureTagNames;
	}

	public boolean isDoIncludeImagesInXlsx() {
		return doIncludeImagesInXlsx;
	}

	public void setDoIncludeImagesInXlsx(boolean doIncludeImagesInXlsx) {
		this.doIncludeImagesInXlsx = doIncludeImagesInXlsx;
	}

	public boolean isDoWriteStructuresInXlsx() {
		return doWriteStructuresInXlsx;
	}

	public void setDoWriteStructuresInXlsx(boolean doWriteStructuresInXlsx) {
		this.doWriteStructuresInXlsx = doWriteStructuresInXlsx;
	}
	
	public boolean isDoWriteStructuresRowBased() {
		return doWriteStructuresRowBased;
	}

	public void setDoWriteStructuresRowBased(boolean doWriteStructuresRowBased) {
		this.doWriteStructuresRowBased = doWriteStructuresRowBased;
	}

	public boolean isDoResume() {
		return doResume;
	}

	public void setDoResume(boolean doResume) {
		this.doResume = doResume;
	}

	public String getCustomEmail() {
		return customEmail;
	}

	public void setCustomEmail(String customEmail) {
		this.customEmail = customEmail;
	}

	public void setUseCache(boolean newValue) {
		this.useCache = newValue;;
	}
	public boolean isUseCache() {
		return useCache;
	}

	public void setFilterHiddenPages(boolean filterHiddenPages) {
		this.filterHiddenPages = filterHiddenPages;

	}
	public boolean isFilterHiddenPages() {
		return filterHiddenPages;
	}


	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof CommonExportPars)) return false;
		CommonExportPars that = (CommonExportPars) o;
		return doExportDocMetadata == that.doExportDocMetadata && doWriteMets == that.doWriteMets && doWriteImages == that.doWriteImages && doExportPageXml == that.doExportPageXml && doExportAltoXml == that.doExportAltoXml && doExportSingleTxtFiles == that.doExportSingleTxtFiles && doWritePdf == that.doWritePdf && doWriteTei == that.doWriteTei && doWriteDocx == that.doWriteDocx && doWriteOneTxt == that.doWriteOneTxt && doSplitTxt == that.doSplitTxt && doWriteTagsXlsx == that.doWriteTagsXlsx && doWriteTagsIob == that.doWriteTagsIob && doWriteTablesXlsx == that.doWriteTablesXlsx && doWriteTableSingleColumn == that.doWriteTableSingleColumn && doIncludeImagesInXlsx == that.doIncludeImagesInXlsx && doWriteOneTableForDoc == that.doWriteOneTableForDoc && doWriteStructuresInXlsx == that.doWriteStructuresInXlsx && doWriteWordsIntoTable == that.doWriteWordsIntoTable && doWritePageMdXlsx == that.doWritePageMdXlsx && doWriteStructureInMets == that.doWriteStructureInMets && tableColumn == that.tableColumn && doCreateTitle == that.doCreateTitle && writeTextOnWordLevel == that.writeTextOnWordLevel && doBlackening == that.doBlackening && splitIntoWordsInAltoXml == that.splitIntoWordsInAltoXml && useHttps == that.useHttps && doOverwrite == that.doOverwrite && useOcrMasterDir == that.useOcrMasterDir && exportTranscriptMetadata == that.exportTranscriptMetadata && updatePageXmlImageDimensions == that.updatePageXmlImageDimensions && doResume == that.doResume && Objects.equals(pages, that.pages) && Objects.equals(useVersionStatus, that.useVersionStatus) && Objects.equals(start_tagname_for_split, that.start_tagname_for_split) && Objects.equals(end_tagname_for_split, that.end_tagname_for_split) && Objects.equals(attributes_for_split_name, that.attributes_for_split_name) && Objects.equals(tags_to_ignore_for_split, that.tags_to_ignore_for_split) && Objects.equals(selectedTags, that.selectedTags) && Objects.equals(structureTagNames, that.structureTagNames) && Objects.equals(dir, that.dir) && Objects.equals(font, that.font) && Objects.equals(pageDirName, that.pageDirName) && Objects.equals(fileNamePattern, that.fileNamePattern) && remoteImgQuality == that.remoteImgQuality && Objects.equals(customEmail, that.customEmail);
	}

	@Override
	public int hashCode() {
		return Objects.hash(pages, doExportDocMetadata, doWriteMets, doWriteImages, doExportPageXml, doExportAltoXml, doExportSingleTxtFiles, doWritePdf, doWriteTei, doWriteDocx, doWriteOneTxt, doSplitTxt, doWriteTagsXlsx, doWriteTagsIob, doWriteTablesXlsx, doWriteTableSingleColumn, doIncludeImagesInXlsx, doWriteOneTableForDoc, doWriteStructuresInXlsx, doWriteWordsIntoTable, doWritePageMdXlsx, doWriteStructureInMets, tableColumn, doCreateTitle, useVersionStatus, start_tagname_for_split, end_tagname_for_split, attributes_for_split_name, tags_to_ignore_for_split, writeTextOnWordLevel, doBlackening, selectedTags, structureTagNames, dir, font, splitIntoWordsInAltoXml, pageDirName, fileNamePattern, useHttps, remoteImgQuality, doOverwrite, useOcrMasterDir, exportTranscriptMetadata, updatePageXmlImageDimensions, doResume, customEmail);
	}

	@Override
	public String toString() {
		return "CommonExportPars{" +
				"pages='" + pages + '\'' +
				", doExportDocMetadata=" + doExportDocMetadata +
				", doWriteMets=" + doWriteMets +
				", doWriteImages=" + doWriteImages +
				", doExportPageXml=" + doExportPageXml +
				", doExportAltoXml=" + doExportAltoXml +
				", doExportSingleTxtFiles=" + doExportSingleTxtFiles +
				", doWritePdf=" + doWritePdf +
				", doWriteTei=" + doWriteTei +
				", doWriteDocx=" + doWriteDocx +
				", doWriteOneTxt=" + doWriteOneTxt +
				", doSplitTxt=" + doSplitTxt +
				", doWriteTagsXlsx=" + doWriteTagsXlsx +
				", doWriteTagsIob=" + doWriteTagsIob +
				", doWriteTablesXlsx=" + doWriteTablesXlsx +
				", doWriteTableSingleColumn=" + doWriteTableSingleColumn +
				", doIncludeImagesInXlsx=" + doIncludeImagesInXlsx +
				", doWriteOneTableForDoc=" + doWriteOneTableForDoc +
				", doWriteStructuresInXlsx=" + doWriteStructuresInXlsx +
				", doWriteWordsIntoTable=" + doWriteWordsIntoTable +
				", doWritePageMdXlsx=" + doWritePageMdXlsx +
				", doWriteStructureInMets=" + doWriteStructureInMets +
				", tableColumn=" + tableColumn +
				", doCreateTitle=" + doCreateTitle +
				", useVersionStatus='" + useVersionStatus + '\'' +
				", start_tagname_for_split='" + start_tagname_for_split + '\'' +
				", end_tagname_for_split='" + end_tagname_for_split + '\'' +
				", attributes_for_split_name='" + attributes_for_split_name + '\'' +
				", tags_to_ignore_for_split='" + tags_to_ignore_for_split + '\'' +
				", writeTextOnWordLevel=" + writeTextOnWordLevel +
				", doBlackening=" + doBlackening +
				", selectedTags=" + selectedTags +
				", structureTagNames=" + structureTagNames +
				", dir='" + dir + '\'' +
				", font='" + font + '\'' +
				", splitIntoWordsInAltoXml=" + splitIntoWordsInAltoXml +
				", pageDirName='" + pageDirName + '\'' +
				", fileNamePattern='" + fileNamePattern + '\'' +
				", useHttps=" + useHttps +
				", remoteImgQuality=" + remoteImgQuality +
				", doOverwrite=" + doOverwrite +
				", useOcrMasterDir=" + useOcrMasterDir +
				", exportTranscriptMetadata=" + exportTranscriptMetadata +
				", updatePageXmlImageDimensions=" + updatePageXmlImageDimensions +
				", doResume=" + doResume +
				", customEmail='" + customEmail + '\'' +
				'}';
	}

	public boolean isStoreValidPageXml() {
		return storeValidPageXml;
	}

	public void setStoreValidPageXml(boolean storeValidPageXml) {
		this.storeValidPageXml = storeValidPageXml;
	}

	public boolean isStore2019PageXml() {
		return store2019PageXml;
	}

	public void setStore2019PageXml(boolean store2019PageXml) {
		this.store2019PageXml = store2019PageXml;
	}

}
