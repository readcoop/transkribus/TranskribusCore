package eu.transkribus.core.model.builder.pdf;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import org.dea.fimgstoreclient.beans.ImgType;
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class PdfExportPars {
	public static final String PARAMETER_KEY = "pdfPars";
	
	boolean doPdfImagesOnly=false;
	boolean doPdfImagesPlusText=true;
	boolean doPdfWithTextPages=false;
	boolean doPdfWithTags=false; 
	boolean doPdfWithArticles=false;
	boolean doPdfA=true;
	boolean doSinglePagePdfs=false;
	//instead of using the line polygons to put text on image we calculate a new bounding rectangle with the baseline and the defaultLineHeight
	boolean doUseLinePolygons=false;
	/*
	 * increase this so that the text underneath the image gets more readable 
	 * BUT for some documents the lines may overlap then and the text gets mixed/messed up
	 */
	Integer defaultLineHeight = 20;
	
	ImgType pdfImgQuality = ImgType.view;
	
	public PdfExportPars() {
		
	}
	
	public PdfExportPars(boolean doPdfImagesOnly, boolean doPdfImagesPlusText, boolean doPdfWithTextPages,
			boolean doPdfWithTags, boolean doPdfWithArticles) {
		super();
		this.doPdfImagesOnly = doPdfImagesOnly;
		this.doPdfImagesPlusText = doPdfImagesPlusText;
		this.doPdfWithTextPages = doPdfWithTextPages;
		this.doPdfWithTags = doPdfWithTags;
		this.doPdfWithArticles = doPdfWithArticles;
		this.doPdfA = false;
	}

	public PdfExportPars(boolean doPdfImagesOnly, boolean doPdfImagesPlusText, boolean doPdfWithTextPages,
			boolean doPdfWithTags, boolean doPdfWithArticles, boolean doPdfA) {
		super();
		this.doPdfImagesOnly = doPdfImagesOnly;
		this.doPdfImagesPlusText = doPdfImagesPlusText;
		this.doPdfWithTextPages = doPdfWithTextPages;
		this.doPdfWithTags = doPdfWithTags;
		this.doPdfWithArticles = doPdfWithArticles;
		this.doPdfA = doPdfA;
	}
	
	public PdfExportPars(boolean doPdfImagesOnly, boolean doPdfImagesPlusText, boolean doPdfWithTextPages,
			boolean doPdfWithTags, boolean doPdfWithArticles, boolean doPdfA, boolean doSinglePagePDFs) {
		super();
		this.doPdfImagesOnly = doPdfImagesOnly;
		this.doPdfImagesPlusText = doPdfImagesPlusText;
		this.doPdfWithTextPages = doPdfWithTextPages;
		this.doPdfWithTags = doPdfWithTags;
		this.doPdfWithArticles = doPdfWithArticles;
		this.doPdfA = doPdfA;
		this.doSinglePagePdfs = doSinglePagePDFs;
	}

	public boolean isDoPdfImagesOnly() {
		return doPdfImagesOnly;
	}

	public void setDoPdfImagesOnly(boolean doPdfImagesOnly) {
		this.doPdfImagesOnly = doPdfImagesOnly;
	}

	public boolean isDoPdfImagesPlusText() {
		return doPdfImagesPlusText;
	}

	public void setDoPdfImagesPlusText(boolean doPdfImagesPlusText) {
		this.doPdfImagesPlusText = doPdfImagesPlusText;
	}

	public boolean isDoPdfWithTextPages() {
		return doPdfWithTextPages;
	}

	public void setDoPdfWithTextPages(boolean doPdfWithTextPages) {
		this.doPdfWithTextPages = doPdfWithTextPages;
	}

	public boolean isDoPdfWithTags() {
		return doPdfWithTags;
	}

	public void setDoPdfWithTags(boolean doPdfWithTags) {
		this.doPdfWithTags = doPdfWithTags;
	}
	
	public boolean isDoPdfWithArticles() {
		return doPdfWithArticles;
	}

	public void setDoPdfWithArticles(boolean doPdfWithArticles) {
		this.doPdfWithArticles = doPdfWithArticles;
	}

	public boolean isDoPdfA() {
		return doPdfA;
	}

	public void setDoPdfA(boolean doPdfA) {
		this.doPdfA = doPdfA;
	}

	public ImgType getPdfImgQuality() {
		return pdfImgQuality;
	}

	public void setPdfImgQuality(ImgType pdfImgQuality) {
		this.pdfImgQuality = pdfImgQuality;
	}

	@Override
	public String toString() {
		return "PdfExportPars [doPdfImagesOnly=" + doPdfImagesOnly + ", doPdfImagesPlusText=" + doPdfImagesPlusText
				+ ", doPdfWithTextPages=" + doPdfWithTextPages + ", doPdfWithTags=" + doPdfWithTags + ", doPdfWithArticles=" + doPdfWithArticles
				+ ", doPdfA=" + doPdfA + ", pdfImgQuality="
				+ pdfImgQuality + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (doPdfImagesOnly ? 1231 : 1237);
		result = prime * result + (doPdfImagesPlusText ? 1231 : 1237);
		result = prime * result + (doPdfWithTags ? 1231 : 1237);
		result = prime * result + (doPdfWithArticles ? 1231 : 1237);
		result = prime * result + (doPdfA ? 1231 : 1237);
		result = prime * result + (doPdfWithTextPages ? 1231 : 1237);
		result = prime * result + ((pdfImgQuality == null) ? 0 : pdfImgQuality.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PdfExportPars other = (PdfExportPars) obj;
		if (doPdfImagesOnly != other.doPdfImagesOnly)
			return false;
		if (doPdfImagesPlusText != other.doPdfImagesPlusText)
			return false;
		if (doPdfWithTags != other.doPdfWithTags)
			return false;
		if (isDoPdfWithArticles() != other.doPdfWithArticles)
			return false;
		if (doPdfA != other.doPdfA)
			return false;
		if (doPdfWithTextPages != other.doPdfWithTextPages)
			return false;
		if (pdfImgQuality != other.pdfImgQuality)
			return false;
		return true;
	}

	public boolean isDoSinglePagePdfs() {
		return doSinglePagePdfs;
	}

	public void setDoSinglePagePdfs(boolean doSinglePagePdfs) {
		this.doSinglePagePdfs = doSinglePagePdfs;
	}

	public boolean isDoUseLinePolygons() {
		return doUseLinePolygons;
	}

	public void setDoUseLinePolygons(boolean doUseLinePolygons) {
		this.doUseLinePolygons = doUseLinePolygons;
	}

	public Integer getDefaultLineHeight() {
		return defaultLineHeight;
	}

	public void setDefaultLineHeight(Integer defaultLineHeight) {
		this.defaultLineHeight = defaultLineHeight;
	}
}
