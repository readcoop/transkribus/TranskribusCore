package eu.transkribus.core.model.builder.tei;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * TEI specific extension of ExportPars
 * 
 * for Darios TEI xslttransformation new params are needed as following:
 * bounding-rectangles (export polygones as bounding rectangle) default: true()
 * rs tag (export person, place, org as tei:rs[@type] (true) or as tei:persName etc.) default: true()
 * tokenize (tokenize white space) default: false()
 * combine continued tags (pull together @continued): default: false()
 * ab tag: if true, all regions will be output as tei:ab with @type (except for paragraph → tei:p and
 * 		heading → tei:head); if false, regions whose type corresponds to a TEI element are translated into to this element. Default: false()
 * word-coordinates: (store to in tei:facsimile//tei:zone if true) default: false()
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class TeiExportPars {
	public static final String PARAMETER_KEY = "teiPars";
	
	public static final String LINE_BREAK_TYPE_LINE_TAG = "LINE_TAG";
	public static final String LINE_BREAK_TYPE_LINE_BREAKS = "LINE_BREAKS";
	
//	CommonExportPars commonPars = new CommonExportPars();
		
	boolean regionZones=true;
	boolean lineZones=true;
	boolean wordZones=false;
	boolean boundingBoxCoords=false;
	String linebreakType = LINE_BREAK_TYPE_LINE_TAG;
	
	boolean lineBreakAtBeginningOfLine=true;
	boolean pbImageNameAsXmlId=true;
	
	boolean lineXmlIds=false; // FIXME
	boolean exportPredefinedTagsAndAttsInTei = false;
	
	/*
	 * September 2023: introcuced new parameters for xslt from Dario
	 * 
	 */
	boolean useXslTransformation = false;
	boolean useBoundingRects = true;
	boolean useRsTags = true;
	boolean tokenizeWhitespace = false;
	boolean combineContinuedTags = false;
	boolean useAbElements = false;
	boolean useWordCoordinates = false;
	boolean withoutTextline = false;
		
	public TeiExportPars() {
	}
	
	public TeiExportPars(boolean regionZones, boolean lineZones, boolean wordZones, boolean boundingBoxCoords,
			String linebreakType) {
		this(regionZones, lineZones, wordZones, boundingBoxCoords, linebreakType, false);
	}

	public TeiExportPars(boolean regionZones, boolean lineZones, boolean wordZones, boolean boundingBoxCoords,
			String linebreakType, boolean exportPredefinedTagsAndAtts) {
		super();
		this.regionZones = regionZones;
		this.lineZones = lineZones;
		this.wordZones = wordZones;
		this.boundingBoxCoords = boundingBoxCoords;
		this.linebreakType = linebreakType;
		this.exportPredefinedTagsAndAttsInTei = exportPredefinedTagsAndAtts;
	}
	
	public TeiExportPars(boolean regionZones, boolean lineZones, boolean wordZones, boolean boundingBoxCoords,
			String linebreakType, boolean exportPredefinedTagsAndAtts, boolean boundingRectangles, boolean combine, 
			boolean ab, boolean tokenize, boolean rs, boolean wordCoords) {
		super();
		this.regionZones = regionZones;
		this.lineZones = lineZones;
		this.wordZones = wordZones;
		this.boundingBoxCoords = boundingBoxCoords;
		this.linebreakType = linebreakType;
		this.exportPredefinedTagsAndAttsInTei = exportPredefinedTagsAndAtts;
		this.boundingBoxCoords = boundingRectangles;
		this.combineContinuedTags = combine;
		this.useAbElements = ab;
		this.tokenizeWhitespace = tokenize;
		this.useRsTags = rs;
		this.useWordCoordinates = wordCoords;
	}

	public boolean isRegionZones() {
		return regionZones;
	}

	public void setRegionZones(boolean regionZones) {
		this.regionZones = regionZones;
	}

	public boolean isLineZones() {
		return lineZones;
	}

	public void setLineZones(boolean lineZones) {
		this.lineZones = lineZones;
	}

	public boolean isWordZones() {
		return wordZones;
	}

	public void setWordZones(boolean wordZones) {
		this.wordZones = wordZones;
	}

	public boolean isBoundingBoxCoords() {
		return boundingBoxCoords;
	}

	public void setBoundingBoxCoords(boolean boundingBoxCoords) {
		this.boundingBoxCoords = boundingBoxCoords;
	}

	public String getLinebreakType() {
		return linebreakType;
	}

	public void setLinebreakType(String linebreakType) {
		this.linebreakType = linebreakType;
	}

	// utility methods:
	public boolean hasZones() {
		return regionZones == true || lineZones == true || wordZones == true;
	}
	
	public boolean isLineBreakType() {
		return (linebreakType.contentEquals(TeiExportPars.LINE_BREAK_TYPE_LINE_BREAKS));
	}
	
	public boolean isLineTagType() {
		return (linebreakType.contentEquals(TeiExportPars.LINE_BREAK_TYPE_LINE_TAG));
	}

	public boolean isLineBreakAtBeginningOfLine() {
		return lineBreakAtBeginningOfLine;
	}

	public void setLineBreakAtBeginningOfLine(boolean lineBreakAtBeginningOfLine) {
		this.lineBreakAtBeginningOfLine = lineBreakAtBeginningOfLine;
	}

	public boolean isPbImageNameAsXmlId() {
		return pbImageNameAsXmlId;
	}

	public void setPbImageNameAsXmlId(boolean pbImageNameAsXmlId) {
		this.pbImageNameAsXmlId = pbImageNameAsXmlId;
	}

	public boolean isTokenizeWhitespace() {
		return tokenizeWhitespace;
	}

	public void setTokenizeWhitespace(boolean tokenizeWhitespace) {
		this.tokenizeWhitespace = tokenizeWhitespace;
	}

	public boolean isCombineContinuedTags() {
		return combineContinuedTags;
	}

	public void setCombineContinuedTags(boolean combineContinuedTags) {
		this.combineContinuedTags = combineContinuedTags;
	}

	public boolean isUseWordCoordinates() {
		return useWordCoordinates;
	}

	public void setUseWordCoordinates(boolean useWordCoordinates) {
		this.useWordCoordinates = useWordCoordinates;
	}

	public boolean isUseBoundingRects() {
		return useBoundingRects;
	}

	public void setUseBoundingRects(boolean useBoundingRects) {
		this.useBoundingRects = useBoundingRects;
	}

	public boolean isUseRsTags() {
		return useRsTags;
	}

	public void setUseRsTags(boolean useRsTags) {
		this.useRsTags = useRsTags;
	}

	public boolean isUseAbElements() {
		return useAbElements;
	}

	public void setUseAbElements(boolean useAbElements) {
		this.useAbElements = useAbElements;
	}

	public boolean isUseXslTransformation() {
		return useXslTransformation;
	}

	public void setUseXslTransformation(boolean useXslTransformation) {
		this.useXslTransformation = useXslTransformation;
	}

	public boolean isWithoutTextline() {
		return withoutTextline;
	}

	public void setWithoutTextline(boolean withoutTextline) {
		this.withoutTextline = withoutTextline;
	}
	
	@Override
	public String toString() {
		return "TeiExportPars [regionZones=" + regionZones + ", lineZones=" + lineZones + ", wordZones=" + wordZones
				+ ", boundingBoxCoords=" + boundingBoxCoords + ", linebreakType=" + linebreakType
				+ ", lineBreakAtBeginningOfLine=" + lineBreakAtBeginningOfLine + ", pbImageNameAsXmlId="
				+ pbImageNameAsXmlId + ", lineXmlIds=" + lineXmlIds + ", exportPredefinedTagsAndAttsInTei="
				+ exportPredefinedTagsAndAttsInTei + ", useXslTransformation=" + useXslTransformation
				+ ", useBoundingRects=" + useBoundingRects + ", useRsTags=" + useRsTags + ", tokenizeWhitespace="
				+ tokenizeWhitespace + ", combineContinuedTags=" + combineContinuedTags + ", useAbElements="
				+ useAbElements + ", useWordCoordinates=" + useWordCoordinates + ", withoutTextline=" + withoutTextline
				+ "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (boundingBoxCoords ? 1231 : 1237);
		result = prime * result + (combineContinuedTags ? 1231 : 1237);
		result = prime * result + (exportPredefinedTagsAndAttsInTei ? 1231 : 1237);
		result = prime * result + (lineBreakAtBeginningOfLine ? 1231 : 1237);
		result = prime * result + (lineXmlIds ? 1231 : 1237);
		result = prime * result + (lineZones ? 1231 : 1237);
		result = prime * result + ((linebreakType == null) ? 0 : linebreakType.hashCode());
		result = prime * result + (pbImageNameAsXmlId ? 1231 : 1237);
		result = prime * result + (regionZones ? 1231 : 1237);
		result = prime * result + (tokenizeWhitespace ? 1231 : 1237);
		result = prime * result + (useAbElements ? 1231 : 1237);
		result = prime * result + (useBoundingRects ? 1231 : 1237);
		result = prime * result + (useRsTags ? 1231 : 1237);
		result = prime * result + (useWordCoordinates ? 1231 : 1237);
		result = prime * result + (useXslTransformation ? 1231 : 1237);
		result = prime * result + (withoutTextline ? 1231 : 1237);
		result = prime * result + (wordZones ? 1231 : 1237);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TeiExportPars other = (TeiExportPars) obj;
		if (boundingBoxCoords != other.boundingBoxCoords)
			return false;
		if (combineContinuedTags != other.combineContinuedTags)
			return false;
		if (exportPredefinedTagsAndAttsInTei != other.exportPredefinedTagsAndAttsInTei)
			return false;
		if (lineBreakAtBeginningOfLine != other.lineBreakAtBeginningOfLine)
			return false;
		if (lineXmlIds != other.lineXmlIds)
			return false;
		if (lineZones != other.lineZones)
			return false;
		if (linebreakType == null) {
			if (other.linebreakType != null)
				return false;
		} else if (!linebreakType.equals(other.linebreakType))
			return false;
		if (pbImageNameAsXmlId != other.pbImageNameAsXmlId)
			return false;
		if (regionZones != other.regionZones)
			return false;
		if (tokenizeWhitespace != other.tokenizeWhitespace)
			return false;
		if (useAbElements != other.useAbElements)
			return false;
		if (useBoundingRects != other.useBoundingRects)
			return false;
		if (useRsTags != other.useRsTags)
			return false;
		if (useWordCoordinates != other.useWordCoordinates)
			return false;
		if (useXslTransformation != other.useXslTransformation)
			return false;
		if (withoutTextline != other.withoutTextline)
			return false;
		if (wordZones != other.wordZones)
			return false;
		return true;
	}
}
