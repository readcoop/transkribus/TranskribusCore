package eu.transkribus.core.model.builder.iiif;

import java.awt.Rectangle;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonString;
import javax.json.JsonValue;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;
import javax.ws.rs.core.Response.Status;
import javax.xml.bind.JAXBException;
import javax.xml.xpath.XPathExpressionException;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.primaresearch.io.UnsupportedFormatVersionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ibm.icu.impl.InvalidFormatException;

import de.digitalcollections.iiif.model.ImageContent;
import de.digitalcollections.iiif.model.MetadataEntry;
import de.digitalcollections.iiif.model.MimeType;
import de.digitalcollections.iiif.model.Motivation;
import de.digitalcollections.iiif.model.OtherContent;
import de.digitalcollections.iiif.model.Service;
import de.digitalcollections.iiif.model.image.ImageApiProfile;
import de.digitalcollections.iiif.model.image.ImageService;
import de.digitalcollections.iiif.model.jackson.IiifObjectMapper;
import de.digitalcollections.iiif.model.openannotation.Annotation;
import de.digitalcollections.iiif.model.openannotation.ContentAsText;
import de.digitalcollections.iiif.model.sharedcanvas.AnnotationList;
import de.digitalcollections.iiif.model.sharedcanvas.Canvas;
import de.digitalcollections.iiif.model.sharedcanvas.Layer;
import de.digitalcollections.iiif.model.sharedcanvas.Manifest;
import de.digitalcollections.iiif.model.sharedcanvas.Resource;
import de.digitalcollections.iiif.model.sharedcanvas.Sequence;
import eu.transkribus.core.exceptions.NullValueException;
import eu.transkribus.core.io.LocalDocConst;
import eu.transkribus.core.io.LocalDocReader;
import eu.transkribus.core.model.beans.TrpDoc;
import eu.transkribus.core.model.beans.TrpDocMetadata;
import eu.transkribus.core.model.beans.TrpImage;
import eu.transkribus.core.model.beans.TrpPage;
import eu.transkribus.core.model.beans.customtags.CustomTag;
import eu.transkribus.core.model.beans.pagecontent.PageType;
import eu.transkribus.core.model.beans.pagecontent.PcGtsType;
import eu.transkribus.core.model.beans.pagecontent_trp.TrpPageType;
import eu.transkribus.core.model.beans.pagecontent_trp.TrpRegionType;
import eu.transkribus.core.model.beans.pagecontent_trp.TrpTextLineType;
import eu.transkribus.core.util.DeaFileUtils;
import eu.transkribus.core.util.PointStrUtils;

public class IIIFUtils {
	
	private static final Logger logger = LoggerFactory.getLogger(IIIFUtils.class);
	static int totalPages = 0;
	static int totalFoldersCompleted;
	
	public static Manifest checkManifestValid(URL url) throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper iiifMapper = new IiifObjectMapper();
		if(checkIiifApi(url) == 200) {
			JSONObject validation = validateManifest(url);
			if(validation.getInt("okay") == 0) {
				logger.error("IIIF is not valid : ", validation);
				throw new IllegalArgumentException("IIIF is not valid (https://iiif.io/api/presentation/validator/service/) : "+validation.getString("error"));
			}
		}
		iiifMapper.enable(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT);
		String responseJson = getJSON(url.toString());
		logger.info(responseJson);
		Manifest manifest = iiifMapper.readValue(responseJson, Manifest.class);
		return manifest;
	}
	
	public static boolean checkManifest(URL url) throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper iiifMapper = new IiifObjectMapper();
		if(checkIiifApi(url) == 200) {
			JSONObject validation = validateManifest(url);
			if(validation.getInt("okay") == 0) {
				logger.error("IIIF is not valid : ", validation);
				throw new IllegalArgumentException("IIIF is not valid (https://iiif.io/api/presentation/validator/service/) : "+validation.getString("error"));
			}
			return true;
		}
		else {
			return false;
		}

	}
	public static Manifest getManifest(URL url) throws JsonParseException, JsonMappingException, IOException, NullPointerException {
		ObjectMapper iiifMapper = new IiifObjectMapper();
		iiifMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		iiifMapper.configure(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES, false);
		iiifMapper.configure(DeserializationFeature.FAIL_ON_NULL_FOR_PRIMITIVES, false);
		iiifMapper.configure(DeserializationFeature.FAIL_ON_UNRESOLVED_OBJECT_IDS, false);
		iiifMapper.configure(DeserializationFeature.FAIL_ON_INVALID_SUBTYPE, false);
		iiifMapper.configure(DeserializationFeature.FAIL_ON_MISSING_CREATOR_PROPERTIES, false);
		iiifMapper.configure(DeserializationFeature.FAIL_ON_MISSING_EXTERNAL_TYPE_ID_PROPERTY, false);
		iiifMapper.configure(DeserializationFeature.FAIL_ON_NULL_CREATOR_PROPERTIES, false);
		iiifMapper.configure(DeserializationFeature.FAIL_ON_NUMBERS_FOR_ENUMS, false);
		iiifMapper.enable(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT);
		iiifMapper.enable(DeserializationFeature.ACCEPT_EMPTY_ARRAY_AS_NULL_OBJECT);
//		iiifMapper.setSerializationInclusion(Include.NON_ABSENT);
//		iiifMapper.setSerializationInclusion(Include.NON_EMPTY);
		String responseJson;
		Manifest manifest = null;
		try {
			responseJson = getJSON(url.toString());
			logger.debug("response json: " + responseJson);
			manifest = iiifMapper.readValue(responseJson, Manifest.class);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return manifest;
	}


	public static String getJSON(String url) {
        HttpsURLConnection c = null;
	    try {
	        URL u = new URL(url);
            int status = 20;

            URLConnection tConn = u.openConnection();
			//FIXME this can't handle file-protocol URLs
            if (tConn instanceof HttpsURLConnection) {
            	c = (HttpsURLConnection) tConn;
                c.setRequestMethod("GET");
                c.setRequestProperty("Content-Type", "application/json");
                c.addRequestProperty("User-Agent", "Mozilla/5.0");
                c.setRequestProperty("Content-length", "0");
                c.setReadTimeout(5000);
                c.setConnectTimeout(5000);
                c.setUseCaches(false);
                c.setAllowUserInteraction(false);
                c.connect();
                status = c.getResponseCode();
                String responseMessage = c.getResponseMessage();
//                String contentEncoding = c.getContentEncoding();
//                int contentLength = c.getContentLength();

                logger.debug("responseCode: " + status + " responseMessage " + responseMessage);
            }
	        switch (status) {
	            case 200:
	            case 201:
	            	StringBuilder sb = new StringBuilder();
	                try (InputStream is = c.getInputStream();
	                   BufferedReader br = new BufferedReader(new InputStreamReader(is))) {
		                   String line;
	                       while ((line = br.readLine()) != null) {
	                    	   sb.append(line+"\n");
	                       }
	                   }
	                //logger.debug("json" + sb.toString());
	                return sb.toString();
				default:
					logger.warn("Undefined state: status code = {}", status);
	        }

	    } catch (MalformedURLException ex) {
	    	logger.debug("info MalformedURLException: " + ex.getMessage());
	    } catch (IOException ex) {
	    	logger.debug("info IOException: " + ex.getMessage());
	    } finally {
	       if (c != null) {
	          try {
	        	  logger.debug("c disconnect");
	              c.disconnect();
	          } catch (Exception ex) {
	        	  logger.info(ex.getMessage());
	          }
	       }
	    }
	    logger.debug("return null");
	    return null;
	}
	
	public static TrpDoc createDocFromIIIF(URL url, String path) throws JsonParseException, JsonMappingException, IOException, SQLException, ReflectiveOperationException, IllegalArgumentException, InvalidFormatException {
	
		logger.debug("Url transmitted to UploadManager : "+url.toString());
		
//		Do not use IIIF Validator as API seems to be down a lot
        // It also would seem a rather large overhead to validate every manifest.
        // Surely it would be better to only validate if it fails to load. 
		Manifest manifest = null;
		TrpDocMetadata md = null;
		
		try {
            // Could be sped up to use:
            // TrpPage page = buildPage(inputDir, pageNr++, imgFile, pageXml, thumbFile, dim, imageRemark);
            // pages.add(page);
			manifest = getManifest(url);
			/*
			 * to omit manifests with lots of pages?
			 */
//			if (manifest.getSequences().get(0).getCanvases().size()>2000) {
//				logger.error("Too many pages in this manifest");
//			}
			md = IIIFUtils.readIiifMetadata(manifest);
			IIIFUtils.getPagesFromIIIF(manifest,path);
		} catch (UnsupportedFormatVersionException | JsonMappingException | JsonParseException | IllegalArgumentException | NullPointerException e) {
			logger.error("Could not get pages from IIIF Manifest, UnsupportedFormatException :"+e.getMessage());
			logger.debug("Now try to import this IIIF manifest with a not so strict process!");
			//if it fails we try to read manifest without the mapping but as json
			md = IIIFUtils.getPagesFromIIIFSimple(url, path);
		}

		TrpDoc doc = new TrpDoc();
			
		doc = LocalDocReader.load(path+ File.separator + "img");
		
		logger.debug("doc has amount of pages: " + doc.getNPages());
		
//		if (md == null && manifest != null) {
//			md = IIIFUtils.readIiifMetadata(manifest);
//		}
		
		if(md.getTitle() == null) {
			md.setTitle("IIIF_Import_"+doc.getId());
		}
		doc.setMd(md);
	
		return doc;
	
	}

    /** 
     * Retrieve a URL to an image on a Canvas. This will retreive the first image so doesn't support
     * layers or more specifically it will retrieve the first layer in the list. It will prioritise
     * the Image Service and then fall back to the image resource Identifier. 
     * 
     * Image servers that restrict the download of full/full will currently fail with this implementation. 
     * 
     * @param Canvas the canvas to retrieve the image from
     * @return URL a URL to the Image which is present on the canvas
     */
    public static URL getImage(Canvas pCanvas) throws MalformedURLException {
        // This only retrieves the first image. Multiple image use case is for
        // layering e.g. multispectual. Code as it is only gets the first.
        Resource tImageResource =  pCanvas.getImages().get(0).getResource();
        if (tImageResource.getServices() != null && !tImageResource.getServices().isEmpty()) {
            Service tImageService = null;
            String tVersion = "2";
            for (Service tService : (List<Service>)tImageResource.getServices()) {
//                for (Profile tProfile : tService.getProfiles()) {
//                    if (tProfile.getIdentifier().toString().startsWith("http://iiif.io/api/image/2")) {
//                        tImageService = tService;
//                        break;
//                    }
//                    if (tProfile.getIdentifier().toString().startsWith("http://iiif.io/api/image/3")) {
//                        tImageService = tService;
//                        tVersion = "3";
//                        break;
//                    }
//                }

            	/*
            	 * work with context instead of profile -> seems to be more trustable
            	 */
                if (tService.getContext().toString().startsWith("http://iiif.io/api/image/2")) {
                    tImageService = tService;
                }
                if (tService.getContext().toString().startsWith("http://iiif.io/api/image/3")) {
                    tImageService = tService;
                    tVersion = "3";
                }
                
            }
            if (tImageService == null) {
            	logger.debug("image  service is null");
                return tImageResource.getIdentifier().toURL();
            } else {
            	logger.debug("image  service is not null");
                if (tVersion.equals("2")) {
                    return new URL(tImageService.getIdentifier() + "/full/full/0/default.jpg");
                } else {
                    return new URL(tImageService.getIdentifier() + "/full/max/0/default.jpg");
                }
            }
        } else {
            // No image service so return resource URL. This is for the use case
            // where the manifest links are to straight non IIIF images
        	logger.debug("get image from resource");
            return tImageResource.getIdentifier().toURL();
        }
    }
	
	public static void getPagesFromIIIF(Manifest manifest,String dir) throws MalformedURLException, IOException, UnsupportedFormatVersionException{
	
		logger.debug("working with valid manifest");
		File altoFile = null;
		
		String imgDirPath = dir + File.separator + "img";
		String altoDirPath = dir + File.separator + "img" + File.separator + LocalDocConst.ALTO_FILE_SUB_FOLDER;
		
		List<Sequence> sequences = manifest.getSequences();
        for(Sequence sequence : sequences) {
            List<Canvas> canvases = sequence.getCanvases();
            for(int i = 0; i<canvases.size(); i++) {
                List<AnnotationList> otherContentList = canvases.get(i).getOtherContent();

                if(otherContentList != null) {
                    // Get Alto	file			
                    for(AnnotationList otherContent : otherContentList) {
                        List<Annotation> otherAnno = otherContent.getResources();
                        if(otherAnno != null) {
                            for(Annotation annotation : otherAnno) {
                                Resource resource = annotation.getResource();
                                if(resource.getIdentifier().toString().endsWith(".xml")) {

                                    altoFile = new File(altoDirPath + File.separator + i+".xml");
                                    logger.debug("Create Alto File");
                                    if(DeaFileUtils.copyUrlToFile(resource.getIdentifier().toURL(), altoFile) >= 400) {
                                        logger.error("Could not download ALTO XML and it will be ignored!");
                                        //don't fail if ALTO XML could not be retrieved
                                        altoFile = null;
                                    }
                                }
                            }
                        }
                    }
                }
                
                URL url = getImage(canvases.get(i));
                int imgCounter = i+1;
                getTheImage(imgCounter, url, imgDirPath);


            }
        }
	}
	
	private static void getTheImage(int i, URL url, String imgDirPath) throws IOException {
		
		File imgFile = null;
        String filename = getProperName(i, ".jpg"); 
        // get Image service rather than resource /@id 

        imgFile = new File(imgDirPath + File.separator + filename);

        String problemMsg = "";

        URLConnection connection = url.openConnection();
        String redirect = connection.getHeaderField("Location");
        if(redirect != null) {
            url = new URL(redirect);
        }	
        int imgDownloadStatus = DeaFileUtils.copyUrlToFile(url, imgFile);

        if(imgDownloadStatus >= 400) {
            //the image URL connection attempt returns a response with code > 400
            problemMsg = getBrokenUrlMsg(url, imgDownloadStatus);
        }
		
	}

	private static String getProperName(int i, String fileType) {
		// TODO Auto-generated method stub
		String fn = String.format("%05d", i).concat(fileType);
		return fn;
	}

	/*
	 * aim is to find the image urls in case the manifest does not validate
	 * -> for IIIF Image API 2 here: sequences.canvases.images.resource.service
	 * -> API 3: path = items.items.items.body.service
	 * service can be missing -> then the url is found as id in resource resp. body
	 * try it this way and fail if no urls are found
	 * 
	 * different manifests to test
	 * https://digital.zlb.de/viewer/api/v1/records/34115495_1923/manifest/
	 * https://dlib.biblhertz.it/iiif/bb7803020/manifest2.json
	 * https://dlib.biblhertz.it/iiif/bb7803020/manifest.json
	 * https://api.digitale-sammlungen.de/iiif/presentation/v2/bsb10635066/manifest
	 * https://www.qdl.qa/en/iiif/81055/vdc_100000000193.0x000064/manifest
	 * https://bvmm.irht.cnrs.fr/iiif/23599/manifest
	 * https://api.digitale-sammlungen.de/iiif/presentation/v2/bsb10635067/manifest
	 * https://gallica.bnf.fr/iiif/ark:/12148/btv1b52502759s/manifest.json
	 * 
	 * It is best to go to the level of sequences.canvases.images.resource.service and then take the img link from the id
	 * -> concatenation of "/full/full/0/default.jpg" is necessary
	 * ..only if not available take the id from sequences.canvases.images.resource; they can differ and the former seem to be the correct one
	 * -> in resource the id url seems not to need concatenation of "/full/full/0/default.jpg"
	 * 
	 * return param: during storing the images in a temporary folder (used for import) we get the TrpDocMetadata and return this for further usage
	 * 
	 */
	public static TrpDocMetadata getPagesFromIIIFSimple(URL url, String dir) throws IOException, InvalidFormatException{
		
		int imgCounter = 0;
		TrpDocMetadata docMd = new TrpDocMetadata();
				
		String imgDirPath = dir + File.separator + "img";
		
		boolean useAPI3Syntax = false;
		logger.debug("url " + url);
		
//		String responseJson = getJSON(url.toString());
//		logger.debug("response json: " + responseJson);
//		
//		System.in.read();
//		InputStream targetStream = new ByteArrayInputStream(responseJson.getBytes());		
        
		/*
		 * cert problem solved this way;
		 * 
		 * Extend JEditorPane to override the getStream() method.
		 * 
		 * Inside that method, you can open a URLConnection. Test whether it is an HttpsURLConnection. 
		 * If it is, initialize your own SSLContext with a custom X509TrustManager that doesn't perform any checks. 
		 * Get the context's SSLSocketFactory and set it as the socket factory for the connection. 
		 * Then return the InputStream from the connection.
		 * 
		 * This will defeat any attempts by the runtime to protect the user from a spoof site serving up malware. If that's really what you want…
		 * 
		 */
		
		JsonReader reader = null;
		try {
			TrustManagerFactory tmf = TrustManagerFactory
			        .getInstance(TrustManagerFactory.getDefaultAlgorithm());
			// Using null here initialises the TMF with the default trust store.
			tmf.init((KeyStore) null);

			// Get hold of the default trust manager
			X509TrustManager x509Tm = null;
			for (TrustManager tm : tmf.getTrustManagers()) {
			    if (tm instanceof X509TrustManager) {
			        x509Tm = (X509TrustManager) tm;
			        break;
			    }
			}

			// Wrap it in your own class.
			final X509TrustManager finalTm = x509Tm;
			X509TrustManager customTm = new X509TrustManager() {
			    @Override
			    public java.security.cert.X509Certificate[] getAcceptedIssuers() {
			        return finalTm.getAcceptedIssuers();
			    }

			    @Override
			    public void checkServerTrusted(java.security.cert.X509Certificate[] chain,
			            String authType) throws CertificateException {
			    }

			    @Override
			    public void checkClientTrusted(java.security.cert.X509Certificate[] chain,
			            String authType) throws CertificateException {
			    }
			};
			SSLContext sslContext = SSLContext.getInstance("TLS");
			sslContext.init(null, new TrustManager[] { customTm }, null);
			URLConnection conn = url.openConnection();
			if (conn instanceof HttpsURLConnection) {
				HttpsURLConnection httpsConn = (HttpsURLConnection) conn;
				httpsConn.setSSLSocketFactory(sslContext.getSocketFactory());
				reader=Json.createReader(httpsConn.getInputStream());
			}

		} catch (KeyManagementException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (KeyStoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

        JsonObject myJson = reader.readObject();
	    
	    String p0 = "label", p1 = "sequences", p2 = "canvases", p3 = "images", p4 = "resource", p5 = "service", p6 = "@id";
	    
	    JsonString context = (JsonString) myJson.get("@context");
	    //to know if it is IIIF Image API 3, otherwise and if it is not available we assume that it is 2
	    if (context != null && context.getString().contains("iiif.io/api/presentation/3")){
	    	logger.debug("Manifest is IIIF Image API 3");
	    	//API 3: path = items.items.items.body.service or for API 2: sequences.canvases.images.resource.service
	    	useAPI3Syntax = true;
	    	p0 = "label"; 
	    	p1 = "items";
	    	p2 = "items";
	    	p3 = "items";
	    	p4 = "body";
	    	p5 = "service";
	    	p6 = "id";
	    }
	    
	    //label gets used for foldername = docname
	    String lbl = null;
    	if (useAPI3Syntax) {
    		JsonObject label = (JsonObject) myJson.get(p0);
    		JsonArray en = (JsonArray) label.get("en");
    		if (en != null && !en.isEmpty()) {
    			JsonValue st = (JsonValue) en.get(0);
    			lbl = st.toString().replace("\"", "");
    			logger.debug("label found and used as doc title: " + lbl);
    		}
    		
    	}
    	else {
    		//JsonObject label = (JsonObject) myJson.get(p0);
    		JsonValue value = (JsonValue) myJson.get(p0);
    		lbl = value.toString().replace("\"", "");
    		logger.debug("label found and used as doc title: " + lbl);
    	}
    	
    	docMd.setTitle(lbl);
	    
	    JsonArray sequences = myJson.getJsonArray(p1);
	    
	    if (sequences == null) {
	    	throw new InvalidFormatException("IIIF manifest does not contain 'sequences' (or 'items' for API 3)");
	    }
	    
	    for (JsonValue seq : sequences) {
	    	JsonObject currSeq = (JsonObject) seq;
	    	JsonArray canvases = currSeq.getJsonArray(p2);
	    	
		    if (canvases == null) {
		    	throw new InvalidFormatException("IIIF manifest does not contain 'canvases' (or 'items' for API 3)");
		    }
	    	
		    for (JsonValue canv : canvases) {
		    	JsonObject currObject = (JsonObject) canv;
		    	JsonArray images = currObject.getJsonArray(p3);
		    	
		    	/*
		    	 * could be used to omit very large manifests
		    	 */
//		    	if (canvases.size()>2000) {
//		    		throw new ForbiddenException("This manifest contains too many images for this import: " + canvases.size());
//		    	}
		    	
			    if (images == null) {
			    	throw new InvalidFormatException("IIIF manifest does not contain 'images' (or 'items' for API 3)");
			    }
		    	
			    for (JsonValue img : images) {

			    	JsonObject service = null;
			    	JsonValue id = null;
			    	
			    	JsonObject imgArray = (JsonObject) img;
			    	JsonObject resource = (JsonObject) imgArray.get(p4);
			    	
				    if (resource == null) {
				    	throw new InvalidFormatException("IIIF manifest does not contain 'resource' (or 'body' for API 3)");
				    } 
				    
			    	if (useAPI3Syntax) {
			    		JsonArray services = (JsonArray) resource.get(p5);
			    		if (services != null && !services.isEmpty()) {
			    			service = (JsonObject) services.get(0);
			    		}
			    		
			    	}
			    	else {
				    	service = (JsonObject) resource.get(p5);
			    	}
			    	
			    	if (service == null) {
			    		id = (JsonValue) resource.get(p6);
			    	}
			    	else {
			    		id = (JsonValue) service.get(p6);
			    	}

			    	String imgId = id.toString().replace("\"", "");
			    	
			    	//service is null -> we take the url from id of resource but there no other path must be concatenated
			    	String imgUrl = service == null ? imgId : useAPI3Syntax ? imgId.concat("/full/max/0/default.jpg") : imgId.concat("/full/full/0/default.jpg");
			    	
			    	logger.debug("found image URL: " + imgUrl + " -> now store to " + imgDirPath);
			    	
				    URL imageUrl = new URL(imgUrl);
				    
				    getTheImage(++imgCounter, imageUrl, imgDirPath);
			    	
			    	//after first image we break:  Multiple image use case is for layering e.g. multispectral. Code as it is only gets the first.
			    	break;
			    }
		    }
	    }
		return docMd;
	}
	
	public static TrpDocMetadata readIiifMetadata(Manifest manifest) {
		TrpDocMetadata md = new TrpDocMetadata();

        md.setExternalId(manifest.getIdentifier().toString());

        if (manifest.getLabel() != null && manifest.getLabelString() != null) {
            md.setTitle(manifest.getLabelString());
        }

      	//fixed NPE bug: test with https://api.digitale-sammlungen.de/iiif/presentation/v2/bsb10635066/manifest
        if (manifest.getDescription() != null && manifest.getDescriptionString() != null) {
            md.setDesc(manifest.getDescriptionString());
        }

        // The following will match very few English manifests
        // as IIIF metadata section is uncontrolled 
		List<MetadataEntry> metaData = manifest.getMetadata();
        if (metaData != null) {
            for(MetadataEntry entry : metaData) {
                switch(entry.getLabelString()) {
                case "Creator":
                case "Author":
                    md.setAuthor(entry.getValueString());
                }
            }
        }
		return md;
	}
	
	
	public static String createPageTextAnnotation(String annotationId ,TrpPage page) throws MalformedURLException, IllegalArgumentException, XPathExpressionException, SAXException, IOException, NullValueException, JAXBException {
		
		String productionBaseUrl = "https://dbis-thure.uibk.ac.at/iiif/2";
		String testBaseUrl = "https://files-test.transkribus.eu/iiif/2/";
		
		ObjectMapper iiifMapper = new IiifObjectMapper();
		
		AnnotationList annoList = new AnnotationList(annotationId);
		List<Annotation> collectAnnos = new ArrayList<>();
		
		Layer layer = new Layer(annotationId+"/layer/regionType");
		layer.addLabel("Text Region Type");
	
		//TODO use chosen transcript version
		
		PcGtsType pcB2P = new PcGtsType();
		pcB2P = page.unmarshallCurrentTranscript();
		PageType pageType = pcB2P.getPage();
		List<TrpRegionType> regions = pageType.getTextRegionOrImageRegionOrLineDrawingRegion();
		
		for(TrpRegionType r : regions){
			TrpPageType trpPage = r.getPage();
			List<TrpTextLineType> lines = trpPage.getLines();
			for(TrpTextLineType line : lines) {
				List<CustomTag> tagList =line.getCustomTagList().getTags();
				Motivation motivationText = new Motivation("sc:painting");
				Annotation anno = new Annotation(annotationId+"/"+line.getId(),motivationText);
				
				ContentAsText text = new ContentAsText(line.getUnicodeText());
				text.setFormat(MimeType.fromTypename("text/plain"));
				anno.setResource(text);
				String pointStr = line.getCoords().getPoints();
				Rectangle boundingBox = PointStrUtils.getBoundingBox(pointStr);
				String iiifCoords = boundingBox.x+","+boundingBox.y+","+boundingBox.width+","+boundingBox.height;
				anno.setOn(new OtherContent(testBaseUrl+""+page.getKey()+"/canvas/"+page.getPageNr()+"#xywh="+iiifCoords));
				
				collectAnnos.add(anno);
				// TODO add another Annotation which describes the entity for tags rather than putting it in PropertyValue array
				for(CustomTag tag : tagList) {
					//ignore readingOrder in custom tag as it is always empty
					Motivation motivationTag = new Motivation("oa:commenting");
					if(!StringUtils.equals("readingOrder", tag.getTagName())) {
						
						Annotation tagAnno = new Annotation(annotationId+"/tagging/"+line.getId()+"/"+tag.getTagName(),motivationTag);
						ContentAsText textTag = new ContentAsText(tag.getTagName());
						textTag.setFormat(MimeType.fromTypename("text/plain"));
						tagAnno.setResource(textTag);
						tagAnno.setOn(new OtherContent(testBaseUrl+""+page.getKey()+"/canvas/"+page.getPageNr()+"#xywh="+iiifCoords));
						collectAnnos.add(tagAnno);
						
					}
					// set region type
					Annotation tagRegionType= new Annotation(annotationId+"/tagging/"+line.getId()+"/regionType",motivationTag);
					ContentAsText textRegionType = new ContentAsText("Text Region Type : "+line.getRegion().getType().toString());
					textRegionType.setFormat(MimeType.fromTypename("text/plain"));
					tagRegionType.setResource(textRegionType);
					tagRegionType.setOn(new OtherContent(testBaseUrl+""+page.getKey()+"/canvas/"+page.getPageNr()+"#xywh="+iiifCoords));
					collectAnnos.add(tagRegionType);
					// set reading Order
					Annotation tagRegionOrder = new Annotation(annotationId+"/tagging/"+line.getId()+"/readingOrder",motivationTag);
					ContentAsText textRegionOrder = new ContentAsText("Reading Order Region Index : "+line.getRegion().getReadingOrderAsInt());
					textRegionOrder.setFormat(MimeType.fromTypename("text/plain"));
					tagRegionOrder.setResource(textRegionOrder);
					tagRegionOrder.setOn(new OtherContent(testBaseUrl+""+page.getKey()+"/canvas/"+page.getPageNr()+"#xywh="+iiifCoords));
					collectAnnos.add(tagRegionOrder);
				}
				
			}
		
		}
		annoList.setResources(collectAnnos);

		//TODO add sequence to structure reading order and region type 
		
		String annotationJson = iiifMapper.writerWithDefaultPrettyPrinter().writeValueAsString(annoList);
		
		logger.debug(annotationJson);
		
		try (PrintWriter out = new PrintWriter("/home/lateknight/Desktop/json/annotation"+page.getPageNr()+".json")){
			out.println(annotationJson);
		}
		return annotationJson;
	}
	
	public static String exportIiifManifest(TrpDoc doc) throws MalformedURLException, JsonProcessingException {
		
		String productionBaseUrl = "https://dbis-thure.uibk.ac.at/iiif/2";
		String testBaseUrl = "https://files-test.transkribus.eu/iiif/2/";
		
		ObjectMapper iiifMapper = new IiifObjectMapper();
		Manifest manifest = new Manifest(testBaseUrl+""+doc.getId()+"/manifest");
		
		Sequence sequence = new Sequence(testBaseUrl+""+doc.getId()+"/sequence");
		
		//TODO add more metadata to manifest
		manifest.addMetadata("Title", doc.getMd().getTitle());
		
		if(doc.getMd().getAuthor() != null) {
			manifest.addMetadata("Author", doc.getMd().getAuthor());
		}
		
		List<TrpPage> pages = doc.getPages();
		List<Canvas> canvasList = new ArrayList<>();
		
		for(TrpPage page : pages) {
			
			TrpImage pageImage = page.getImage();
			
			Canvas canvas = new Canvas(testBaseUrl+""+page.getKey()+"/canvas/"+page.getPageNr());
			canvas.setWidth(pageImage.getWidth());
			canvas.setHeight(pageImage.getHeight());
			canvas.addIIIFImage(testBaseUrl+""+pageImage.getKey(), ImageApiProfile.LEVEL_ONE);
			
			ImageContent thumbnail = new ImageContent(pageImage.getThumbUrl().toString());
			thumbnail.addService(new ImageService(pageImage.getThumbUrl().toString(), ImageApiProfile.LEVEL_ONE));
			canvas.addThumbnail(thumbnail);
			
			canvas.addSeeAlso(new OtherContent(page.getCurrentTranscript().getUrl().toString(), "application/xml"));
			String annotationId = testBaseUrl+""+page.getKey()+"/contentAsText/"+page.getPageNr();
			AnnotationList annoList = new AnnotationList(annotationId);
			annoList.addLabel("Text of page "+page.getPageNr());
			canvas.addOtherContent(annoList);
			canvasList.add(canvas);
			
			try {
				createPageTextAnnotation(annotationId, page);
			} catch (IllegalArgumentException | XPathExpressionException | SAXException | IOException
					| JAXBException e) {
				e.printStackTrace();
			}
			
				
		}
		sequence.setCanvases(canvasList);
		manifest.addSequence(sequence);
		
		String manifestJson = iiifMapper.writerWithDefaultPrettyPrinter().writeValueAsString(manifest);
		
		try (PrintWriter out = new PrintWriter("/home/lateknight/Desktop/json/manifest.json")){
			out.println(manifestJson);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		logger.debug(manifestJson);
		
		return manifestJson;
		
	}
	
	public static int checkIiifApi (final URL url) throws IOException {
		URL validationUrl = new URL("https://iiif.io/api/presentation/validator/service/validate?format=json&version=2.1&url="+url.toString());
		HttpURLConnection con = (HttpURLConnection) validationUrl.openConnection();
		con.addRequestProperty("User-Agent", "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:74.0) Gecko/20100101 Firefox/74.0");
		con.setRequestMethod("GET");	
		return con.getResponseCode();
	}
	
	public static int checkURL (final URL url) throws IOException {
		HttpURLConnection con = (HttpURLConnection) url.openConnection();
		con.setRequestMethod("GET");
		con.setRequestProperty("Content-Type", "application/json");
		con.addRequestProperty("User-Agent", "Mozilla");	
		return con.getResponseCode();
	}
	
	public static JSONObject validateManifest (final URL url) throws IOException {
		URL validationUrl = new URL("https://iiif.io/api/presentation/validator/service/validate?format=json&version=2.1&url="+url.toString());
		HttpURLConnection con = (HttpURLConnection) validationUrl.openConnection();
		con.setRequestMethod("GET");
		con.setRequestProperty("Content-Type", "application/json");
		con.addRequestProperty("User-Agent", "Mozilla");
		
		int status = con.getResponseCode();
		StringBuffer content = new StringBuffer();
		try(BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));) {
			String inputLine;
			while ((inputLine = in.readLine()) != null) {
				content.append(inputLine);
			}
		} finally {
			con.disconnect();
		}
		JSONObject jsonResponse = new JSONObject(content.toString());
		return jsonResponse;
	}
	
	public static String getBrokenUrlMsg(final URL url, final Integer statusCode) {
		String msg = "Image could not be loaded from " + url.toString();
		if(statusCode != null) {
			msg += "(" + statusCode + " " + Status.fromStatusCode(statusCode).getReasonPhrase() + ")";
		}
		return msg;
	}

}
