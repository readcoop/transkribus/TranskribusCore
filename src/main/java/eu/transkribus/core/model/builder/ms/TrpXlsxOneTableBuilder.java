package eu.transkribus.core.model.builder.ms;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.poi.openxml4j.util.ZipSecureFile;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.WorkbookUtil;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.eclipse.core.runtime.IProgressMonitor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.transkribus.core.io.LocalDocReader;
import eu.transkribus.core.model.beans.JAXBPageTranscript;
import eu.transkribus.core.model.beans.TrpDoc;
import eu.transkribus.core.model.beans.TrpPage;
import eu.transkribus.core.model.beans.TrpTranscriptMetadata;
import eu.transkribus.core.model.beans.pagecontent_trp.TrpPageType;
import eu.transkribus.core.model.beans.pagecontent_trp.TrpRegionType;
import eu.transkribus.core.model.beans.pagecontent_trp.TrpTableCellType;
import eu.transkribus.core.model.beans.pagecontent_trp.TrpTableRegionType;
import eu.transkribus.core.model.builder.ExportCache;
import eu.transkribus.core.model.builder.NoTablesException;



public class TrpXlsxOneTableBuilder {
	private final static Logger logger = LoggerFactory.getLogger(TrpXlsxOneTableBuilder.class);
	Workbook wb;
	
	public TrpXlsxOneTableBuilder() {
		
	}
	
	public void writeXlsxForTables(TrpDoc doc, File exportFile, Set<Integer> pageIndices, IProgressMonitor monitor, ExportCache cache, int coordsColumn) throws NoTablesException, IOException, InterruptedException {
		//TrpTableRegionType is contained in the regions too

		List<TrpPage> pages = doc.getPages();
		String exportPath = exportFile.getPath();
		
		boolean isExtended = false;
		int rowNr = 0;
				
		int totalPages = pageIndices==null ? pages.size() : pageIndices.size();
		if (monitor!=null) {
			monitor.beginTask("Exporting tables to Excel", totalPages);
		}
		
		File currExportFile = new File(exportPath);
		if (currExportFile.exists()) {
			try {
				isExtended = true;
				logger.debug("open the file once more");
				// for getting the information of the file
				FileInputStream fip = new FileInputStream(currExportFile);
				//to avoid java.io.IOException: Zip bomb detected!
				ZipSecureFile.setMinInflateRatio(0);
				// Getting the workbook instance for XLSX file
				wb = new XSSFWorkbook(fip);
				Sheet currSheet;
				if (wb.getSheetAt(0) != null){
					currSheet = wb.getSheetAt(0);
				}
				else{
					currSheet = wb.createSheet(WorkbookUtil.createSafeSheetName("mySheet"));
				}
				rowNr = currSheet.getLastRowNum();
				//wb = new XSSFWorkbook(exportPath);
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		else {
			wb = new XSSFWorkbook();
		}
				
		int c=0;

		for (int i=0; i<pages.size(); ++i) {
			if (pageIndices!=null && !pageIndices.contains(i))
				continue;
			
			if (monitor!=null) {
				if (monitor.isCanceled()) {
					throw new InterruptedException("Export was canceled by user");
//					logger.debug("Xlsx export cancelled!");
//					return;
				}
				monitor.subTask("Processing page "+(c+1));
			}
			
			TrpPage page = pages.get(i);
			//try to get previously loaded JAXB transcript
			JAXBPageTranscript tr = null;
			if(cache != null) {
				tr = cache.getPageTranscriptAtIndex(i);
			}
			if (tr == null){
				TrpTranscriptMetadata md = page.getCurrentTranscript();
				tr = new JAXBPageTranscript(md);
				tr.build();
			}			
			
			TrpPageType trpPage = tr.getPage();
			String fname = page.getImgFileName();
			String pageNr = String.valueOf(page.getPageNr());
			
			List<TrpRegionType> regions = trpPage.getRegions();
						
			for (int j=0; j<regions.size(); ++j) {
				TrpRegionType r = regions.get(j);
				
				if (r instanceof TrpTableRegionType){

					logger.debug("is table");
					TrpTableRegionType table = (TrpTableRegionType) r;
					
					int cols = table.getNCols();
					int rows = table.getNRows();
															
					List<List<TrpTableCellType>> allRowCells = new ArrayList<List<TrpTableCellType>>();
					for (int k = 0; k<rows; k++){
						allRowCells.add(table.getRowCells(k));
					}
					
		            List<HashMap<Integer, TrpTableCellType>> allRows = new ArrayList<HashMap<Integer, TrpTableCellType>>();
		            		            
		            for (List<TrpTableCellType> rowCells : allRowCells){				          
		            	allRows.add(new HashMap<Integer, TrpTableCellType>());
		            }
		           
		            int rowIdx = 0;
		            for (List<TrpTableCellType> rowCells : allRowCells){
		            
		            	HashMap<Integer, TrpTableCellType> currRowMap = allRows.get(rowIdx);

		            	/*
		            	 * fill up all cells which are not set in TRP because they were merged there (needed for vertical and horizontal xslx cell merge)
		            	 */	            	
		            	for (TrpTableCellType cell : rowCells){
			            	//logger.debug("table cell text " + cell.getUnicodeTextFromLines());
		            		//logger.debug("curr row, add cell.getCol() " + cell.getCol() + " whats in the cell: " + cell.getRow());
			            	currRowMap.put(cell.getCol(), cell);
			            	
			            	if (cell.getRowSpan() > 1){
			            		for (int k = 1; k<cell.getRowSpan(); k++){
			            			HashMap<Integer, TrpTableCellType> tmpRowMap = allRows.get(rowIdx+k);
			            			tmpRowMap.put(cell.getCol(), null);
			            			allRows.remove(rowIdx+k);
			            			allRows.add(rowIdx+k, tmpRowMap);
			            		}
			            	}
			            	if (cell.getColSpan() > 1){
			            		for (int k = 1; k<cell.getColSpan(); k++){
			            			currRowMap.put(cell.getCol()+k, null);
			            		}
			            	}
		            	}
		            	allRows.remove(rowIdx);
		            	allRows.add(rowIdx, currRowMap);
		            	rowIdx++;
		            }

		            //createTable_LAC(rows, cols, allRows, doc.getMd().getTitle(), pageNr);
		            createTable(rows, cols, allRows, fname, coordsColumn, pageNr);

				}
				
		
				logger.debug("writing xlsx for page "+(i+1)+"/"+doc.getNPages());
	
				
				if (monitor!=null) {
					monitor.worked(c);
				}
			}
			++c;
		}
		

		/*
		 * auto size the columns
		 */
		for (int i = 0; i < wb.getNumberOfSheets(); i++){
			int numberOfCellsInHeader = 0;
            int numberOfCells = 0;
            Iterator<Row> rowIterator = wb.getSheetAt(i).rowIterator();
            /**
             * Escape the header row *
             */
            if (rowIterator.hasNext())
            {
                Row headerRow = (Row) rowIterator.next();
                numberOfCellsInHeader = headerRow.getPhysicalNumberOfCells();
                if (rowIterator.hasNext()) {
                	Row secondRow = (Row) rowIterator.next();
                               
	                //get the number of cells in the header row
	                numberOfCells = secondRow.getPhysicalNumberOfCells();
	                int cellNumbers = (numberOfCells>numberOfCellsInHeader)? numberOfCells : numberOfCellsInHeader;
	                for (int j = 0; j<cellNumbers; j++){
	                	try {
							wb.getSheetAt(i).autoSizeColumn(j, true);
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
	                }
                }
                              
            }
                        
		}

		FileOutputStream fOut;
		try {
			//means no tables at all
			if (wb.getNumberOfSheets() == 0){
				//throw new NoTablesException("Sorry - No tables available for export");
				logger.info("Sorry - No tables available for export");
				Sheet noTables = wb.createSheet(WorkbookUtil.createSafeSheetName("No tables found"));
				CreationHelper crHelper = wb.getCreationHelper();
				Row firstRow = noTables.createRow(0);
				firstRow.createCell(0).setCellValue(crHelper.createRichTextString("Sorry - there were no tables available for your export. Please check the transcripts!"));
			}
			fOut = new FileOutputStream(exportPath);
			wb.write(fOut);
			fOut.close();
		} catch (IOException e) {
			if (!(e instanceof NoTablesException)) {
				logger.error(e.getMessage(), e);
			}
			throw e;
		}
		logger.info("wrote xlsx to: "+ exportPath);
	}
	
	private void createTable(int rows, int cols, List<HashMap<Integer, TrpTableCellType>> allRows, String filename, int coordsColumn, String pageNr) {
		
		if (coordsColumn == 0) {
			
			createTableWithCoords(rows, cols, allRows, filename, pageNr);
			return;
		}

		String sheetName = "table";
		Sheet currSheet;
		if (wb.getSheet(sheetName) != null){
			currSheet = wb.getSheet(sheetName);
		}
		else{
			currSheet = wb.createSheet(WorkbookUtil.createSafeSheetName(sheetName));
		}
		
		CellStyle style=wb.createCellStyle();
		style.setBorderBottom(BorderStyle.THIN);
		style.setBorderTop(BorderStyle.THIN);
		style.setBorderRight(BorderStyle.THIN);
		style.setBorderLeft(BorderStyle.THIN);
		style.setWrapText(true);
		
		CellStyle rowStyle = (CellStyle) wb.createCellStyle();
		rowStyle.setWrapText(true);	
        		
	    int i = currSheet.getLastRowNum();
	    int lastRowIdxOfPrevPages = (i != -1) ? i+1 : 0;
	    
	    //how many extra columns are used at the beginning - currently page number and filename
	    int descriptiveColCount = 2;

	    /*
	     * for this export we write the filename to each row and omit to have it at the beginning of each table
	     */
//	    if (filename != null) {
//	    	
//		    Row startRow = currSheet.createRow(i);
//	    	Cell currCell = startRow.createCell(0);
//	    	currCell.setCellValue(filename);   
//	    	currCell.setCellStyle(style);
//	    	i++;
//	    }

	    for (HashMap<Integer, TrpTableCellType> entry : allRows) {
	    	i = i+1;       
	        if (entry.keySet().size() != cols){
	        	logger.debug("size of entries does not match columns ");
	        }

	    	Row nextRow = currSheet.createRow(i);
	    	nextRow.setRowStyle(rowStyle);
	    	
        	Cell currPNrCell = nextRow.createCell(0);
        	currPNrCell.setCellValue(pageNr);
        	currPNrCell.setCellStyle(style);
	    	
	        /*
	         * create cell for the filename in the first column
	         */
        	Cell currFnCell = nextRow.createCell(1);
        	currFnCell.setCellValue(filename);
        	currFnCell.setCellStyle(style);

	    	int maxLines = 0;
	    	
	        boolean coordsAdded = false;
	        
	        for (Integer key : entry.keySet()) {
	        	        	
	        	int colSpan = 0;
	        	int rowSpan = 0;
	        	boolean mergedVertical = false;
	        	boolean mergedHorizontal = false;
	        		        	
	        	if (entry.get(key) != null){
	        		
	        		colSpan = entry.get(key).getColSpan();
	        		rowSpan = entry.get(key).getRowSpan();
	        		
		        	if (rowSpan > 1){
		        		mergedVertical = true;
		        	}

		        	if (colSpan > 1){
		        		mergedHorizontal = true;
		        	}

		        	// we add the filename at column 1 and (if set) coordinates at column 2 -> index must be adjusted	        	
		        	int colID = coordsAdded? entry.get(key).getCol() + (descriptiveColCount + 1) : entry.get(key).getCol() + descriptiveColCount;
		        	int rowID = entry.get(key).getRow() + lastRowIdxOfPrevPages;
		        	
		        	String text = entry.get(key).getUnicodeTextFromLines();
		        	
		        	Cell currCell = nextRow.createCell(colID);
		        	currCell.setCellValue(text);   
		        	currCell.setCellStyle(style);
		        			        	
	        		if(mergedVertical || mergedHorizontal){
//	        			logger.debug("entry.get(key).getUnicodeTextFromLines() " + entry.get(key).getUnicodeTextFromLines());
//	        			logger.debug(" row ID " + rowID + " rowSpan " + rowSpan + " merged H "+ mergedHorizontal + " colID " + colID + " colSpan " + colSpan + " merged V " + mergedVertical);
	        			
	        			String [] textlines = text.split(System.lineSeparator());
	        			
	        			if (textlines.length > 0){
		        			int length = textlines.length;
		        			if (maxLines < length){
		        				maxLines = length;
		        			}
		        			
	        			}
	        			try {
							currSheet.addMergedRegion(new CellRangeAddress(rowID,rowID+rowSpan-1,colID,colID+colSpan-1));
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

	        		}
	        		
			        /*
			         * coords != null -> user has chosen a column for putting the coords into the table row
			         * use case: column is very important and needs to get checked in the image with the coordinates
			         */
	            	/*
	            	 * to store the coordinates for a single cell, e.g. for lse we want the coordinates of the wealth in the Excel
	            	 * user can choose the column number in export settings
	            	 */
	            	if (coordsColumn != -1 && ((colID) == coordsColumn)) {
	            		String coords = entry.get(key).getCoordinates();
	            		logger.debug("coords found " + coords);
	            		colID=colID+1;
			        	Cell coordsCell = nextRow.createCell(colID);
			        	coordsCell.setCellValue(coords);
			        	coordsCell.setCellStyle(style);
			        	coordsAdded=true;
			        }

	        	}
	        	else{
	        		int celId = key+descriptiveColCount;
		        	Cell currCell = nextRow.createCell(celId);
		        	currCell.setCellStyle(style);
	        	}
	        }
	        
	        /*
	         * set the height of a row manually since it does not work for merged region cells!!!
	         * so if there are merged cells in a row we remember the maximum number of text lines in one of these cells to set the appropriate height
	         */	
			if (maxLines > 0){
				logger.debug("row contains maximum nr.- of lines in cell of: "  + maxLines);
				nextRow.setHeight((short) (nextRow.getHeight()*maxLines));
			}
			
	    }
	}
	
	private void createTable_LAC(int rows, int cols, List<HashMap<Integer, TrpTableCellType>> allRows, String docname, String pageNr) {
		
		boolean isFirstRow = false; 
		
		String rememberDate1 = null;
		String rememberDate2 = null;
		String rememberDate3 = null;
		
		String sheetName = "table";
		Sheet currSheet;
		if (wb.getSheet(sheetName) != null){
			currSheet = wb.getSheet(sheetName);
		}
		else{
			currSheet = wb.createSheet(WorkbookUtil.createSafeSheetName(sheetName));
			isFirstRow = true;
		}
		
		CellStyle style=wb.createCellStyle();
		style.setBorderBottom(BorderStyle.THIN);
		style.setBorderTop(BorderStyle.THIN);
		style.setBorderRight(BorderStyle.THIN);
		style.setBorderLeft(BorderStyle.THIN);
		style.setWrapText(true);
		
		CellStyle rowStyle = (CellStyle) wb.createCellStyle();
		rowStyle.setWrapText(true);	
        		
	    int i = currSheet.getLastRowNum()+1;
	    
		Row firstRow = currSheet.getRow(0);
		if (firstRow == null) {
			firstRow = currSheet.createRow(0);
		}
		if (isFirstRow) {
			Cell firstCell = firstRow.createCell(0);
			firstCell.setCellValue("Reel Number");   
			firstCell.setCellStyle(style);
			Cell secondCell = firstRow.createCell(1);
			secondCell.setCellValue("Image Number");   
			secondCell.setCellStyle(style);
			Cell thirdCell = firstRow.createCell(2);
			thirdCell.setCellValue("Number");   
			thirdCell.setCellStyle(style);
			Cell cell4 = firstRow.createCell(3);
			cell4.setCellValue("Number computed");   
			cell4.setCellStyle(style);
			Cell cell5 = firstRow.createCell(4);
			cell5.setCellValue("Department");   
			cell5.setCellStyle(style);
			Cell cell6 = firstRow.createCell(5);
			cell6.setCellValue("Name and Subject");   
			cell6.setCellStyle(style);
			Cell cell7 = firstRow.createCell(6);
			cell7.setCellValue("Subject(s)");   
			cell7.setCellStyle(style);
			Cell cell8 = firstRow.createCell(7);
			cell8.setCellValue("Date of Receipt in Privy Council");   
			cell8.setCellStyle(style);
			Cell cell9 = firstRow.createCell(8);
			cell9.setCellValue("Date of Receipt normalized");   
			cell9.setCellStyle(style);
			Cell cell10 = firstRow.createCell(9);
			cell10.setCellValue("Date of Report of Committee of Privy Council");   
			cell10.setCellStyle(style);
			Cell cell11 = firstRow.createCell(10);
			cell11.setCellValue("Date of Report normalized");   
			cell11.setCellStyle(style);
			Cell cell12 = firstRow.createCell(11);
			cell12.setCellValue("Date when Confirmed in Privy council");   
			cell12.setCellStyle(style);
			Cell cell13 = firstRow.createCell(12);
			cell13.setCellValue("Date when confirmed normalized");   
			cell13.setCellStyle(style);
			Cell cell14 = firstRow.createCell(13);
			cell14.setCellValue("Remarks");   
			cell14.setCellStyle(style);
			i++;
		}

	    for (HashMap<Integer, TrpTableCellType> entry : allRows) {
	        	        
	        if (entry.keySet().size() != cols){
	        	logger.debug("size of entries does not match columns ");
	        }

	    	Row nextRow = currSheet.createRow(i);
	    	nextRow.setRowStyle(rowStyle);
	    	
        	Cell currPNrCell = nextRow.createCell(0);
        	currPNrCell.setCellValue(docname);
        	currPNrCell.setCellStyle(style);
	    	
	        /*
	         * create cell for the filename in the first column
	         */
        	Cell currFnCell = nextRow.createCell(1);
        	currFnCell.setCellValue(pageNr);
        	currFnCell.setCellStyle(style);

	    	int maxLines = 0;
	    	int increaseColId = 2;
	    	
	    	i++;
	        boolean coordsAdded = false;
	        
	        for (Integer key : entry.keySet()) {
	        	        	
	        	int colSpan = 0;
	        	int rowSpan = 0;
	        	boolean mergedVertical = false;
	        	boolean mergedHorizontal = false;
	        		        	
	        	if (entry.get(key) != null){
	        		
	        		colSpan = entry.get(key).getColSpan();
	        		rowSpan = entry.get(key).getRowSpan();
	        		
		        	if (rowSpan > 1){
		        		mergedVertical = true;
		        	}

		        	if (colSpan > 1){
		        		mergedHorizontal = true;
		        	}
		        	
		        	int currCol = entry.get(key).getCol();

		        	// we add the filename at column 1 and (if set) coordinates at column 2 -> index must be adjusted	        	
		        	int colID = entry.get(key).getCol() + increaseColId;
		        	int rowID = entry.get(key).getRow();
		        	
		        	String text = entry.get(key).getUnicodeTextFromLinesAsOneLine();
		        	text = text.trim();
		        	
		        	//remember the date and insert if ditto observed
		        	if (currCol == 3 || currCol == 4 || currCol == 5) {
		        		logger.debug("curr col should contain date " + text);
		        		
		        		if (text.length()>1) {
			        	    String code_str = String.valueOf(text.charAt(0));
			        	    logger.debug("code string: " + code_str);
		        		}
		        		
		        		//https://www.compart.com/de/unicode/U+3003
		                String fixedChar = "\u3003";
		                //String regex = "\\s*" + fixedChar + "(?:\\s+" + fixedChar + ")*";
		                // Regular expression pattern
		                String regex = "\\s*"+fixedChar+"[\\s*" +fixedChar + "]*";
		               // String regex = ch_code;
		                
//		                if (text.equals("〃〃〃")) {
//		                	logger.debug("thisworks");
//		                }

		                // Compile the pattern
		                Pattern pattern = Pattern.compile(regex);
	                    Matcher matcher = pattern.matcher(text);
	                    logger.debug(" muster: " + matcher.pattern());
	                    if (matcher.matches()) {
	                        logger.debug("ditto pattern found");
	                        String date2Use = text;
	                        switch(currCol){
	                        	case 3:
	                        		date2Use = rememberDate1;
	                        		break;
	                        	case 4:
	                        		date2Use = rememberDate2;
	                        		break;
	                        	case 5:
	                        		date2Use = rememberDate3;
	                        		break;
                    	
	                        }
	    		        	Cell currCell = nextRow.createCell(colID);
	    		        	currCell.setCellValue(date2Use);   
	    		        	currCell.setCellStyle(style);

	                    } else {
	                    	logger.debug("NO ditto pattern found");
	                    	logger.debug("curr col should contain date " + text);
	                        switch(currCol){
	                        	case 3:
	                        		rememberDate1 = text;
	                        		break;
	                        	case 4:
	                        		rememberDate2 = text;
	                        		break;
	                        	case 5:
	                        		rememberDate3 = text;
	                        		break;
                        	
	                        }
	    		        	Cell currCell = nextRow.createCell(colID);
	    		        	currCell.setCellValue(text);   
	    		        	currCell.setCellStyle(style);
	                    }
		        	}
		        	else {
		        		logger.debug("curr col contains " + text);
		            	Cell currCell = nextRow.createCell(colID);
			        	currCell.setCellValue(text);   
			        	currCell.setCellStyle(style);
		        	}
		        	
		        	//create empty col after theses col numbers
		        	if (currCol == 0 || (currCol > 1 && currCol < 6)) {
		        		int addColId = colID+increaseColId;
			        	Cell nextCell = nextRow.createCell(addColId);
			        	nextCell.setCellValue("");   
			        	nextCell.setCellStyle(style);
			        	increaseColId += 1;
		        	}
		        	
//	        		if(mergedVertical || mergedHorizontal){
////	        			logger.debug("entry.get(key).getUnicodeTextFromLines() " + entry.get(key).getUnicodeTextFromLines());
////	        			logger.debug(" row ID " + rowID + " rowSpan " + rowSpan + " merged H "+ mergedHorizontal + " colID " + colID + " colSpan " + colSpan + " merged V " + mergedVertical);
//	        			
//	        			String [] textlines = text.split(System.lineSeparator());
//	        			
//	        			if (textlines.length > 0){
//		        			int length = textlines.length;
//		        			if (maxLines < length){
//		        				maxLines = length;
//		        			}
//		        			
//	        			}
//	        			currSheet.addMergedRegion(new CellRangeAddress(rowID,rowID+rowSpan-1,colID,colID+colSpan-1));
//	        			//maxColId = colID+colSpan-1;
//	        		}

	        	}
//	        	else{
//		        	Cell currCell = nextRow.createCell(key);
//		        	currCell.setCellStyle(style);
//	        	}
	        }

	        /*
	         * set the height of a row manually since it does not work for merged region cells!!!
	         * so if there are merged cells in a row we remember the maximum number of text lines in one of these cells to set the appropriate height
	         */	
			if (maxLines > 0){
				logger.debug("row contains maximum nr.- of lines in cell of: "  + maxLines);
				nextRow.setHeight((short) (nextRow.getHeight()*maxLines));
			}
			
	    }
	}
	
	private void createTableWithCoords(int rows, int cols, List<HashMap<Integer, TrpTableCellType>> allRows, String filename, String pageNr) {

		String sheetName = "table";
		Sheet currSheet;
		if (wb.getSheet(sheetName) != null){
			currSheet = wb.getSheet(sheetName);
		}
		else{
			currSheet = wb.createSheet(WorkbookUtil.createSafeSheetName(sheetName));
		}
		
		CellStyle style=wb.createCellStyle();
		style.setBorderBottom(BorderStyle.THIN);
		style.setBorderTop(BorderStyle.THIN);
		style.setBorderRight(BorderStyle.THIN);
		style.setBorderLeft(BorderStyle.THIN);
		style.setWrapText(true);
		
		CellStyle rowStyle = (CellStyle) wb.createCellStyle();
		rowStyle.setWrapText(true);	
        		
	    int i = currSheet.getLastRowNum()+1;
	    
	    //first Row -> add the structure types
	    boolean firstRow = (i == 0);

	    for (HashMap<Integer, TrpTableCellType> entry : allRows) {
	        	        
	        if (entry.keySet().size() != cols){
	        	logger.debug("size of entries does not match columns ");
	        }

	    	Row initialRow = null;
	        if (firstRow) {
		    	initialRow = currSheet.createRow(i);
		    	initialRow.setRowStyle(rowStyle);
		    	i++;
	        	Cell initFnCell = initialRow.createCell(0);
	        	initFnCell.setCellValue("Filename");
	        	initFnCell.setCellStyle(style);
	        	
	        	Cell initNrCell = initialRow.createCell(1);
	        	initNrCell.setCellValue("PageNr");
	        	initNrCell.setCellStyle(style);
	        }

	    	Row nextRow = currSheet.createRow(i);
	    	nextRow.setRowStyle(rowStyle);
	    	
	        /*
	         * create cell for the filename in the first column
	         */
        	Cell currFnCell = nextRow.createCell(0);
        	currFnCell.setCellValue(filename);
        	currFnCell.setCellStyle(style);
        	
        	Cell currPageNrCell = nextRow.createCell(1);
        	currPageNrCell.setCellValue(pageNr);
        	currPageNrCell.setCellStyle(style);
	    	
	    	int maxLines = 0;
	    	int maxColId = 0;
	    	int nrCoordsAdded = 0;
	    	
	    	i++;
	        
	        for (Integer key : entry.keySet()) {
	        	        	
	        	int colSpan = 0;
	        	int rowSpan = 0;
	        	boolean mergedVertical = false;
	        	boolean mergedHorizontal = false;
	        		        	
	        	if (entry.get(key) != null){
	        		
	        		colSpan = entry.get(key).getColSpan();
	        		rowSpan = entry.get(key).getRowSpan();
	        		
		        	if (rowSpan > 1){
		        		mergedVertical = true;
		        	}

		        	if (colSpan > 1){
		        		mergedHorizontal = true;
		        	}

		        	// we must add the nr of coords added until now and 2 (for the added filename and pageNr)        	
		        	int colID = entry.get(key).getCol() + nrCoordsAdded + 2;
		        	// because of the added first line
		        	int rowID = entry.get(key).getRow() + 1;
		        	
			        if (firstRow) {
			        	Cell structureCell = initialRow.createCell(colID);
			        	structureCell.setCellValue(entry.get(key).getStructure());
			        	structureCell.setCellStyle(style);
			        	
			        }
		        	
		        	String text = entry.get(key).getUnicodeTextFromLines();
		        	
		        	Cell currCell = nextRow.createCell(colID);
		        	currCell.setCellValue(text);   
		        	currCell.setCellStyle(style);
		        	
		        	maxColId = colID;
		        	
	        		if(mergedVertical || mergedHorizontal){
//	        			logger.debug("entry.get(key).getUnicodeTextFromLines() " + entry.get(key).getUnicodeTextFromLines());
//	        			logger.debug(" row ID " + rowID + " rowSpan " + rowSpan + " merged H "+ mergedHorizontal + " colID " + colID + " colSpan " + colSpan + " merged V " + mergedVertical);
	        			
	        			String [] textlines = text.split(System.lineSeparator());
	        			
	        			if (textlines.length > 0){
		        			int length = textlines.length;
		        			if (maxLines < length){
		        				maxLines = length;
		        			}
		        			
	        			}
	        			currSheet.addMergedRegion(new CellRangeAddress(rowID,rowID+rowSpan-1,colID,colID+colSpan-1));
	        			maxColId = colID+colSpan-1;
	        		}

	            	/*
	            	 * to store the coordinates for each single cell
	            	 */
            		String coords = entry.get(key).getCoordinates();
            		colID=colID+1;
            		nrCoordsAdded = nrCoordsAdded+1;
		        	Cell coordsCell = nextRow.createCell(colID);
		        	coordsCell.setCellValue(coords);
		        	coordsCell.setCellStyle(style);

	        	}
	        	else{
		        	Cell currCell = nextRow.createCell(key+2);
		        	currCell.setCellStyle(style);
	        	}
	        }

	        /*
	         * set the height of a row manually since it does not work for merged region cells!!!
	         * so if there are merged cells in a row we remember the maximum number of text lines in one of these cells to set the appropriate height
	         */	
			if (maxLines > 0){
				logger.debug("row contains maximum nr.- of lines in cell of: "  + maxLines);
				nextRow.setHeight((short) (nextRow.getHeight()*maxLines));
			}
			//afer first run it can't be the firstRow anymore
			firstRow=false;
			
	    }
	}
	
	public static void main(String[] args) throws Exception {		
		
		TrpDoc docWithTables = LocalDocReader.load("C:\\tmp\\Transkribus_Exporte\\Matrikel_lister_over_plantagerne_1799_Mappe_nr__2\\Matrikel_lister_over_plantagerne_1799_Mappe_nr__2\\");
		//TrpDoc docWithTables = LocalDocReader.load("C:/Neuer Ordner/TRAINING_TESTSET_NAF_Poll_Tax_M_5/TRAINING_TESTSET_NAF_Poll_Tax_M_5/");
		
		Set<Integer> pageIndices = null; // null means every page
		
		//pageIndices must be set here instead of being null because it gets used in ExportUtils
		if (pageIndices == null){
			pageIndices = new HashSet<Integer>();
			for (int i = 0; i<docWithTables.getNPages(); i++){
				pageIndices.add(i);
			}
		}
		
		TrpXlsxOneTableBuilder txslx = new TrpXlsxOneTableBuilder();
		txslx.writeXlsxForTables(docWithTables, new File("C:\\tmp\\Transkribus_Exporte\\Matrikel_lister_over_plantagerne_1799_Mappe_nr__2\\testExport.xlsx"), pageIndices, null, null,-1);
		System.out.println("finished");
		
	}

}
