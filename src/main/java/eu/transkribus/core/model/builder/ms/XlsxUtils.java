package eu.transkribus.core.model.builder.ms;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.ImageOutputStream;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.ss.usermodel.CellType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class XlsxUtils {
	private final static Logger logger = LoggerFactory.getLogger(XlsxUtils.class);

	static void autosizeTable(Workbook wb) {
		/*
		 * auto size the columns
		 */
		for (int i = 0; i<wb.getNumberOfSheets(); i++) {
			XSSFSheet currSheet = (XSSFSheet) wb.getSheetAt(i);
            Iterator<Row> rowIterator = currSheet.rowIterator();
            /**
             * Escape the header row *
             */
            if (rowIterator.hasNext())
            {
                Row headerRow = (Row) rowIterator.next();
                               
                //get the number of cells in the header row
                int numberOfCells = headerRow.getPhysicalNumberOfCells();
                int maxLines = 1;
                for (int j = 0; j<numberOfCells; j++){
                	Cell currCell = headerRow.getCell(j);
                	if(currCell != null && currCell.getCellType()==CellType.STRING) {
                    	String text = currCell.getStringCellValue();
        				String [] textlines = text.split(System.getProperty("line.separator"));
        				if (textlines.length > 0){
        					//logger.debug("textlines.length: " + textlines.length);
        					int length = textlines.length;
        					if (maxLines < length){
        						maxLines = length;
        					}
        				}
                	}

                	currSheet.autoSizeColumn(j, true);
                }  
			    /*
			     * set the height of a row manually since it does not work for merged region cells!!!
			     * so if there are merged cells in a row we remember the maximum number of text lines in one of these cells to set the appropriate height
			     */	
				if (maxLines > 0){
					headerRow.setHeight((short) (headerRow.getHeight()*maxLines));
				}
            }  
		}
	}
	
	static void autosizeTable(XSSFSheet currSheet) {
		/*
		 * auto size the columns
		 */
        Iterator<Row> rowIterator = currSheet.rowIterator();
        /**
         * Escape the header row *
         */
        if (rowIterator.hasNext())
        {
            Row headerRow = (Row) rowIterator.next();
                           
            //get the number of cells in the header row
            int numberOfCells = headerRow.getPhysicalNumberOfCells();
            for (int j = 0; j<numberOfCells; j++){
            	currSheet.autoSizeColumn(j, true);
            }
                          
        }                        
		
	}
	
    static byte[] compressImageToByteArray(BufferedImage image, float quality)
            throws IOException {
        // save jpeg image with specific quality. "1f" corresponds to 100% , "0.7f" corresponds to
        // 70%

        ImageWriter jpgWriter = ImageIO.getImageWritersByFormatName("jpg").next();
        ImageWriteParam jpgWriteParam = jpgWriter.getDefaultWriteParam();
        jpgWriteParam.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
        jpgWriteParam.setCompressionQuality(quality);

        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ImageOutputStream ios = ImageIO.createImageOutputStream(bos);
        jpgWriter.setOutput(ios);
        IIOImage outputImage = new IIOImage(image, null, null);
        jpgWriter.write(null, outputImage, jpgWriteParam);
        byte[] result = bos.toByteArray();
        jpgWriter.dispose();
        return result;
    }
    
	public static int[] getCropValues(String coords, BufferedImage src){
		int[] values = new int[4];
		
		String[] singleCoords = coords.split(" ");
		
		ArrayList<Integer> xPts = new ArrayList<Integer>();
		ArrayList<Integer> yPts = new ArrayList<Integer>();	
		
		for(String s: singleCoords){
			String[] PtCoords = s.split(",");
			xPts.add(Integer.parseInt(PtCoords[0]));					//All X coords
			yPts.add(Integer.parseInt(PtCoords[1]));					//All Y coords
		}
		
		int CoordX1 = xPts.get(0);
		int CoordX2 = xPts.get(0);
		int CoordY1 = yPts.get(0);
		int CoordY2 = yPts.get(0);
		
		//find outlining points
		for(int x : xPts){
			if(CoordX1>x){
				CoordX1 = x;
			}
			if(CoordX2<x){
				CoordX2 = x;
			}
		}
		for(int y : yPts){
			if(CoordY1>y){
				CoordY1 = y;
			}
			if(CoordY2<y){
				CoordY2 = y;
			}
		}			
	
		int width = Math.abs(CoordX2-CoordX1);
		int height = Math.abs(CoordY2-CoordY1);
		
		/*
		 * to get broader crops
		 */
		width = (int)(width*1.35f);
//		height = (int)(height*2f);
		CoordX1-=(int)(width*0.125f);
//		CoordY1-=(int)(height*0.25f);
		
		int imgWidth = src.getMinX()+src.getWidth();
		int imgHeight = src.getMinY()+src.getHeight();
		
		if(CoordX1<src.getMinX()) {
			CoordX1=src.getMinX();
		}
		if(CoordY1<src.getMinY()) {
			CoordY1=src.getMinY();
		}
		if (CoordX1+width > src.getMinX()+imgWidth) {
			width = imgWidth-CoordX1;
		}
		if (CoordY1+height > src.getMinY()+imgHeight) {
			height = imgHeight-CoordY1;
		}
		
		if (width <= 0) {
			width = 10;
		}
		if (height <= 0) {
			height = 10;
		}

		
		values[0]=CoordX1;
		values[1]=CoordY1;
		values[2]=width;
		values[3]=height;		
		
		return values;
	}

}
