package eu.transkribus.core.exceptions;

/**
 * Exception should be thrown when a concurrent execution is issued but is not allowed.
 * Example: execution of HTR recognition on same page of already running job.
 */
public class ConcurrentExecutionException extends IllegalArgumentException {
	private static final long serialVersionUID = 768323166196997281L;
	
	public ConcurrentExecutionException(String message) {
		super(message);
	}
	
	public ConcurrentExecutionException(String message, Throwable cause) {
        super(message, cause);
    }
}
