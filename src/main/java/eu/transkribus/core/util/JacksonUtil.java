package eu.transkribus.core.util;

import com.fasterxml.jackson.databind.DeserializationFeature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

public class JacksonUtil {
	private static final Logger logger = LoggerFactory.getLogger(JacksonUtil.class);
	
	public static ObjectMapper createDefaultMapper(boolean enableIndent) {
		final ObjectMapper mapper = new ObjectMapper();
		mapper.setSerializationInclusion(Include.NON_NULL);
		//in case the source object's version is different from the receiver's one fields could differ. Ignore unknown fields.
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		if (enableIndent) {
			mapper.enable(SerializationFeature.INDENT_OUTPUT);
		}
		return mapper;
	}
	
	public static String toJson(Object o) throws JsonProcessingException {
		return createDefaultMapper(false).writeValueAsString(o);
	}
	
	public static String toJson2(Object o) {		
		try {
			if (o == null) {
				return null;
			}			
			
			return toJson(o);
		} catch (Throwable e) {
			logger.warn("Failed to serialize object of type {}", o.getClass().getSimpleName(), e);
			return null;
		}
	}
	
	public static <T> T fromJson(String jsonStr, Class<T> valueType) throws JsonProcessingException {
		return createDefaultMapper(false).readValue(jsonStr, valueType);
	}
	
	public static <T> T fromJson2(String jsonStr, Class<T> valueType) {
		try {
			if (jsonStr == null) {
				return null;
			}
			
			return fromJson(jsonStr, valueType);
		} catch (Throwable e) {
			return null;
		}
	}

}
