package eu.transkribus.core.util;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.text.WordUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @deprecated not used. This does not support ISO 639-2 alpha-3 codes
 */
public class LocaleUtils {
	private static final Logger logger = LoggerFactory.getLogger(LocaleUtils.class);
	
	private LocaleUtils() {}
	
	/**
	 * LocaleUtils will by default resolve english language display labels
	 */
	public static final Locale DEFAULT_LOCALE = Locale.ENGLISH;

	private static final LocaleResolver DEFAULT_RESOLVER = new LocaleResolver(DEFAULT_LOCALE);
	
	public static Locale resolveLocaleFromDisplayLanguage(String displayLanguage) {
		return DEFAULT_RESOLVER.resolveLocaleFromDisplayLanguage(displayLanguage);
	}

	public static String normalizeDisplayLanguage(String displayLanguage) {
		return WordUtils.capitalize(
				StringUtils.lowerCase(
						StringUtils.normalizeSpace(
								StringUtils.trim(displayLanguage)
								)
						)
				);
	}
	
	public static class LocaleResolver {
		/**
		 * The language of display country values to resolve.
		 */
		private final Locale locale;
		private final Map<String, Locale> displayCountryCodeLookup;
		private final Set<Locale> availableLocales;

		/**
		 * Instantiate a LocaleResolver for language display country labels in a given language.
		 * E.g. passing <pre>new Locale("en")</pre> will resolve the english label "German" to the fitting Locale for "de". 
		 * @param locale the language of display country values to resolve.
		 */
		public LocaleResolver(Locale locale) {
			this.locale = locale;
			this.displayCountryCodeLookup = loadLocales(this.locale);
			this.availableLocales = this.displayCountryCodeLookup.entrySet().stream()
					.map(e -> e.getValue())
					.collect(Collectors.toSet());
		}
		
		public Locale resolveLocaleFromDisplayLanguage(String displayCountry) {
			if(StringUtils.isEmpty(displayCountry)) {
				return null;
			}
			Locale l = displayCountryCodeLookup.get(normalizeDisplayLanguage(displayCountry).toLowerCase());
			if(l == null) {		
				//try to interpret as iso code, don't mess with upper-/lowercase.
				//FIXME: this will not resolve 3-letter ISO639-3 codes though! We might need to implement another fallback here for this...
				try {
					l = org.apache.commons.lang3.LocaleUtils.toLocale(displayCountry.trim());
					logger.debug("Resolved Locale from language code: Locale: {}, displayName = {}, displayLanguage = {}, displayCountry = {}", 
							l, l.getDisplayName(DEFAULT_LOCALE), l.getDisplayLanguage(DEFAULT_LOCALE), l.getDisplayCountry(DEFAULT_LOCALE));
				} catch (IllegalArgumentException e) {
					logger.debug(e.getMessage());
					l = null;
				}
			}
			return l;
		}
		
		public Set<Locale> getAvailableLocales() {
			//don't expose internal field as it must not be changed
			Set<Locale> locales = new HashSet<>();
			locales.addAll(availableLocales);
			return locales;
		}
		
		public Map<String, Locale> loadLocales(Locale locale) {
			Map<String, Locale> lookup = null;

			try {
				lookup = loadLocalesFromFile(locale);
			} catch (IOException e) {
				logger.warn("Could init Local lookup map: " + e.getMessage());
				lookup = null;
			}

			if(lookup == null) {
				logger.warn("Building lookup table with available locales from runtime for language: {}", locale);
				lookup = loadLocalesFromRuntime(locale);
			}
			//convert all keys to lower case for easier lookup.
			Map<String, Locale> lcLookup = new HashMap<>();
			for(Entry<String, Locale> e : lookup.entrySet()) {
				lcLookup.put(e.getKey().toLowerCase(), e.getValue());
			}
			return lcLookup;
		}
		
		private Map<String, Locale> loadLocalesFromFile(Locale locale) throws IOException {
			URL resource = LocaleUtils.class.getClassLoader().getResource("language-codes.json");
			if(resource == null) {
				//the list of available locales contains only 160 entries (Java 8) and misses some included in the Transkribus Lite JSON file. 
				throw new FileNotFoundException("Could not find language code list on classpath!");
			}
			Object[] data;
			try {
				data = (Object[]) new ObjectMapper().readValue(resource, Object[].class);
			} catch (IOException e) {
				throw new IOException("Could not load language code list on classpath!", e);
			}
			Map<String, Locale> locales = new HashMap<>();
			for(Object o : data) {
				@SuppressWarnings("unchecked")
				Map<String, String> entry = ((Map<String, String>) o);
				String alpha2 = entry.get("alpha2");
				Locale l = new Locale(alpha2, "");
				//the tag named like the display language contains the display labels in that language (WTF?) and possibly synonyms
				String labelStr = entry.get(locale.getDisplayLanguage(DEFAULT_LOCALE));
				List<String> labels = CoreUtils.parseStringList(labelStr, ";", true, true);
				if(labels.isEmpty()) {
					//we only have a resource file with english labels now. label will not be set here.
					//try to generate label from Java Locale
					String label = l.getDisplayLanguage(locale);
					logger.warn("Generated displayLanguage from Locale '{}': {}", alpha2, label);
					labels.add(label);
				}
				if(labels.isEmpty()) {
					logger.warn("No label found for tag: {}", alpha2);
					continue;
				}
				for(String displayLanguage : labels) {
					locales.put(displayLanguage, l);
				}
			}
			return locales;
		}
		
		private Map<String, Locale> loadLocalesFromRuntime(Locale locale) {
			Map<String, Locale> locales = new HashMap<String, Locale>();
			for (Locale l : Locale.getAvailableLocales()) {
				logger.debug("Adding Locale: {}, displayName = {}, displayLanguage = {}, displayCountry = {}", 
						l, l.getDisplayName(locale), l.getDisplayLanguage(locale), l.getDisplayCountry(locale));
				locales.put(l.getDisplayLanguage(locale), l);
			}
			logger.debug("Built lookup map for {} locales", locales.size());
			return locales;
		}
	}

	public static boolean isValidLanguageCode(String code) {
		return DEFAULT_RESOLVER.getAvailableLocales().contains(new Locale(code, ""));
	}
	
	/**
	 * Parse the language field from TrpHtr: split csv values, normalize Strings (whitespace, casing), try to resolve ISO language codes from the values.
	 * @param csvString
	 * @return ResolvedLocales object containing the original csvString, a normalized CSV string, and a list of resolved ISO language codes.
	 * @deprecated using iso 639-2 codes instead
	 */
	public static ResolvedLocales parseLangCsvString(String csvString) {
		List<String> langList = CoreUtils.parseStringList(csvString, "[,;/]+", true, true);
		List<Locale> locales = new ArrayList<>();
		for(int i = 0; i < langList.size(); i++) {
			String lang = langList.get(i);
			//try to resolve a valid locale
			Locale locale = LocaleUtils.resolveLocaleFromDisplayLanguage(lang);
			if(locale != null) {
				//add to locale list
				locales.add(locale);
				
				//variant 1: replace value in list with display lanuage based on default locale? In some situations this might override what the user has entered.
//				langList.set(i, locale.getDisplayLanguage(LocaleUtils.DEFAULT_LOCALE));
				
				//variant 2: Replace only if normalized form matches the displayLanguage exactly. 
				//This will fix values like "german", "GERMAN", etc. but not "old church slavonic" as it does not match the display lanugage...
//				String normStr = LocaleUtils.normalizeDisplayLanguage(lang);
//				if(!normStr.equals(lang) && normStr.equals(locale.getDisplayLanguage(LocaleUtils.DEFAULT_LOCALE))) {
//					//Replace with normalized value. This may be somethin like
//					langList.set(i, normStr);
//				}
				
				//=> variant 3: don't touch this.
			} else {
				//this is not a valid display lanuage name nor a valid iso code. Don't change what the user has entered
//				langList.set(i, LocaleUtils.normalizeDisplayLanguage(lang));
			}
		}
		return new ResolvedLocales(csvString, langList, locales);
	}
	
	/**
	 * @deprecated
	 *
	 */
	public static class ResolvedLocales {
		private final String input;
		private final List<String> normalizedLangs;
		private final List<Locale> locales;
		private ResolvedLocales(String input, List<String> normalizedLangs, List<Locale> locales) {
			this.input = input;
			this.normalizedLangs = normalizedLangs;
			this.locales = locales;
		}
		public String getInput() {
			return input;
		}
		public List<String> getNormalizedLangs() {
			return normalizedLangs;
		}
		public String getNormalizedLangsCsv() {
			return normalizedLangs.stream().collect(Collectors.joining(", "));
		}
		public List<Locale> getLocales() {
			return locales;
		}
	}
}
