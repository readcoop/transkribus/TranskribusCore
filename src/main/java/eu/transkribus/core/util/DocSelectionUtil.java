package eu.transkribus.core.util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.function.Predicate;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.transkribus.core.model.beans.DocSelection;
import eu.transkribus.core.model.beans.TrpDocMetadata;
import eu.transkribus.core.model.beans.enums.EditStatus;

public class DocSelectionUtil {
	private static final Logger logger = LoggerFactory.getLogger(DocSelectionUtil.class);
	
	public static List<DocSelection> fromDocIds(List<Integer> docIds, Map<Integer, String> pages, EditStatus editStatus, Boolean doNotUseLatestTranscriptIfEditStatusNotFound) {
		List<DocSelection> ds = new ArrayList<>();
		for (int docId : docIds) {
			String pagesStr = pages!=null ? pages.get(docId) : null;
			ds.add(new DocSelection(docId, pagesStr, editStatus, doNotUseLatestTranscriptIfEditStatusNotFound));
		}
		return ds;
	}
	
	public static List<DocSelection> fromDocIds(int... docIds) {
		return fromDocIds(CoreUtils.asList(docIds), null, null, null);
	}
	
	/**
	 * Compute nrOfPages on the basis of {@link TrpDocMetadata#getNrOfPages()} and a given page string in docSelectionDetails.
	 * 
	 * @param docSelectionDetails a map containing the selected documents' metadata and an optional page string, that may not yet be validated.
	 * @param docMdPredicate if not null, document metadata that do not match this predicate will be not considered in the sum
	 * @return a positive int
	 */
	public static int sumDocSelectionNrOfPages(Map<TrpDocMetadata, DocSelection> docSelectionDetails, Predicate<TrpDocMetadata> docMdPredicate) {
		if(docSelectionDetails == null || docSelectionDetails.isEmpty()) {
			return 0;
		}
		int nrOfPages = 0;
		for(Entry<TrpDocMetadata, DocSelection> e : docSelectionDetails.entrySet()) {
			TrpDocMetadata d = e.getKey();
			String pageStr = e.getValue().getPages();
			if(docMdPredicate != null &&  !docMdPredicate.test(d)) {
				logger.debug("Not summing selected pages for docId = {} as it does not match docMd predicate {}", d.getDocId(), docMdPredicate);
				continue;
			}
			int nrOfPagesInDoc = 0;
			if(StringUtils.isBlank(pageStr)) {
				nrOfPagesInDoc = d.getNrOfPages();
			} else {
				try {
					nrOfPagesInDoc = PagesStrUtil.getPageIndices(pageStr, d.getNrOfPages(), false).size();
				} catch (IOException e1) {
					logger.warn("Invalid page string: docId = {}, pageStr = {}", d.getDocId(), pageStr);
					//assume full document, but this case may be handled differently by each tool!
					nrOfPagesInDoc = d.getNrOfPages();
				}
			}
			nrOfPages += nrOfPagesInDoc;
		}
		return nrOfPages;
	}
	
}
