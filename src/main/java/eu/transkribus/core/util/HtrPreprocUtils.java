package eu.transkribus.core.util;

import java.io.File;

public class HtrPreprocUtils {
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(HtrPreprocUtils.class);
	
	public static final String TEXT_FEATS_CFG_FILENAME = "textFeatsCfg";
	public static final String TRP_PREPROC_PARS_FILENAME = "trpPreprocPars.json";
	
	public static File getPreprocessingConfigFile(String path) {
		File ppFile =  new File(path, TRP_PREPROC_PARS_FILENAME);
		if (ppFile.exists()) {
			logger.debug("Found TRP config file at: "+ppFile);
			return ppFile;
		}
		ppFile = new File(path, TEXT_FEATS_CFG_FILENAME);
		if (ppFile.exists()) {
			logger.debug("Found textFeats config file at: "+ppFile);
			return ppFile;
		}
		return null;
	}

}
