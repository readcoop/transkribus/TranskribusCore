package eu.transkribus.core.util;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.fasterxml.jackson.core.JsonProcessingException;
import eu.transkribus.core.model.beans.TrpHtr;
import org.apache.commons.collections.CollectionUtils;

import eu.transkribus.core.model.beans.TrpEntityAttribute;

public class AttributeUtils {
	private AttributeUtils() {}
	
	public static TrpEntityAttribute buildAttribute(String type, int entityId, String name, String value) {
		TrpEntityAttribute a = new TrpEntityAttribute();
		a.setType(type);
		a.setEntityId(entityId);
		a.setName(name);
		a.setValue(value);
		return a;
	}
	
	public static List<TrpEntityAttribute> findAttributes(List<TrpEntityAttribute> attributes, String type, String name) {
		if(name == null) {
			//null not allowed
			return new ArrayList<>(0);
		}
		List<TrpEntityAttribute> byType = findAttributes(attributes, type);
		if(byType.isEmpty()) {
			return byType;
		}
		return byType.stream()
				.filter(a -> name.equals(a.getName()))
				.collect(Collectors.toList());
	}
	
	public static List<TrpEntityAttribute> findAttributes(List<TrpEntityAttribute> attributes, String type) {
		if(CollectionUtils.isEmpty(attributes)) {
			return new ArrayList<>(0);
		}
		if(type == null) {
			//null not allowed
			return new ArrayList<>(0);
		}
		return attributes.stream()
			.filter(a -> type.equals(a.getType()))
			.collect(Collectors.toList());
	}

    public static Object deserializeObject(TrpEntityAttribute a) {
		if(a == null) {
			return null;
		}
		if(a.getValue() == null) {
			return null;
		}
		if(!TrpHtr.ATTRIBUTE_PROPERTY_OBJ.equals(a.getType())) {
			throw new IllegalArgumentException("Not a property object type attribute. Attribute ID = " + a.getAttributeId());
		}
		try {
			Class<?> clazz = Class.forName(a.getName());
			return JacksonUtil.createDefaultMapper(false).readValue(a.getValue(), clazz);
		} catch (ClassNotFoundException | JsonProcessingException e) {
			throw new IllegalStateException("Could not deserialize attribute object of type = " + a.getName(), e);
		}
    }

	public static TrpEntityAttribute serializeObject(int entityId, Object obj) {
		if(obj == null) {
			return null;
		}
		//for now there is only this type, labeling the attribute as a json object
		final String type = TrpHtr.ATTRIBUTE_PROPERTY_OBJ;
		try {
			String value = JacksonUtil.createDefaultMapper(false).writeValueAsString(obj);
			return buildAttribute(type, entityId, obj.getClass().getCanonicalName(), value);
		} catch (JsonProcessingException e) {
			throw new IllegalStateException("Could not serialize object of type = " + obj.getClass().getName(), e);
		}
	}
}
