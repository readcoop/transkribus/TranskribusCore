package eu.transkribus.core.util;

import eu.transkribus.core.model.beans.auth.TrpRole;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class AuthUtils {
	
	public static boolean isOwner(TrpRole role) {
		return role!=null && role.getValue()>=TrpRole.Owner.getValue();
	}
	
	/**
	 * Can this role e.g. upload a document
	 * @param role
	 * @return
	 */
	public static boolean canManage(TrpRole role) {
		return role != null && role.getValue() >= TrpRole.Editor.getValue();
	}

	/**
	 * Can this role do any editing
	 * @param role
	 * @return
	 */
	public static boolean canTranscribe(TrpRole role) {
		return role != null && role.getValue() >= TrpRole.CrowdTranscriber.getValue();
	}

	/**
	 * Check if collection role is authorized to execute recognition jobs
	 * @param role
	 * @return
	 */
	public static boolean canRunRecognition(TrpRole role) {
		return role != null && role.getValue() >= TrpRole.Transcriber.getValue();
	}

	public static boolean canRead(TrpRole role){
		return role != null && role.getValue() >= TrpRole.Reader.getValue();
	}

	public static List<TrpRole> getAssignableRoles(TrpRole role) {
		if (role == null) {
			return Collections.EMPTY_LIST;
		}
		switch (role) {
			case Admin:
			case Owner:
				return Arrays.asList(TrpRole.Owner, TrpRole.Editor, TrpRole.Transcriber, TrpRole.ManualTranscriber, TrpRole.Reader);
			case Editor:
				return Arrays.asList(TrpRole.Editor, TrpRole.Transcriber, TrpRole.ManualTranscriber, TrpRole.Reader);
			default:
				return Collections.EMPTY_LIST;
		}
	}
}
