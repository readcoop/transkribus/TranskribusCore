package eu.transkribus.core.util;

import java.util.Arrays;
import java.util.HashSet;

import org.apache.commons.lang3.StringUtils;

import eu.transkribus.core.model.beans.TrpModelMetadata;
import eu.transkribus.core.model.beans.job.enums.JobImpl;

/**
 * This class contains constants for all model providers which are short names for a specific tool.
 * Note: All providers should be unique and should not be changed once they are used!
 *
 */
public class ModelUtil {
	
	public final static String PROVIDER_CITLAB = "CITlab";
	public final static String PROVIDER_CITLAB_PLUS = "CITlabPlus";
	public final static String PROVIDER_PYLAIA = "PyLaia";
	public final static String PROVIDER_CITLAB_BASELINES = "CITlab-Baselines";
	public final static String PROVIDER_TRANSKRIBUS_BASELINES = "TranskribusBaselines";
	public final static String PROVIDER_TRHTR = "TrHtr";
	public final static String PROVIDER_LA2 = "fields";
	public final static String PROVIDER_TABLE = "tables";
	
	public final static String TYPE_TEXT = "text";
	public final static String TYPE_LAYOUT = "layout";
	
	public final static String[] ALL_PROVIDER = { PROVIDER_CITLAB, PROVIDER_CITLAB_PLUS, PROVIDER_PYLAIA, PROVIDER_CITLAB_BASELINES, PROVIDER_TRANSKRIBUS_BASELINES, PROVIDER_TRHTR, PROVIDER_LA2, PROVIDER_TABLE }; // provider are shown in UI in this order
	public static final String[] ALL_TYPES = { TYPE_TEXT, TYPE_LAYOUT };
	
	static {
		// assert that all provider names are unique
		if (new HashSet<>(Arrays.asList(ALL_PROVIDER)).size() != ALL_PROVIDER.length) {
			throw new AssertionError("Model provider names are not unique!");
		}
		
		if (new HashSet<>(Arrays.asList(ALL_TYPES)).size() != ALL_TYPES.length) {
			throw new AssertionError("Model type names are not unique!");
		}		
	}
	
	public static boolean hasModelLanguageModel(TrpModelMetadata md) {
		return md!=null && md.isLanguageModelExists()!=null && md.isLanguageModelExists();
	}
	
	public static String getLossLabelForModel(TrpModelMetadata md) {
		if (md == null) {
			return "";
		}
		else {
			return getLossLabelForType(md.getType());
		}
	}
	
	public static String getLossLabelForType(String type) {
		if (type == null) {
			return null;
		}
		
		switch (type) {
		case TYPE_TEXT:
			return "CER";
		case TYPE_LAYOUT:
			return "Loss";
		default:
			return "Loss";
		}
	}
	
	public static String[] getProviderForType(String type) {
		if (type == null) { // type == null means all types
			return ALL_PROVIDER;
		}
		
		switch (type) {
		case TYPE_TEXT:
			return new String[] { PROVIDER_CITLAB_PLUS, PROVIDER_PYLAIA, PROVIDER_TRHTR };
		case TYPE_LAYOUT:
			return new String[] { PROVIDER_CITLAB_BASELINES, PROVIDER_LA2, PROVIDER_TABLE, PROVIDER_TRANSKRIBUS_BASELINES };
		default:
			return new String[] {};
		}		
	}

	public static String getTypeForProvider(String provider) {
		if (provider==null) {
			return null;
		}
		switch (provider) {
			case PROVIDER_CITLAB_BASELINES:
			case PROVIDER_TABLE:
			case PROVIDER_LA2:
			case PROVIDER_TRANSKRIBUS_BASELINES:
				return TYPE_LAYOUT;
			case PROVIDER_CITLAB:
			case PROVIDER_CITLAB_PLUS:
			case PROVIDER_PYLAIA:
			case PROVIDER_TRHTR:
				return TYPE_TEXT;
			default:
				return null;
		}
	}
	
	public static boolean hasTypeLanguage(String type) {
		if (type == null) { // type == null means all types
			return true;
		}
		
		switch (type) {
		case TYPE_TEXT:
			return true;
		case TYPE_LAYOUT:
			return false;
		default:
			return false;
		}
	}
	
	public static String getLabelForModelProvider(String provider) {
		if (StringUtils.isEmpty(provider)) {
			return null;
		}
		switch (provider) {
		case ModelUtil.PROVIDER_CITLAB:
			return "CITlab HTR";
		case ModelUtil.PROVIDER_CITLAB_PLUS:
			return "CITlab HTR+";
		case ModelUtil.PROVIDER_PYLAIA:
			return "PyLaia HTR";
		case ModelUtil.PROVIDER_TRHTR:
			return "Transformer HTR";					
		case ModelUtil.PROVIDER_CITLAB_BASELINES:
			return "Baselines";
		case ModelUtil.PROVIDER_TRANSKRIBUS_BASELINES:
			return "Transkribus Baselines";
		default:
			return provider;
		}
	}
	
}
