package eu.transkribus.core.util;

import eu.transkribus.core.model.beans.TrpPage;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class PagesStrUtil {
	
	public static List<Integer> getPageIndices(String pagesStr, int nPages, boolean sorted) throws IOException {
		List<Integer> pageIndices = CoreUtils.parseRangeListStrToList(pagesStr, nPages);
		if (sorted) {
			Collections.sort(pageIndices);
		}
		return pageIndices;
	}
	
	public static List<Integer> getPageNrs(String pagesStr, int nPages, boolean sorted) throws IOException {
		return getPageIndices(pagesStr, nPages, sorted).stream().map(i -> i+1).collect(Collectors.toList());
	}

	public static List<Integer> getPageNrsFiltered(String pagesStr, List<TrpPage> allPages, boolean sorted)  throws IOException {
        List<Integer> pageIndices = new ArrayList<>();
        for (TrpPage p : allPages) {
            if (isAcceptedBy(p, pagesStr)) {
                pageIndices.add(p.getPageNr());
            }
        }
        if (sorted) {
			Collections.sort(pageIndices);
		}
		return pageIndices;
	}

	private static boolean isAcceptedBy(TrpPage p, String pagesStr) throws IOException {

		int currentPageNumber = p.getPageNr();

		if (StringUtils.isEmpty(pagesStr))
			return false;

		String[] ranges = pagesStr.split(",");
		for (String r : ranges) {
			r = r.trim();
			if (r.isEmpty())
				continue;

			String[] numbers = r.split("-");
			try {
				if (numbers.length == 2) {
					int pageNrStart = Integer.parseInt(numbers[0]);
					int pageNrEnd = Integer.parseInt(numbers[1]);
					if (pageNrStart<=currentPageNumber && pageNrEnd>=currentPageNumber) {
						return true;
					}
				} else if (numbers.length == 1) {
					int s = Integer.parseInt(numbers[0]);
					if (s == currentPageNumber) {
						return true;
					}
				} else {
					throw new IOException("Could not parse invalid range: " + r);
				}
			} catch (NumberFormatException e) {
				throw new IOException("Error parsing number - " + e.getMessage(), e);
			}
		}
		return false;



	}
}
