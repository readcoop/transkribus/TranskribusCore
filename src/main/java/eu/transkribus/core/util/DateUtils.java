package eu.transkribus.core.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DateUtils {
	/**
	 * Get the remaining seconds until the next instance of hour:minute:second
	 * @param hour
	 * @param minute
	 * @param second
	 * @return
	 */
	public static long getSecondsTill(final int hour, final int minute, final int second){ 
		long result = 0;
		Calendar now = Calendar.getInstance();
		final int currSecOfDay = (now.get(Calendar.HOUR_OF_DAY) * 3600) 
				+ (now.get(Calendar.MINUTE) * 60) + now.get(Calendar.SECOND);
		final int targetSecOfDay = (hour * 3600) + (minute * 60) + second;
		
		if(currSecOfDay == targetSecOfDay) {
			return result;
		} else if(currSecOfDay < targetSecOfDay) {
			result = targetSecOfDay - currSecOfDay;
		} else {
			//Fun fact: a day has 60*60*24 = 86400 seconds
			result = (86400 - currSecOfDay) + targetSecOfDay;
		}
		return result;
	}

	/**
	 * Determines if two dates are equal up to no more than the specified 
     * most significant field.<br>
	 * Same as {@link org.apache.commons.lang3.time.DateUtils#truncatedEquals(Date, Date, int)} but also does null checks.
	 * 
	 * @param date1 the first date
     * @param date2 the second date
     * @param field the field from {@code Calendar}
     * @return <code>true</code> if equal; otherwise <code>false</code>
	 */
	public static boolean truncatedEquals(Date date1, Date date2, int field) {
		if(date1 == null && date2 == null) {
			return true;
		}
		if(date1 == null && date2 != null) {
			return false;
		}
		if(date1 != null && date2 == null) {
			return false;
		}
		return org.apache.commons.lang3.time.DateUtils.truncatedEquals(date1, date2, field);
	}
	
	/**
	 * Determines how two dates compare up to no more than the specified 
     * most significant field.<br>
	 * Same as {@link org.apache.commons.lang3.time.DateUtils#truncatedCompareTo(Date, Date, int)} but also does null checks.
	 * 
	 * @param date1 the first date
     * @param date2 the second date
     * @param field the field from {@code Calendar}
     * @param nullFirst if true, then null values are considered greater than a non-null date
     * @return a negative integer, zero, or a positive integer as the first 
     * date is less than, equal to, or greater than the second.
	 */
	public static int truncatedCompareTo(Date date1, Date date2, int field, boolean nullFirst) {
		if(date1 == null && date2 == null) {
			return 0;
		}
		if(date1 == null && date2 != null) {
			return nullFirst ? -1 : 1;
		}
		if(date1 != null && date2 == null) {
			return nullFirst ? 1 : -1;
		}
		return org.apache.commons.lang3.time.DateUtils.truncatedCompareTo(date1, date2, field);
	}
	
	public static Calendar getCalendarStartOfToday() {
		Calendar cal = Calendar.getInstance();
		//set time fields to start of day
		cal.set(Calendar.MILLISECOND, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		return cal;
	}
	
	public static Calendar getCalendarStartOfWeek() {
		Calendar cal = getCalendarStartOfToday();
		//set to start of this week
		cal.set(Calendar.DAY_OF_WEEK, cal.getFirstDayOfWeek());
		return cal;
	}
	
	public static Calendar getCalendarStartOfMonth() {
		Calendar cal = getCalendarStartOfToday();
		//set to start of this month
		cal.set(Calendar.DAY_OF_MONTH, 1);
		return cal;
	}

	/**
	 * @return a {@link Calendar} object set to Jan 1st 00:00:00.000 of this year
	 */
	public static Calendar getCalendarStartOfYear() {
		return getCalendarStartOfYear(null);
	}

	/**
	 * @param year the year to set the calendar to, or null to leave it at the current year
	 * @return a {@link Calendar} object set to the 1.1. 00:00:00.000 of the year
	 */
	public static Calendar getCalendarStartOfYear(Integer year) {
		Calendar cal = getCalendarStartOfMonth();
		//set to first month of year
		cal.set(Calendar.MONTH, Calendar.JANUARY);
		if(year != null) {
			cal.set(Calendar.YEAR, year);
		}
		return cal;
	}
	
	/*
	 * get the Date from the year string
	 */
	public static Date getDate(String dateString) {
		
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy");
        
        if (dateString == null || dateString == "") {
        	 return null;
        }
        
        if (!dateString.matches("[0-9]{4}")){
        	Pattern pattern = Pattern.compile("[0-9]{4}");
            Matcher matcher = pattern.matcher(dateString);
        	int startIdx = matcher.find() ? matcher.start() : -1;
        	int endIdx = (startIdx+4<=dateString.length()) ? startIdx+4 : -1;
        	
        	if (startIdx == -1 || endIdx == -1) {
        		return null;
        	}
        	
        	dateString = dateString.substring(startIdx, endIdx);
        	
//        	if (dateString.startsWith("[before ")) {
//        		dateString = dateString.substring(dateString.lastIndexOf(" ")+1, dateString.lastIndexOf(" ")+5);
//        	}
//        	if (dateString.startsWith("[") || dateString.startsWith("¹")) {
//        		dateString = dateString.substring(1, 5);
//        	}
//        	if (dateString.endsWith(".")) {
//        		dateString = dateString.substring(0, dateString.lastIndexOf("."));
//        	}
//        	if (dateString.startsWith("ca. ")) {
//        		dateString = dateString.substring(dateString.lastIndexOf(" ")+1);
//        	}
        }

        try {

            Date date = formatter.parse(dateString);
            return date;

        } catch (ParseException e) {
        	//System.in.read();
            e.printStackTrace();
        }
		return null;
		
	}
}
