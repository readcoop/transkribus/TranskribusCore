package eu.transkribus.core.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ibm.icu.text.ArabicShaping;
import com.ibm.icu.text.ArabicShapingException;
import com.ibm.icu.text.Bidi;

import eu.transkribus.core.model.builder.txt.TrpTxtBuilder;

public class RTLUtils {
	private final static Logger logger = LoggerFactory.getLogger(RTLUtils.class);
	
    public static String bidiReorder(String text)
    {
        try {
        	Bidi bidi = new Bidi((new ArabicShaping(ArabicShaping.LETTERS_SHAPE)).shape(text), Bidi.DIRECTION_DEFAULT_LEFT_TO_RIGHT);
            bidi.setReorderingMode(0);
            String res = bidi.writeReordered(8);
            logger.debug("bidi string: " + res);
            return res;
        }
        catch (ArabicShapingException ase3) {
        	return text;
        }
    }
    
	public static boolean textIsRTL(String text) {
        for (int i = 0; i < text.length(); i++) {
            byte direction = Character.getDirectionality(text.charAt(i));

            if (direction == Character.DIRECTIONALITY_RIGHT_TO_LEFT
                    || direction == Character.DIRECTIONALITY_RIGHT_TO_LEFT_ARABIC
                    || direction == Character.DIRECTIONALITY_RIGHT_TO_LEFT_EMBEDDING
                    || direction == Character.DIRECTIONALITY_RIGHT_TO_LEFT_OVERRIDE)

                return true;
        }

        return false;
	}
	
	public static String fixPunctuation2(String text) {
        	
			char lastChar = text.charAt(text.length()-1);
        	logger.debug("char at end: " + lastChar);
            byte direction = Character.getDirectionality(lastChar);

            if (direction != Character.DIRECTIONALITY_RIGHT_TO_LEFT
                    && direction != Character.DIRECTIONALITY_RIGHT_TO_LEFT_ARABIC
                    		&& direction != Character.DIRECTIONALITY_RIGHT_TO_LEFT_EMBEDDING
                    				&& direction != Character.DIRECTIONALITY_RIGHT_TO_LEFT_OVERRIDE) {
            	String replacedString = ""+lastChar + text.substring(0, text.length()-2);
            	logger.debug("replaced string " + replacedString);
                return replacedString;
            }

        return text;
	}
	
	public static String fixPunctuation(String text) {
    	
		char lastChar = text.charAt(text.length()-1);
    	logger.debug("char at end: " + lastChar);
        
        String pattern = "(?!['\"\\[\\)\\(\\]])\\p{Punct}";
        
        String lastD = String.valueOf(lastChar);

        // Create a Pattern object
        Pattern r = Pattern.compile(pattern, Pattern.CASE_INSENSITIVE);

        // Now create matcher object.
        Matcher m = r.matcher(lastD);
        if (m.find()) {
        	String replacedString = ""+lastChar + text.substring(0, text.length()-1);
        	logger.debug("replaced string " + replacedString);
            return replacedString;
        }

        return text;
	}

}
