package eu.transkribus.core.util;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.collections.map.UnmodifiableMap;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.text.WordUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * ISO 639-2 language code lookup utility. Data is loaded from a resource json file.
 */
public class IsoLangUtils {
	private static final Logger logger = LoggerFactory.getLogger(IsoLangUtils.class);

	private static final String RESOURCE_FILE_NAME = "ISO-639-2-Languages.json";
	
	private IsoLangUtils() {}

	/**
	 * LocaleUtils will by default resolve english language display labels
	 */
	public static final Locale DEFAULT_LOCALE = Locale.ENGLISH;
	
	/**
	 * All Locales where display labels are included in the resource file {@value #RESOURCE_FILE_NAME}
	 */
	public static final Locale[] SUPPORTED_LOCALES = { 
			Locale.ENGLISH, 
			Locale.FRENCH, 
			Locale.GERMAN, 
			new Locale("pt"),
			Locale.ITALIAN,
			new Locale("es"),
			new Locale("nl")
	};

	public static final IsoLangResolver DEFAULT_RESOLVER = new IsoLangResolver(DEFAULT_LOCALE);
	public static final Map<Locale, IsoLangResolver> RESOLVERS = initResolverMap();

	@SuppressWarnings("unchecked")
	private static Map<Locale, IsoLangResolver> initResolverMap() {
		Map<Locale, IsoLangResolver> map = new HashMap<>();
		for(Locale l : SUPPORTED_LOCALES) {
			map.put(l, new IsoLangResolver(l));
		}
		return UnmodifiableMap.decorate(map);
	}
	
	/**
	 * Resolve ISO code for a single language name. Label is matched against all known synonyms for that language.
	 * 
	 * @param displayLanguage 
	 * @return
	 */
	public static String resolveCodeFromDisplayLanguage(String displayLanguage) {
		return DEFAULT_RESOLVER.resolveCodeFromDisplayLanguage(displayLanguage);
	}

	/**
	 * Resolve ISO code for a language name label. The label is the name from the resource file, including all synonyms.
	 * @param code
	 * @return
	 */
	public static String resolveLabelFromCode(String code) {
		return DEFAULT_RESOLVER.resolveLabelFromCode(code);
	}
	
	/**
	 * Resolve ISO code from a language name label. Label must match the value in the resource file.
	 * @param label
	 * @return
	 */
	public static String resolveCodeFromLabel(String label) {
		return DEFAULT_RESOLVER.resolveCodeFromLabel(label);
	}
	
	public static String normalizeDisplayLanguage(String displayLanguage) {
		return WordUtils
				.capitalize(StringUtils.lowerCase(StringUtils.normalizeSpace(StringUtils.trim(displayLanguage))));
	}
	
	public static boolean isValidLanguageCode(String code) {
		return DEFAULT_RESOLVER.getAvailableLangs().contains(code);
	}
	
	public static ResolvedIsoLangs parseLangCsvString(String csvString) {
		return DEFAULT_RESOLVER.parseLangCsvString(csvString);
	}
	
	/**
	 * Parse the csvString, collect all valid ISO 639-2 codes and return them as List
	 * @param csvString
	 * @return List with valid ISO language codes.
	 */
	public static List<String> extractValidIsoCodes(String csvString) {
		Set<String> isoCodes = new HashSet<>();
		for(String lang : CoreUtils.parseStringList(csvString, "[,;/]+", true, true)) {
			if(isValidLanguageCode(lang)) {
				isoCodes.add(lang);
			}
		}
		return new ArrayList<>(isoCodes);
	}

	public static class IsoLangResolver {
		/**
		 * The language of display country values to resolve.
		 */
		private final Locale locale;
		private final Map<String, String> codeToLabelLookup;
		private final Map<String, String> labelToCodeLookup;
		private final Map<String, String> displayCountryToCodeLookup;

		/**
		 * Instantiate a LocaleResolver for language display country labels in a given
		 * language. E.g. passing
		 * 
		 * <pre>
		 * new Locale("en")
		 * </pre>
		 * 
		 * will resolve the english label "German" to the fitting Locale for "de".
		 * 
		 * @param locale the language of display country values to resolve.
		 */
		public IsoLangResolver(Locale locale) {
			this.locale = locale;
			this.codeToLabelLookup = loadLangs(this.locale);
			this.labelToCodeLookup = buildReverseLookupMap(this.codeToLabelLookup);
			this.displayCountryToCodeLookup = buildDisplayCountryToCodeLookupMap(this.codeToLabelLookup);
			logger.trace("Initialized IsoLangResolver for locale = {}. Nr. of known ISO codes = {}", 
					locale, codeToLabelLookup.size());
		}
		
		public Locale getLocale() {
			return this.locale;
		}

		private Map<String, String> buildReverseLookupMap(Map<String, String> codeToLabelLookup) {
			//convert all keys to lower case for easier lookup.
			Map<String, String> lookup = new HashMap<>();
			for(Entry<String, String> e : codeToLabelLookup.entrySet()) {
				String code = e.getKey();
				String label = e.getValue();
				if(lookup.get(label) != null) {
					logger.warn("Duplicate in resource file for Locale = {}. Code = {}, Label = {}", this.locale, code, label);
					logger.warn("Existing entry: Code = {}, Label = {}", code, lookup.get(label));
				}
				logger.trace("Adding lookup entry: {} -> {}", label, code);
				lookup.put(label, code);
			}
			logger.trace("Loaded ISO lang label lookup map for Locale = {}. Size = {}", this.locale, lookup.size());
			return lookup;
		}
		
		private Map<String, String> buildDisplayCountryToCodeLookupMap(Map<String, String> codeToLabelLookup) {
			//convert all keys to lower case for easier lookup.
			Map<String, String> lcLookup = new HashMap<>();
			for(Entry<String, String> e : codeToLabelLookup.entrySet()) {
				List<String> labels = CoreUtils.parseStringList(e.getValue(), ";", true, true);
				if (labels.isEmpty()) {
					logger.warn("No label found for tag: {}", e.getKey());
					continue;
				}
				for (String displayLanguage : labels) {
					String lcLabel = displayLanguage.toLowerCase();
					if(lcLookup.get(lcLabel) != null
							//duplicates are OK as long as they resolve to the same code
							&& !lcLookup.get(lcLabel).equals(e.getKey())) {
						logger.trace("Duplicate synonym in resource file for Locale = {}. Label = {}", this.locale, lcLabel);
						if(!lcLookup.get(lcLabel).equals(e.getKey())) {
							logger.warn("Conflicting codes: {} <-> {}", lcLookup.get(lcLabel), e.getKey());
						}
					}
					logger.trace("Adding lookup entry: {} -> {}", lcLabel, e.getKey());
					lcLookup.put(lcLabel, e.getKey());
				}
			}
			logger.trace("Loaded ISO lang synonym lookup map for Locale = {}. Size = {}", this.locale, lcLookup.size());
			return lcLookup;
		}

		
		public String resolveLabelFromCode(String code) {
			final String normalizedCode = StringUtils.lowerCase(StringUtils.trim(code));
			return this.codeToLabelLookup.get(normalizedCode);
		}
		
		public String resolveCodeFromLabel(String label) {
			return this.labelToCodeLookup.get(label);
		}

		public String resolveCodeFromDisplayLanguage(String displayCountry) {
			if (StringUtils.isEmpty(displayCountry)) {
				return null;
			}
			final String normalizedLabel = normalizeDisplayLanguage(displayCountry).toLowerCase();
			logger.trace("Looking up iso code for label: {}", normalizedLabel);
			return displayCountryToCodeLookup.get(normalizedLabel);
		}
		
		/**
		 * Get all known names for a language in this resolver's Locale language
		 * @param code alpha-3 code
		 * @return a list with known names
		 */
		public List<String> resolveDisplayLanguagesFromCode(String code) {
			String label = resolveLabelFromCode(code);
			return CoreUtils.parseStringList(label, ";", true, true);
		}

		public Set<String> getAvailableLangs() {
			// don't expose internal field as it must not be changed
			Set<String> langs = new HashSet<>();
			langs.addAll(this.codeToLabelLookup.keySet());
			return langs;
		}
		
		public Map<String, String> getCodeToLabelMap() {
			Map<String, String> codeToLabelLookupClone = new HashMap<>();
			for (String key : this.codeToLabelLookup.keySet()) {
				codeToLabelLookupClone.put(key, this.codeToLabelLookup.get(key));
			}
			return codeToLabelLookupClone;
		}

		private Map<String, String> loadLangs(Locale locale) {
			Map<String, String> lookup = null;
			try {
				lookup = loadLangsFromFile(locale);
			} catch (IOException e) {
				throw new IllegalStateException("Could init Local lookup map: " + e.getMessage(), e);
			}
			return lookup;
		}
		
		public Map<String, String> loadLangsFromFile(Locale locale) throws IOException {
			URL resource = IsoLangUtils.class.getClassLoader().getResource(RESOURCE_FILE_NAME);
			if (resource == null) {
				// the list of available locales contains only 160 entries (Java 8) and misses
				// some included in the Transkribus Lite JSON file.
				throw new FileNotFoundException("Could not find language code list on classpath!");
			}
			Object[] data;
			try {
				data = (Object[]) new ObjectMapper().readValue(resource, Object[].class);
			} catch (IOException e) {
				throw new IOException("Could not load language code list on classpath!", e);
			}
			Map<String, String> langs = new HashMap<>();
			for (Object o : data) {
				@SuppressWarnings("unchecked")
				Map<String, String> entry = ((Map<String, String>) o);
				String code = entry.get("ISO 639-2 Code");
				String label = entry.get(locale.getDisplayLanguage(DEFAULT_LOCALE));
				if (StringUtils.isEmpty(label)) {
					//The resource file does not include a label for each code in each language
					logger.trace("No label in Locale {} found for code: {}", this.locale, code);
					//fallback to english label
					label = IsoLangUtils.DEFAULT_RESOLVER.resolveLabelFromCode(code);
					logger.trace("Fallback to {} label: {}", DEFAULT_LOCALE, label);
					continue;
				}
				if(langs.get(code) != null) {
					logger.warn("Found duplicate in resource file for Locale = {}. Code = {}", locale, code);
				}
				langs.put(code, label);
			}
			logger.trace("Got {} language entries for Locale = {}", langs.size(), locale);
			return langs;
		}
		
		/**
		 * Parse the language field from TrpHtr: split csv values, normalize Strings (whitespace, casing), try to resolve ISO language codes from the values.
		 * @param csvString
		 * @return ResolvedLocales object containing the original csvString, a normalized CSV string, and a list of resolved ISO language codes.
		 */
		public ResolvedIsoLangs parseLangCsvString(String csvString) {
			List<String> langList = CoreUtils.parseStringList(csvString, "[,;/]+", true, true);
			Set<String> isoCodes = new HashSet<>();
			for(int i = 0; i < langList.size(); i++) {
				String lang = langList.get(i);
				//try to resolve a valid locale
				String code = resolveCodeFromDisplayLanguage(lang);
				if(code != null) {
					//add to locale list
					isoCodes.add(code);
				}
			}
			return new ResolvedIsoLangs(csvString, langList, new ArrayList<>(isoCodes));
		}
		
		/**
		 * Takes a comma separated list with iso codes or custom languages and outputs
		 *  a comma separated string with resolved ISO language labels where possible.
		 */
		public String getLanguageWithResolvedIsoCodes(String languageStringWithIsoCodes) {
			List<String> langs = new ArrayList<>();
			for (String iso : CoreUtils.parseStringList(languageStringWithIsoCodes, ",", true, true)) {
				String lang = IsoLangUtils.DEFAULT_RESOLVER.resolveLabelFromCode(iso);
				langs.add(lang==null ? iso : lang);
			}
			return CoreUtils.join(langs, "; ", "", "");
		}
	}

	public static class ResolvedIsoLangs {
		private final String input;
		private final List<String> normalizedLangs;
		private final List<String> isoCodes;
		private ResolvedIsoLangs(String input, List<String> normalizedLangs, List<String> isoCodes) {
			this.input = input;
			this.normalizedLangs = normalizedLangs;
			this.isoCodes = isoCodes;
		}
		public String getInput() {
			return input;
		}
		public List<String> getNormalizedLangs() {
			return normalizedLangs;
		}
		public String getNormalizedLangsCsv() {
			return normalizedLangs.stream().collect(Collectors.joining(", "));
		}
		public List<String> getIsoCodes() {
			return isoCodes;
		}
	}
}
