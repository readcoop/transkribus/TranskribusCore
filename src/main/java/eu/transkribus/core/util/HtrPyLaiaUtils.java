package eu.transkribus.core.util;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;

import javax.persistence.EntityNotFoundException;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.transkribus.core.model.beans.TrpHtr;
import eu.transkribus.core.model.beans.rest.ParameterMap;

public class HtrPyLaiaUtils {
	private static final Logger logger = LoggerFactory.getLogger(HtrPyLaiaUtils.class);
	
//	public final static String PROVIDER_PYLAIA = "PyLaia";
	public final static String WORKDIR_NAME = ModelUtil.PROVIDER_PYLAIA+"Workdir";
	public static final String CREATE_MODEL_PARS_FILENAME = "createModelPars";
	public static final String TRAIN_CTC_PARS_FILENAME = "trainCtcPars";
	public static final String CREATE_MODEL_PARS_NEXTGEN_FILENAME = "model_conf.yaml";
	public static final String TRAIN_CTC_PARS_NEXTGEN_FILENAME = "train_conf.yaml";

	public static final String SYMBOLS_FILENAME = "symbols.txt";
	public static final String LM_SUBDIR = "lm";
	public static final String HYPOTHESES_FILENAME_PREFIX = "hypotheses";
	public static final String HYPOTHESES_FILENAME = HYPOTHESES_FILENAME_PREFIX+".out";
	public static final String LM_CREATED_SUCCESS_FN = "created.success";
	public static final String MODEL_DIR_NAME = "model";
	public static final String DECODE_LINES_LST = "DecodeLines.lst";
	
	public static final String PYLAIA_PROVIDER_STRING="READ-COOP";
	public static final String PYLAIA_NAME_STRING = "PyLaia@TranskribusPlatform";
	public static final String PYLAIA_LM_PROVIDED_STRING = "provided";
	public static final String PYLAIA_LM_NONE_STRING = "none";
	
	public static final String CHECKPOINT_PREFIX_BEST_CER = "experiment.ckpt.lowest-valid-cer-";
	public static final String CHECKPOINT_PREFIX_BEST_WER = "experiment.ckpt.lowest-valid-wer-";
	public static final String CHECKPOINT_PREFIX_LATEST = "experiment.ckpt-";
	
	public static final String BASE_MODEL_ID_KEY = "baseModelId";
	public static final String BASE_MODEL_NAME_KEY = "baseModelName";
	
	public static final String CONDA_ENV_PROD = "pylaia_upvlc";
	public static final String CONDA_ENV_TEST = "pylaia_upvlc_test";
	
	public static final String[] CHECKPOINT_PREFIXES = { CHECKPOINT_PREFIX_LATEST, CHECKPOINT_PREFIX_BEST_CER, CHECKPOINT_PREFIX_BEST_WER, "model.ckpt-" };
	public static final String[] NEXTGEN_CHECKPOINT_PREFIXES = { "-last", "-lowest_va_cer", "-lowest_va_wer", "-lowest_va_loss" };
	
	public static void removeOldCheckpoints(File modelDir, int nKeep) {
		logger.debug("removing old checkpoints from "+modelDir+", nKeep = "+nKeep);
		Comparator<File> ckptComparator = new Comparator<File>() {
				@Override
				public int compare(File o1, File o2) {
					int e1 = getEpochFromCheckpointFile2(o1);
					int e2 = getEpochFromCheckpointFile2(o2);
					return Integer.compare(e1, e2);
				}
		};

		boolean isNextGen = isNextGenModelDir(modelDir);
		String[] prefixes = isNextGen ? NEXTGEN_CHECKPOINT_PREFIXES : CHECKPOINT_PREFIXES;
		int c=0;
		for (String prefix : prefixes) {
			List<File> ckpts = listCheckpoints(modelDir, prefix, isNextGen);
			ckpts.sort(ckptComparator);
			if (ckpts.size() > nKeep) {
				for (int i=0; i<ckpts.size()-nKeep; ++i) {
					File f = ckpts.get(i);
					logger.debug("deleting old checkpoint file: "+f.getName());
					f.delete();
					++c;
				}
			}
		}
		if (isNextGen) { // if next-gen, remove all legacy checkpoints also - happens when training from legacy base model
			for (String prefix : CHECKPOINT_PREFIXES) {
				List<File> ckpts = listCheckpoints(modelDir, prefix, false);
				for (File f : ckpts) {
					f.delete();
					++c;
				}
			}
		}
		logger.debug("deleted "+c+" old checkpoints!");
	}
	
	public static String getCheckpointParameter(File modelDir, String preferred, boolean forTraining) throws IOException {
		boolean isNextGen = isNextGenModelDir(modelDir);
		String p = getCheckpointPrefix(modelDir, preferred, isNextGen);
		if (isNextGen) {
			String pattern = "epoch=*"+p+".ckpt"; // FIXME does this work for training?
			// logger.info("nextgen checkpoint pattern: "+pattern);
			return pattern;
		}
		else {
			return forTraining ? StringUtils.removeStart(p, "experiment.")+"*" : p+"*";
		}
	}
	
	public static int getEpochFromCheckpoint(File modelDir, String preferredPrefix) throws IOException {
		boolean isNextGen = isNextGenModelDir(modelDir);
		String p = getCheckpointPrefix(modelDir, preferredPrefix, isNextGen);
		List<File> ckpts = listCheckpoints(modelDir, p, isNextGen);
		Collections.sort(ckpts);
		File f = ckpts.get(ckpts.size()-1);
		return getEpochFromCheckpointFile(f);
	}
	
	private static int getEpochFromCheckpointFile2(File f) {
		try {
			return getEpochFromCheckpointFile(f);
		} catch (IOException e) {
			return -1;
		}
	}
	
	private static int getEpochFromCheckpointFile(File f) throws IOException {
		String epochStr;
		if (isNextGenCheckpoint(f.getName())) {
			epochStr = f.getName().substring("epoch=".length(), f.getName().indexOf("-"));
		}
		else {
			epochStr = f.getName().substring(f.getName().lastIndexOf("-")+1);
		}
		try {
			return Integer.parseInt(epochStr);
		} catch (NumberFormatException e) {
			throw new IOException("Could not Parse epochStr: "+epochStr, e);
		}		
	}
	
	private static String getCheckpointPrefix(File modelDir, String preferredPrefix, boolean isNextGen) throws IOException {
		List<String> possible = listPossibleCheckpointPrefixes(modelDir, isNextGen);
		if (possible.isEmpty()) {
			throw new IOException("No checkpoint found in model directory: "+modelDir);
		}
		
		String p = possible.get(0);
		if (preferredPrefix != null && possible.contains(preferredPrefix)) {
			p = preferredPrefix;
		}
		
		return p;
	}	
	
	private static List<String> listPossibleCheckpointPrefixes(File modelDir, boolean isNextGen) {
		String[] prefixes = isNextGen ? NEXTGEN_CHECKPOINT_PREFIXES : CHECKPOINT_PREFIXES;
		List<String> possible = new ArrayList<>();
		for (String p : prefixes) {
			List<File> ckpts = listCheckpoints(modelDir, p, isNextGen);
			if (!ckpts.isEmpty()) {
				possible.add(p);
			}
		}
		return possible;
	}
	
	private static List<File> listCheckpoints(File modelDir, String prefix, boolean isNextGen) {
    	File[] ckpts = modelDir.listFiles(new FilenameFilter() {
			@Override public boolean accept(File dir, String name) {
				if (isNextGen) {
					return isNextGenCheckpoint(name) && (prefix == null || name.contains(prefix));
				}
				else {
					return name.startsWith(prefix);
				}
			}
		});
    	return Arrays.asList(ckpts);
	}

	private static boolean isNextGenCheckpoint(String filename) {
		filename = FilenameUtils.getName(filename).toLowerCase();
		return filename.startsWith("epoch=") && filename.endsWith(".ckpt");
	}

	private static boolean isNextGenModelDir(File modelDir) {
		return listCheckpoints(modelDir, null, true).size() > 0;
	}
	///// end of checkpoint releated methods
	
	public static String getCreatorString(String version, String modelId, boolean usedInternalLM) {
		String str = "prov="+PYLAIA_PROVIDER_STRING;
		str += ":name="+PYLAIA_NAME_STRING;
		if (!StringUtils.isEmpty(version)) {
			str += ":version="+version;
		}
		if (!StringUtils.isEmpty(modelId)) {
			str += ":model_id="+modelId;
		}
		if (usedInternalLM) {
			str += ":lm="+PYLAIA_LM_PROVIDED_STRING;
		} else {
			str += ":lm="+PYLAIA_LM_NONE_STRING;
		}
		if (true) {
			str += ":date="+CoreUtils.newDateFormat().format(new Date());
		}

		return str;
	}
	
	public static List<Double> getCerSeries(File cerTrain, boolean toFraction) throws IOException {
		List<String> lines = Files.readAllLines(Paths.get(cerTrain.getAbsolutePath()), Charset.defaultCharset());
		List<Double> cerSeries = new ArrayList<>();
		for (String l : lines) {
			try {
				Double v = Double.parseDouble(l);
				if (toFraction) {
					v /= 100.0d;
				}
				cerSeries.add(v);
			} catch (Exception e) {
				logger.error("Could not parse CER string: " + l + " - skipping!");
			}
		}
		
		return cerSeries;
	}
	
	public static String getCerSeriesString(File cerTrain, boolean toFraction) throws IOException {
		List<Double> cerSeries = getCerSeries(cerTrain, toFraction);
		return StringUtils.join(cerSeries, '\n');
	}
	
	public static File getSymbolsFile(TrpHtr htr) throws EntityNotFoundException, InvalidParameterException {
		if (htr == null) {
			throw new InvalidParameterException("htr cannot be null");
		}
		
		File symbolsFile = new File(htr.getPath()+"/"+HtrPyLaiaUtils.SYMBOLS_FILENAME);
		if (!symbolsFile.isFile()) {
			throw new EntityNotFoundException("Symbols file not found for model "+htr.getHtrId()+"!");
		}
		return symbolsFile;
	}
	
	public static SortedSet<String> getCharsetFromSymbolsFile(File symbolsFile) throws IOException {
		List<String> lines = Files.readAllLines(Paths.get(symbolsFile.getAbsolutePath()), Charset.forName("UTF-8"));
		SortedSet<String> charset = new TreeSet<String>();
		for (String l : lines) {
			charset.add(l.split("\\s+")[0]);
		}
		return charset;
	}

	public static String getCharsetFromSymbolsFileAsString(File symbolsFile) throws IOException {
		SortedSet<String> s = getCharsetFromSymbolsFile(symbolsFile);
		return CoreUtils.join(s, " ", null, null);
	}
	
	public static boolean hasEqualSymbols(File symbolsFile, TrpHtr baseHtr) throws EntityNotFoundException, InvalidParameterException, IOException {
		if (symbolsFile == null) { 
			throw new InvalidParameterException("symbolsFile cannot be null");
		}
		
		File baseSymbolsFile = getSymbolsFile(baseHtr);
		SortedSet<String> baseSymbols = getCharsetFromSymbolsFile(baseSymbolsFile);
		SortedSet<String> symbols = getCharsetFromSymbolsFile(symbolsFile);
		
		logger.info("Checking equality of symbols: N = "+baseSymbols.size()+"/"+symbols.size());
		
		return baseSymbols.equals(symbols);
	}
	
	public static String getDefaultCondaEnv(boolean isProdDb) {
		return  isProdDb ? HtrPyLaiaUtils.CONDA_ENV_PROD : HtrPyLaiaUtils.CONDA_ENV_TEST;
	}

	public static String getPyLaiaNextGenConf(String baseConf, ParameterMap map, List<String> keysToIgnore, Map<String, String> keyReplaceMap) {
		for (String key : map.getParamMap().keySet()) {
			String value = map.getParamMap().get(key);
			logger.debug("key = "+key+" value = "+value);

			key = key.replaceFirst("--", "");
			if (key.startsWith("cnn_") || key.startsWith("rnn_")) {
				if (value.contains(" ")) {
					value = "["+value.replaceAll(" ", ",")+"]";
				}
			}
			if (keysToIgnore.contains(key)) {
				continue;
			}
			if (keyReplaceMap.containsKey(key)) {
				key = keyReplaceMap.get(key);
			}
			if (value.equals("True") || value.equals("False")) {
				value = value.toLowerCase();
			}
			logger.debug("key: "+key+" value: "+value);

			String regex = "("+key+":\\s+)(.*)";
			String replacement = "$1"+value;
			baseConf=baseConf.replaceFirst(regex, replacement);
		}

		return baseConf;
	}

	public static boolean isNextGenEnv(String condaEnv) {
		return StringUtils.contains(condaEnv, "nextgen");
	}
	
	public static void main(String[] args) throws Exception {
//		SortedSet<String> baseSymbols = getCharsetFromSymbolsFile(new File("T:/HTR/DEA/PyLaia/26087", HtrPyLaiaUtils.SYMBOLS_FILENAME));
//		SortedSet<String> symbols1 = getCharsetFromSymbolsFile(new File("T:/HTR/DEA/PyLaia/26087", HtrPyLaiaUtils.SYMBOLS_FILENAME));
//		SortedSet<String> symbols2 = getCharsetFromSymbolsFile(new File("T:/HTR/DEA/PyLaia/25766", HtrPyLaiaUtils.SYMBOLS_FILENAME));
//		
//		System.out.println(baseSymbols.size() + "/" + symbols1.size() + "/"+ + symbols2.size());
//		
//		System.out.println(baseSymbols.equals(symbols1));
//		System.out.println(baseSymbols.equals(symbols2));
	}

}
