package eu.transkribus.core.util;

import eu.transkribus.core.model.beans.TrpTranscriptMetadata;

public class BaselineTrainDataSelector extends PageTranscriptSelector {
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(BaselineTrainDataSelector.class);
	
	@Override
	protected String getTrainableItemCount(TrpTranscriptMetadata tmd) {
		return tmd.getNrOfLines() + " lines";
	}
	
	@Override
	public boolean isQualifiedForTraining(TrpTranscriptMetadata tmd) {
		if(tmd == null) {
			//null should not be passed
			logger.warn("Transcript object is null!");
			return false;
		}
		return tmd.getNrOfLines() > 0;
	}
}
