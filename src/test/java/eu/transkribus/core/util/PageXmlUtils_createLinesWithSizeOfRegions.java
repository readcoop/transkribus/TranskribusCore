package eu.transkribus.core.util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import javax.xml.bind.JAXBException;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.transkribus.core.model.beans.TrpTranscriptMetadata;
import eu.transkribus.core.model.beans.pagecontent.PcGtsType;
import eu.transkribus.core.model.beans.pagecontent_trp.TrpPageType;
import eu.transkribus.core.model.beans.pagecontent_trp.TrpRegionType;
import eu.transkribus.core.model.beans.pagecontent_trp.TrpTextLineType;
import eu.transkribus.core.model.beans.pagecontent_trp.TrpTextRegionType;

public class PageXmlUtils_createLinesWithSizeOfRegions {
	private static final Logger logger = LoggerFactory.getLogger(PageXmlUtils_createLinesWithSizeOfRegions.class);
	
	/*
	 * test has not worked
	 * problem are the samll lines in the same column!! so the ro jumps around
	 * so it is more predictable to jump from right to left from top to bottom
	 */
	public static void test() {
		final File startDir = new File("T:\\leo4_data\\proj-parlamentsschriftgut-aumann\\unzips\\Test_readingOrder\\page_start");
		
		for (File file : startDir.listFiles()) {
			try {
				PcGtsType pc = PageXmlUtils.unmarshal(file);
				String fulltext = PageXmlUtils.getFulltextFromLines(pc);
				File textDir = new File(startDir.getParentFile().getAbsolutePath() + "\\text");
				File pageDir = new File(startDir.getParentFile().getAbsolutePath() + "\\page");
				FileUtils.forceMkdir(textDir);
				FileUtils.forceMkdir(pageDir);
				FileUtils.writeStringToFile(new File(textDir.getAbsolutePath()+"\\"+FilenameUtils.getBaseName(file.getName())+".txt"), fulltext);
				File pageFile = new File(pageDir.getAbsolutePath()+"\\"+file.getName());
				PageXmlUtils.marshalToFile(pc, pageFile);
			} catch (JAXBException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}
	
	public static void main(String[] args) throws Exception {
		
		createLinesWithSizeOfRegions();

		//createRegionsAndLinesWithSizeOfPage();
		//readJson();
	}
	
	
	private static void readJson() throws FileNotFoundException, IOException, ParseException {
		
		final File startDir = new File("R:\\projects\\Testprojekte\\json2PageXML\\Filae_test_Images\\page_start");
		
		try {
			
			for (File pageFile : startDir.listFiles()) {
			
				//final File pageFile = new File("R:\\projects\\Testprojekte\\json2PageXML\\Filae_test_Images\\page_start\\"+file.getName());
				
				File txtDir = new File(pageFile.getParentFile().getParentFile().getAbsolutePath() + "\\txt");
				final File txtFile = new File(txtDir.getAbsolutePath()+"\\"+FilenameUtils.getBaseName(pageFile.getName())+".txt");

				PcGtsType pc = PageXmlUtils.unmarshal(pageFile);
				TrpPageType page = (TrpPageType) pc.getPage();
				
				int h = page.getImageHeight();
				int w = page.getImageWidth();
			
		       // parsing file "JSONExample.json"
		        Object obj = new JSONParser().parse(new FileReader(new File("R:\\projects\\Testprojekte\\json2PageXML\\Filae_training_data.json")));
		                
		        // typecasting obj to JSONObject
		        JSONObject jo = (JSONObject) obj;
		        
		        String imageNameString = FilenameUtils.getBaseName(pageFile.getName())+".jpg";
		          
		        JSONArray ja = (JSONArray) jo.get(imageNameString);
		
		        Iterator itr2 = ja.iterator();
		          
		        while (itr2.hasNext()) 
		        {
		        	String coords;
		        	String id;
		        	String transcript;
		        	JSONObject region = (JSONObject) itr2.next();
		        	
		        	transcript = (String) region.get("truth_transcription");
		        	JSONArray coordsArray = (JSONArray) region.get("line_polygon");
		        	id = (String) region.get("line_number");
		        	System.out.println("transcript : " + transcript);
		        	System.out.println("coords : " + coordsArray);
		        	System.out.println("id : " + id);
		        	
		        	coords = coordsArray.toJSONString();
		        	coords = coords.replaceAll("],", " ");
		        	coords = coords.replaceAll("[\\[\\]]", "");
		        	
		        	System.out.println("coords2 : " + coords);
		        	
		        	//now we can create region and line with the fetched information
					TrpTextRegionType r = new TrpTextRegionType((TrpPageType) page);
					r.setId("r_"+id);
					logger.debug("new coords " + coords);
					r.setCoordinates(coords, null);
					r.setUnicodeText(transcript, null);
					page.getTextRegionOrImageRegionOrLineDrawingRegion().add(r);
					
					writeToTextFile(txtFile.getAbsolutePath(), transcript);
					
					/*
					 * if lines and baselines should also be created
					 */
					
//		        	TrpTextLineType tl = new TrpTextLineType();
//	
//			    	tl.setId("r_"+id+"_l_"+id); // create new ID based on ID of original line
//			    	tl.setCoordinates(coords, null);
//			    	tl.setUnicodeText(transcript, null);
//			    	tl.setParent(r);
//							    		
//			    	
//					TrpBaselineType newBl = new TrpBaselineType(tl);
//					newBl.setPoints((int)tl.getBoundingBox().getMinX()+","+((int)tl.getBoundingBox().getMaxY()-30)+" "+(int)tl.getBoundingBox().getMaxX()+","+((int)tl.getBoundingBox().getMaxY()-30));
//					newBl.setReadingOrder(-1, null);
//					tl.setBaseline(newBl);
//					
//					r.getTextLine().add(0, tl);
				}
				pc.setPage(page);		
				
				File pageDir = new File(pageFile.getParentFile().getParentFile().getAbsolutePath() + "\\page");
				FileUtils.forceMkdir(pageDir);
				File resultPageFile = new File(pageDir.getAbsolutePath()+"\\"+pageFile.getName());
				PageXmlUtils.marshalToFile(pc, resultPageFile);
			}
	
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
            
	}
	
	/*
	 * get the TrpDoc from the platform and update the transcripts
	 * return the updated doc
	 */
	public PcGtsType createLinesWithSizeOfRegions(TrpTranscriptMetadata currentTr) {
			
			try {
				
				PcGtsType pc = currentTr.unmarshallTranscript();
				TrpPageType page = (TrpPageType) pc.getPage();

				List<TrpTextRegionType> regions = page.getTextRegions(true);
				
				if (regions.isEmpty()) {
					return null;
				}
				
				List<TrpRegionType> allRegions = page.getTextRegionOrImageRegionOrLineDrawingRegion();

				for (int i=0; i<allRegions.size(); ++i) {
					
					logger.debug("regions not empty........");
					TrpTextRegionType tr = (TrpTextRegionType) allRegions.get(i);
					String id = tr.getId();
					String coordsStr = tr.getCoordinates();
					
					//remove old lines and baselines 
					//tr.getChildren(true).clear();
					tr.getTextLine().clear();

		        	TrpTextLineType tl = new TrpTextLineType();

		        	String formattedNum = String.format("%03d", i);
			    	tl.setId(id+"_line_"+formattedNum); // create new ID based on ID of original line
			    	tl.setCoordinates(coordsStr, null);
			    	tl.setUnicodeText("", null);
			    	tl.setParent(tr);
					tr.getTextLine().add(0, tl);		    		
			    	
//					TrpBaselineType newBl = new TrpBaselineType(tl);
//					newBl.setPoints(tl.getBoundingBox().getMinX()+","+(tl.getBoundingBox().getMaxY()-10)+" "+tl.getBoundingBox().getMaxX()+","+(tl.getBoundingBox().getMaxY()-10));
//					newBl.setReadingOrder(-1, null);
//					tl.setBaseline(newBl);
				}
				pc.setPage(page);
				return pc;
				
			} catch (JAXBException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
		
		
	}
	
	/*
	 * use case: for structural region we often need lines with the size of the region
	 * create TrpLines with the size of the region
	 */
	private static void createLinesWithSizeOfRegions() {
		//
		final File startDir = new File("C:\\Users\\Schorsch\\Desktop\\Transkribus_tests\\Teilantiphonar_St__Florian_(OSA)_-_A-SF_XI_409\\Teilantiphonar_St__Florian_(OSA)_-_A-SF_XI_409\\page_start");
		
		for (File file : startDir.listFiles()) {
			try {
				PcGtsType pc = PageXmlUtils.unmarshal(file);
				TrpPageType page = (TrpPageType) pc.getPage();

				List<TrpTextRegionType> regions = page.getTextRegions(true);
				
				if (regions.isEmpty()) {
					continue;
				}
				
				List<TrpRegionType> allRegions = page.getTextRegionOrImageRegionOrLineDrawingRegion();

				for (int i=0; i<allRegions.size(); ++i) {
					
					logger.debug("regions not empty........");
					TrpTextRegionType tr = (TrpTextRegionType) allRegions.get(i);
					String id = tr.getId();
					String coordsStr = tr.getCoordinates();
					
					//remove old lines and baselines 
					//tr.getChildren(true).clear();
					tr.getTextLine().clear();

		        	TrpTextLineType tl = new TrpTextLineType();

		        	String formattedNum = String.format("%03d", i);
			    	tl.setId(id+"_line_"+formattedNum); // create new ID based on ID of original line
			    	tl.setCoordinates(coordsStr, null);
			    	tl.setUnicodeText("", null);
			    	tl.setParent(tr);
					tr.getTextLine().add(0, tl);		    		
			    	
//					TrpBaselineType newBl = new TrpBaselineType(tl);
//					newBl.setPoints(tl.getBoundingBox().getMinX()+","+(tl.getBoundingBox().getMaxY()-10)+" "+tl.getBoundingBox().getMaxX()+","+(tl.getBoundingBox().getMaxY()-10));
//					newBl.setReadingOrder(-1, null);
//					tl.setBaseline(newBl);
				}
				pc.setPage(page);		
				
				File pageDir = new File(startDir.getParentFile().getAbsolutePath() + "\\page");
				FileUtils.forceMkdir(pageDir);
				File pageFile = new File(pageDir.getAbsolutePath()+"\\"+file.getName());
				PageXmlUtils.marshalToFile(pc, pageFile);
			} catch (JAXBException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	/*
	 * use case: for line snippets - one image is one region is one line
	 * create TrpRegions and TrpLines with the size of the image
	 */
	private static void createRegionsAndLinesWithSizeOfPage() {
		//
		final File startDir = new File("C:\\Users\\Schorsch\\Desktop\\Transkribus_tests\\Graduale_aus_dem_Stift_Seckau_-_A-Gu_17\\Graduale_aus_dem_Stift_Seckau_-_A-Gu_17\\page_start");
		
		for (File file : startDir.listFiles()) {
			try {
				PcGtsType pc = PageXmlUtils.unmarshal(file);
				TrpPageType page = (TrpPageType) pc.getPage();
				
				int h = page.getImageHeight();
				int w = page.getImageWidth();

				List<TrpTextRegionType> regions = page.getTextRegions(true);
				
				if (regions.isEmpty()) {
					logger.debug("regions empty........");
					TrpTextRegionType r = new TrpTextRegionType((TrpPageType) page);
					r.setId("r_1");
					String coords = "0,0 " + "0," + h + " " + w + "," + h + " " + w + "," + "0";
					logger.debug("new coords " + coords);
					r.setCoordinates(coords, null);
					page.getTextRegionOrImageRegionOrLineDrawingRegion().add(r);
				}
				
				List<TrpRegionType> allRegions = page.getTextRegionOrImageRegionOrLineDrawingRegion();

				for (int i=0; i<allRegions.size(); ++i) {
					
					logger.debug("regions not empty........");
					TrpTextRegionType tr = (TrpTextRegionType) allRegions.get(i);
					String id = tr.getId();
					String coordsStr = tr.getCoordinates();

		        	TrpTextLineType tl = new TrpTextLineType();

			    	tl.setId(id+"_l"+(i)); // create new ID based on ID of original line
			    	tl.setCoordinates(coordsStr, null);
			    	tl.setUnicodeText("", null);
			    	tl.setParent(tr);
					tr.getTextLine().add(0, tl);		    		
			    	
//					TrpBaselineType newBl = new TrpBaselineType(tl);
//					newBl.setPoints(tl.getBoundingBox().getMinX()+","+(tl.getBoundingBox().getMaxY()-10)+" "+tl.getBoundingBox().getMaxX()+","+(tl.getBoundingBox().getMaxY()-10));
//					newBl.setReadingOrder(-1, null);
//					tl.setBaseline(newBl);
				}
				pc.setPage(page);		
				
				File pageDir = new File(startDir.getParentFile().getAbsolutePath() + "\\page");
				FileUtils.forceMkdir(pageDir);
				File pageFile = new File(pageDir.getAbsolutePath()+"\\"+file.getName());
				PageXmlUtils.marshalToFile(pc, pageFile);
			} catch (JAXBException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public static void writeToTextFile(String txtName, String text){

	 	   try {
				//File file = new File(startPath + "errors.txt" );
				   BufferedWriter buff = new BufferedWriter(new FileWriter(txtName, true));
				   System.out.println(text);
				   buff.write(text);
				   buff.newLine();
				   buff.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
}
