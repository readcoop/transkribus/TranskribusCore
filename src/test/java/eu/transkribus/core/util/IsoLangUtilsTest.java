package eu.transkribus.core.util;

import java.util.List;
import java.util.Locale;

import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.transkribus.core.util.IsoLangUtils.IsoLangResolver;

public class IsoLangUtilsTest {
	private static final Logger logger = LoggerFactory.getLogger(IsoLangUtilsTest.class);
	
	@Test
	public void testResolveLocaleFromDisplayCountry() {
		String[] positives = { "English", "english", "German" };
		for(String t : positives) {
			String lang = IsoLangUtils.resolveCodeFromDisplayLanguage(t);
			logger.debug("Resolve {} -> {}", t, lang);
			Assert.assertNotNull(lang);
		}
	}
	
	@Test
	public void testResolveLocaleFromUnknownDisplayCountry() {
		String[] negatives = { "Englisch", "englisch", "Weißwurscht", "Deutsch", "deutsch"};
		for(String t : negatives) {
			String lang = IsoLangUtils.resolveCodeFromDisplayLanguage(t);
			logger.debug("Resolve {} -> {}", t, lang);
			Assert.assertNull(lang);
		}
	}
	
	@Test
	public void testResolveLocale() {
		Assert.assertEquals("eng", IsoLangUtils.resolveCodeFromDisplayLanguage("english"));
		Assert.assertEquals("deu", IsoLangUtils.resolveCodeFromDisplayLanguage(" germAn"));
		Assert.assertEquals("zul", IsoLangUtils.resolveCodeFromDisplayLanguage("zulu"));
		//three letter codes are currently not possible. Is this even possible with Locale?
		Assert.assertEquals("chu", IsoLangUtils.resolveCodeFromDisplayLanguage("Church Slavic"));
		Assert.assertNull(IsoLangUtils.resolveCodeFromDisplayLanguage(" germAny"));
	}
	
	@Test
	public void testSynonyms() {
		final String churchSlavicTag = "chu";
		
		//this resolves to "Church Slavic" via Apache LocalUtils => Apache LangUtils seems to know this Locale somehow
		String slav = IsoLangUtils.resolveCodeFromDisplayLanguage(churchSlavicTag);
		logger.debug("" + slav);
		
//		for(Locale l : Locale.getAvailableLocales()) {
//			if(displayLang.equals(l)) {
//				logger.warn("Found display language in available langs: {}", l);
//			}
//			if(churchSlavicTag.equals(l.getCountry())) {
//				logger.warn("Found country in available langs: {}", l);
//			}
//		}
		for (String synonym : new String[] { "Church Slavic", "Old Slavonic", "Church Slavonic", "Old Bulgarian", "Old Church Slavonic"}) {
			Assert.assertEquals("chu", 
					IsoLangUtils.resolveCodeFromDisplayLanguage(synonym));
		}
	}
	
	@Test
	public void testNormalizeDisplayCountry() {
		Assert.assertEquals(null, IsoLangUtils.normalizeDisplayLanguage(null));
		Assert.assertEquals("", IsoLangUtils.normalizeDisplayLanguage(""));
		Assert.assertEquals("", IsoLangUtils.normalizeDisplayLanguage("   "));
		Assert.assertEquals("English", IsoLangUtils.normalizeDisplayLanguage("English"));
		Assert.assertEquals("English", IsoLangUtils.normalizeDisplayLanguage("english"));
		Assert.assertEquals("Old Church Slavonic", IsoLangUtils.normalizeDisplayLanguage("Old Church Slavonic"));
		Assert.assertEquals("Old Church Slavonic", IsoLangUtils.normalizeDisplayLanguage(" old  chUrch           slavoniC  "));
	}
	
	@Test
	public void testGermanResolver() {
		IsoLangResolver resolver = new IsoLangUtils.IsoLangResolver(Locale.GERMAN);
		IsoLangResolver enResolver = new IsoLangUtils.IsoLangResolver(Locale.ENGLISH);
		
		for(String l : enResolver.getAvailableLangs()) {
			if(!resolver.getAvailableLangs().contains(l)) {
				logger.warn("Not contained in de resolver: {}", l);
			}
		}
		
		Assert.assertEquals("eng", resolver.resolveCodeFromDisplayLanguage("Englisch"));
		Assert.assertEquals("deu", resolver.resolveCodeFromDisplayLanguage(" Deutsch"));
	}
	
	@Test
	public void testMultiLangResolving() {
		String[] codes = { "chu" };
		for(Locale locale : IsoLangUtils.SUPPORTED_LOCALES) {
			IsoLangResolver resolver = new IsoLangUtils.IsoLangResolver(locale);
			for(String code : codes) {
				List<String> displayLanguages = resolver.resolveDisplayLanguagesFromCode(code);
				logger.debug("Labels found for code '{}' in Locale '{}' => {}", code, locale, displayLanguages);
				Assert.assertTrue(displayLanguages != null && !displayLanguages.isEmpty());
			}			
		}
	}
	
	@Test
	public void testInitMultiLangResolving() {
		final String code = "deu";
		for(IsoLangResolver r : IsoLangUtils.RESOLVERS.values()) {
			logger.debug("{} Resolver: {} -> {}", r.getLocale(), code, r.resolveDisplayLanguagesFromCode(code));
		}
	}
}
