package eu.transkribus.core.util;

import eu.transkribus.core.model.beans.pagecontent.PcGtsType;
import eu.transkribus.core.model.beans.pagecontent.TranskribusMetadataType;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.dea.fimgstoreclient.FimgStoreGetClient;
import org.dea.fimgstoreclient.IFimgStoreGetClient;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.bind.JAXBException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactoryConfigurationException;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

public class PageXmlProcessorTest {

	private static final Logger logger = LoggerFactory.getLogger(PageXmlProcessorTest.class);
	SebisStopWatch ssw = new SebisStopWatch();

	@Rule
	public TemporaryFolder tempDir = new TemporaryFolder(new File("target"));

//	@Test
//	public void testGetAllRegionsIds() throws XPathFactoryConfigurationException, ParserConfigurationException, MalformedURLException, IllegalArgumentException, XPathExpressionException, SAXException, IOException {
//		final String key = "CMTSKWAFFAQPTGSHECQTKDCM";
//		PageXmlProcessor proc = PageXmlProcessorFactory.newInstance();
//		List<String> ids = proc.getAllTextRegionIds(key);
//		ids.stream().forEach(id -> logger.debug(id));
//	}
	
	@Test
	public void testGetAllLines() {
		final String keyGT = "CMTSKWAFFAQPTGSHECQTKDCM";
		final String keyHyp = "MXDDRBWLXBQMEVAGMGDQMJJS";
		PageXmlProcessor proc = null;
		List<String> idsGT = null;
		List<String> idsHyp = null;
		ssw.start();
		IFimgStoreGetClient getter = new FimgStoreGetClient("files.transkribus.eu", "/");
		try {
			proc = new PageXmlRemoteFileProcessor(getter);
		} catch (XPathFactoryConfigurationException | ParserConfigurationException e) {
			e.printStackTrace();
		}
		try {
			idsGT = proc.getAllLineIds(keyGT);
			idsHyp = proc.getAllLineIds(keyHyp);
		} catch (IllegalArgumentException | XPathExpressionException | SAXException | IOException e) {	
			e.printStackTrace();
		}
		ssw.stop(true);
		logger.debug("GT Id size : "+idsGT.size());
		logger.debug("Hyp Id size : "+idsHyp.size());
		
		ssw.start();
		if(idsGT.size() != idsHyp.size()) {
			logger.debug("Cannot compare document with diffrent Layout Analysis");
		}
		ssw.stop(true);
//		for(String s : idsGT) {
//			logger.debug("Line id : "+s);
//		}
		String a = idsGT.parallelStream().sorted().collect(Collectors.joining(""));
		String b = idsHyp.parallelStream().sorted().collect(Collectors.joining(""));
		a.equals(b);
		logger.debug(a);
		logger.debug(b);
		
	}
	
	@Test
	public void testGetUnicodeCharacterCountFromLines() 
			throws MalformedURLException, IllegalArgumentException, SAXException, IOException, XPathFactoryConfigurationException, ParserConfigurationException, JAXBException, XPathExpressionException {
		final String xmlKey = "CMTSKWAFFAQPTGSHECQTKDCM";
		final String lineSeparator = "";
		
		IFimgStoreGetClient getter = new FimgStoreGetClient("files.transkribus.eu", "/");
		ssw.start();
		PcGtsType pc = PageXmlUtils.unmarshal(getter.getUriBuilder().getFileUri(xmlKey).toURL());
		final String pcFulltext = PageXmlUtils.getFulltextFromLines(pc, lineSeparator);
		final int pcCount = pcFulltext.length();
		logger.info("PcGtsType text length = {}, text = {}", pcCount, StringUtils.abbreviateMiddle(pcFulltext, "...", 20));
		logger.info("Time = {} ms", ssw.stop(false));
		
		PageXmlProcessor proc = new PageXmlRemoteFileProcessor(getter);
		ssw.start();
		Document dom = proc.getDocument(xmlKey);
		final String domFulltext = proc.getUnicodeTextFromLines(dom, lineSeparator);
		final int domCount = domFulltext.length();
		logger.info("DOM text length = {}, text = {}", domCount, StringUtils.abbreviateMiddle(domFulltext, "...", 20));
		logger.info("Time = {} ms", ssw.stop(false));
		
		logger.debug("PcGts text:\n{}", pcFulltext);
		logger.debug("DOM text:\n{}", domFulltext);
		Assert.assertEquals("Text length extracted from DOM differs from pcGtsType text length", pcCount, domCount);
	}
	
	@Test
	public void testDeleteNode() throws TransformerFactoryConfigurationError, TransformerException, JAXBException, IOException, 
			XPathExpressionException, XPathFactoryConfigurationException, ParserConfigurationException, SAXException {
		URL testResource = this.getClass().getClassLoader().getResource("PageContentFilter/deterioratedRegion.xml");
		File tmp = new File(tempDir.getRoot(), this.getClass().getSimpleName() + "-" + UUID.randomUUID() + ".xml");
		logger.debug("tmpFile = {}", tmp.getAbsolutePath());
		FileUtils.copyFile(FileUtils.toFile(testResource), tmp);
		final int docId = 1234;
		final String status = "kaputt";
		
		//add metadata node
		PcGtsType pc = PageXmlUtils.unmarshal(tmp);
		TranskribusMetadataType md = new TranskribusMetadataType();
		md.setDocId(docId);
		md.setStatus(status);
		pc.getMetadata().setTranskribusMetadata(md);
		PageXmlUtils.marshalToFile(pc, tmp);
		logger.info(new String(PageXmlUtils.marshalToBytes(pc)));
		
		//check node existence in file on disk
		pc = PageXmlUtils.unmarshal(tmp);
		TranskribusMetadataType md2 = pc.getMetadata().getTranskribusMetadata();
		Assert.assertEquals(docId, md2.getDocId());
		Assert.assertEquals(status, md2.getStatus());
		
		//remove node using XML Processor
		PageXmlFileProcessor xmlProc = new PageXmlFileProcessor();
		Document xmlDoc = xmlProc.parse(tmp);
		xmlProc.deleteNodeByXPath(xmlDoc, "//TranskribusMetadata");
		xmlProc.writeToFile(xmlDoc, tmp, true);
		
		//log war content in file
		String rawContent = FileUtils.readFileToString(tmp);
		logger.info("\n\nString=\n{}", rawContent);
		//check that file has at least one two line breaks
		Assert.assertTrue(StringUtils.countMatches(rawContent, "\n") > 2);
		//check that there are no empty lines
		Assert.assertTrue(StringUtils.countMatches(rawContent, "\n\n") == 0);
		
		//check that node is gone in file
		pc = PageXmlUtils.unmarshal(tmp);
		logger.info(new String(PageXmlUtils.marshalToBytes(pc)));
		TranskribusMetadataType md3 = pc.getMetadata().getTranskribusMetadata();
		Assert.assertNull(md3);
	}
	
	
	/*
	 * https://gitlab.com/readcoop/transkribus/TranskribusCore/-/issues/56
	 */
	@Test
	public void testParseFile() throws TransformerFactoryConfigurationError, JAXBException, IOException,
			XPathFactoryConfigurationException, ParserConfigurationException, SAXException {
		File testResourceDir = tempDir.newFolder();
		//create arbitrary PAGE XML file with Umlaut in name
		final String imgBaseName = "0003_Primär";
		File pageFile = PageXmlUtils.createEmptyPAGEFile(imgBaseName + ".jpg", 1543, 1543,
				new File(testResourceDir, imgBaseName + ".xml"));
		logger.debug("Trying to parse XML file: {}", pageFile.getAbsolutePath());
		logger.debug("Content:\n{}", FileUtils.readFileToString(pageFile));
		PcGtsType pc = PageXmlUtils.unmarshal(pageFile);
		Assert.assertNotNull(pc);

		PageXmlFileProcessor xmlProc = new PageXmlFileProcessor();
		Document xmlDoc = xmlProc.parse(pageFile);
		Assert.assertNotNull(xmlDoc);
	}
	
	@Test
	public void testWriteToFile() throws IOException, TransformerFactoryConfigurationError, TransformerException, XPathFactoryConfigurationException, ParserConfigurationException, SAXException, XPathExpressionException {
		//any XML will work
		URL testResource = this.getClass().getClassLoader().getResource("PageContentFilter/deterioratedRegion.xml");
				
		PageXmlFileProcessor xmlProc = new PageXmlFileProcessor();
		Document xmlDoc = xmlProc.parse(testResource);
		xmlProc.deleteNodeByXPath(xmlDoc, "//TranskribusMetadata");
		//construct a filename with spaces, brackets and umlaut
		final File outFile = new File(tempDir.getRoot(), "test_write_to_file with spaces in name (ü).xml");
		logger.debug("Out file path = {}", outFile.getAbsolutePath());
		
		//this is how LocalDocReader constructs the URL from it.
		final URL outFileUrl = outFile.toURI().toURL();
		logger.debug("File URL = {}", outFileUrl);
		
		//In ADocImportJob the URL is used for writing the output of the file operation
		final File pageFile = FileUtils.toFile(outFileUrl);
		logger.debug("File shall be written to: {}", pageFile.getAbsolutePath());
		
		//this must point to the same file as before
		Assert.assertTrue(pageFile.getAbsolutePath().equals(outFile.getAbsolutePath()));
		
		//PageXmlProcessor instantiated StreamResult from the file and not from a FileOutputStream and this is what was written to disk.
		final File badOutFile = new File(tempDir.getRoot(), "test_write_to_file%20with%20spaces%20in%20name%20(%C3%BC).xml");

		xmlProc.writeToFile(xmlDoc, pageFile, true);
		logger.debug("Output written to: {}", pageFile.getAbsolutePath());

		Assert.assertFalse("TrpXPathProcessor wrote the file to the wrong location!", badOutFile.isFile());

		//The must be at the expected location.
		Assert.assertTrue("TrpXPathProcessor did not write the expected output file!", pageFile.isFile());
	}
}
