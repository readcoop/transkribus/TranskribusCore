package eu.transkribus.core.util;

import java.util.Locale;

import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.transkribus.core.util.LocaleUtils.LocaleResolver;
import eu.transkribus.core.util.LocaleUtils.ResolvedLocales;

public class LocaleUtilsTest {
	private static final Logger logger = LoggerFactory.getLogger(LocaleUtilsTest.class);
	
	@Test
	public void testResolveLocaleFromDisplayCountry() {
		String[] positives = { "English", "english", "German" };
		for(String t : positives) {
			Locale locale = LocaleUtils.resolveLocaleFromDisplayLanguage(t);
			logger.debug("Resolve {} -> {}", t, locale);
			Assert.assertNotNull(locale);
		}
	}
	
//	@Test
//	public void testLocale() throws IOException {
//		URL resource = LocaleUtils.class.getClassLoader().getResource("ISO-639-2-Languages.json");
//		if (resource == null) {
//			// the list of available locales contains only 160 entries (Java 8) and misses
//			// some included in the Transkribus Lite JSON file.
//			throw new FileNotFoundException("Could not find language code list on classpath!");
//		}
//		Object[] data;
//		try {
//			data = (Object[]) new ObjectMapper().readValue(resource, Object[].class);
//		} catch (IOException e) {
//			throw new IOException("Could not load language code list on classpath!", e);
//		}
//		Map<String, String> locales = new HashMap<>();
//		for (Object o : data) {
//			@SuppressWarnings("unchecked")
//			Map<String, String> entry = ((Map<String, String>) o);
//			String code = entry.get("ISO 639-2 Code");
//			Locale resolvedLocale = LocaleUtils.resolveLocaleFromDisplayLanguage(entry.get("English"));
//			logger.debug("Code from file = '{}' <-> resolved locale = '{}'", code, resolvedLocale == null ? "null" : resolvedLocale.getISO3Country());
//		}
//	}
	
	@Test
	public void testResolveLocaleFromUnknownDisplayCountry() {
		String[] negatives = { "Englisch", "englisch", "Weißwurscht", "Deutsch", "deutsch"};
		for(String t : negatives) {
			Locale locale = LocaleUtils.resolveLocaleFromDisplayLanguage(t);
			logger.debug("Resolve {} -> {}", t, locale);
			Assert.assertNull(locale);
		}
	}
	
	@Test
	public void testResolveLocale() {
		Assert.assertEquals("English", LocaleUtils.resolveLocaleFromDisplayLanguage("english").getDisplayLanguage(LocaleUtils.DEFAULT_LOCALE));
		Assert.assertEquals("German", LocaleUtils.resolveLocaleFromDisplayLanguage(" germAn").getDisplayLanguage(LocaleUtils.DEFAULT_LOCALE));
		Assert.assertEquals("German", LocaleUtils.resolveLocaleFromDisplayLanguage("de_AT").getDisplayLanguage(LocaleUtils.DEFAULT_LOCALE));
		Assert.assertEquals("German", LocaleUtils.resolveLocaleFromDisplayLanguage("de_AT").getDisplayLanguage(LocaleUtils.DEFAULT_LOCALE));
		Assert.assertEquals("German", LocaleUtils.resolveLocaleFromDisplayLanguage("de").getDisplayLanguage(LocaleUtils.DEFAULT_LOCALE));
		Assert.assertEquals("Zulu", LocaleUtils.resolveLocaleFromDisplayLanguage("zu").getDisplayLanguage(LocaleUtils.DEFAULT_LOCALE));
		//three letter codes are currently not possible. Is this even possible with Locale?
		Assert.assertEquals("Church Slavic", LocaleUtils.resolveLocaleFromDisplayLanguage("cu").getDisplayLanguage(LocaleUtils.DEFAULT_LOCALE));
		Assert.assertNull(LocaleUtils.resolveLocaleFromDisplayLanguage(" germAny"));
	}
	
	@Test
	public void testSynonyms() {
		final String churchSlavicTag = "cu";
		
		//this resolves to "Church Slavic" via Apache LocalUtils => Apache LocaleUtils seems to know this Locale somehow
		Locale slav = LocaleUtils.resolveLocaleFromDisplayLanguage(churchSlavicTag);
		logger.debug("" + slav.getDisplayLanguage(LocaleUtils.DEFAULT_LOCALE));
		
		Locale slav2 = new Locale(churchSlavicTag, "");
		final String displayLang = slav2.getDisplayLanguage(LocaleUtils.DEFAULT_LOCALE);
		logger.debug("Instantiated from tag: " + displayLang);
		
		for(Locale l : Locale.getAvailableLocales()) {
			if(displayLang.equals(l.getDisplayLanguage(LocaleUtils.DEFAULT_LOCALE))) {
				logger.warn("Found display language in available locales: {}", l);
			}
			if(churchSlavicTag.equals(l.getCountry())) {
				logger.warn("Found country in available locales: {}", l);
			}
		}
		for (String synonym : new String[] { "Church Slavic", "Old Slavonic", "Church Slavonic", "Old Bulgarian", "Old Church Slavonic"}) {
			Assert.assertEquals("Church Slavic", 
					LocaleUtils.resolveLocaleFromDisplayLanguage(synonym).getDisplayLanguage(LocaleUtils.DEFAULT_LOCALE));
		}
	}
	
	@Test
	public void testNormalizeDisplayCountry() {
		Assert.assertEquals(null, LocaleUtils.normalizeDisplayLanguage(null));
		Assert.assertEquals("", LocaleUtils.normalizeDisplayLanguage(""));
		Assert.assertEquals("", LocaleUtils.normalizeDisplayLanguage("   "));
		Assert.assertEquals("English", LocaleUtils.normalizeDisplayLanguage("English"));
		Assert.assertEquals("English", LocaleUtils.normalizeDisplayLanguage("english"));
		Assert.assertEquals("Old Church Slavonic", LocaleUtils.normalizeDisplayLanguage("Old Church Slavonic"));
		Assert.assertEquals("Old Church Slavonic", LocaleUtils.normalizeDisplayLanguage(" old  chUrch           slavoniC  "));
	}
	
	@Test
	public void testGermanResolver() {
		LocaleResolver resolver = new LocaleUtils.LocaleResolver(Locale.GERMAN);
		Assert.assertEquals("English", resolver.resolveLocaleFromDisplayLanguage("Englisch").getDisplayLanguage(LocaleUtils.DEFAULT_LOCALE));
		Assert.assertEquals("German", resolver.resolveLocaleFromDisplayLanguage(" Deutsch").getDisplayLanguage(LocaleUtils.DEFAULT_LOCALE));
	}
	
	private ResolvedLocales testResolveLocales(String csvString) {
		ResolvedLocales result = LocaleUtils.parseLangCsvString(csvString);
		logger.debug("languages = {}", result.getNormalizedLangsCsv());
		logger.debug("locales = {}", result.getLocales());
		logger.debug("Original language field: {}", result.getInput());
		if(!result.getInput().equals(result.getNormalizedLangsCsv())) {
			logger.debug("Normalized form: {}", result.getNormalizedLangsCsv());
		} else {
			logger.debug("Already normalized.");
		}
		if(result.getLocales().isEmpty()) {
			logger.debug("No ISO languages could be resolved from language field!");
		} else {
			logger.debug("Resolved {} locales from language field: {}", result.getLocales().size(), result.getLocales());
		}
		return result;
	}
}
