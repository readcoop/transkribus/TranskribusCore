package eu.transkribus.core.util;

import eu.transkribus.core.model.beans.TrpTranscriptStatistics;
import eu.transkribus.core.model.beans.pagecontent.PcGtsType;
import eu.transkribus.core.model.beans.pagecontent.TableRegionType;
import eu.transkribus.core.model.beans.pagecontent.TextRegionType;
import eu.transkribus.core.model.beans.pagecontent_trp.TrpPageType;
import eu.transkribus.core.model.beans.pagecontent_trp.TrpRegionType;
import eu.transkribus.core.model.beans.pagecontent_trp.TrpTextLineType;
import eu.transkribus.core.model.beans.pagecontent_trp.TrpTextRegionType;
import eu.transkribus.core.util.SebisStopWatch.SSW;
import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.junit.Assume;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBException;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;

public class PageXmlUtilsTest {
	private static final Logger logger = LoggerFactory.getLogger(PageXmlUtilsTest.class);
	
	@Rule
	public TemporaryFolder tempDir = new TemporaryFolder();
	
	@Test
	public void testIfPageGetsSortedByReadingOrderTagOnUnmarshal() throws Exception {
		logger.info("testSortByReadingOrderOnly");
		File pageFile = new File("src/test/resources/page_samples/table-with-empty-ro-tags.xml");
		PcGtsType pc = PageXmlUtils.unmarshal(pageFile);
		// now, elements should be sorted according to ro-tag!
		TrpPageType page = (TrpPageType) pc.getPage();
		
		Assert.assertEquals(1, page.getTextRegionOrImageRegionOrLineDrawingRegion().indexOf(page.getRegion("r_1_1")));
		Assert.assertEquals(0, page.getTextRegionOrImageRegionOrLineDrawingRegion().indexOf(page.getRegion("tbl_2_2")));
		
		TrpTextRegionType tr = page.getTextRegion("r_1_1");
		Assert.assertEquals(0, tr.getTrpTextLine().indexOf(tr.getTextLine("r_1_1l1")));
		Assert.assertEquals(1, tr.getTrpTextLine().indexOf(tr.getTextLine("r_1_1l2")));
		
		if (false) { // use to check written file... 
			File pageOutFile = new File("src/test/resources/page_samples/table-with-empty-ro-tags-sorted.xml");
			PageXmlUtils.marshalToFile(pc, pageOutFile);
		}
	}
	
	@Test
	public void testMarshalUnmarshalPageWithTranskribusTables() throws JAXBException {
		File pageFile = new File("src/test/resources/page_samples/table.xml");
		PcGtsType pc = PageXmlUtils.unmarshal(pageFile);
		TrpPageType p = (TrpPageType) pc.getPage();
		
		TableRegionType t=null;
		for (TrpRegionType r : p.getTextRegionOrImageRegionOrLineDrawingRegion()) {
			if (r instanceof TableRegionType) {
				t = (TableRegionType) r;
				logger.info("found table, n-cells = "+t.getTableCell().size()+" rows/cols = "+t.getRows()+"/"+t.getColumns());
				break;
			}
		}
		Assert.assertNotNull(t);
		Assert.assertEquals(152, t.getTableCell().size());
	}
	
	@Test
	public void testMarshalUnmarshalPageWithAlternatives() throws JAXBException, IOException {
		// load PAGE-XML without alternatives
		File pageFile = new File("src/test/resources/page_samples/StAZ-Sign.2-1_001.xml");
		PcGtsType pc = PageXmlUtils.unmarshal(pageFile);
		TrpPageType p = (TrpPageType) pc.getPage();
		TrpTextLineType line = (TrpTextLineType) p.getTextRegions(false).get(0).getTextLine().get(0);
		line.setUnicodeText("i am the main text of the line", null);
		
		// write some alternatives into the first line
		for (int i=0; i<3; ++i) {
			line.getTextEquiv().getUnicodeAlternative().add("i am an alternative "+i);	
		}
		File pageOutFile = new File("src/test/resources/page_samples/StAZ-Sign.2-1_001_out.xml");
		PageXmlUtils.marshalToFile(pc, pageOutFile);
		
		// now reload the written file and check if content was saved:
		pc = PageXmlUtils.unmarshal(pageOutFile);
		p = (TrpPageType) pc.getPage();
		line = (TrpTextLineType) p.getTextRegions(false).get(0).getTextLine().get(0);
		
		Assert.assertEquals(3, line.getTextEquiv().getUnicodeAlternative().size());
		for (int i=0; i<3; ++i) {
			Assert.assertEquals("i am an alternative "+i, line.getTextEquiv().getUnicodeAlternative().get(i));
		}
		
		if (!pageOutFile.delete()) {
			throw new IOException("Could not delete file: "+pageOutFile);
		}
		
		// load a file that was previously written and included alternatives:
		File pageAltFile = tempDir.newFile("StAZ-Sign.2-1_001_with_alternatives.xml");
		PageXmlUtils.marshalToFile(pc, pageAltFile);
		line = (TrpTextLineType) p.getTextRegions(false).get(0).getTextLine().get(0);
		
		Assert.assertEquals(3, line.getTextEquiv().getUnicodeAlternative().size());
		for (int i=0; i<3; ++i) {
			Assert.assertEquals("i am an alternative "+i, line.getTextEquiv().getUnicodeAlternative().get(i));
		}		
	}

	@Test
	public void testBatchUnmarshalling() throws JAXBException {
		File pageFile = new File("src/test/resources/page_samples/StAZ-Sign.2-1_001.xml");
		final int testSize = 2;
		for(int i = 0; i < testSize; i++) {
			PageXmlUtils.unmarshal(pageFile);
		}
	}
	
	/**
	 * A test outlining an issue that might be causing https://github.com/Transkribus/TranskribusServer/issues/61
	 *
	 * @throws JAXBException
	 * @throws IOException
	 * @throws URISyntaxException
	 */
	@Ignore("For large input texts I did not see this method return and that's why the test is set ignored.")
	@Test
	public void testAssignPlainTextToPage() throws JAXBException, IOException, URISyntaxException {
		String basePath = "/mnt/read_scratch/TRP/test/gui_issue_61/";

		File baseDir = new File(basePath);
		Assume.assumeThat(String.format("'%s' is not a directory", baseDir.getAbsolutePath()),
				baseDir.isDirectory(), is(true));

		Assume.assumeTrue(new File(basePath).isDirectory());
		
		File pageFile = new File(basePath + "/page", "p001.xml");
		PcGtsType pc = PageXmlUtils.unmarshal(pageFile);
		
		String lineTxt = FileUtils.readFileToString(new File(basePath, "plaintext_no_linebreaks.txt"));
		
		String txt = "";
		//The method runtime seems to depend on the nr of lines parameter here.
		//For small values it finishes quickly. The method blocked on the server for > 8 hours in some instances.
		int nrOfLines = 10;
		for(int i = 0; i < nrOfLines; i++) {
			txt += lineTxt + "\n";
		}
		
		logger.info("Test text length = {}", txt.length());
		PageXmlUtils.applyTextToLines((TrpPageType) pc.getPage(), txt);
	}
	
	@Test
	public void testExtractStats() throws JAXBException {
		URL testXmlUrl = this.getClass().getClassLoader().getResource("TrpTestDoc_20131209/StAZ-Sign.2-1_001.xml");
		PcGtsType pc = PageXmlUtils.unmarshal(testXmlUrl);
		SSW sw = new SSW();
		TrpTranscriptStatistics stats = PageXmlUtils.extractStats(pc);
		logger.debug("Extracted stats in {} ms: {}", sw.stop(false), stats);
		
		Assert.assertEquals(Integer.valueOf(6), stats.getNrOfRegions());
		Assert.assertEquals(Integer.valueOf(5), stats.getNrOfTranscribedRegions());
		Assert.assertEquals(Integer.valueOf(134), stats.getNrOfWordsInRegions());
		Assert.assertEquals(Integer.valueOf(38), stats.getNrOfLines());
		Assert.assertEquals(Integer.valueOf(37), stats.getNrOfTranscribedLines());
		Assert.assertEquals(Integer.valueOf(166), stats.getNrOfWordsInLines());
		Assert.assertEquals(Integer.valueOf(167), stats.getNrOfWords());
		Assert.assertEquals(Integer.valueOf(167), stats.getNrOfTranscribedWords());
		Assert.assertEquals(Integer.valueOf(904), stats.getNrOfCharsInLines());
		Assert.assertEquals(Integer.valueOf(0), stats.getNrOfTables());
	}
	
	@Test
	public void testGetFulltextFromLines() throws JAXBException {
		URL pcUrl = this.getClass().getClassLoader().getResource("PageXmlUtilsTest/getFulltextFromLines.xml");
		PcGtsType pc = PageXmlUtils.unmarshal(pcUrl);		
		final String expected = "1 1822 May 20 ttional col () 1. The supreme operative bady should act without division, "
				+ "2. It should not act in so many as two bodies. even though the members were located by the best compounded set of locators ie. "
				+ "Electors – much less in more than two. a Much less should it be composed of two or more independently-operating bodies_ "
				+ "each taking cognizance of the same measure after the othes. 3. Itill less should enter into the composition of it a "
				+ "body of men located by a Chief Func -tionary, or set of Chief & unctionaries, esta blished for the purpose, sitting for a "
				+ "long time term of years or for life 4. Least of all should enter into the compose- tion of it a body of men, "
				+ "succeeding to one another in the way of genealagical succession.  78 S 4";
		final String fulltextFromLines = PageXmlUtils.getFulltextFromLines(pc, " ");
		Assert.assertEquals("Unexpected output from PageXmlUtils::getFulltextFromLines", expected, fulltextFromLines);
	}
	
	public static void testGetTextRegions() throws Exception {
		String transcriptWithTables = "https://files.transkribus.eu/Get?id=VCLTRLDSWETCXIHQNHKOPRLS";
		
		PcGtsType t = PageXmlUtils.unmarshal(new URL(transcriptWithTables));
		
		List<TextRegionType> tr = PageXmlUtils.getTextRegions(t);
		
		for (TextRegionType r : tr) {
			System.out.println("tr: "+r.getClass().getSimpleName()+" id: "+r.getId()+" n-lines: "+r.getTextLine().size());
		}
	}
	
	public static void testValidation() {
		final String path = "/mnt/read_scratch/TRP/Bentham_box_002/page/002_080_001.xml";
		try {
			logger.info(""+PageXmlUtils.isValid(new File(path)));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) throws Exception {
//		testGetTextRegions();
//		testSth();
		testValidation();
	}
}
