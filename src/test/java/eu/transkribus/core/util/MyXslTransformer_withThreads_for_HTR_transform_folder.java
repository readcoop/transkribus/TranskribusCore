package eu.transkribus.core.util;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.xml.XMLConstants;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

public class MyXslTransformer_withThreads_for_HTR_transform_folder {
	
	private static final Logger logger = LoggerFactory.getLogger(MyXslTransformer_withThreads_for_HTR_transform_folder.class);
	
	static String relPath = "";
	
	final static String transformToTextXslt = "/PageToTextfile.xsl";
	final static String transformToAltoXslt = "/PageToAltoWordLevel.xsl";
	final static String transformToPageXslt = "/PageToPage_WithoutWords.xsl";
	final static String transformToPageWithSeparatorRegionsXslt = "/Page2Page_SeparatorsToTextRegions.xsl";
	final static String transformAltoToPageXslt = "/AltoToPage2013.xsl";
	final static String transformAltoToPageInclusiveTagsXslt = "/AltoToPage2013WithTags.xsl";
	final static String transformPageToAbbyyXslt = "/PageToAbbyy10.xsl";
	final static String transformAbbyyToPageXslt = "/Abbyy10ToPage2013_linePolygons.xsl";
	final static String transformAbbyyToPageLAXslt = "/Abbyy10ToPage2013_LA.xsl";
	
	static String startDir = "U:\\projects\\crkn\\CRKN_ICR_Batch_9\\";
	static String htrFolder = "03_workDir_HTR";
	
	//static String startTxt = startDir + "ready_for_Transforming.txt\\";
	static String destTxt = startDir + "ready_for_HTR.txt\\";	
	
	static String startFolderName = "ocr";
	static String destFolderName = "page";
	
	final static String textExtension = ".txt";
	final static String xmlExtension = ".xml";
	static int i = 1;
	
	public enum XSL { PAGE2TEXT, PAGE2ALTO, PAGE2PAGE, ALTO2PAGE, ALTO2PAGEWITHTAGS, PAGE2ABBYY, ABBYY2PAGE, ABBYY2PAGELA, NO, PAGE2PAGE_SEP }	
	public static String transformXslt;
	
	static int counter = 0;
	static int totalCounter = 0;
	static int folderCounter = 0;
	
	static boolean overwrite = false;
	static boolean changeNamespaceInAlto = false;
	static boolean useMainFolder = false;
	static boolean checkFilenumbers = true;
	static boolean doValidationAlto = true;
	static boolean doValidationPage = false;
	
	static boolean transformMM = false;
	static boolean useTxtFileWithPaths = false;
	
	/*
	 * change this for the wanted transformation type
	 * PAGE2TEXT: export all text lines into a text file, the hyphens(¬) are replaced with '=': needs to be adapted if needed
	 * PAGE2ALTO: alto with words: only with estimated word coordinates
	 * PAGE2PAGE: identical transformation except for the hyphens(¬)
	 * ALTO2PAGE: mainly for getting layout but also text from (Abbyy Finereader) ALTO files
	 * ALTO2PAGEWITHTAGS: layout, text and tagrefs get converted to PAGE XML
	 * 
	 * After transforming to PAGE XML the PAGE XML gets unmashalled to check validity with NRW_unmarshallerWithThreads
	 */
	public static XSL xslName = XSL.PAGE2ALTO;
	public static List<String> errors = new ArrayList<String>();
	public static Set<File> fileLinks = new HashSet<File>();
	
	private static String startPath = "T:\\projects\\crkn\\";

	private static String[] paths = new String[] {startPath+"oocihm.lac_reel_c8971",startPath+"oocihm.lac_reel_c8972",startPath+"oocihm.lac_reel_c8973",startPath+"oocihm.lac_reel_c8974",startPath+"oocihm.lac_reel_c8975",startPath+"oocihm.lac_reel_c8976",startPath+"oocihm.lac_reel_c8977",startPath+"oocihm.lac_reel_c8978",startPath+"oocihm.lac_reel_c8979"};
	
    public static void main(String[] args) throws IOException{
    	
    	//String startPath = destPath;
    	//pool = Executors.newFixedThreadPool(1);
    	
    	switch (xslName){
    	case PAGE2TEXT:
    		startFolderName = "page";
    		destFolderName = "txt";
    		transformXslt = transformToTextXslt;
    		break;
    	case PAGE2ALTO:
    		if (transformMM){
    			startFolderName = "page_mm";
    		}
    		else{
    			startFolderName = "page";
    		}
    		destFolderName = "alto";
    		transformXslt = transformToAltoXslt;
    		break;
    	case PAGE2PAGE:
    		destFolderName = "page";
    		transformXslt = transformToPageXslt;
    		break;
    	case PAGE2PAGE_SEP:
    		startFolderName = "page";
    		destFolderName = "page";
    		transformXslt = transformToPageWithSeparatorRegionsXslt;
    		break;
    	case ALTO2PAGE:
    		if (transformMM){
    			destFolderName = "page_mm";
    			changeNamespaceInAlto = true;
    		}
    		else{
    			destFolderName = "page";
    		}
    		
    		transformXslt = transformAltoToPageXslt;
    		break;
    	case ALTO2PAGEWITHTAGS:
    		destFolderName = "page_mm";
    		transformXslt = transformAltoToPageInclusiveTagsXslt;
    		break;
    	case PAGE2ABBYY:
    		destFolderName = "ocr";
    		transformXslt = transformPageToAbbyyXslt;
    		break;
    	case ABBYY2PAGE:
    		startFolderName = "ocr";
    		destFolderName = "page";
    		//destFolderName = "page_sep";
    		transformXslt = transformAbbyyToPageXslt;
    		break;
    	case ABBYY2PAGELA:
    		startFolderName = "ocr";
    		destFolderName = "page";
    		transformXslt = transformAbbyyToPageLAXslt;
    		break;
		default:
			break;
    	}

    	
    	/*
    	 * redo folder
    	 */
    	boolean startTransformer = true;
    	
    	while (startTransformer) {
    	
        	if (useMainFolder){
        		File startDirectory = new File(startDir);
        		int i = 0;
        		
            	//File [] dirs = startDirectory.listFiles((File pathname) -> pathname.isDirectory() && !pathname.getName().contains("zips") && !pathname.getName().contains("Batch001"));
            	File [] dirs = startDirectory.listFiles((File pathname) -> pathname.isDirectory());
            	for (File dir : dirs){
            		relPath = dir.getName();
            		logger.debug("dir to do: " + relPath);
            		//recursiveFileRunner(startPath, dir.getName());
            		fileLinks.clear();
            		recursiveFileCollector(startDir, dir.getName());
            		transformFilesInHashSet();
            		writeToFile( i++ + ": (debug) " + counter + " done - go on with next");
            	}
	
        	}
        	else {
	    		for (String dir : paths) {
	        		logger.debug("dir to do: " + dir);
	        		fileLinks.clear();
	        		recursiveFileCollector(dir, "");
	        		transformFilesInHashSet();
	    		}
        	}
        	startTransformer = false;
    	
    	}
    	
    	for (String error: errors){
    		logger.debug("error for file/folder: " + error);
    	}

    }
    
    
	private static void collectAndTransformFiles(String startFolder, boolean moveDir, String absoluteDestFolder) {
		
		fileLinks.clear();
		try {
			
			recursiveFileCollector(startFolder, "");

			transformFilesInHashSet();

			if (moveDir) {
				FileUtils.moveDirectory(new File(startFolder), new File(absoluteDestFolder));
			}
		
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

    
	/*
	 * use case: collect all files in a HashSet to use parallelStream
	 *     	   
	 */
    public static void recursiveFileCollector(final String startDir, String relPathString) throws IOException {
    	
    	File startDirectory = new File(startDir+"/"+relPathString);
    
    	File [] files = startDirectory.listFiles((File pathname) -> !pathname.getName().endsWith(".db") && !pathname.isDirectory());
    	File [] dirs = startDirectory.listFiles((File pathname) -> pathname.isDirectory());
    	
    	if (files != null && files.length>0){
    		Arrays.sort(files);	
    	}
    	
    	if (dirs != null && dirs.length>0){
    		Arrays.sort(dirs);
    	}
    	
//    	if (files == null) {
//    		writeToFile("(error) no files in dir " + startDirectory.getAbsolutePath());
//    		return;
//    	}
    	
    	logger.debug("start Dir is " + startDirectory.getAbsolutePath());
    	    	
    	if (files != null && files.length != 0 && startDirectory.getName().equals(startFolderName) && !startDirectory.getName().contains("zips")){

    		String pageFolder = startDirectory.getParentFile() + "/page/";
    		
    		logger.debug("go into files " + files.length);

    		/*
    		 * this is for finding files where the conversion from Alto to Page went wrong
    		 */
//    	    for (File file : files) {
//        		if (!new File(pageFolder+file.getName()).exists()){
//        			writeToFile(counter++ + ") no page found for " + file);
//        			fileLinks.add(file);
//        		}
//    	    } 
    	    for (File file : files) {
    	    	fileLinks.add(file);
    	    }  
    		
    	}
    	else{
    		logger.debug("go into dirs " + dirs.length);
    		for(File dir : dirs){
    			relPath = relPathString != "" ? relPathString + "/" + dir.getName() : dir.getName();
    			recursiveFileCollector(startDir, relPath);
    		}
    	}
    
    }
    
    private static void transformFilesInHashSet(){
    	
    	String ext = xslName.equals(XSL.PAGE2TEXT) ? textExtension : xmlExtension;
    	//
    	fileLinks.parallelStream().forEach((fileLink) -> {checkAndTransform_forParallelStream(fileLink, ext);});
    			
    	/*
    	 * this was used to see if PAGE XML was updated correctly -> date must be after a given date
    	 */
    	//fileLinks.parallelStream().forEach((fileLink) -> {checkFileModified_forParallelStream(fileLink, ext);});
	}
    

    
	private static void checkAndTransform_forParallelStream(File file, String ext) {
		
		String destFolder = file.getParentFile().getAbsolutePath().replace(startFolderName, destFolderName);			
		
		File destFolderFile = new File(destFolder);
		
		
//		if (destFolderFile.exists() && !overwrite && destFolderFile.listFiles().length>0 && !checkFilenumbers){
//			totalCounter += destFolderFile.listFiles().length;
//			logger.debug("dest folder exists already - move on!! total files: (" + totalCounter + ")");
//			return;
//		}
		
		destFolderFile.mkdirs();		
		
		String destFn = destFolder + "/" + FilenameUtils.getBaseName(file.getName()) + ext;
		writeToFile("transform file (" + i++ + ") from " + file.getAbsolutePath() + " into " + destFn);
		

		File destFile = new File(destFn);  
	      
		if(destFile.exists() && !overwrite && destFile.length()>5000) {
			writeToFile("exists already!!");
//			if (destFile.length()<5000) {
//				writeToFile("filesize < 5kB " + destFile.getAbsolutePath());
//				try {
//					System.in.read();
//				} catch (IOException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//			}
			return;
		}
		else{
			try {
				if (changeNamespaceInAlto){
					/*
					 * used to change the namespace in a PAGE XML
					 * 
					 * xsi:noNamespaceSchemaLocation="//192.168.10.50/docworks/docWORKS/schema/alto-1-2.xsd"
					 * 
					 * "xsi:noNamespaceSchemaLocation=\"//192.168.10.50/docworks/docWORKS/schema/alto-1-2.xsd\""
					 * or //192.168.10.50/docworks/docWORKS/schema/alto-1-2.xsd
					 * xsi:schemaLocation="http://www.loc.gov/standards/alto/ns-v2# http://www.loc.gov/standards/alto/alto.xsd"
					 */
					InputStream alto_withNew_Ns = transformNamespace(file, "xsi:noNamespaceSchemaLocation=\"//Themis/docworks/docWORKS/schema/alto-1-2.xsd\"", "xmlns=\"http://www.loc.gov/standards/alto/ns-v2#\" xmlns:page=\"http://schema.primaresearch.org/PAGE/gts/pagecontent/2013-07-15\" xsi:schemaLocation=\"http://www.loc.gov/standards/alto/ns-v2# http://www.loc.gov/standards/alto/alto.xsd\"");
					alto_withNew_Ns = transformNamespace(file, "xsi:noNamespaceSchemaLocation=\"//192.168.10.50/docworks/docWORKS/schema/alto-1-2.xsd\"", "xmlns=\"http://www.loc.gov/standards/alto/ns-v2#\" xmlns:page=\"http://schema.primaresearch.org/PAGE/gts/pagecontent/2013-07-15\" xsi:schemaLocation=\"http://www.loc.gov/standards/alto/ns-v2# http://www.loc.gov/standards/alto/alto.xsd\"");
					transform(alto_withNew_Ns, destFile);
					
					//System.in.read();
				}
				else{
					//System.in.read();
//					if (!destFile.exists()) {
//						transform(file, destFile);
//					}
					//
					transform(file, destFile);
					//System.in.read();
					/*
					 * validate the result - ToDo test
					 * reason: some files for NAN was corrupt because of missing or additional characters (randomly - no technichal reason found)
					 * e.g. NTENT="van" instead of CONTENT or HEIGHT=="166"
					 * http://schema.primaresearch.org/PAGE/gts/pagecontent/2013-07-15/pagecontent.xsd
					 */

				}
				if (doValidationAlto){
					if (!XmlValidator.validateAltoFile(file, destFile)) {
						writeToFile("not valid error-"+ file.getAbsolutePath());
						transform(file, destFile);
					}
				}	
				
			} catch (TransformerException | IOException e) {
				// TODO Auto-generated catch block
				writeToFile("tranformer error-"+ file.getAbsolutePath());
				errors.add("(error) " + file.getAbsolutePath() + System.lineSeparator()); 
				e.printStackTrace();
				return;
			}

		}
		
	}

	public static void writeToFile(String text){

    	   try {
			//File file = new File(startPath + "errors.txt" );
    		   logger.debug(text);
			   BufferedWriter buff = new BufferedWriter(new FileWriter(startDir + "debug.txt", true));
			   buff.write(text);
			   buff.newLine();
			   buff.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    } 
	
    public static void writeToResult(String text) throws IOException {

    	logger.debug("result: " + text);
    	BufferedWriter buff = new BufferedWriter(new FileWriter(destTxt, true));
    	buff.write(text);
    	buff.newLine();
    	buff.close();
    } 
    
    /*
     * used case: xmlns from PAGE_2016 should become PAGE_2013
     * problem: with xslt we cannot change xmlns, hence we do it with text manipulation
     */
	private static InputStream transformNamespace(File xmlFile, String oldNs, String newNs) throws TransformerException, IOException {
		        
        // Let's get XML file as String using BufferedReader
        // FileReader uses platform's default character encoding
        // if you need to specify a different encoding, use InputStreamReader
        Reader fileReader = new FileReader(xmlFile);
        BufferedReader bufReader = new BufferedReader(fileReader);
        
        StringBuilder sb = new StringBuilder();
        String line = bufReader.readLine();
        
        while( line != null){
        	
        	if (line.contains("noNamespaceSchemaLocation")){
        		//line = line.replace(oldNs, newNs);
	        	String[] splits = line.split(" ");
	        	for (int i = 0; i<splits.length; i++){
	        		if (i!=2){
	        			sb.append(splits[i]);
	        		}
	        		else{
	        			sb.append(newNs);
	        		}
	        		if (i<(splits.length-1)){
	        			sb.append(" ");
	        		}
	        	}
	        	sb.append("\n");
        	}
        	else{
                sb.append(line).append("\n");
        	}
        	line = bufReader.readLine();
        }
        String xml2String = sb.toString();
//        logger.debug("XML to String using BufferedReader : ");
//        logger.debug(xml2String);

        InputStream stream = new ByteArrayInputStream(xml2String.getBytes(StandardCharsets.UTF_8));
        
        //stringToDom(xml2String, destFile);
        
        bufReader.close();
        return stream;
	} 
	
	public static void stringToDom(String xmlSource, File destFile) 
	        throws IOException {
	    java.io.FileWriter fw = new java.io.FileWriter(destFile.getAbsolutePath());
	    fw.write(xmlSource);
	    fw.close();
	}

	private static void transform(File origFile, File destFile) throws TransformerException {
		
		TransformerFactory factory = TransformerFactory.newInstance("net.sf.saxon.TransformerFactoryImpl", null);
        
        InputStream res = MyXslTransformer_withThreads_for_HTR_transform_folder.class.getResourceAsStream(transformXslt);
        Source xslt = new StreamSource(res);
        Transformer transformer = factory.newTransformer(xslt);

        Source text = new StreamSource(origFile);
        transformer.transform(text, new StreamResult(destFile));
		
	} 
	
	private static void transform(InputStream origFile, File destFile) throws TransformerException {
		
		TransformerFactory factory = TransformerFactory.newInstance("net.sf.saxon.TransformerFactoryImpl", null);
        
        InputStream res = MyXslTransformer_withThreads_for_HTR_transform_folder.class.getResourceAsStream(transformXslt);
        Source xslt = new StreamSource(res);
        Transformer transformer = factory.newTransformer(xslt);

        Source text = new StreamSource(origFile);
        transformer.transform(text, new StreamResult(destFile));
		
	}
}
