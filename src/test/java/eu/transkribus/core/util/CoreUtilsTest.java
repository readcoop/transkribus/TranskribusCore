package eu.transkribus.core.util;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.docx4j.wml.P;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CoreUtilsTest {
	private static final Logger logger = LoggerFactory.getLogger(CoreUtilsTest.class);
	
	/**
	 * Test strategy: convert a pages string into page indices, convert those to a pages string,
	 * then convert this string again to pages indices and check if both page indices are the same! 
	 */
	@Test
	public void testRangeStringToListAndBack() throws Exception {
		String[] strs = new String[] {
				"1-2,4,7,10-12", // regular test case for a compressed pages string
				"1,2,3,4,5,10-21,45,46,47,48-50" // test case where multiple subsequent values get "compressed" when converting into a pages string 
		};
		int nPages = 50;
		
		for (int i=0; i<2; ++i) {
			String str = strs[i];
			
			List<Integer> pageIndices = CoreUtils.parseRangeListStrToList(str, nPages);
			System.out.println("pageIndices = "+pageIndices);

			String createdPagesStr = CoreUtils.getRangeListStrFromList(pageIndices);
			System.out.println("createdPagesStr = "+createdPagesStr);
			
			List<Integer> createdPageIndices = CoreUtils.parseRangeListStrToList(createdPagesStr, nPages);
			System.out.println("createdPageIndices = "+pageIndices);
			
			Assert.assertTrue("createdPageIndices contains wrong values!", 
					createdPageIndices.size() == pageIndices.size() && createdPageIndices.containsAll(pageIndices));		
		}
		
	}
	
	/**
	 * Test page range String parsing and inversion of result
	 */
	@Test
	public void testRangeStringMethods() {
		final String testString = "1-2,4-5,7-10";
		final int nPages = 14;
		
		Set<Integer> expectedSet = new HashSet<>();
		Set<Integer> expectedSetInverted = new HashSet<>();
		final Integer[] pageIndices = {0, 1, 3, 4, 6, 7, 8, 9};
		final Integer[] invertedPageIndices = {2, 5, 10, 11, 12, 13};
		expectedSet.addAll(Arrays.asList(pageIndices));
		expectedSetInverted.addAll(Arrays.asList(invertedPageIndices));
		
		Set<Integer> result = null;
		try {
			result = CoreUtils.parseRangeListStr(testString, nPages);
		} catch (IOException e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
		
		Assert.assertTrue("Result contains wrong values!", 
				result.size() == expectedSet.size() && result.containsAll(expectedSet));
		
		Set<Integer> invertedResult = CoreUtils.invertPageIndices(result, 14);
		
		Assert.assertTrue("Inverted result contains wrong values!", 
				invertedResult.size() == expectedSetInverted.size() && invertedResult.containsAll(expectedSetInverted));
	}
	
	@Test
	public void testParseStringList() {
		Assert.assertEquals(0, CoreUtils.parseStringList(",,,", ",", true, true).size());
		Assert.assertEquals(0, CoreUtils.parseStringList(null, ",", true, true).size());
		Assert.assertEquals(0, CoreUtils.parseStringList("", ",", true, true).size());
		
		String s = ",a@b.c,,";
		List<String> list = CoreUtils.parseStringList(s, ",", true, true);
		Assert.assertEquals(1, list.size());
		Assert.assertEquals(list.get(0), "a@b.c");
		
		s = "a@b.c";
		list = CoreUtils.parseStringList(s, ",", true, true);
		Assert.assertEquals(1, list.size());
		Assert.assertEquals(list.get(0), "a@b.c");
		
		s = "a@b.c, d@e.f";
		list = CoreUtils.parseStringList(s, ",", true, true);
		Assert.assertEquals(2, list.size());
		Assert.assertEquals(list.get(0), "a@b.c");
		Assert.assertEquals(list.get(1), "d@e.f");
	}


	/**
	 * Debugging Test for reading a docx and to check if soft linefeeds can be read
	 */
	@Test
	public void testReadingDOCX() {
//		String path = "src/test/resources/testSoftLineFeeds.docx";
//		String path = "C:\\Users\\quebo\\Downloads\\Correspondence between Field Marshal Lord William Birdwood and Lady Janetta Birdwood, 1915 - output_2.docx";
//		String path = "C:\\Users\\quebo\\Documents\\Compute_Accuracy-Lord_Strathcona_Monument_Fund_Committee_Minute_Book.docx";
		String path = "C:\\Users\\quebo\\Downloads\\export_job_12161183\\4311592\\Test_TIM_-_Lord_Strathcona_Monument_Fund_Committee_Minute_Book.docx";
		try {
			String text = CoreUtils.extractTextFromDocx(path);
			System.out.println(text);

		} catch (Exception e) {
			logger.error("Error reading docx during UnitTest", e);
		}
	}





}
