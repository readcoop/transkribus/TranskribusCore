package eu.transkribus.core.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import eu.transkribus.core.model.beans.TrpDocMetadata;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class JacksonUtilTest {
    private static final Logger logger = LoggerFactory.getLogger(JacksonUtilTest.class);
    @Test
    public void testMarshalTrpDocMetadata() throws JsonProcessingException {
        TrpDocMetadata md = new TrpDocMetadata();
        md.setTitle("Test Title");
        md.setNrOfTranscribedLines(123);
        logger.info(md.toString());

        ObjectMapper om = JacksonUtil.createDefaultMapper(false);
        
        String s1 = om.writeValueAsString(md);
        logger.info(s1);

        TrpDocMetadata md2 = JacksonUtil.createDefaultMapper(false).readValue(s1, TrpDocMetadata.class);
        logger.info(md2.toString());

        Assert.assertEquals(md, md2);
        Assert.assertEquals(md2.getTotalStats(), md.getTotalStats());
        
    }
}
