package eu.transkribus.core.util;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLStreamHandler;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

public class DeaFileUtilsTest {

	@Rule
	public TemporaryFolder tempDir = new TemporaryFolder();

	/**
	 * 
	 * Check Workflow completes successfully with URL
	 * http://digi.landesbibliothek.at/viewer/content/AC10109954/1000/0/00000024.jpg
	 * 
	 */
	@Test
	public void testWorkingImageUrlStr() throws Exception {
		
		// arrange
		File out = tempDir.newFile("file1");
		HttpURLConnection mockUrlCon = mock(HttpURLConnection.class);
		// random byte sequence, no real image
		byte[] expectedImageBytes = {0x47, 0x49, 0x46, 0x38, 0x39, 0x61, 0x0A, 0x00, 0x0A, 0x00};
    	InputStream is = new ByteArrayInputStream(expectedImageBytes);
		doReturn(is).when(mockUrlCon).getInputStream();
		doReturn(200).when(mockUrlCon).getResponseCode();
		URLStreamHandler stubUrlHandler = createStub(mockUrlCon);
		URL url = new URL("https", "digi.landesbibliothek.at", 80, "/viewer/content/AC10109954/1000/0/00000024.jpg", stubUrlHandler);

		// act
		int code = DeaFileUtils.copyUrlToFile(url, out);
		
		// assert
		assertEquals(200, code);
		assertTrue(out.exists());
		verify(mockUrlCon, times(1)).getInputStream();
	}
	

	/**
	 * 
	 * Check Workflow fails with URL
	 * https://dbis-thure.uibk.ac.at/f/Get?id=thisDoesNotWork
	 * 
	 */
	@Test
	public void testNonWorkingUrlStr() throws Exception {
		//File out = new File(tmpDir.getAbsolutePath() + File.separator + "outFile");

		// arrange
		File out = new File(tempDir + File.separator + "file2");
		HttpURLConnection mockUrlCon = mock(HttpURLConnection.class);
		when(mockUrlCon.getResponseCode()).thenReturn(400);
		URLStreamHandler stubUrlHandler = createStub(mockUrlCon);
		URL url = new URL("https", "dbis-thure.uibk.ac.at", 80, "/f/Get?id=thisDoesNotWork", stubUrlHandler);

		// act
		int code = DeaFileUtils.copyUrlToFile(url, out);

		// assert
		assertEquals(400, code);
		verify(mockUrlCon, never()).getInputStream();
	}
	
	/**
	 * Wrap given Connection Mock in StreamHandler
	 * @param conn
	 * @return
	 */
	URLStreamHandler createStub(HttpURLConnection conn) {
		return new URLStreamHandler() {
			@Override
			protected URLConnection openConnection(URL u) throws IOException {
				return conn;
			}            
		};
	}
}
