package eu.transkribus.core.io.formats;

import eu.transkribus.core.util.PageXmlUtils;
import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBException;
import java.io.File;
import java.io.IOException;

public class Page2010ConverterTest {
	private static final Logger logger = LoggerFactory.getLogger(Page2010ConverterTest.class);
	
	@Rule
	public TemporaryFolder tempDir = new TemporaryFolder();
	
	@Test
	public void test2010ConversionSingleFile() throws IOException, JAXBException {
		/**
		 * To investigate:
		 * This test fails on Windows at the move File operations in Page2010Converter with IOException: "concurrent access"
		 * 
		 * Update 2021-03-10: FileUtils.moveFile was replaced with FileUtils.copyFile 
		 */
		
		File page2010Xml = new File("src/test/resources/page2010/002_080_001.xml");
		Assert.assertTrue("Could not find test resource: " + page2010Xml.getAbsolutePath(), page2010Xml.exists());

		File testFile = tempDir.newFile(page2010Xml.getName());
		FileUtils.copyFile(page2010Xml, testFile);
		
		File backupDir = tempDir.newFolder("backup");
		
		Page2010Converter.updatePageFormatSingleFile(testFile, backupDir.getAbsolutePath());
		
		try {
			Assert.assertNotNull("Updated PAGE XML could not be read!", PageXmlUtils.unmarshal(testFile));
		} catch (Exception e) {
			Assert.fail(e.getMessage());
		}
		
		File backup = new File(backupDir, page2010Xml.getName());
		Assert.assertTrue("Backup file was not created at: " + backup.getAbsolutePath(), backup.isFile());
	}
	
	@Test
	public void test2010ConversionSingleFileOldMethod() throws IOException, JAXBException {
		File page2010Xml = new File("src/test/resources/page2010/002_080_001.xml");
		Assert.assertTrue("Could not find test resource: " + page2010Xml.getAbsolutePath(), page2010Xml.exists());
		
		File testFile = tempDir.newFile(page2010Xml.getName());
		FileUtils.copyFile(page2010Xml, testFile);
		
		File backupDir = tempDir.newFolder("backup");
		Page2010Converter.updatePageFormatSingleFileOld(testFile, backupDir.getAbsolutePath());
		
		try {
			Assert.assertNotNull("Updated PAGE XML could not be read!", PageXmlUtils.unmarshal(testFile));
		} catch (Exception e) {
			Assert.fail(e.getMessage());
		}
		
		File backup = new File(backupDir, page2010Xml.getName());
		Assert.assertTrue("Backup file was not created at: " + backup.getAbsolutePath(), backup.isFile());
	}
}
