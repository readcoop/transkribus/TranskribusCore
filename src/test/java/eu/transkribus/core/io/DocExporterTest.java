package eu.transkribus.core.io;

import eu.transkribus.core.model.beans.TrpDoc;
import eu.transkribus.core.model.builder.CommonExportPars;
import org.apache.commons.io.FileUtils;
import org.dea.fimgstoreclient.FimgStoreGetClient;
import org.junit.Assert;
import org.junit.Assume;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

import static org.hamcrest.CoreMatchers.is;

public class DocExporterTest {
	private static final Logger logger = LoggerFactory.getLogger(DocExporterTest.class);
	@Rule
	public TemporaryFolder tempDir = new TemporaryFolder(new File("target"));
	private DocExporter exporter;
	
	@Before
	public void init() throws IOException {
		exporter = newDocExporter();
	}
	
	/**
	 * Cleaning export directory from a NFS share failed when streams were not properly closed during the export.
	 * For each open file NFS contains a hidden .nfsXXXX file, that will show up in the IOException message when 
	 * calling FileUtils::deleteDirectory more than once (see http://nfs.sourceforge.net/#faq_d2).
	 * Those files will block deletion and the export dir will be a mess eventually.
	 * 
	 * @return
	 * @throws IOException
	 */
	private File createTempDirOnNfs() throws IOException {
		return Files.createTempDirectory(new File("/mnt/transkribus/export_test").toPath(), "DocExporterTest-").toFile();
	}

	/**
	 * create a standard DocExporter. Method may be overriden to set up a ServerSideDocExporter in specific tests.
	 * 
	 * @return
	 */
	protected DocExporter newDocExporter() {
		return new DocExporter(new FimgStoreGetClient("files-test.transkribus.eu", "/"));
	}

	@Test
	public void testTeiExportAndDelete() throws Exception {
		final File testDocFile = tempDir.newFolder();
		final File testResource = new File(this.getClass().getClassLoader().getResource("TrpTestDoc_20131209").toURI());
		FileUtils.copyDirectory(testResource, testDocFile);

		File outputDir = tempDir.newFolder();
		Assume.assumeThat(String.format("'%s' is not an existing path", testDocFile.getAbsolutePath()),
				testDocFile.exists(), is(true));

		TrpDoc doc = LocalDocReader.load(testDocFile.getAbsolutePath());
		File teiFile = exportTei(outputDir, doc);
		Assert.assertTrue(teiFile.isFile());
		final float filesizeMb = teiFile.length() / 1000000f;
		Assert.assertTrue(filesizeMb > 0);
		logger.info("Wrote TEI file to {}, size = {} MB", teiFile.getAbsolutePath(), filesizeMb);
		
		//test if cleanup of the exportDir works.
		try {
			FileUtils.deleteDirectory(outputDir);
			Assert.assertTrue(!outputDir.exists());
		} catch (IOException e) {
			logger.error("Cleanup of tmpDir failed!", e);
			throw e;
		}
	}
	
	@Test
	public void testAltoWordLevelExportAndDelete() throws Exception {
		final File testDocFile = tempDir.newFolder();
		final File testResource = new File(this.getClass().getClassLoader().getResource("TrpTestDoc_20131209").toURI());
		FileUtils.copyDirectory(testResource, testDocFile);
		
		File exportTarget = tempDir.newFolder();
		Assume.assumeThat(String.format("'%s' is not an existing path", testDocFile.getAbsolutePath()),
				testDocFile.exists(), is(true));

		TrpDoc doc = LocalDocReader.load(testDocFile.getAbsolutePath());
		File outputDir = exportAlto(exportTarget, doc);
		Assert.assertTrue(outputDir.isDirectory());
		logger.info("Wrote export to {}", outputDir.getAbsolutePath());
		
		//test if cleanup of the exportDir works.
		try {
			FileUtils.deleteDirectory(exportTarget);
			Assert.assertTrue(!exportTarget.exists());
		} catch (IOException e) {
			logger.error("Cleanup of tmpDir failed!", e);
			throw e;
		}
	}
	
	protected File exportAlto(File tmpDir, TrpDoc doc) throws Exception {
		try {
			CommonExportPars commonPars = new CommonExportPars();
			commonPars.setPages("1-" + doc.getNPages());
			commonPars.setDoExportAltoXml(true);
			commonPars.setSplitIntoWordsInAltoXml(true);
			commonPars.setDir(tmpDir.getAbsolutePath());
			commonPars.setDoWriteMets(true);
			return exporter.exportDoc(doc, commonPars);
		} catch (Exception e) {
			logger.error("TEI export failed!", e);
			throw e;
		}
	}
	
	protected File exportTei(File tmpDir, TrpDoc doc) throws Exception {
		try {
			CommonExportPars commonPars = new CommonExportPars();
			commonPars.setPages("1-" + doc.getNPages());
			File exportFile = new File(tmpDir, "teiExportTest.xml");
			String exportFilename = exportFile.getAbsolutePath();
			exporter.writeTEI(doc, exportFilename, commonPars, null);
			return exportFile;
		} catch (Exception e) {
			logger.error("TEI export failed!", e);
			throw e;
		}
	}
}
