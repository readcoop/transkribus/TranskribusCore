package eu.transkribus.core.model.beans;

import org.junit.Assert;
import org.junit.Test;

public class TrpPageTest {
    @Test
    public void testPageCopyEquals() {
        TrpPage page = new TrpPage();
        TrpPage copy = new TrpPage(page);
        Assert.assertTrue(page.equals(copy));
        Assert.assertTrue(copy.equals(page));
    }
}
