package eu.transkribus.core.model.beans.job.enums;


/**
 * This class will produce an humann readable csv table combining the values from JobImpl and JobTask enums.
 */
public class CreateJobImplAndJobTaskTable {

    public static void main(String[] args) {
        System.out.println("JobImpl,JobClass,JobTask,JobTaskType,JobTaskLabel,JobTaskDefaultProvider");
        for (JobImpl jobImpl : JobImpl.values()) {
            for (JobTask jobTask : JobTask.values()) {
                if (jobImpl.getTask() == jobTask) {
                    System.out.println(jobImpl + "," + jobImpl.getClassName() + "," + jobTask + "," + jobTask.getJobType() + "," + jobTask.getLabel() + "," + jobTask.getDefaultProvider());
                }
            }
        }
    }

}
