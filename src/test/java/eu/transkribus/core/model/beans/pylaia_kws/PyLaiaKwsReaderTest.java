package eu.transkribus.core.model.beans.pylaia_kws;

import eu.transkribus.core.model.beans.pagecontent_miniOcr.MiniOcrType;
import org.junit.Assert;
import org.junit.Test;

import javax.xml.bind.JAXBException;
import java.io.File;
import java.io.IOException;

public class PyLaiaKwsReaderTest {

    @Test
    public void testKwsReaderCreateMiniOcr() throws IOException, JAXBException {
        String pathToKwsExample = "src/test/resources/kwsSample.json";
        String outpath = "src/test/resources/kws_miniocr_output.xml";
        File outFile = new File(outpath);
        outFile.delete();
        PyLaiaKwsReader reader = new PyLaiaKwsReader(pathToKwsExample);
        reader.createMiniOCR(1234, 500, 700, outpath);
        Assert.assertTrue(outFile.exists());
    }

}
