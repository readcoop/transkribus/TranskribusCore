package eu.transkribus.core.model.beans.pagecontent.filter;

import eu.transkribus.core.model.beans.TrpStructureStats;
import eu.transkribus.core.model.beans.pagecontent.PcGtsType;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

public class StructureStatisticsFilterTest {
	private static final Logger logger = LoggerFactory.getLogger(StructureStatisticsFilterTest.class);
	PageContentTestResourceLoader loader = new PageContentTestResourceLoader();

	@Test
	public void testFilterOnPage() throws JAXBException, IOException {
		StructureStatisticsFilter filter = new StructureStatisticsFilter();
		// Load a page content without any custom structure tags but legacy page-native structures set, which should be ignored by the filter
		PcGtsType pc = loader.loadXmlResource("gapAndUnclearTaggedLines.xml");
		filter.doFilter(pc);
		TrpStructureStats stats = filter.getStats();
		logger.debug("Stats = {}", stats);
		assertThat(stats.getRegionTypes()).size().isEqualTo(2);
		TrpStructureStats.RegionTypeStats regionTypeStats = stats.getRegionTypes().get(0);
		assertThat(regionTypeStats.getRegionType()).isEqualTo(TrpStructureStats.RegionType.TEXT_REGION);
		assertThat(regionTypeStats.getOverallCount()).isEqualTo(3);
		assertThat(regionTypeStats.getStructureTypes()).size().isEqualTo(1);
		TrpStructureStats.StructureType structureType = regionTypeStats.getStructureTypes().get(0);
		//should this be blank?
		assertThat(structureType.getName()).isNull();
		assertThat(structureType.getCount()).isEqualTo(3);
	}

	@Test
	public void testFilterOnPageLineCount() throws JAXBException, IOException {
		StructureStatisticsFilter filter = new StructureStatisticsFilter();
		TranscriptStatisticsFilter transcriptStatisticsFilter = new TranscriptStatisticsFilter();
		// Load a page content without any custom structure tags but legacy page-native structures set, which should be ignored by the filter
		PcGtsType pc = loader.loadXmlResource("gapAndUnclearTaggedLines.xml");
		filter.doFilter(pc);
		transcriptStatisticsFilter.doFilter(pc);
		TrpStructureStats stats = filter.getStats();
		logger.debug("Stats = {}", stats);
		assertThat(stats.getRegionTypes()).size().isEqualTo(2);
		TrpStructureStats.RegionTypeStats lineTypeStats = stats.getRegionTypes().get(1);
		assertThat(lineTypeStats.getRegionType()).isEqualTo(TrpStructureStats.RegionType.TEXT_LINE);
		TrpStructureStats.StructureType structureType = lineTypeStats.getStructureTypes().get(0);
		assertThat(structureType.getName()).isNull();

		//This shall be the same count as in the common transcript statistics
		assertThat(lineTypeStats.getOverallCount()).isEqualTo(transcriptStatisticsFilter.getStats().getNrOfLines());
		assertThat(structureType.getCount()).isEqualTo(transcriptStatisticsFilter.getStats().getNrOfLines());
	}

	@Test
	public void testFilterOnPageWithInitStats() throws JAXBException, IOException {
		StructureStatisticsFilter filter = new StructureStatisticsFilter();
		PcGtsType pc = loader.loadXmlResource("gapAndUnclearTaggedLines.xml");
		filter.doFilter(pc);
		TrpStructureStats stats = filter.getStats();
		logger.debug("Stats = {}", stats);

		final String myStructToCount = "myStructToCount";
		TrpStructureStats initStats = new TrpStructureStats();
		TrpStructureStats.RegionTypeStats regionTypeStats = new TrpStructureStats.RegionTypeStats();
		regionTypeStats.setRegionType(TrpStructureStats.RegionType.TEXT_REGION);
		regionTypeStats.setOverallCount(0);
		initStats.getRegionTypes().add(regionTypeStats);
		TrpStructureStats.StructureType type = new TrpStructureStats.StructureType();
		type.setName(myStructToCount);
		type.setCount(0);
		regionTypeStats.getStructureTypes().add(type);

		StructureStatisticsFilter filter2 = new StructureStatisticsFilter(initStats); //Arrays.asList(myStructToCount));
		filter2.doFilter(pc);
		TrpStructureStats nonEmptyStats = filter2.getStats();
		Optional<TrpStructureStats.RegionTypeStats> typedStatsOptional = nonEmptyStats.getRegionTypes().stream().filter(t -> t.getRegionType().equals(TrpStructureStats.RegionType.TEXT_REGION)).findFirst();
		assertThat(typedStatsOptional).isPresent();
		TrpStructureStats.RegionTypeStats regionTypeStats2 = typedStatsOptional.get();
		Optional<TrpStructureStats.StructureType> typeOptional = regionTypeStats2.getStructureTypes().stream().filter(t -> t.getName().equals(myStructToCount)).findFirst();
		assertThat(typeOptional).isPresent().hasValueSatisfying(t -> assertThat(t.getCount()).isEqualTo(0));
		logger.debug("Non-empty stats = {}", nonEmptyStats);
	}
}
