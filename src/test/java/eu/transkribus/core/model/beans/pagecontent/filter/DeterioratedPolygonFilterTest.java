package eu.transkribus.core.model.beans.pagecontent.filter;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;

import javax.xml.bind.JAXBException;

import org.junit.Assert;
import org.junit.Test;

import eu.transkribus.core.model.beans.pagecontent.PcGtsType;
import eu.transkribus.core.util.PageXmlUtils;

public class DeterioratedPolygonFilterTest {
	PageContentTestResourceLoader loader = new PageContentTestResourceLoader();
	
	@Test
	public void testDeterioratedRegionFilter() throws IOException, JAXBException {
		PcGtsType pc = loadDeterioratedRegionXml();
		//this resource contains 3 regions. One of the polygons consists of 1 point only and should be removed.
		final int nrOfRegionsInitial = PageXmlUtils.getTextRegions(pc).size();
		DeterioratedPolygonFilter filter = new DeterioratedPolygonFilter(false);
		filter.doFilter(pc);
		final int nrOfRegionsAfterFilter = PageXmlUtils.getTextRegions(pc).size();
		//check if exactly one region was removed.
		final int expectRegionsRemoved = 1;
		Assert.assertEquals(expectRegionsRemoved, filter.getRemovedRegionCount());
		Assert.assertEquals(nrOfRegionsInitial - expectRegionsRemoved, nrOfRegionsAfterFilter);
	}
	
	/**
	 * Test different point strings for being "deteriorated" and would cause CITlab/Planet tools to fail when passed to them.
	 */
	@Test
	public void testIsDeteriorated() {
		DeterioratedPolygonFilter filter = new DeterioratedPolygonFilter(false);
		
		assertThat(filter.isDeteriorated(null)).isTrue();
		assertThat(filter.isDeteriorated("")).isTrue();
		
		String noPointStr = "those are really no coordinates";
		assertThat(filter.isDeteriorated(noPointStr)).isTrue();
		
		String polygon = "0,0 123,0 123,123 0,123";
		assertThat(filter.isDeteriorated(polygon)).isFalse();
		
		String polygonUnsorted = "0,123 123,123 0,0, 123,0";
		assertThat(filter.isDeteriorated(polygonUnsorted)).isFalse();
		
		String vector = "0,0, 123,123";
		assertThat(filter.isDeteriorated(vector)).isTrue();
		
		String emptyPolygon = "123,123 123,123 123,123 123,123";
		assertThat(filter.isDeteriorated(emptyPolygon)).isTrue();
	}
	
	/**
	 * @return a PcGtsType with a region defined by only 1 point.
	 * @throws JAXBException
	 * @throws IOException
	 */
	private PcGtsType loadDeterioratedRegionXml() throws JAXBException, IOException {
		return loader.loadXmlResource("deterioratedRegion.xml");
	}
}
