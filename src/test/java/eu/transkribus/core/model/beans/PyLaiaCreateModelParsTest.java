package eu.transkribus.core.model.beans;

import java.io.File;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.junit.Test;

public class PyLaiaCreateModelParsTest {
    @Test
    public void testGetNextGenConf() throws Exception {
//		PyLaiaCreateModelPars p = PyLaiaCreateModelPars.getDefault();
//		System.out.println(p.toSimpleString("\n"));
//		System.out.println("-------");
//		System.out.println(p.toSingleLineString());
		
		// String str = "--print_args True --train_path ./model --model_filename model --logging_level info --cnn_kernel_size 3 3 3 3 --cnn_dilation 1 1 1 1 --cnn_num_features 12 24 48 48 --cnn_batchnorm True True True True --cnn_activations LeakyReLU LeakyReLU LeakyReLU LeakyReLU --cnn_poolsize 2 2 0 2 --use_masked_conv True --rnn_type LSTM --rnn_layers 3 --rnn_units 256 --rnn_dropout 0.5 --lin_dropout 0.5 --logging_also_to_stderr info --logging_file train-crnn.log --empty_par_inside --logging_overwrite False --empty_last_par";
		// PyLaiaCreateModelPars p = PyLaiaCreateModelPars.fromSingleLineString(str);
		// logger.info("str1 = "+str);
		// logger.info("pars = "+p.toSingleLineString());
		// if (str.equals(p.toSingleLineString())) {
		// 	logger.info("SUCCESS!");
		// }
		// else {
		// 	logger.info("ERROR!");
		// }

		// print current working directory:
		PyLaiaCreateModelPars p = PyLaiaCreateModelPars.getDefault();

		InputStream in = p.getClass().getClassLoader().getResourceAsStream("model_conf.yaml");
		String baseConfString = IOUtils.toString(in, StandardCharsets.UTF_8);

		String confString = p.getNextGenConf(baseConfString);
		System.out.println("confString: "+confString);
    }

	public static void main(String[] args) throws Exception {
        PyLaiaCreateModelParsTest t = new PyLaiaCreateModelParsTest();
        t.testGetNextGenConf();
	}    
}
