package eu.transkribus.core.model.builder;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.google.common.io.Files;
import com.ibm.icu.impl.InvalidFormatException;

import de.digitalcollections.iiif.model.sharedcanvas.Canvas;
import de.digitalcollections.iiif.model.sharedcanvas.Manifest;
import eu.transkribus.core.model.beans.TrpDocMetadata;
import eu.transkribus.core.model.builder.iiif.IIIFUtils;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/*
 * It seems that depending on the maven or surefire plugin version, this test will never be executed due to the name (*Tests instead of *Test) but it should be (see https://www.baeldung.com/maven-cant-find-junit-tests).
 * It's invisible to our CI pipeline, but on some PCs it's executed.
 * Running it currently fails, as IIIFUtils::getManifest can't deal with file-protocol URLs. Therefore, ignore it for now.
 */
@Ignore("IIIFUtils::getManifest can't deal with file-protocol URLs")
public class IIIFTests {
	private final static Logger logger = LoggerFactory.getLogger(IIIFTests.class);
    @Rule
    public TemporaryFolder tempDir = new TemporaryFolder();

    protected Canvas getFirstCanvas(final Manifest pManifest) {
        return pManifest.getSequences().get(0).getCanvases().get(0);
    }
    
    protected Manifest getManifest(final String pManifestPath) throws JsonParseException, JsonMappingException, IOException {
        URL tManifestFile = getClass().getResource(pManifestPath);
		logger.debug("Reading manifest from {}", tManifestFile);
        return IIIFUtils.getManifest(tManifestFile);
    }

	@Test
	public void testManifest() throws Exception {
        Manifest tManifest = this.getManifest("/iiif/manifest_with_service.json");
        
        URL tImageURL = IIIFUtils.getImage(this.getFirstCanvas(tManifest));

        assertEquals("Expected to see the Image Service URL.", "https://iiif.io/api/image/2.1/example/reference/918ecd18c2592080851777620de9bcb5-gottingen/full/full/0/default.jpg", tImageURL.toString());
    }


	@Test
	public void testManifestWithoutImgService() throws Exception {
        Manifest tManifest = this.getManifest("/iiif/manifest_without_service.json");
        
        URL tImageURL = IIIFUtils.getImage(this.getFirstCanvas(tManifest));

        assertEquals("Expected to see Resource ID if no image service.", "https://fixtures.iiif.io/images/Glen/photos/gottingen.jpg", tImageURL.toString());
    }


	@Test
	public void testGetPages() throws Exception {
        Manifest tManifest = this.getManifest("/iiif/manifest_with_service.json");

        File tRootDir =  tempDir.newFolder();

        IIIFUtils.getPagesFromIIIF(tManifest, tRootDir.getPath());
        File tImageDir = new File(tRootDir, "img");
        assertTrue("Expected method to download a single image to " + tImageDir.getPath() + "img/0.jpg", new File(tImageDir, "0.jpg").exists());
    }

	@Test
	public void testMetadata() throws Exception {
        Manifest tManifest = this.getManifest("/iiif/manifest_with_service.json");

        TrpDocMetadata tMetadata = IIIFUtils.readIiifMetadata(tManifest);

        assertEquals("Unexpected title. Expected title to match label", "Simple Manifest", tMetadata.getTitle());
        assertEquals("Expected external ID to match Manifest ID", tManifest.getIdentifier().toString(), tMetadata.getExternalId());
        assertEquals("Expected manifest description to be in metadata", "Manifest description", tMetadata.getDesc());
    }
	
	private void getImagesSimple() {
		/*
		 * test without a context -> where we get the Image API version: manifest_without_context
		 * test for Image API 3: hertziana_manifest_3
		 * test for Image API 2: hertziana_manifest_2
		 * manifest_without_context
		 * manifest_with_context
		 */
		URL tManifestFile = getClass().getResource("/iiif/manifest_without_context.json");
		
		try {
			File tRootDir = Files.createTempDir();
			IIIFUtils.createDocFromIIIF(tManifestFile, tRootDir.getPath());
			//IIIFUtils.getPagesFromIIIFSimple(tManifestFile, null);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ReflectiveOperationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	public static void main(String[] args) {
		IIIFTests test = new IIIFTests();
		test.getImagesSimple();
	}
}
