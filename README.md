# TranskribusCore
Core components for the Transkribus project. Used in client and server packages.

## Building
Here is a short guide with steps that need to be performed
to build your project.

### Requirements
- Java >= version 8
- Maven
- All further dependencies are gathered via Maven

### Build Steps
```
git clone https://github.com/Transkribus/TranskribusCore
cd TranskribusCore
mvn install
```



